/*
 * FILENAME
 *     TestAuditLogService.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.service;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.hashcode.iwmi.publications.domain.AuditLog;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;
import com.hashcode.iwmi.publications.services.AuditLogService;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Test class for {@link AuditLogService}
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:/application-beans.xml", "classpath:/entitymanager.xml"
})
public class TestAuditLogService
{
    private static final Logger log = LoggerFactory.getLogger(TestAuditLogService.class);

    @Autowired
    private AuditLogService auditLogService;

    @Transactional
    @Test
    public void testSaveNFindAuditLogs() throws EPublicationException
    {
        log.info("Test : Save and Find audit logs --> {}", auditLogService);

        auditLogService.addAudit("amila", "Ran Test cases");
        auditLogService.addAudit("amila", "Logged in to the system");
        auditLogService.addAudit("amila", "Logout from the system");
        auditLogService.addAudit("nethma", "Logged in");
        auditLogService.addAudit("nethma", "Perform a search");
        auditLogService.addAudit("nethma", "Perform a new Job add");
        auditLogService.addAudit("nethma", "Perform a new costing");
        auditLogService.addAudit("madavi", "changed user permission");

        List<AuditLog> auditLogs = auditLogService.findLastNDaysLogs("amila", 1);
        assertEquals(3, auditLogs.size());

        auditLogs = auditLogService.findLastNDaysLogs("nethma", 1);
        assertEquals(4, auditLogs.size());

        auditLogs = auditLogService.findLastNDaysLogs("madavi", 1);
        assertEquals(1, auditLogs.size());

        auditLogs = auditLogService.findLastNDaysLogs("madavi", 5);
        assertEquals(1, auditLogs.size());

        auditLogs = auditLogService.findLastNDaysLogs("priyani", 5);
        assertEquals(0, auditLogs.size());

    }

}

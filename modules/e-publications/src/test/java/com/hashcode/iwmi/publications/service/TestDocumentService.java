/*
 * FILENAME
 *     TestDocumentService.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.hashcode.iwmi.publications.domain.Document;
import com.hashcode.iwmi.publications.domain.PublicationItem;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;
import com.hashcode.iwmi.publications.services.DocumentService;
import com.hashcode.iwmi.publications.services.PublicationItemService;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Test class for {@link DocumentService}
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:/application-beans.xml", "classpath:/entitymanager.xml"
})
public class TestDocumentService
{
    private static final Logger log = LoggerFactory.getLogger(TestDocumentService.class);

    @Autowired
    private DocumentService documentService;

    @Autowired
    private PublicationItemService publicationItemService;

    @Transactional
    @Test
    public void testSaveDocument() throws EPublicationException
    {
        log.info("Test : Save document --> {}", documentService);

        Document attachment1 = new Document();
        attachment1.setFileLocation("/temp");
        attachment1.setFileName("test_1.txt");
        attachment1.setFileType("docx");
        attachment1.setComments("Sample file");

        documentService.saveDocument(attachment1);

        assertNotNull(attachment1.getId());
    }

    @Transactional
    @Test
    public void testDeleteDocument() throws EPublicationException
    {
        log.info("Test : Save document --> {}", documentService);

        Document attachment1 = new Document();
        attachment1.setFileLocation("/temp");
        attachment1.setFileName("test_1.txt");
        attachment1.setFileType("docx");
        attachment1.setComments("Sample file");
        documentService.saveDocument(attachment1);

        List<Document> docs = documentService.findByFileName("test_1.txt");
        assertEquals(1, docs.size());

        documentService.deleteDocument(attachment1);

        docs = documentService.findByFileName("test_1.txt");
        assertEquals(0, docs.size());
    }

    @Transactional
    @Test
    public void testFindDocumentsAttachedToPublicationItem() throws EPublicationException
    {
        log.info("Test : Save document --> {}", documentService);

        PublicationItem item1 = new PublicationItem();
        item1.setDescOfWork("Test 1");

        PublicationItem item2 = new PublicationItem();
        item2.setDescOfWork("Test 2");

        PublicationItem item3 = new PublicationItem();
        item3.setDescOfWork("Test 3");

        publicationItemService.save(item1);
        publicationItemService.save(item2);
        publicationItemService.save(item3);

        Document attachment1 = new Document();
        attachment1.setFileLocation("/temp");
        attachment1.setFileName("test_1.txt");
        attachment1.setFileType("docx");
        attachment1.setComments("Sample file");
        attachment1.setPublicationItem(item1);

        Document attachment2 = new Document();
        attachment2.setFileLocation("/temp");
        attachment2.setFileName("test_2.txt");
        attachment2.setFileType("xlsx");
        attachment2.setComments("Sample file");
        attachment2.setPublicationItem(item1);

        Document attachment3 = new Document();
        attachment3.setFileLocation("/temp");
        attachment3.setFileName("test_3.txt");
        attachment3.setFileType("pdf");
        attachment3.setComments("Sample file");
        attachment3.setPublicationItem(item3);

        documentService.saveDocument(attachment1);
        documentService.saveDocument(attachment2);
        documentService.saveDocument(attachment3);

        List<Document> docs = documentService.findDocumentsByPublicationItem(item1.getId());
        assertEquals(2, docs.size());

        docs = documentService.findDocumentsByPublicationItem(item2.getId());
        assertEquals(0, docs.size());

        docs = documentService.findDocumentsByPublicationItem(item3.getId());
        assertEquals(1, docs.size());

        documentService.deleteDocumentsAttachedToPublication(item3.getId());

        docs = documentService.findDocumentsByPublicationItem(item3.getId());
        assertEquals(0, docs.size());

        docs = documentService.findDocumentsByPublicationItem(item1.getId());
        assertEquals(2, docs.size());

    }

}

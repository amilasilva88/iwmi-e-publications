/*
 * FILENAME
 *     TestEmailMessageService.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.service;

import java.util.HashMap;
import java.util.Map;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.hashcode.iwmi.publications.dao.MessageTemplateDao;
import com.hashcode.iwmi.publications.domain.EmailMessage;
import com.hashcode.iwmi.publications.domain.MessageTemplate;
import com.hashcode.iwmi.publications.domain.MessageTemplate.BodyTag;
import com.hashcode.iwmi.publications.domain.MessageTemplate.Type;
import com.hashcode.iwmi.publications.domain.NotificationStatus;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;
import com.hashcode.iwmi.publications.services.EmailMessageService;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Test class for {@link EmailMessageService}
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:/application-beans.xml", "classpath:/entitymanager.xml"
})
public class TestEmailMessageService
{
    private static final Logger log = LoggerFactory.getLogger(TestEmailMessageService.class);

    @Autowired
    private EmailMessageService emailMessageService;

    @Autowired
    private MessageTemplateDao messageTemplateDao;

    @Transactional
    @Test
    public void testSendMail() throws EPublicationException
    {
        log.info("Test : Save document --> {}", emailMessageService);

        //create message template
        MessageTemplate template = new MessageTemplate();
        template.setType(Type.JOB_INITIATION_DONE);
        template.setSubject("Publication Job : {title}, {job_id}");
        template.setMailBody("Dear {to_name},\n \t{header_1}\n {content_1}\n {link_param_1}, Thanks, Cheers, System");

        MessageTemplate template2 = new MessageTemplate();
        template2.setType(Type.AFTER_JOB_COMPLETE_USER);
        template2.setSubject("Publication Job : {title}, {job_id}");
        template2.setMailBody("Dear {to_name},\n \t{header_1}\n {content_1}\n {link_param_1}, Current Date {date}, Thanks, Cheers, System");

        messageTemplateDao.create(template);
        messageTemplateDao.create(template2);

        Map<MessageTemplate.SubjectTag, String> subjectMap = new HashMap<MessageTemplate.SubjectTag, String>();
        subjectMap.put(MessageTemplate.SubjectTag.SUBJECT_TITLE, "Test Box");
        subjectMap.put(MessageTemplate.SubjectTag.SUBJECT_JOB_ID, "00102");
        subjectMap.put(MessageTemplate.SubjectTag.SUBJECT_OTHER_TEXT_2, "00102");

        Map<MessageTemplate.BodyTag, Object> bodyMap = new HashMap<MessageTemplate.BodyTag, Object>();
        bodyMap.put(MessageTemplate.BodyTag.BODY_TO_NAME, "Yasoma");
        bodyMap.put(MessageTemplate.BodyTag.BODY_HEADER_1, "Test Message");
        bodyMap.put(MessageTemplate.BodyTag.BODY_CONTENT_SET_1, "My Test content");
        bodyMap.put(MessageTemplate.BodyTag.BODY_LINK_PARAM_1, "http://google.com");

        EmailMessage emailMessage =
            emailMessageService.composeEmail(MessageTemplate.Type.JOB_INITIATION_DONE, subjectMap, bodyMap);

        log.info("Composed email message : \n\tSubject :{},\n\tBody :{}", emailMessage.getSubject(),
            emailMessage.getMessage());

        emailMessage.setRecipients("amilasilva88@gmail.com");
        emailMessage.setCc("amila@hashcodesys.com");
        emailMessage.setStatus(NotificationStatus.NEW);

        emailMessageService.send(emailMessage);
        log.info("Email sent- please check your inbox Amila...");
    }
    
    
    @Transactional
    @Test
    public void testSendMailToApproval() throws EPublicationException
    {
        log.info("Test : Save document --> {}", emailMessageService);

        //create message template
        MessageTemplate template = new MessageTemplate();
        template.setType(Type.TO_JOB_APPROVAL);
        template.setSubject("To Your Approval : {title}, {job_id}");
        template.setMailBody("Hi {to_name},\n  You have got a new job request to Approval. Job number is {job_id}."
            + "Link to Job: {link_param_1} \n  You can check the submitted Job's cost estimation by clicking the following link\n\t\t"
            + " {content_4} \n\t\t - Approve the job please click the link below \n\t\t {content_2}"
            + " Reject the job's cost estimation please click the link below\n\t\t {content_3}"
            + " Thanks ePublication Team");
  
        
        MessageTemplate template2 = new MessageTemplate();
        template2.setType(Type.AFTER_JOB_COMPLETE_USER);
        template2.setSubject("Publication Job : {title}, {job_id}");
        template2.setMailBody("Dear {to_name},\n \t{header_1}\n {content_1}\n {link_param_1}, Current Date {date}, Thanks, Cheers, System");

        messageTemplateDao.create(template);
        messageTemplateDao.create(template2);

        Map<MessageTemplate.SubjectTag, String> subjectMap = new HashMap<MessageTemplate.SubjectTag, String>();
        subjectMap.put(MessageTemplate.SubjectTag.SUBJECT_TITLE, "Test Box");
        subjectMap.put(MessageTemplate.SubjectTag.SUBJECT_JOB_ID, "00102");
        subjectMap.put(MessageTemplate.SubjectTag.SUBJECT_OTHER_TEXT_2, "00102");

        Map<MessageTemplate.BodyTag, Object> bodyMap = new HashMap<MessageTemplate.BodyTag, Object>();
        bodyMap.put(BodyTag.BODY_TO_NAME,  "Amila Silva");
        bodyMap.put(BodyTag.BODY_JOB_ID, "00102");
        bodyMap.put(BodyTag.BODY_CONTENT_SET_1, "Amila Silva");
        bodyMap.put(BodyTag.BODY_CONTENT_SET_2,  "/directApprove/asas" );
        bodyMap.put(BodyTag.BODY_CONTENT_SET_3,  "/directApprove/asasa" );
        bodyMap.put(BodyTag.BODY_CONTENT_SET_4, "costEstimationFileName");

        EmailMessage emailMessage =
            emailMessageService.composeEmail(MessageTemplate.Type.TO_JOB_APPROVAL, subjectMap, bodyMap);

        log.info("Composed email message : \n\tSubject :{},\n\tBody :{}", emailMessage.getSubject(),
            emailMessage.getMessage());

        emailMessage.setRecipients("amilasilva88@gmail.com");
        emailMessage.setCc("amila@hashcodesys.com");
        emailMessage.setStatus(NotificationStatus.NEW);

        emailMessageService.send(emailMessage);
        log.info("Email sent- please check your inbox Amila...");
    }
}

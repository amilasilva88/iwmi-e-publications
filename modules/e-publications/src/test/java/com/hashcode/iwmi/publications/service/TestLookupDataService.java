/*
 * FILENAME
 *     TestLookupService.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.hashcode.iwmi.publications.dao.LookupDataDao;
import com.hashcode.iwmi.publications.domain.DivisionsLookup;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;
import com.hashcode.iwmi.publications.services.LookupDataService;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Test class for {@link LookupDataService}
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:/application-beans.xml", "classpath:/entitymanager.xml"
})
public class TestLookupDataService
{
    private static final Logger log = LoggerFactory.getLogger(TestPublicationItemService.class);

    @Autowired
    private LookupDataService lookupDataService;
    
    @Autowired
    private LookupDataDao lookupDataDao;

    @Transactional
    @Test
    public void testLookupData1() throws EPublicationException
    {
        log.info("Test : Lookup data with division --> {}", lookupDataService);
        
        DivisionsLookup division1 = new DivisionsLookup();
        division1.setDisplay("Division 1");
        division1.setValue("Division 1");
        
        DivisionsLookup division2 = new DivisionsLookup();
        division2.setDisplay("Division 2");
        division2.setValue("Division 2");
        
        DivisionsLookup division3 = new DivisionsLookup();
        division3.setDisplay("Division 3");
        division3.setValue("Division 3");
        
        lookupDataDao.create(division1);
        lookupDataDao.create(division2);
        lookupDataDao.create(division3);
        
        assertNotNull(division1.getId());
        assertNotNull(division2.getId());
        assertNotNull(division3.getId());
        
        List<DivisionsLookup> lookupData = lookupDataService.findAllLookupData(DivisionsLookup.class);
        
        assertEquals(3, lookupData.size());
        assertEquals(division3, lookupData.get(2));
    }

}

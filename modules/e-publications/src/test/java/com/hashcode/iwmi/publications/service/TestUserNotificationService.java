/*
 * FILENAME
 *     TestUserNotificationService.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDateTime;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.hashcode.iwmi.publications.domain.NotificationStatus;
import com.hashcode.iwmi.publications.domain.UserNotification;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;
import com.hashcode.iwmi.publications.services.UserNotificationService;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Test class for {@link UserNotificationService}
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:/application-beans.xml", "classpath:/entitymanager.xml"
})
public class TestUserNotificationService
{

    private static final Logger log = LoggerFactory.getLogger(TestUserNotificationService.class);

    @Autowired
    private UserNotificationService userNotificationService;

    @Transactional
    @Test
    public void testSaveNotification() throws EPublicationException
    {
        log.info("Test : Save Notification --> {}", userNotificationService);

        UserNotification userNotification = new UserNotification();
        userNotification.setMessage("You have a new update");
        userNotification.setUsername("amila");
        userNotification.setNotifiedTime(LocalDateTime.now());
        userNotification.setStatus(NotificationStatus.NEW);

        userNotificationService.addUserNotification(userNotification);

        assertNotNull(userNotification.getId());
    }

    @Transactional
    @Test
    public void testFindNotification() throws EPublicationException
    {
        log.info("Test : Find Notifications --> {}", userNotificationService);

        UserNotification userNotification1 = new UserNotification();
        userNotification1.setMessage("You have a new update");
        userNotification1.setUsername("amila");
        userNotification1.setNotifiedTime(LocalDateTime.now());
        userNotification1.setStatus(NotificationStatus.NEW);

        UserNotification userNotification2 = new UserNotification();
        userNotification2.setMessage("You have a new update");
        userNotification2.setUsername("amila");
        userNotification2.setNotifiedTime(LocalDateTime.now());
        userNotification2.setStatus(NotificationStatus.NEW);

        UserNotification userNotification3 = new UserNotification();
        userNotification3.setMessage("You have a new update");
        userNotification3.setUsername("Madavi");
        userNotification3.setNotifiedTime(LocalDateTime.now());
        userNotification3.setStatus(NotificationStatus.NEW);

        UserNotification userNotification4 = new UserNotification();
        userNotification4.setMessage("You have a new update");
        userNotification4.setUsername("Nethma");
        userNotification4.setNotifiedTime(LocalDateTime.now());
        userNotification4.setStatus(NotificationStatus.NEW);

        userNotificationService.addUserNotification(userNotification1);
        userNotificationService.addUserNotification(userNotification2);
        userNotificationService.addUserNotification(userNotification3);
        userNotificationService.addUserNotification(userNotification4);

        List<UserNotification> notifications = userNotificationService.findUserNotificationByUser("amila", 20);
        assertEquals(2, notifications.size());

        notifications = userNotificationService.findUserNotificationByUser("Madavi", 20);
        assertEquals(1, notifications.size());

        notifications = userNotificationService.findUserNotificationByUser("Nethma", 20);
        assertEquals(1, notifications.size());

        notifications = userNotificationService.findUserNotificationByUser("amila", 1);
        assertEquals(1, notifications.size());
    }

}

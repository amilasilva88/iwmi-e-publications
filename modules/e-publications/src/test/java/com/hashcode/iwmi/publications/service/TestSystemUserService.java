/*
 * FILENAME
 *     TestSystemUserService.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.service;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.hashcode.iwmi.publications.domain.SystemUser;
import com.hashcode.iwmi.publications.domain.UserRole;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;
import com.hashcode.iwmi.publications.services.SystemUserService;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Test class for {@link SystemUserService}
 * </p>
 *
 * @author Manuja Jayamanna
 * @email manuja@hashcodesys.com
 *
 **/
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:/application-beans.xml", "classpath:/entitymanager.xml"
})
public class TestSystemUserService
{
    private static final Logger log = LoggerFactory.getLogger(TestSystemUserService.class);
    
    @Autowired
    private SystemUserService systemUserService;
    
    @Transactional
    @Test
    public void testFindUser() throws EPublicationException
    {
        log.info("Test : FindUser --> {}", systemUserService);

        SystemUser systemUser = new SystemUser();
        systemUser.setFirstName("Manuja");
        systemUser.setLastName("Jayamanna");
        systemUser.setUsername("manuja");
        
        List<UserRole> roles = new ArrayList<UserRole>();
        roles.add(UserRole.USER);
        roles.add(UserRole.VIEWER);
        
        systemUser.setUserRoles(roles);
        
        systemUserService.createUser(systemUser);
        
        SystemUser refreshedUser = systemUserService.findUser("manuja");
        assertEquals("Manuja", refreshedUser.getFirstName());
        assertEquals("Jayamanna", refreshedUser.getLastName());
        assertEquals(2, refreshedUser.getUserRoles().size());
    }

}

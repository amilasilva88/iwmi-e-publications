/*
 * FILENAME
 *     TestPublicationItemService.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.service;

import static org.junit.Assert.assertNotNull;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.hashcode.iwmi.publications.domain.GraphicsCost;
import com.hashcode.iwmi.publications.domain.PublicationItem;
import com.hashcode.iwmi.publications.domain.TypeSettingCost;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;
import com.hashcode.iwmi.publications.services.PublicationItemService;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Test class for {@link PublicationItemService}
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:/application-beans.xml", "classpath:/entitymanager.xml"
})
public class TestPublicationItemService
{
    private static final Logger log = LoggerFactory.getLogger(TestPublicationItemService.class);

    @Autowired
    private PublicationItemService publicationItemService;

    @Transactional
    @Test
    public void testSavePublicationItem() throws EPublicationException
    {
        log.info("Test : SavePublicationItem --> {}", publicationItemService);
        PublicationItem pubItem = new PublicationItem();
        pubItem.setProjectCode("0003");
        pubItem.setDescOfWork("Desc");
        pubItem.setDivision("MGT-02");
        pubItem.setRequesterName("Requester");
        pubItem.setCreatedBy("Amila Silva");
//        pubItem.setRequiredDeliveryDate(LocalDate.now());
//        pubItem.setCompletionDate(LocalDate.now());
        pubItem.setOtherInfo("Part time work");
        pubItem.setAuthor("Amila Silva");
        pubItem.setIsbn_issn("ISBN-1990-0887-97");

        GraphicsCost e1 = new GraphicsCost();
        e1.setCostPerUnit(2.30);
        e1.setNoOfCopies(100);
        e1.setOtherCosts(120.00);
        e1.setSubTotal((2.30 * 100) + 120);

        TypeSettingCost e2 = new TypeSettingCost();
        e2.setCostPerUnit(0.50);
        e2.setNoOfCopies(100);
        e2.setOtherCosts(50.00);
        e2.setSubTotal((0.50 * 100) + 50);

        pubItem.getCostEstimations().add(e1);
        pubItem.getCostEstimations().add(e2);

        publicationItemService.save(pubItem);
        assertNotNull(pubItem.getId());
    }
}

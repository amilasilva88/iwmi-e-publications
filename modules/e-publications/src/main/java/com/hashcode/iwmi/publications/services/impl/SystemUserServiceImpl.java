/*
 * FILENAME
 *     SystemUserServiceImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.services.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hashcode.iwmi.publications.dao.SystemUserDao;
import com.hashcode.iwmi.publications.domain.SystemUser;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;
import com.hashcode.iwmi.publications.services.SystemUserService;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * System User Service Implementation.
 * </p>
 *
 * @author Manuja Jayamanna
 * @email manuja@hashcodesys.com
 **/
@Service
@Transactional
public class SystemUserServiceImpl implements SystemUserService
{
    private static final Logger log = LoggerFactory.getLogger(SystemUserServiceImpl.class);

    @Autowired
    private SystemUserDao systemUserDao;

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.services.SystemUserService#createUser(SystemUser)
     */
    @Override
    public void createUser(final SystemUser systemUser) throws EPublicationException
    {
        try
        {
            log.debug("Create user [ User  : {}]", systemUser.getUsername());
            systemUserDao.create(systemUser);

            log.debug("New user successfully created");
        }
        catch (Exception e)
        {
            throw new EPublicationException("Error occurred while creating user,", e);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.services.SystemUserService#deleteUser(SystemUser)
     */
    @Override
    public void deleteUser(final SystemUser systemUser) throws EPublicationException
    {
        try
        {
            log.debug("Delete user [ User  : {}]", systemUser.getUsername());
            systemUserDao.delete(systemUser);

            log.debug("User successfully deleted");
        }
        catch (Exception e)
        {
            throw new EPublicationException("Error occurred while deleting user,", e);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.services.SystemUserService#updateUser(SystemUser)
     */
    @Override
    public void updateUser(final SystemUser systemUser) throws EPublicationException
    {
        try
        {
            log.debug("Update user [ User  : {}]", systemUser.getUsername());
            systemUserDao.update(systemUser);

            log.debug("User successfully updated");
        }
        catch (Exception e)
        {
            throw new EPublicationException("Error occurred while updating user,", e);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.services.SystemUserService#findUser(java.lang.String)
     */
    @Override
    public SystemUser findUser(String username) throws EPublicationException
    {
        try
        {
            log.debug("Find User By username [ User  : {}]", username);
            SystemUser systemUser = systemUserDao.findUser(username);

            if (systemUser == null)
                log.info("No User found");
            else
                log.info("User found for username {}", username);
            log.debug("New user retrieve  successfully");

            return systemUser;
        }
        catch (Exception e)
        {
            throw new EPublicationException("Error occurred while finding user by username,", e);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.services.SystemUserService#findUserByEmail(java.lang.String)
     */
    @Override
    public SystemUser findUserByEmail(String email) throws EPublicationException
    {
        try
        {
            log.debug("Find User By email [ User  : {}]", email);
            SystemUser systemUser = systemUserDao.findUserByEmail(email);

            if (systemUser == null)
                log.info("No User found");
            else
                log.info("User found for email {}", email);
            log.debug("New user retrieve  successfully");

            return systemUser;
        }
        catch (Exception e)
        {
            throw new EPublicationException("Error occurred while finding user by email,", e);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.services.SystemUserService#findById(java.lang.Long)
     */
    @Override
    public SystemUser findById(Long userId) throws EPublicationException
    {
        try
        {
            log.debug("Find User By id [ User id  : {}]", userId);
            SystemUser systemUser = systemUserDao.findById(userId);

            if (systemUser == null)
                log.info("No User found");
            else
                log.info("User found for id {}", userId);
            log.debug("New user retrieve  successfully");

            return systemUser;
        }
        catch (Exception e)
        {
            throw new EPublicationException("Error occurred while finding user by id,", e);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.services.SystemUserService#findAll()
     */
    @Override
    public List<SystemUser> findAll() throws EPublicationException
    {
        try
        {
            log.debug("Find all users");
            List<SystemUser> systemUsers = systemUserDao.findAll();

            if (systemUsers == null || systemUsers.isEmpty())
                log.info("No users found");
            else
                log.info("{} users found", systemUsers.size());
            log.debug("Users retrieve  successfully");

            return systemUsers;
        }
        catch (Exception e)
        {
            throw new EPublicationException("Error occurred while finding all users,", e);
        }
    }
}

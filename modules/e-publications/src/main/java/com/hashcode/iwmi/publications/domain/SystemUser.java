/*
 * FILENAME
 *     SystemUser.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.domain;

import static javax.persistence.FetchType.EAGER;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * System User.
 * </p>
 *
 * @author Manuja Jayamanna
 * @email manuja@hashcodesys.com
 **/
@Entity
@Table(name = "SYSTEM_USER")
public class SystemUser extends BaseModel
{
    private static final long serialVersionUID = -5557797332689263396L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "user_name", length = 30)
    private String username;

    @Column(name = "first_name", length = 100)
    private String firstName;

    @Column(name = "last_name", length = 100)
    private String lastName;

    @Column(name = "email", length = 256)
    private String email;

    @ElementCollection(targetClass = UserRole.class, fetch=EAGER)
    @CollectionTable(name = "SYS_USER_ROLE", joinColumns = @JoinColumn(name = "sys_user_id"))
    @Column(name = "user_role_id")
    private List<UserRole> userRoles = new ArrayList<UserRole>();
    
    /**
     * <p>
     * Check has User role.
     * </p>
     *
     * @return boolean
     *
     */
    public boolean isUser() {
        Predicate<UserRole> role = r -> r.getLabel().equals(UserRole.USER.getLabel());
        return userRoles.stream().anyMatch(role);
    }
    
    /**
     * <p>
     * Check has Officer role.
     * </p>
     *
     * @return boolean
     *
     */
    public boolean isOfficer() {
        Predicate<UserRole> role = r -> r.getLabel().equals(UserRole.SUPERUSER.getLabel());
        return userRoles.stream().anyMatch(role);
    }
    
    /**
     * <p>
     * Check has Supervisor role.
     * </p>
     *
     * @return boolean
     *
     */
    public boolean isSupervisor() {
        Predicate<UserRole> role = r -> r.getLabel().equals(UserRole.SUPERVISOR.getLabel());
        return userRoles.stream().anyMatch(role);
    }
    
    /**
     * <p>
     * Check user has any given role
     * </p>
     *
     * @param userRole
     * @return
     *
     */
    @Transient
    public boolean hasAnyRole(UserRole userRole) {
        Predicate<UserRole> role = r -> r.getLabel().equals(userRole.getLabel());
        return userRoles.stream().anyMatch(role);
    }

    /**
     * <p>
     * Getter for id.
     * </p>
     * 
     * @return the id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * <p>
     * Getter for username.
     * </p>
     * 
     * @return the username
     */
    public String getUsername()
    {
        return username;
    }

    /**
     * <p>
     * Setting value for username.
     * </p>
     * 
     * @param username
     *            the username to set
     */
    public void setUsername(String username)
    {
        this.username = username;
    }

    /**
     * <p>
     * Getter for firstName.
     * </p>
     * 
     * @return the firstName
     */
    public String getFirstName()
    {
        return firstName;
    }

    /**
     * <p>
     * Setting value for firstName.
     * </p>
     * 
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    /**
     * <p>
     * Getter for lastName.
     * </p>
     * 
     * @return the lastName
     */
    public String getLastName()
    {
        return lastName;
    }

    /**
     * <p>
     * Setting value for lastName.
     * </p>
     * 
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    /**
     * <p>
     * Getter for userRoles.
     * </p>
     * 
     * @return the userRoles
     */
    public List<UserRole> getUserRoles()
    {
        return userRoles;
    }

    /**
     * <p>
     * Setting value for userRoles.
     * </p>
     * 
     * @param userRoles
     *            the userRoles to set
     */
    public void setUserRoles(List<UserRole> userRoles)
    {
        this.userRoles = userRoles;
    }

    /**
     * <p>
     * Getter for email.
     * </p>
     * 
     * @return the email
     */
    public String getEmail()
    {
        return email;
    }

    /**
     * <p>
     * Setting value for email.
     * </p>
     * 
     * @param email
     *            the email to set
     */
    public void setEmail(String email)
    {
        this.email = email;
    }

    /**
     * {@inheritDoc}
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((email == null) ? 0 : email.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SystemUser other = (SystemUser) obj;
        if (email == null)
        {
            if (other.email != null)
                return false;
        }
        else if (!email.equals(other.email))
            return false;
        return true;
    }
    
    

}

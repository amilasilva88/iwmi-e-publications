/*
 * FILENAME
 *     NotificationDao.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.dao;

import java.util.List;

import com.hashcode.iwmi.publications.domain.UserNotification;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * DAO for user notification
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
public interface UserNotificationDao extends GenericDao<UserNotification, Long>
{

    /**
     * <p>
     * Find user notifications by username
     * </p>
     *
     * @param username
     *            username
     * @param limit
     *            number of rows to fetch
     * @return list of {@link UserNotification}
     *
     */
    List<UserNotification> findUserNotificationByUser(final String username, final int limit);
}

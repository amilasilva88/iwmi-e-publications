/*
 * FILENAME
 *     LookupData.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Base Entity for all the existing lookups in the system.
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 * 
 **/
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class LookupData implements Serializable, Comparable<LookupData>
{
    private static final long serialVersionUID = 4955273206526584953L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    protected Long id;

    @Column(name = "display")
    private String display;

    @Column(name = "value")
    private String value;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "LOOKUP_CHILDRENS")
    private List<LookupData> childLookups;

    @ManyToOne(targetEntity = LookupData.class)
    @PrimaryKeyJoinColumn(name = "parent_id")
    private LookupData parentId;

    /**
     * <p>
     * Getter for display.
     * </p>
     * 
     * @return the display
     */
    public String getDisplay()
    {
        return display;
    }

    /**
     * <p>
     * Setting value for display.
     * </p>
     * 
     * @param display
     *            the display to set
     */
    public void setDisplay(String display)
    {
        this.display = display;
    }

    /**
     * <p>
     * Getter for value.
     * </p>
     * 
     * @return the value
     */
    public String getValue()
    {
        return value;
    }

    /**
     * <p>
     * Setting value for value.
     * </p>
     * 
     * @param value
     *            the value to set
     */
    public void setValue(String value)
    {
        this.value = value;
    }

    /**
     * <p>
     * Getter for childLookups.
     * </p>
     * 
     * @return the childLookups
     */
    public List<LookupData> getChildLookups()
    {
        if (childLookups == null)
        {
            this.childLookups = new ArrayList<LookupData>();
        }
        return childLookups;
    }

    /**
     * <p>
     * Setting value for childLookups.
     * </p>
     * 
     * @param childLookups
     *            the childLookups to set
     */
    public void setChildLookups(List<LookupData> childLookups)
    {
        this.childLookups = childLookups;
    }

    /**
     * <p>
     * Getter for id.
     * </p>
     * 
     * @return the id
     */
    public abstract Long getId();

    /**
     * <p>
     * Getter for parentId.
     * </p>
     * 
     * @return the parentId
     */
    public LookupData getParentId()
    {
        return parentId;
    }

    /**
     * <p>
     * Setting value for parentId.
     * </p>
     * 
     * @param parentId
     *            the parentId to set
     */
    public void setParentId(LookupData parentId)
    {
        this.parentId = parentId;
    }

    /**
     * {@inheritDoc}
     *
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(LookupData o)
    {
        if (o == null)
            return 1;
        else
            return this.getDisplay().compareTo(o.getDisplay());
    }

}

/*
 * FILENAME
 *     HomeController.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.controllers;

import static com.hashcode.iwmi.publications.util.ApplicationUtils.addUserToModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Home Controller.
 * </p>
 *
 * @author Amila Silva
 *
 * @version $Id$
 **/
@Controller
@RequestMapping("/home")
public class HomeController
{
    private static final Logger log = LoggerFactory.getLogger(HomeController.class);

    @RequestMapping("/dashboard")
    public String home(Model model)
    {
        log.info("Request came to load dashboard");
        addUserToModel(model);
        return "index";
    }

}

/*
 * FILENAME
 *     EmailMessage.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.domain;

import static com.hashcode.iwmi.publications.util.ApplicationUtils.isNotNullOrEmpty;

import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Email Message entity model.
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
@Entity
@Table(name = "EMAIL_MESSAGES")
public class EmailMessage extends BaseModel
{
    private static final long serialVersionUID = -6159485627783668875L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "subject", length = 260)
    private String subject;

    @Column(name = "message", length = 3000)
    private String message;

    @Column(name = "recipients", length = 1000)
    private String recipients;

    @Column(name = "cc", length = 1000)
    private String cc;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private NotificationStatus status;

    @Transient
    private InternetAddress[] recipientsList;

    @Transient
    private String[] ccList;

    /**
     * <p>
     * Getter for cc.
     * </p>
     * 
     * @return the cc
     */
    public String getCc()
    {
        return cc;
    }

    /**
     * <p>
     * Setting value for cc.
     * </p>
     * 
     * @param cc
     *            the cc to set
     */
    public void setCc(String cc)
    {
        this.cc = cc;
    }

    /**
     * <p>
     * Getter for ccList.
     * </p>
     * 
     * @return the ccList
     */
    public InternetAddress[] getCcList()
    {
        if (isNotNullOrEmpty(cc))
        {
            String[] addStr = recipients.split(",", -1);
            List<InternetAddress> addresses = new ArrayList<InternetAddress>();
            for (int i = 0; i < addStr.length; i++)
            {
                try
                {
                    if (addStr[i] != null && !addStr[i].isEmpty())
                    {
                        addresses.add(new InternetAddress(addStr[i]));
                    }
                }
                catch (AddressException e)
                {

                }
            }
            return addresses.toArray(new InternetAddress[addresses.size()]);
        }
        return new InternetAddress[1];
    }

    /**
     * <p>
     * Getter for recipientsList.
     * </p>
     * 
     * @return the recipientsList
     */
    public InternetAddress[] getRecipientsList()
    {
        if (isNotNullOrEmpty(recipients))
        {
            String[] addStr = recipients.split(",", -1);
            List<InternetAddress> addresses = new ArrayList<InternetAddress>();
            for (int i = 0; i < addStr.length; i++)
            {
                try
                {
                    if (addStr[i] != null && !addStr[i].isEmpty())
                    {
                        addresses.add(new InternetAddress(addStr[i]));
                    }
                }
                catch (AddressException e)
                {

                }
            }
            return addresses.toArray(new InternetAddress[addresses.size()]);
        }
        return new InternetAddress[1];
    }

    /**
     * <p>
     * Getter for subject.
     * </p>
     * 
     * @return the subject
     */
    public String getSubject()
    {
        return subject;
    }

    /**
     * <p>
     * Setting value for subject.
     * </p>
     * 
     * @param subject
     *            the subject to set
     */
    public void setSubject(String subject)
    {
        this.subject = subject;
    }

    /**
     * <p>
     * Getter for message.
     * </p>
     * 
     * @return the message
     */
    public String getMessage()
    {
        return message;
    }

    /**
     * <p>
     * Setting value for message.
     * </p>
     * 
     * @param message
     *            the message to set
     */
    public void setMessage(String message)
    {
        this.message = message;
    }

    /**
     * <p>
     * Getter for recipients.
     * </p>
     * 
     * @return the recipients
     */
    public String getRecipients()
    {
        return recipients;
    }

    /**
     * <p>
     * Setting value for recipients.
     * </p>
     * 
     * @param recipients
     *            the recipients to set
     */
    public void setRecipients(String recipients)
    {
        this.recipients = recipients;
    }

    /**
     * <p>
     * Getter for status.
     * </p>
     * 
     * @return the status
     */
    public NotificationStatus getStatus()
    {
        return status;
    }

    /**
     * <p>
     * Setting value for status.
     * </p>
     * 
     * @param status
     *            the status to set
     */
    public void setStatus(NotificationStatus status)
    {
        this.status = status;
    }

    /**
     * <p>
     * Setting value for recipientsList.
     * </p>
     * 
     * @param recipientsList
     *            the recipientsList to set
     */
    public void setRecipientsList(InternetAddress[] recipientsList)
    {
        this.recipientsList = recipientsList;
    }

    /**
     * <p>
     * Setting value for ccList.
     * </p>
     * 
     * @param ccList
     *            the ccList to set
     */
    public void setCcList(String[] ccList)
    {
        this.ccList = ccList;
    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.domain.BaseModel#getId()
     */
    @Override
    public Long getId()
    {
        return this.id;
    }

}

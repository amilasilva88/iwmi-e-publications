/*
 * FILENAME
 *     AuditLogDaoImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.dao.impl;

import static com.hashcode.iwmi.publications.util.ApplicationUtils.isNotNullOrEmpty;

import java.util.List;

import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.hashcode.iwmi.publications.dao.DocumentDao;
import com.hashcode.iwmi.publications.domain.Document;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * {@link DocumentDao} implementation.
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 **/
@Repository
public class DocumentDaoImpl extends GenericDaoImpl<Document, Long> implements DocumentDao
{

    private static final Logger log = LoggerFactory.getLogger(DocumentDaoImpl.class);

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.dao.DocumentDao#findByFileName(java.lang.String)
     */
    @Override
    public List<Document> findByFileName(final String fileName)
    {
        log.debug("Find Document by file name :{}", fileName);

        String file = isNotNullOrEmpty(fileName) ? fileName : "%";

        String queryText =
            "SELECT d FROM " + Document.class.getName() + " d "
                + " WHERE d.fileName like :name ORDER BY d.fileName ASC ";

        TypedQuery<Document> query = entityManager.createQuery(queryText, Document.class);
        query.setParameter("name", file);

        List<Document> documents = query.getResultList();
        log.debug("Found Documents by file name :{}, [ Results :{}]", file, documents.size());

        return documents;

    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.dao.DocumentDao#findDocumentsByPublicationItem(java.lang.Long)
     */
    @Override
    public List<Document> findDocumentsByPublicationItem(Long publicationId)
    {
        log.debug("Find Documents by pulication id:{}", publicationId);

        String queryText =
            "SELECT d FROM " + Document.class.getName() + " d "
                + " WHERE d.publicationItem.id = :publicationItemId ORDER BY d.fileName ASC ";

        TypedQuery<Document> query = entityManager.createQuery(queryText, Document.class);
        query.setParameter("publicationItemId", publicationId);

        List<Document> documents = query.getResultList();
        log.debug("Found Documents by Publication Id :{}, [ Results :{}]", publicationId, documents.size());

        return documents;
    }

}

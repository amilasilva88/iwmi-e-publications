/*
 * FILENAME
 *     JobDetailReportExcel.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.exports;

import static com.hashcode.iwmi.publications.util.ApplicationUtils.getInt;
import static com.hashcode.iwmi.publications.util.ApplicationUtils.getString;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import javax.transaction.Transactional;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hashcode.iwmi.publications.domain.CostElement;
import com.hashcode.iwmi.publications.domain.PublicationItem;
import com.hashcode.iwmi.publications.domain.PublicationStatus;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;
import com.hashcode.iwmi.publications.services.SystemUserService;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Job Details Excel Report
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
@Service
public class JobDetailReportExcel
{

    private static final Logger log = LoggerFactory.getLogger(JobDetailReportExcel.class);
    
    @Autowired
    private SystemUserService userService;
    
    private Map<String, String> userNamesMap = new HashMap<String, String>();
    
    private String getName(String username)
    {
        if (userNamesMap.isEmpty())
        {
            try
            {
                userService.findAll().forEach(u -> {
                    userNamesMap.put(u.getUsername(), u.getFirstName() + " " + u.getLastName());
                });
            }
            catch (EPublicationException e)
            {
                log.error("Error in loading the users,", e);
            }
        }
        return userNamesMap.get(username);
    }

    @Transactional
    public ByteArrayOutputStream generateReport(List<PublicationItem> publications)
    {
        log.info("Job Detail Report - Start generation of excel report");
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try
        {
            XSSFWorkbook workbook = new XSSFWorkbook();
            XSSFSheet spreadsheet = workbook.createSheet("Publication Job Details");

            AtomicInteger rowId = new AtomicInteger(0);

            // header row
            rowId = createHeaderRow(workbook, spreadsheet, rowId);
            createDataRow(publications, spreadsheet, rowId);

            workbook.write(out);

            log.info("Job Detail Report - excel report completed");
        }
        catch (Exception e)
        {
            log.error("Error occurred while generating the Job Detail report");
        }
        return out;
    }

    private void createDataRow(List<PublicationItem> publications, XSSFSheet spreadsheet, final AtomicInteger rowId)
    {
        publications
            .stream()
            .filter(p -> p.getStatus() != PublicationStatus.DELETED)
            .forEach(
                p -> {

                    JSONObject jData =
                        (p.getPublicationStatusJson() == null) ? new JSONObject() : new JSONObject(p
                            .getPublicationStatusJson());
                    XSSFRow row = spreadsheet.createRow(rowId.getAndAdd(1));
                    int cellid = 0;
                    XSSFCell cell = row.createCell(cellid++);
                    cell.setCellValue(p.getId());
                    cell = row.createCell(cellid++);
                    cell.setCellValue(p.getJobNumber());
                    cell = row.createCell(cellid++);
                    cell.setCellValue(getString(jData, "relatedProgram"));
                    cell = row.createCell(cellid++);
                    cell.setCellValue(p.getType());
                    cell = row.createCell(cellid++);
                    cell.setCellValue(p.getSeries());
                    cell = row.createCell(cellid++);
                    cell.setCellValue(p.getTitle());
                    cell = row.createCell(cellid++);
                    cell.setCellValue(getString(jData, "author"));
                    cell = row.createCell(cellid++);
                    cell.setCellValue(getString(jData, "manuScriptReceiveDate"));
                    cell = row.createCell(cellid++);
                    cell.setCellValue(getInt(jData, "noOfManuScriptPages"));
                    cell = row.createCell(cellid++);
                    cell.setCellValue(getString(jData, "isbn_issn"));
                    cell = row.createCell(cellid++);
                    cell.setCellValue(getName(p.getCreatedBy()));
                    cell = row.createCell(cellid++);
                    cell.setCellValue(getName(p.getSupervisorName()));
                    cell = row.createCell(cellid++);
                    cell.setCellValue(p.getProjectCode());
                    cell = row.createCell(cellid++);
                    cell.setCellValue("" + p.getCreatedDate());
                    cell = row.createCell(cellid++);
                    cell.setCellValue("" + p.getApprovedDate());
                    cell = row.createCell(cellid++);
                    cell.setCellValue(getString(jData, "processStartDate"));
                    cell = row.createCell(cellid++);
                    cell.setCellValue("" + p.getCompletionDate());
                    cell = row.createCell(cellid++);
                    cell.setCellValue(getString(jData, "estimatedCompletionDate"));
                    cell = row.createCell(cellid++);
                    cell.setCellValue("" + p.getCreatedDate());

                    double totalOverHead = 0.0, totalEstimation = 0.0;
                    for (CostElement element : p.getActualCostings())
                    {
                        totalOverHead += element.getOtherCosts();
                        totalEstimation += element.getSubTotal();
                    }

                    cell = row.createCell(cellid++);
                    cell.setCellValue(totalEstimation + totalOverHead);
                });
    }

    private AtomicInteger createHeaderRow(XSSFWorkbook workbook, XSSFSheet spreadsheet, AtomicInteger rowId)
    {
        XSSFCellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setFillForegroundColor(new XSSFColor(new Color(115, 194, 255)));
        headerStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
        
        XSSFFont font= workbook.createFont();
        font.setFontHeightInPoints((short)10);
        font.setFontName("Arial");
        font.setBold(true);
        font.setItalic(false);
        headerStyle.setFont(font);
        
        XSSFRow headerRow = spreadsheet.createRow(rowId.getAndAdd(1));
        
        
        int cellid = 0;
        XSSFCell cell = headerRow.createCell(cellid++);
        cell.setCellValue("Id");
        cell.setCellStyle(headerStyle);
        cell = headerRow.createCell(cellid++);
        cell.setCellValue("Job Number");
        cell.setCellStyle(headerStyle);
        cell = headerRow.createCell(cellid++);
        cell.setCellValue("Related program");
        cell.setCellStyle(headerStyle);
        cell = headerRow.createCell(cellid++);
        cell.setCellValue("Type of the job");
        cell.setCellStyle(headerStyle);
        cell = headerRow.createCell(cellid++);
        cell.setCellValue("Series");
        cell.setCellStyle(headerStyle);
        cell = headerRow.createCell(cellid++);
        cell.setCellValue("Title");
        cell.setCellStyle(headerStyle);
        cell = headerRow.createCell(cellid++);
        cell.setCellValue("Author");
        cell.setCellStyle(headerStyle);
        cell = headerRow.createCell(cellid++);
        cell.setCellValue("Manuscript received date");
        cell.setCellStyle(headerStyle);
        cell = headerRow.createCell(cellid++);
        cell.setCellValue("Number of manuscript pages");
        cell.setCellStyle(headerStyle);
        cell = headerRow.createCell(cellid++);
        cell.setCellValue("ISBN-ISSN");
        cell.setCellStyle(headerStyle);
        cell = headerRow.createCell(cellid++);
        cell.setCellValue("Requested By");
        cell.setCellStyle(headerStyle);
        cell = headerRow.createCell(cellid++);
        cell.setCellValue("Supervisor");
        cell.setCellStyle(headerStyle);
        cell = headerRow.createCell(cellid++);
        cell.setCellValue("Project code");
        cell.setCellStyle(headerStyle);
        cell = headerRow.createCell(cellid++);
        cell.setCellValue("Job requested date");
        cell.setCellStyle(headerStyle);
        cell = headerRow.createCell(cellid++);
        cell.setCellValue("Job approve date");
        cell.setCellStyle(headerStyle);
        cell = headerRow.createCell(cellid++);
        cell.setCellValue("Process started date");
        cell.setCellStyle(headerStyle);
        cell = headerRow.createCell(cellid++);
        cell.setCellValue("Process ended date");
        cell.setCellStyle(headerStyle);
        cell = headerRow.createCell(cellid++);
        cell.setCellValue("Estimated job completing date");
        cell.setCellStyle(headerStyle);
        cell = headerRow.createCell(cellid++);
        cell.setCellValue("Date entered into the system");
        cell.setCellStyle(headerStyle);
        cell = headerRow.createCell(cellid++);
        cell.setCellValue("Actual Cost");
        cell.setCellStyle(headerStyle);
        return rowId;
    }
}

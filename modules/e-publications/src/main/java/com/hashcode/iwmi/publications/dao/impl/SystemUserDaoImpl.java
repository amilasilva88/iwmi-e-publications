/*
 * FILENAME
 *     SystemUserDaoImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.dao.impl;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.hashcode.iwmi.publications.dao.SystemUserDao;
import com.hashcode.iwmi.publications.domain.SystemUser;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * System User Dao implemenation.
 * </p>
 *
 * @author Manuja Jayamanna
 * @email manuja@hashcodesys.com
 **/
@Repository
public class SystemUserDaoImpl extends GenericDaoImpl<SystemUser, Long> implements SystemUserDao
{
    private static final Logger log = LoggerFactory.getLogger(SystemUserDaoImpl.class);

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.dao.SystemUserDao#findUser(java.lang.String)
     */
    @Override
    public SystemUser findUser(String username)
    {
        log.debug("Find user by username, [ Username : {}]", username);

        String queryText = "SELECT n FROM " + SystemUser.class.getName() + " n " + " WHERE n.username = :username";

        try
        {
            TypedQuery<SystemUser> query = entityManager.createQuery(queryText, SystemUser.class);
            query.setParameter("username", username);
            query.setMaxResults(1);
            SystemUser systemUser = query.getSingleResult();
            log.debug("Found user for username :{}, [ Results :{}]", username, systemUser.getFirstName());
            return systemUser;
        }
        catch (NoResultException e)
        {
            log.debug("No user found for username :{}, [ Results :{}]", username);
            return null;
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.dao.SystemUserDao#findEmail(java.lang.String)
     */
    @Override
    public SystemUser findUserByEmail(String email)
    {
        log.debug("Find user by email, [ Email : {}]", email);

        String queryText = "SELECT n FROM " + SystemUser.class.getName() + " n " + " WHERE n.email = :email";

        try
        {
            TypedQuery<SystemUser> query = entityManager.createQuery(queryText, SystemUser.class);
            query.setParameter("email", email);
            query.setMaxResults(1);
            SystemUser systemUser = query.getSingleResult();
            log.debug("Found user for email :{}, [ Results :{}]", email, systemUser.getFirstName());
            return systemUser;
        }
        catch (NoResultException e)
        {
            log.debug("No user found for email :{}, [ Results :{}]", email);
            return null;
        }
    }
}

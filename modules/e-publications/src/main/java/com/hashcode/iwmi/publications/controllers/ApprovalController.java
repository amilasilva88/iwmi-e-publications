/*
 * FILENAME
 *     CostEstimationController.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.controllers;

import static com.hashcode.iwmi.publications.controllers.InitiationController.ACTION_NEXT;
import static com.hashcode.iwmi.publications.domain.PublicationStatus.APPROVED;
import static com.hashcode.iwmi.publications.domain.PublicationStatus.COMPLETED;
import static com.hashcode.iwmi.publications.domain.PublicationStatus.DELETED;
import static com.hashcode.iwmi.publications.domain.PublicationStatus.NEED_AN_AMENDMENT;
import static com.hashcode.iwmi.publications.domain.PublicationStatus.REJECTED;
import static com.hashcode.iwmi.publications.util.ApplicationUtils.addUserToModel;
import static com.hashcode.iwmi.publications.util.ApplicationUtils.createResponse;
import static com.hashcode.iwmi.publications.util.ApplicationUtils.getResponceDataMap;
import static com.hashcode.iwmi.publications.util.ApplicationUtils.setItemStatus;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Map;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hashcode.iwmi.publications.domain.ApproverComment;
import com.hashcode.iwmi.publications.domain.PublicationItem;
import com.hashcode.iwmi.publications.domain.PublicationStatus;
import com.hashcode.iwmi.publications.domain.SystemUser;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;
import com.hashcode.iwmi.publications.listeners.PublicationStateChangeListener;
import com.hashcode.iwmi.publications.services.PublicationItemService;
import com.hashcode.iwmi.publications.services.SystemUserService;
import com.hashcode.iwmi.publications.util.ApplicationUtils;
import com.hashcode.iwmi.publications.util.ResponseStatus;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Initiation Controller for Cost estimation.
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 **/
@Controller
@RequestMapping("/approval")
public class ApprovalController
{
    private static final Logger log = LoggerFactory.getLogger(ApprovalController.class);

    @Autowired
    private PublicationItemService publicationItemService;

    @Autowired
    private SystemUserService userService;
    
    @Autowired
    @Qualifier(value = "approvedStateChange")
    private PublicationStateChangeListener approvedStateChangeListener;
    
    @Autowired
    @Qualifier(value = "amendmentRequestStateChange")
    private PublicationStateChangeListener amendmentRequestStateChangeListener;
    
    @Autowired
    @Qualifier(value = "rejectedStateChange")
    private PublicationStateChangeListener rejectedStateChangeListener;
    
    

    @RequestMapping("/page")
    public String pageLoad(@RequestParam("id") long id, Model model)
    {
        log.info("Request came to load the approval page....");
        addUserToModel(model);
        model.addAttribute("id", id);
        return "job-approval";
    }

    @RequestMapping(value = "/load/{id}", method = {
        RequestMethod.GET
    })
    @ResponseBody
    @Transactional
    public String loadData(@PathVariable("id") long id) throws EPublicationException
    {
        log.info("Request came to load Approval data for publication [ Id :{}]", id);

        PublicationItem item = publicationItemService.findById(id);
        log.info("Found Publication Item, Id :{}", item.getId());
        
        String username = ApplicationUtils.getCurrentUser().getUsername();
        SystemUser currentUser = userService.findUser(username);

        JSONObject data = new JSONObject();
        try
        {
            if (item != null)
            {
                data.accumulate("id", item.getId());
                if(!item.getApproverComments().isEmpty()) {
                    data.accumulate("status", item.getApproverComments().get(0).getStatus());
                    data.accumulate("approverComment", item.getApproverComments().get(0).getComment());
                }
                
                setItemStatus(item, data);
                log.info("Approval data populated, [ JSON Data :{}]", data.toString());
                
                boolean hasAccessPermission = checkAccessPermission(currentUser, item);
                log.debug("Approval data access permission, [ User :{}, can edit :{}]",
                    currentUser.getUsername(), hasAccessPermission);
                data.put("canEdit", hasAccessPermission);
            }
        }
        catch (Throwable e)
        {
            log.error("Approval error", e);
        }
        return data.toString();
    }

    public boolean checkAccessPermission(SystemUser currentUser, PublicationItem item)
    {
        if (item.getStatus().equals(REJECTED) || item.getStatus().equals(DELETED) || item.getStatus().equals(COMPLETED))
            return false;
        else if (currentUser.isSupervisor())
            return true;
        else
            return false;
    }
    

    @RequestMapping(value = "/directApprove/{code}", method = {
        RequestMethod.GET
    })
    @Transactional
    @ResponseBody
    public String directApprove(@PathVariable("code") String code)
    {
        log.debug("Request came to directApprove using email link");
        try
        {
            String decryptedCode = ApplicationUtils.decryptText(code);
            if (decryptedCode != null)
            {
                // <AP/REJ>;<item_ID>;<user_email>;<length>;
                String[] splits = decryptedCode.split(";", -1);
                if (splits.length > 3)
                {
                    String approveStatus = (splits[0].equals("AP")) ? "APPROVED" : "REJECTED";
                    long jobId = Long.valueOf(splits[1]);
                    String username = splits[2];

                    PublicationItem item = updateItemStatus(ACTION_NEXT, username, jobId, approveStatus, "");
                    return "Successfully Updated.";
                }
            }
            else
            {
                log.warn("Invalid Request : encrypted code cannot identify by the system. [Code :{}]", code);
                return "Invalid Request : encrypted code cannot identify by the system.";
            }
        }
        catch (Exception e)
        {
            log.error("Error occurred in saving/update publication,", e);
            Map<String, Object> params = getResponceDataMap();
            params.put("error", "Error encountered in direct approval update in Publication Job");
            return createResponse(ResponseStatus.FAILED, params).toString();
        }
        return "Invalid Request : encrypted code cannot identify by the system.";
    }
        
        
    @RequestMapping(value = "/saveData", method = {
        RequestMethod.POST, RequestMethod.PUT
    })
    @Transactional
    @ResponseBody
    public String saveOrUpdateData(@RequestBody String data)
    {
        log.debug("Request came to update approval data for publications");
        try
        {
            String username = ApplicationUtils.getCurrentUser().getUsername();

            JSONObject jsonData = new JSONObject(data);
            String action = jsonData.getString("action");
            long jobId = (jsonData.has("id")) ? jsonData.getLong("id") : -1;
            String approveStatus = jsonData.getString("status");
            String comment = jsonData.getString("approverComment");
            
            PublicationItem item = updateItemStatus(action, username, jobId, approveStatus, comment);

            Map<String, Object> params = getResponceDataMap();
            params.put("id", item.getId());
            params.put("item_status", item.getStatus());
            params.put("data", "Publication item approval data saved successfully.");

            JSONObject jsonReponse = createResponse(ResponseStatus.SUCCESS, params);
            log.debug("JSON Response for load divisions, Response : {}", jsonReponse);
            return jsonReponse.toString();
        }
        catch (Exception e)
        {
            log.error("Error occurred in saving/update publication,", e);
            Map<String, Object> params = getResponceDataMap();
            params.put("error", "Error encountered in saving/update Publication");
            return createResponse(ResponseStatus.FAILED, params).toString();
        }
    }

    private PublicationItem updateItemStatus(String action, String username, long jobId, String approveStatus, String comment)
        throws EPublicationException
    {
        PublicationItem item = publicationItemService.findById(jobId);

        ApproverComment appComment = new ApproverComment();
        appComment.setComment(comment);
        appComment.setApprovedTime(LocalDateTime.now());

        
        log.debug("Publication Item approval Id :{}, Status :{}", item.getId(), item.getStatus());
        if (approveStatus.equals(PublicationStatus.APPROVED.name()))
        {
            item.setApprovedDate(LocalDate.now());
            item.setStatus(APPROVED);
            appComment.setStatus(APPROVED);
        }
        else if (approveStatus.equals(PublicationStatus.NEED_AN_AMENDMENT.name()))
        {
            item.setStatus(NEED_AN_AMENDMENT);
            appComment.setStatus(NEED_AN_AMENDMENT);
        }
        else
        {
            item.setStatus(REJECTED);
            appComment.setStatus(REJECTED);
        }

        appComment.setApprover(username);
        appComment.setPublicationItem(item);
        item.getApproverComments().clear();
        item.getApproverComments().add(appComment);
        item.setLastModifiedBy(username);
        publicationItemService.update(item);

        if (action.equals(ACTION_NEXT))
        {
            switch (item.getStatus())
            {
                case APPROVED:
                    approvedStateChangeListener.onStateChange(username, item);
                    break;
                    
                case NEED_AN_AMENDMENT:
                    amendmentRequestStateChangeListener.onStateChange(username, item);
                    break;    
                    
                case REJECTED:
                     rejectedStateChangeListener.onStateChange(username, item);
                     break; 
                default:
                    break;
            }
        }
        return item;
    }
}

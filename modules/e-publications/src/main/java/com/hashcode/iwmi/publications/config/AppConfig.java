/*
 * FILENAME
 *     AppConfig.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import com.hashcode.iwmi.publications.services.EmailMessageService;
import com.hashcode.iwmi.publications.services.impl.EmailMessageServiceImpl;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Spring 4 Application Configuration.
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
@Configuration
@ComponentScan("com.hashcode.iwmi.publications")
@PropertySource("classpath:application.properties")
@PropertySource("classpath:database.properties")
@ImportResource("classpath:application-beans.xml")
//@ImportResource("classpath:entitymanager.xml")
@EnableWebMvc
@EnableScheduling
public class AppConfig
{
    private static final Logger log = LoggerFactory.getLogger(AppConfig.class);

    /**
     * <p>
     * URL based view resolver.
     * </p>
     *
     * @return {@link UrlBasedViewResolver}
     *
     */
    @Bean
    public UrlBasedViewResolver setupViewResolver()
    {
        log.info("AppConfig - setupViewResolver");
        UrlBasedViewResolver resolver = new UrlBasedViewResolver();
        resolver.setPrefix("/views/");
        resolver.setSuffix(".jsp");
        resolver.setViewClass(JstlView.class);
        log.info("AppConfig - setupViewResolver - [Completed]");
        return resolver;
    }

    @Bean
    public MultipartResolver multipartResolver()
    {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        multipartResolver.setMaxUploadSize(500000000);
        return multipartResolver;
    }

    @Bean(initMethod = "init", destroyMethod = "cleanUp")
    public EmailMessageService emailMessageService()
    {
        EmailMessageService emailMessageService = new EmailMessageServiceImpl();
        return emailMessageService;
    }
}

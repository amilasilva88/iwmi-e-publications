/*
 * FILENAME
 *     JobStatusReportPDF.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.exports;

import static com.hashcode.iwmi.publications.util.ApplicationUtils.getString;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hashcode.iwmi.publications.domain.CostElement;
import com.hashcode.iwmi.publications.domain.DesigningCost;
import com.hashcode.iwmi.publications.domain.EditingCost;
import com.hashcode.iwmi.publications.domain.GraphicsCost;
import com.hashcode.iwmi.publications.domain.PrintingCost;
import com.hashcode.iwmi.publications.domain.ProofReadingCost;
import com.hashcode.iwmi.publications.domain.PublicationItem;
import com.hashcode.iwmi.publications.domain.RePrintingCost;
import com.hashcode.iwmi.publications.domain.TranslatingCost;
import com.hashcode.iwmi.publications.domain.TypeSettingCost;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;
import com.hashcode.iwmi.publications.services.PublicationItemService;
import com.hashcode.iwmi.publications.services.SystemUserService;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Job Status Report PDF generates here.
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
@Service
public class JobStatusReportPDF extends AbstractReportPDF
{
    private static final Logger log = LoggerFactory.getLogger(JobStatusReportPDF.class);

    @Autowired
    private PublicationItemService publicationService;
    
    @Autowired
    private SystemUserService userService;

    private Map<String, String> userNamesMap = new HashMap<String, String>();
    
    private String getName(String username)
    {
        if (userNamesMap.isEmpty())
        {
            try
            {
                userService.findAll().forEach(u -> {
                    userNamesMap.put(u.getUsername(), u.getFirstName() + " " + u.getLastName());
                });
            }
            catch (EPublicationException e)
            {
                log.error("Error in loading the users,", e);
            }
        }
        return userNamesMap.get(username);
    }

    
    @Override
    @Transactional
    public ByteArrayOutputStream generateReport(Long publicationItemId)
    {
        try
        {
            long startTime = System.currentTimeMillis();
            log.info("Cost Estimation Report Generation started, Item id :{}", publicationItemId);

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            Document iTextPdf = new Document(PageSize.A4);
            PdfWriter.getInstance(iTextPdf, outputStream);
            iTextPdf.open();

            PublicationItem item = publicationService.findById(publicationItemId);
            String statusJsn = item.getPublicationStatusJson();
            JSONObject statusData = (statusJsn == null) ? new JSONObject() : new JSONObject(statusJsn);

            PdfPTable documentTable = createDocumentTable();
            documentTitle("Publication Job: " + item.getTitle(), TITLE_BOLD, documentTable);

            double totalOverHead = 0.0, totalEstimation = 0.0;
            List<PdfPTable> costElementTables = new ArrayList<PdfPTable>();
            for (CostElement element : item.getActualCostings())
            {
                totalOverHead += element.getOtherCosts();
                totalEstimation += element.getSubTotal();

                switch (element.getType())
                {
                    case "TYPE_SETTING_COST":
                        PdfPTable typeSettingTable = typeSettingCostTable((TypeSettingCost) element, statusData);
                        costElementTables.add(typeSettingTable);
                        break;
                    case "EDITING_COST":
                        PdfPTable editCostTable = editingCostTable((EditingCost) element, statusData);
                        costElementTables.add(editCostTable);
                        break;
                    case "DESIGNING_COST":
                        PdfPTable dsngCostTable = desingingCostTable((DesigningCost) element, statusData);
                        costElementTables.add(dsngCostTable);
                        break;
                    case "GRAPHICS_COST":
                        PdfPTable graphicCostTable = graphicCostTable((GraphicsCost) element, statusData);
                        costElementTables.add(graphicCostTable);
                        break;
                    case "PROOF_READ_COST":
                        PdfPTable pReadCostTable = proofReadCostTable((ProofReadingCost) element, statusData);
                        costElementTables.add(pReadCostTable);
                        break;
                    case "TRANSLATING_COST":
                        PdfPTable translatingCostTable = translatingCostTable((TranslatingCost) element, statusData);
                        costElementTables.add(translatingCostTable);
                        break;
                    case "PRINTING_COST":
                        PdfPTable printingCostTable = printingCostTable((PrintingCost) element, statusData);
                        costElementTables.add(printingCostTable);
                        break;
                    case "REPRINTING_COST":
                        PdfPTable rePrintingCostTable = rePrintingCostTable((RePrintingCost) element, statusData);
                        costElementTables.add(rePrintingCostTable);
                        break;
                    default:
                        break;
                }

            }

            PdfPTable jobDetailTable = createJobDetailTable(item, statusData, (totalOverHead + totalEstimation));
            documentTable.addCell(createCellNoBorder(jobDetailTable, 1));
            documentTable.addCell(emptyCellNoBorder(1));

            for (PdfPTable costTable : costElementTables)
            {
                documentTable.addCell(createCellNoBorder(costTable, 1));
                documentTable.addCell(emptyCellNoBorder(1));
            }

            iTextPdf.add(documentTable);
            iTextPdf.close();

            long endTime = System.currentTimeMillis();

            log.info("Time taken to create PDF :" + (endTime - startTime) + " milis");

            return outputStream;
        }
        catch (Exception e)
        {
            log.error("Error occurred while generating the PDF report,", e);
            return null;
        }
    }

    private PdfPTable createJobDetailTable(PublicationItem item, JSONObject data, double totalActualCost)
        throws Exception
    {

        PdfPTable jobDetailTable = new PdfPTable(2);
        jobDetailTable.setWidthPercentage(100);
        jobDetailTable.setSpacingBefore(5);
        jobDetailTable.setSpacingAfter(5);

        PdfPCell jobTitleCell = new PdfPCell(new Phrase("Job Details", BOLD));
        jobTitleCell.setColspan(2);
        jobTitleCell.setBackgroundColor(LIGHT_BLUE_PDF_TABLE_HEADER);
        jobTitleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        jobDetailTable.addCell(jobTitleCell);

        PdfPTable field = fieldTable(LABEL_BOLD, NORMAL, "   Job Number:", item.getJobNumber());
        jobDetailTable.addCell(createCellNoBorder(field, 1));
        field = fieldTable(LABEL_BOLD, NORMAL, "   Related program:", getString(data, "relatedProgram"));
        jobDetailTable.addCell(createCellNoBorder(field, 1));
        field = fieldTable(LABEL_BOLD, NORMAL, "   Type of the job:", item.getType());
        jobDetailTable.addCell(createCellNoBorder(field, 1));
        field = fieldTable(LABEL_BOLD, NORMAL, "   Series:", item.getSeries());
        jobDetailTable.addCell(createCellNoBorder(field, 1));
        field = fieldTable(LABEL_BOLD, NORMAL, "   Title:", item.getTitle());
        jobDetailTable.addCell(createCellNoBorder(field, 1));
        field = fieldTable(LABEL_BOLD, NORMAL, "   Author:", getString(data, "author"));
        jobDetailTable.addCell(createCellNoBorder(field, 1));
        field =
            fieldTable(LABEL_BOLD, NORMAL, "   Manuscript received date:", getString(data, "manuScriptReceiveDate"));
        jobDetailTable.addCell(createCellNoBorder(field, 1));
        field =
            fieldTable(LABEL_BOLD, NORMAL, "   Number of manuscript pages:", getString(data, "noOfManuScriptPages"));
        jobDetailTable.addCell(createCellNoBorder(field, 1));
        field = fieldTable(LABEL_BOLD, NORMAL, "   ISBN-ISSN:", getString(data, "isbn_issn"));
        jobDetailTable.addCell(createCellNoBorder(field, 1));
        field = fieldTable(LABEL_BOLD, NORMAL, "   Requested By:", getName(item.getCreatedBy()));
        jobDetailTable.addCell(createCellNoBorder(field, 1));
        field = fieldTable(LABEL_BOLD, NORMAL, "   Supervisor:", getName(item.getSupervisorName()));
        jobDetailTable.addCell(createCellNoBorder(field, 1));
        field = fieldTable(LABEL_BOLD, NORMAL, "   Project code:", item.getProjectCode());
        jobDetailTable.addCell(createCellNoBorder(field, 1));
        field = fieldTable(LABEL_BOLD, NORMAL, "   Job requested date:", "" + item.getCreatedDate());
        jobDetailTable.addCell(createCellNoBorder(field, 1));
        field = fieldTable(LABEL_BOLD, NORMAL, "   Job approve date:", "" + item.getApprovedDate());
        jobDetailTable.addCell(createCellNoBorder(field, 1));
        field = fieldTable(LABEL_BOLD, NORMAL, "   Process started date:", getString(data, "processStartDate"));
        jobDetailTable.addCell(createCellNoBorder(field, 1));
        field =
            fieldTable(LABEL_BOLD, NORMAL, "   Estimated job completing date:",
                getString(data, "estimatedCompletionDate"));
        jobDetailTable.addCell(createCellNoBorder(field, 1));
        field = fieldTable(LABEL_BOLD, NORMAL, "   Process ended date:", "" + item.getCompletionDate());
        jobDetailTable.addCell(createCellNoBorder(field, 1));
        field = fieldTable(LABEL_BOLD, NORMAL, "   Date entered into the system:", "" + item.getCreatedDate());
        jobDetailTable.addCell(createCellNoBorder(field, 1));
        field = fieldTable(LABEL_BOLD, NORMAL, "   Actual Cost:", "" + totalActualCost);
        jobDetailTable.addCell(createCellNoBorder(field, 1));
        jobDetailTable.addCell(emptyCellNoBorder(1));

        return jobDetailTable;
    }

    private PdfPTable printingCostTable(PrintingCost cost, JSONObject data)
    {
        PdfPTable elementTable = new PdfPTable(2);
        elementTable.setWidthPercentage(100);
        elementTable.setSpacingBefore(5);
        elementTable.setSpacingAfter(5);

        PdfPCell elementTitleCell = new PdfPCell(new Phrase("Printing", BOLD));
        elementTitleCell.setColspan(2);
        elementTitleCell.setBackgroundColor(LIGHT_BLUE_PDF_TABLE_HEADER);
        elementTitleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        elementTable.addCell(elementTitleCell);

        PdfPTable field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Printer:", getString(data, "printer"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field =
            fieldTableForValues(LABEL_BOLD, NORMAL, "    Quotation called date:", getString(data, "quotation_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field =
            fieldTableForValues(LABEL_BOLD, NORMAL, "    Quotations received date:",
                getString(data, "quotation_received_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Sent date for printing:", getString(data, "sent_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Rate:", cost.getCostPerUnit());
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Number of pages:", cost.getNoOfCopies());
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Purchase order number:", getString(data, "order_num"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Print cost:", cost.getSubTotal());
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field =
            fieldTableForValues(LABEL_BOLD, NORMAL, "    Estimated date of completion:",
                getString(data, "completion_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Progress:", getString(data, "progress"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field =
            fieldTableForValues(LABEL_BOLD, NORMAL, "    Machine proof expected date:",
                getString(data, "machine_proof_expected_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field =
            fieldTableForValues(LABEL_BOLD, NORMAL, "    Machine proof received date:",
                getString(data, "machine_proof_received_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field =
            fieldTableForValues(LABEL_BOLD, NORMAL, "    Machine proof approved date:",
                getString(data, "machine_proof_approved_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Delay cause:", getString(data, "delay"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Actual delivery:", getString(data, "actual_delivery"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        return elementTable;
    }

    private PdfPTable rePrintingCostTable(RePrintingCost cost, JSONObject data)
    {
        PdfPTable elementTable = new PdfPTable(2);
        elementTable.setWidthPercentage(100);
        elementTable.setSpacingBefore(5);
        elementTable.setSpacingAfter(5);

        PdfPCell elementTitleCell = new PdfPCell(new Phrase("Re-Printing", BOLD));
        elementTitleCell.setColspan(2);
        elementTitleCell.setBackgroundColor(LIGHT_BLUE_PDF_TABLE_HEADER);
        elementTitleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        elementTable.addCell(elementTitleCell);

        PdfPTable field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Printer:", getString(data, "printer"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field =
            fieldTableForValues(LABEL_BOLD, NORMAL, "    Quotation called date:", getString(data, "quotation_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field =
            fieldTableForValues(LABEL_BOLD, NORMAL, "    Quotations received date:",
                getString(data, "quotation_received_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Sent date for printing:", getString(data, "sent_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Rate:", cost.getCostPerUnit());
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Number of pages:", cost.getNoOfCopies());
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Purchase order number:", getString(data, "order_num"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Print cost:", cost.getSubTotal());
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field =
            fieldTableForValues(LABEL_BOLD, NORMAL, "    Estimated date of completion:",
                getString(data, "completion_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Progress:", getString(data, "progress"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field =
            fieldTableForValues(LABEL_BOLD, NORMAL, "    Machine proof expected date:",
                getString(data, "machine_proof_expected_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field =
            fieldTableForValues(LABEL_BOLD, NORMAL, "    Machine proof received date:",
                getString(data, "machine_proof_received_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field =
            fieldTableForValues(LABEL_BOLD, NORMAL, "    Machine proof approved date:",
                getString(data, "machine_proof_approved_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Delay cause:", getString(data, "delay"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Actual delivery:", getString(data, "actual_delivery"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        return elementTable;
    }

    private PdfPTable translatingCostTable(TranslatingCost cost, JSONObject data)
    {
        PdfPTable elementTable = new PdfPTable(2);
        elementTable.setWidthPercentage(100);
        elementTable.setSpacingBefore(5);
        elementTable.setSpacingAfter(5);

        PdfPCell elementTitleCell = new PdfPCell(new Phrase("Translating", BOLD));
        elementTitleCell.setColspan(2);
        elementTitleCell.setBackgroundColor(LIGHT_BLUE_PDF_TABLE_HEADER);
        elementTitleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        elementTable.addCell(elementTitleCell);

        PdfPTable field =
            fieldTableForValues(LABEL_BOLD, NORMAL, "    Contract number:", getString(data, "contract_number"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Translator:", getString(data, "translator"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Rate:", cost.getCostPerUnit());
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Number of pages:", cost.getNoOfCopies());
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Cost:", cost.getSubTotal());
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Date sent for translating:", getString(data, "sent_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field =
            fieldTableForValues(LABEL_BOLD, NORMAL, "    Estimated date of receiving translation:",
                getString(data, "est_receiving_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field =
            fieldTableForValues(LABEL_BOLD, NORMAL, "    Date received translated:", getString(data, "receiving_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Progress:", getString(data, "progress"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Comments:", getString(data, "comments"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        return elementTable;
    }

    private PdfPTable proofReadCostTable(ProofReadingCost cost, JSONObject data)
    {
        PdfPTable elementTable = new PdfPTable(2);
        elementTable.setWidthPercentage(100);
        elementTable.setSpacingBefore(5);
        elementTable.setSpacingAfter(5);

        PdfPCell elementTitleCell = new PdfPCell(new Phrase("Proofreading", BOLD));
        elementTitleCell.setColspan(2);
        elementTitleCell.setBackgroundColor(LIGHT_BLUE_PDF_TABLE_HEADER);
        elementTitleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        elementTable.addCell(elementTitleCell);

        PdfPTable field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Proof reader:", getString(data, "reader"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Started date:", getString(data, "started_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Estimated ending date:", getString(data, "est_end_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Ended date:", getString(data, "ended_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Progress:", getString(data, "progress"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field =
            fieldTableForValues(LABEL_BOLD, NORMAL, "    Final proof sent to author:",
                getString(data, "final_sent_to_author_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field =
            fieldTableForValues(LABEL_BOLD, NORMAL, "    Final proof approved date:",
                getString(data, "final_sent_approved_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Pre-press in date:", getString(data, "pre_press_in_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field =
            fieldTableForValues(LABEL_BOLD, NORMAL, "    Estimated pre-press out date:",
                getString(data, "est_pre_press_out_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field =
            fieldTableForValues(LABEL_BOLD, NORMAL, "    Pre-press out date:", getString(data, "pre_press_out_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field =
            fieldTableForValues(LABEL_BOLD, NORMAL, "    Pre-press response:", getString(data, "pre_press_response"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field =
            fieldTableForValues(LABEL_BOLD, NORMAL, "    Pre-press progress:", getString(data, "pre_press_progress"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        return elementTable;
    }

    private PdfPTable graphicCostTable(GraphicsCost cost, JSONObject data)
    {
        PdfPTable elementTable = new PdfPTable(2);
        elementTable.setWidthPercentage(100);
        elementTable.setSpacingBefore(5);
        elementTable.setSpacingAfter(5);

        PdfPCell elementTitleCell = new PdfPCell(new Phrase("Graphics", BOLD));
        elementTitleCell.setColspan(2);
        elementTitleCell.setBackgroundColor(LIGHT_BLUE_PDF_TABLE_HEADER);
        elementTitleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        elementTable.addCell(elementTitleCell);

        PdfPTable field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Designer:", getString(data, "designer"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Rate:", cost.getCostPerUnit());
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Designing hours:", getString(data, "designing_hours"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Designing cost:", cost.getSubTotal());
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Trimmed size:", getString(data, "trimmed_size"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Web version:", getString(data, "web_version_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Started date:", getString(data, "started_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field =
            fieldTableForValues(LABEL_BOLD, NORMAL, "    Estimated designing out date:",
                getString(data, "designing_out_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Ended date:", getString(data, "ended_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Progress:", getString(data, "progress"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        return elementTable;
    }

    private PdfPTable desingingCostTable(DesigningCost cost, JSONObject data)
    {
        PdfPTable elementTable = new PdfPTable(2);
        elementTable.setWidthPercentage(100);
        elementTable.setSpacingBefore(5);
        elementTable.setSpacingAfter(5);

        PdfPCell elementTitleCell = new PdfPCell(new Phrase("Designing", BOLD));
        elementTitleCell.setColspan(2);
        elementTitleCell.setBackgroundColor(LIGHT_BLUE_PDF_TABLE_HEADER);
        elementTitleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        elementTable.addCell(elementTitleCell);

        PdfPTable field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Designer:", getString(data, "designer"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Rate:", cost.getCostPerUnit());
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Designing hours:", getString(data, "designing_hours"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Designing cost:", cost.getSubTotal());
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Trimmed size:", getString(data, "trimmed_size"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Web version:", getString(data, "web_version_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Started date:", getString(data, "started_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field =
            fieldTableForValues(LABEL_BOLD, NORMAL, "    Estimated designing out date:",
                getString(data, "designing_out_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Ended date:", getString(data, "ended_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Progress:", getString(data, "progress"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));

        return elementTable;
    }

    private PdfPTable typeSettingCostTable(TypeSettingCost cost, JSONObject data)
    {
        PdfPTable elementTable = new PdfPTable(2);
        elementTable.setWidthPercentage(100);
        elementTable.setSpacingBefore(5);
        elementTable.setSpacingAfter(5);

        PdfPCell elementTitleCell = new PdfPCell(new Phrase("Typesetting", BOLD));
        elementTitleCell.setColspan(2);
        elementTitleCell.setBackgroundColor(LIGHT_BLUE_PDF_TABLE_HEADER);
        elementTitleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        elementTable.addCell(elementTitleCell);

        PdfPTable field =
            fieldTableForValues(LABEL_BOLD, NORMAL, "    Consultancy reference number:", getString(data, "ref_number"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Typesetter:", getString(data, "type_setter"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Rate:", cost.getCostPerUnit());
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Number of pages:", cost.getNoOfCopies());
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Typesetting cost:", cost.getSubTotal());
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Date sent for typesetting:", getString(data, "date_sent"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Started date:", getString(data, "started_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Estimated ending date:", getString(data, "est_end_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Ended date:", getString(data, "ended_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Progress:", getString(data, "progress"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));

        return elementTable;
    }

    private PdfPTable editingCostTable(EditingCost cost, JSONObject data)
    {
        PdfPTable elementTable = new PdfPTable(2);
        elementTable.setWidthPercentage(100);
        elementTable.setSpacingBefore(5);
        elementTable.setSpacingAfter(5);

        PdfPCell elementTitleCell = new PdfPCell(new Phrase("Editing", BOLD));
        elementTitleCell.setColspan(2);
        elementTitleCell.setBackgroundColor(LIGHT_BLUE_PDF_TABLE_HEADER);
        elementTitleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        elementTable.addCell(elementTitleCell);

        PdfPTable field =
            fieldTableForValues(LABEL_BOLD, NORMAL, "    Consultancy reference number:", getString(data, "ref_number"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Editor:", getString(data, "editor"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Rate:", cost.getCostPerUnit());
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Number of pages:", cost.getNoOfCopies());
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Editing  cost:", cost.getSubTotal());
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Date sent for editing:", getString(data, "date_sent"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Started date:", getString(data, "started_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Estimated ending date:", getString(data, "est_end_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Ended date:", getString(data, "ended_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Progress:", getString(data, "progress"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field =
            fieldTableForValues(LABEL_BOLD, NORMAL, "    Manuscript sent for author’s approval:",
                getString(data, "authors_approval_date"));
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        return elementTable;
    }
}

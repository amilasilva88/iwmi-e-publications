/*
 * FILENAME
 *     DocumentDao.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.dao;

import java.util.List;

import com.hashcode.iwmi.publications.domain.Document;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * DAO for Document
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
public interface DocumentDao extends GenericDao<Document, Long>
{

    /**
     * <p>
     * Find the documents attached to given publication item
     * </p>
     *
     *
     * @param publicationId
     *            publication item id
     * @return list of attachments for publication item.
     *
     */
    List<Document> findDocumentsByPublicationItem(Long publicationId);

    /**
     * <p>
     * Find Documents by File name.
     * </p>
     *
     * @param fileName
     *            file name
     * @return list of {@link Document}
     *
     */
    List<Document> findByFileName(String fileName);
}

/*
 * FILENAME
 *     SystemUserDao.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.dao;

import com.hashcode.iwmi.publications.domain.SystemUser;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * System User Dao.
 * </p>
 *
 * @author Manuja Jayamanna
 * @email manuja@hashcodesys.com
 **/
public interface SystemUserDao extends GenericDao<SystemUser, Long>
{
    /**
     * <p>
     * Find user by username
     * </p>
     *
     * @param username
     *            username
     * 
     * @return {@link SystemUser} instance
     *
     */
    SystemUser findUser(final String username);

    /**
     * <p>
     * Find user by email
     * </p>
     *
     * @param email
     *            email
     * 
     * @return {@link SystemUser} instance
     *
     */
    SystemUser findUserByEmail(final String email);

    /**
     * <p>
     * Find user by id
     * </p>
     *
     * @param email
     *            email
     * 
     * @return {@link SystemUser} instance
     *
     */
    SystemUser findById(final Long userId);
}

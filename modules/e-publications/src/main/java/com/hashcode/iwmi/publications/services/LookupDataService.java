/*
 * FILENAME
 *     LookupDataService.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.services;

import java.util.List;

import com.hashcode.iwmi.publications.domain.LookupData;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Lookup Data Service for all the lookup menus
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
public interface LookupDataService
{
    /**
     * <p>
     * Find All Lookup data for given lookup type.
     * </p>
     *
     * @param clazz
     *            lookup class type
     * @return list of Lookup data
     * @throws EPublicationException
     *             throws in any error
     *
     */
    <T extends LookupData> List<T> findAllLookupData(Class<T> clazz) throws EPublicationException;

    /**
     * <p>
     * Find Lookup data by id
     * </p>
     *
     * @param clazz
     *            lookup class type
     * @return list of Lookup data
     * @throws EPublicationException
     *             throws in any error
     *
     */
    <T extends LookupData> T findLookupById(Class<T> clazz, Long id) throws EPublicationException;

    /**
     * <p>
     * Find Lookups related to given parent lookup id
     * </p>
     *
     * @param clazz
     *            lookup class type
     * @param parentId
     *            parent lookup id
     * @return
     * @throws EPublicationException
     *
     */
    <T extends LookupData> List<T> findAllLookupForParent(Class<T> clazz, Long parentId) throws EPublicationException;

}

/*
 * FILENAME
 *     LookupDataServiceImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.services.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hashcode.iwmi.publications.dao.LookupDataDao;
import com.hashcode.iwmi.publications.domain.LookupData;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;
import com.hashcode.iwmi.publications.services.LookupDataService;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Implementation for the {LookupDataService}
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
@Service
@Transactional
public class LookupDataServiceImpl implements LookupDataService
{

    @Autowired
    private LookupDataDao lookupDataDao;

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.services.LookupDataService#findAllLookupData(java.lang.Class)
     */
    @Override
    public <T extends LookupData> List<T> findAllLookupData(Class<T> clazz) throws EPublicationException
    {
        return lookupDataDao.findAll(clazz);
    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.services.LookupDataService#findLookupById(java.lang.Class, java.lang.Long)
     */
    @Override
    public <T extends LookupData> T findLookupById(Class<T> clazz, Long id) throws EPublicationException
    {
        return lookupDataDao.findById(clazz, id);
    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.services.LookupDataService#findAllLookupForParent(java.lang.Class,
     *      java.lang.Long)
     */
    @Override
    public <T extends LookupData> List<T> findAllLookupForParent(Class<T> clazz, Long parentId)
        throws EPublicationException
    {
        return lookupDataDao.findByParentId(clazz, parentId);
    }

}

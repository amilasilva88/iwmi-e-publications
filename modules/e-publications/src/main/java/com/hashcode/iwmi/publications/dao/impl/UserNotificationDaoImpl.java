/*
 * FILENAME
 *     UserNotificationDaoImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.dao.impl;

import java.util.List;

import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.hashcode.iwmi.publications.dao.UserNotificationDao;
import com.hashcode.iwmi.publications.domain.UserNotification;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * UserNotificationDao Implementation
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
@Repository
public class UserNotificationDaoImpl extends GenericDaoImpl<UserNotification, Long> implements UserNotificationDao
{

    private static final Logger log = LoggerFactory.getLogger(UserNotificationDaoImpl.class);

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.dao.UserNotificationDao#findUserNotificationByUser(java.lang.String, int)
     */
    @Override
    public List<UserNotification> findUserNotificationByUser(String username, int limit)
    {
        log.debug("Find user notifications by user, [ Username : {}]", username);

        String queryText =
            "SELECT n FROM " + UserNotification.class.getName() + " n "
                + " WHERE n.username = :username ORDER BY n.notifiedTime DESC, n.status ASC ";

        TypedQuery<UserNotification> query = entityManager.createQuery(queryText, UserNotification.class);
        query.setParameter("username", username);
        query.setMaxResults(limit);

        List<UserNotification> userNotifications = query.getResultList();
        log.debug("Found notifications for user :{}, [ Results :{}]", username, userNotifications.size());
        return userNotifications;
    }

}

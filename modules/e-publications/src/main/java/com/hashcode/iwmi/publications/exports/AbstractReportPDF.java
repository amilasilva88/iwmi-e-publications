/*
 * FILENAME
 *     AbstractReportPDF.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.exports;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Abstract Report PDF generator class
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
public abstract class AbstractReportPDF implements PDFReportGenerator
{
    private static final Logger log = LoggerFactory.getLogger(AbstractReportPDF.class);
    protected Font BOLD, TITLE_BOLD, LABEL_BOLD, NORMAL;

    public AbstractReportPDF()
    {
        try
        {
            BOLD = new Font(BaseFont.createFont(BaseFont.TIMES_ROMAN, "", false), 8.0f, Font.BOLD);
            TITLE_BOLD = new Font(BaseFont.createFont(BaseFont.TIMES_ROMAN, "", false), 10.0f, Font.BOLD);
            LABEL_BOLD = new Font(BaseFont.createFont(BaseFont.TIMES_ROMAN, "", false), 8.0f, Font.BOLD);
            LABEL_BOLD.setColor(BLUE_LABEL_TEXT);
            NORMAL = new Font(BaseFont.createFont(BaseFont.TIMES_ROMAN, "", false), 8.0f, Font.NORMAL);
        }
        catch (Exception e)
        {
            log.error("Error occurred in initializing AbstractReportPDF,", e);
        }
    }

    protected PdfPCell createCellNoBorder(PdfPTable field, int colSpan)
    {
        PdfPCell cell = new PdfPCell(field);
        cell.setColspan(colSpan);
        cell.setBorder(Rectangle.BOTTOM);  
        cell.setBorderWidth(0.7f);
//        cell.setBorder(0);
        return cell;
    }

    protected PdfPCell createCellNoBorder(PdfPTable field, int colSpan, BaseColor baseBgColor)
    {
        PdfPCell cell = new PdfPCell(field);
        cell.setColspan(colSpan);
        cell.setBorder(Rectangle.BOTTOM);  
        cell.setBorderWidth(0.7f);
        cell.setBackgroundColor(baseBgColor);
        return cell;
    }

    protected PdfPTable fieldTable(final Font labelFont, final Font valueFont, String label, String value)
    {
        PdfPTable field = new PdfPTable(2);
        PdfPCell labelCell = new PdfPCell(new Phrase(label, labelFont));
        PdfPCell valueCell = new PdfPCell(new Phrase(value, valueFont));
        labelCell.setBorder(0);
        valueCell.setBorder(0);
        field.addCell(labelCell);
        field.addCell(valueCell);

        return field;
    }
    
    protected PdfPTable fieldTableForValues(final Font labelFont, final Font valueFont, String label, String value)
    {

        PdfPTable field = new PdfPTable(2);
        PdfPCell labelCell = new PdfPCell(new Phrase(label, labelFont));
        PdfPCell valueCell = new PdfPCell(new Phrase("" + value, valueFont));
        labelCell.setBorder(0);
        valueCell.setBorder(0);
        field.addCell(labelCell);
        field.addCell(valueCell);

        return field;
    }

    protected PdfPTable fieldTableForValues(final Font labelFont, final Font valueFont, String label, double value)
    {

        PdfPTable field = new PdfPTable(2);
        PdfPCell labelCell = new PdfPCell(new Phrase(label, labelFont));
        PdfPCell valueCell = new PdfPCell(new Phrase("" + value, valueFont));
        labelCell.setBorder(0);
        valueCell.setBorder(0);
        valueCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        field.addCell(labelCell);
        field.addCell(valueCell);

        return field;
    }

    protected void documentTitle(String title, Font titleFont, PdfPTable documentTable)
    {
        PdfPCell titleCell = new PdfPCell(new Phrase(title, titleFont));
        titleCell.setBackgroundColor(SKY_BLUE_PDF_TABLE_HEADER);
        titleCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        titleCell.setBorder(0);
        documentTable.addCell(titleCell);
    }

    protected PdfPCell emptyCellWithBorder(int colSpan)
    {
        PdfPCell cell = new PdfPCell(Phrase.getInstance(""));
        cell.setColspan(colSpan);
        cell.setBorder(1);
        return cell;
    }

    protected PdfPCell emptyCellNoBorder(int colSpan)
    {
        PdfPCell cell = new PdfPCell(Phrase.getInstance(""));
        cell.setColspan(colSpan);
        cell.setBorder(Rectangle.BOTTOM);  
        cell.setBorderWidth(0.7f);
        return cell;
    }

    protected PdfPCell emptyCellNoBorder(int colSpan, BaseColor baseBgColor)
    {
        PdfPCell cell = new PdfPCell(Phrase.getInstance(""));
        cell.setColspan(colSpan);
        cell.setBorder(Rectangle.BOTTOM);  
        cell.setBorderWidth(0.7f);
//        cell.setBorder(0);
        cell.setBackgroundColor(baseBgColor);
        return cell;
    }

    protected PdfPTable createDocumentTable()
    {
        PdfPTable documentTable = new PdfPTable(1);
        documentTable.setWidthPercentage(98);
        return documentTable;
    }
}

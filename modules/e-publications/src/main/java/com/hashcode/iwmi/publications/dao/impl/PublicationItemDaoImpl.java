/*
 * FILENAME
 *     PublicationItemDaoImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.dao.impl;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.hashcode.iwmi.publications.dao.PublicationItemDao;
import com.hashcode.iwmi.publications.domain.PublicationItem;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * DAO Implementation for Publication Item DAO
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
@Repository
public class PublicationItemDaoImpl extends GenericDaoImpl<PublicationItem, Long> implements PublicationItemDao
{

    private static final Logger log = LoggerFactory.getLogger(PublicationItemDaoImpl.class);

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.dao.impl.GenericDaoImpl#create(com.hashcode.iwmi.publications.domain.BaseModel)
     */
    @Override
    public void create(PublicationItem pubItem)
    {
        super.create(pubItem);
        LocalDate date = LocalDate.now();
        String jobNumber = "EP-" + date.getYear() + date.getMonth().getValue() + date.getDayOfMonth() + "-" + pubItem.getId();
        pubItem.setJobNumber(jobNumber);
        super.update(pubItem);
        
        log.info("New E-publication Item created with Job number: {}", jobNumber);
    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.dao.PublicationItemDao#statusUpdate(com.hashcode.iwmi.publications.domain.PublicationItem)
     */
    @Override
    public void statusUpdate(PublicationItem publicationItem) throws EPublicationException
    {
        log.debug("Publication Item id :{}", publicationItem.getId());

//        String queryText = "UPDATE " + PublicationItem.class.getName() + " p SET p.status = :status WHERE p.id = :id";
//
//        Query query = entityManager.createQuery(queryText);
//        query.setParameter("status", publicationItem.getStatus());
//        query.setParameter("id", publicationItem.getId());

        super.update(publicationItem);

        log.debug("Publication Item status updated, id:{}, status :{}", publicationItem.getId(),
            publicationItem.getStatus());
    }
    
    @Override
    public List<PublicationItem> findByIds(List<Long> ids) throws EPublicationException
    {
        log.debug("Publication Item ids :{}", ids);

        String queryText = "SELECT p FROM " + PublicationItem.class.getName() + " p WHERE p.id in :ids ";
        TypedQuery<PublicationItem> query = entityManager.createQuery(queryText, PublicationItem.class);
        query.setParameter("ids", ids);

        List<PublicationItem> items =  query.getResultList();
        log.debug("Publication Items, No of Records :{}", items.size());
        
        return items;
    }

}

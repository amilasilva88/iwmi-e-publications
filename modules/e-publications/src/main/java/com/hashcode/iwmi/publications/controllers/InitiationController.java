/*
 * FILENAME
 *     InitiationController.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.controllers;

import static com.hashcode.iwmi.publications.domain.PublicationStatus.COMPLETED;
import static com.hashcode.iwmi.publications.domain.PublicationStatus.DELETED;
import static com.hashcode.iwmi.publications.domain.PublicationStatus.INITIALISED;
import static com.hashcode.iwmi.publications.domain.PublicationStatus.NEED_AN_AMENDMENT;
import static com.hashcode.iwmi.publications.domain.PublicationStatus.NEW;
import static com.hashcode.iwmi.publications.domain.PublicationStatus.REJECTED;
import static com.hashcode.iwmi.publications.util.ApplicationUtils.addUserToModel;
import static com.hashcode.iwmi.publications.util.ApplicationUtils.createResponse;
import static com.hashcode.iwmi.publications.util.ApplicationUtils.getResponceDataMap;
import static com.hashcode.iwmi.publications.util.ApplicationUtils.setItemStatus;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hashcode.iwmi.publications.domain.CostElement;
import com.hashcode.iwmi.publications.domain.CostElementFactory;
import com.hashcode.iwmi.publications.domain.Document;
import com.hashcode.iwmi.publications.domain.PublicationItem;
import com.hashcode.iwmi.publications.domain.PublicationStatus;
import com.hashcode.iwmi.publications.domain.SystemUser;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;
import com.hashcode.iwmi.publications.listeners.PublicationStateChangeListener;
import com.hashcode.iwmi.publications.services.PublicationItemService;
import com.hashcode.iwmi.publications.services.SystemUserService;
import com.hashcode.iwmi.publications.util.ApplicationUtils;
import com.hashcode.iwmi.publications.util.ResponseStatus;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Initiation Controller for Initiation page.
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 **/
@Controller
@RequestMapping("/initiation")
public class InitiationController
{
    private static final Logger log = LoggerFactory.getLogger(InitiationController.class);

    public static final String ACTION_SAVE = "SAVE";
    public static final String ACTION_NEXT = "NEXT";

    @Autowired
    private PublicationItemService publicationItemService;

    @Autowired
    private SystemUserService userService;

    @Autowired
    @Qualifier(value = "initiationStateChange")
    private PublicationStateChangeListener stateChangeListener;

    @RequestMapping(value = "/new", method = {
        RequestMethod.GET
    })
    public String loadPage(Model model) throws EPublicationException
    {
        log.debug("Request came to load new form");
        addUserToModel(model);
        return "job-initiation";
    }

    @RequestMapping("/page")
    public String pageLoad(@RequestParam("id") long id, Model model)
    {
        log.info("Request came to load the status page....");
        addUserToModel(model);
        model.addAttribute("id", id);
        return "job-initiation";
    }

    @RequestMapping(value = "/load/{id}", method = {
        RequestMethod.GET
    })
    @ResponseBody
    @Transactional
    public String loadData(@PathVariable("id") long id) throws EPublicationException
    {
        log.debug("Request came to load intital data for publications [ Id :{}]", id);

        PublicationItem item = publicationItemService.findById(id);
        
        String username = ApplicationUtils.getCurrentUser().getUsername();
        SystemUser currentUser = userService.findUser(username);

        log.debug("Found Publication Item, Id :{}", item);
        JSONObject data = populateDataForUI(item, currentUser);
        return data.toString();
    }

    private JSONObject populateDataForUI(PublicationItem item, SystemUser currentUser) throws EPublicationException
    {
        JSONObject data = new JSONObject();
        log.info("populateDataForUI");
        if (item != null)
        {
            log.info("populateDataForUI > 1");
            data.accumulate("id", item.getId());
            log.info("populateDataForUI > item.getStatus()= " + item.getStatus());
            data.accumulate("typeOfWork", item.getTypeOfWork());
            data.accumulate("title", item.getTitle());
            data.accumulate("descOfWork", item.getDescOfWork());
            data.accumulate("printRun", item.getPrintRun());
            data.accumulate("projectCode", item.getProjectCode());
            data.accumulate("requiredDeliveryDate", item.getRequiredDeliveryDate());
            data.accumulate("division", item.getDivision());
            data.accumulate("requesterName", item.getRequesterName());
            data.accumulate("supervisorName", item.getSupervisorName());
            data.accumulate("otherInfo", item.getOtherInfo());
            
            SystemUser createdByUser = userService.findUser(item.getCreatedBy());
            String requestedByUser  = "";
            if (createdByUser != null)
                requestedByUser = createdByUser.getFirstName() + " "+ createdByUser.getLastName();
            data.accumulate("created_by", requestedByUser);

            JSONArray costElements = new JSONArray();
            for (CostElement element : item.getCostEstimations())
            {
                JSONObject jSonData = new JSONObject();
                jSonData.accumulate("cost_type", element.getType());
                jSonData.accumulate("no_of_pages", element.getNoOfCopies());
                jSonData.accumulate("cost_per_unit", element.getCostPerUnit());
                jSonData.accumulate("other_costs", element.getOtherCosts());
                costElements.put(jSonData);
            }
            data.put("costElements", costElements);
            boolean hasAccessPermission = checkAccessPermission(currentUser, item);
            log.debug("Intital data for publications, Permission, [ User :{}, can edit :{}]",
                currentUser.getUsername(), hasAccessPermission);
            data.put("canEdit", hasAccessPermission);
            
            List<Document> attachments = item.getAttachments();

            log.debug("Found Publication Item, Attachments :{}", attachments.size());

            JSONArray docAttachments = new JSONArray();
            attachments.stream().forEach(d -> {

                JSONObject jsonDoc = new JSONObject();
                jsonDoc.accumulate("doc_id", d.getId());
                jsonDoc.accumulate("doc_file_name", d.getFileName());
                jsonDoc.accumulate("doc_file_url", d.getFileUrl());
                docAttachments.put(jsonDoc);
            });

            data.put("docs", docAttachments);
            setItemStatus(item, data);
        }
        return data;
    }

    public boolean checkAccessPermission(SystemUser currentUser, PublicationItem item)
    {
        if (item.getStatus().equals(REJECTED) || item.getStatus().equals(DELETED) || item.getStatus().equals(COMPLETED))
        {
            return false;
        }
        else if (currentUser.isUser()
            && (item.getStatus().equals(NEW) || item.getStatus().equals(INITIALISED) || item.getStatus().equals(NEED_AN_AMENDMENT)))
        {
            return true;
        }
        else if (currentUser.isSupervisor() || currentUser.isOfficer())
        {
            return true;
        }
        else
            return false;
    }

    @RequestMapping(value = "/saveData", method = {
        RequestMethod.POST, RequestMethod.PUT
    })
    @Transactional
    @ResponseBody
    public String saveOrUpdateData(@RequestBody String data)
    {
        try
        {
            log.debug("Request came to save/update intital data for publications");
            PublicationItem item = null;

            String username = ApplicationUtils.getCurrentUser().getUsername();

            JSONObject jsonData = new JSONObject(data);
            String action = jsonData.getString("action");
            long id = (jsonData.has("id")) ? jsonData.getLong("id") : -1;

            item = (id > 0) ? publicationItemService.findById(id) : new PublicationItem();
            item.setTitle(jsonData.getString("title"));
            item.setTypeOfWork(jsonData.getString("typeOfWork"));
            item.setDescOfWork(jsonData.getString("descOfWork"));
            item.setPrintRun(jsonData.getString("printRun"));
            item.setProjectCode(jsonData.getString("projectCode"));
            item.setRequiredDeliveryDate(LocalDate.parse(jsonData.getString("requiredDeliveryDate")));
            item.setDivision(jsonData.getString("division"));
            item.setRequesterName(jsonData.getString("requesterName"));
            item.setSupervisorName(jsonData.getString("supervisorName"));
            item.setOtherInfo(jsonData.getString("otherInfo"));
            
            item.getCostEstimations().clear();
            JSONArray costElements =
                jsonData.has("costElements") ? jsonData.getJSONArray("costElements") : new JSONArray();
            if ((null != costElements) && (costElements.length() > 0))
            {
                for (int i = 0; i < costElements.length(); i++)
                {
                    JSONObject element = (JSONObject) costElements.get(i);
                    CostElement costElement = CostElementFactory.getCostElement(element.getString("cost_type"));
                    costElement.setNoOfCopies(element.getInt("no_of_pages"));
                    costElement.setCostPerUnit(element.getDouble("cost_per_unit"));
                    costElement.setOtherCosts(element.getDouble("other_costs"));
                    item.getCostEstimations().add(costElement);
                }
            }
            //update
            if (id > 0)
            {
                item.setLastModifiedBy(username);
                publicationItemService.update(item);
            }
            else
            {
                item.setStatus(PublicationStatus.NEW);
                item.setCreatedBy(username);
                publicationItemService.save(item);
            }

            if (action.equals(ACTION_NEXT))
            {
                stateChangeListener.onStateChange(username, item);
            }

            Map<String, Object> params = getResponceDataMap();
            params.put("id", item.getId());
            params.put("item_status", item.getStatus());
            params.put("data", "Publication initial data saved successfully.");

            JSONObject jsonReponse = createResponse(ResponseStatus.SUCCESS, params);
            log.debug("JSON Response for load divisions, Response : {}", jsonReponse);
            return jsonReponse.toString();
        }
        catch (Exception e)
        {
            log.error("Error occurred in saving/update publication,", e);
            Map<String, Object> params = getResponceDataMap();
            params.put("error", "Error encountered in saving/update Publication");
            return createResponse(ResponseStatus.FAILED, params).toString();
        }
    }
}

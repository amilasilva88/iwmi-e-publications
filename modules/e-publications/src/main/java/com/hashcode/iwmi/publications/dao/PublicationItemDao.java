/*
 * FILENAME
 *     PublicationItemDao.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.dao;

import java.util.List;

import com.hashcode.iwmi.publications.domain.PublicationItem;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * DAO for Publicatin Items
 * </p>
 * 
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
public interface PublicationItemDao extends GenericDao<PublicationItem, Long>
{

    /**
     * <p>
     * Update the status of the Publication item
     * </p>
     *
     * @param publicationItem
     *            publication Item
     * @return id of the item
     * @throws EPublicationException
     *             throws at any error
     *
     */
    void statusUpdate(PublicationItem publicationItem) throws EPublicationException;

    /**
     * <p>
     * Find Items by ids.
     * </p>
     *
     * @param ids
     * @return
     * @throws EPublicationException
     **/
    List<PublicationItem> findByIds(List<Long> ids) throws EPublicationException;

}

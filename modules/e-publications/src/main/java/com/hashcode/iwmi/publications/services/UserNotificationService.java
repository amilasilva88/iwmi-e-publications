/*
 * FILENAME
 *     UserNotificationService.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.services;

import java.util.List;

import com.hashcode.iwmi.publications.domain.UserNotification;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * User Notification Service.
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
public interface UserNotificationService
{

    /**
     * <p>
     * Add user notification.
     * </p>
     *
     * @param userNotification
     *            user notification
     * @return
     * @throws EPublicationException
     *
     */
    Long addUserNotification(UserNotification userNotification) throws EPublicationException;

    /**
     * <p>
     * Get User notifications by User.
     * </p>
     *
     * @param username
     * @return
     * @throws EPublicationException
     */
    List<UserNotification> findUserNotificationByUser(String username, int limit) throws EPublicationException;

}

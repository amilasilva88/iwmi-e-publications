/*
 * FILENAME
 *     MessageService.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.services;

import java.util.List;
import java.util.Map;

import com.hashcode.iwmi.publications.domain.EmailMessage;
import com.hashcode.iwmi.publications.domain.MessageTemplate;
import com.hashcode.iwmi.publications.domain.MessageTemplate.Type;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Generic Interface for all messaging through out the platform.
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
public interface EmailMessageService
{
    /**
     * <p>
     * Compose e-mail body using the given template.
     * </p>
     *
     * @param templateName
     *            e-mail body template
     * @param subjectPlaceholderMap
     *            value map to replace values with template place holders
     * @param bodyPlaceholderMap
     *            value map to replace values with template place holders
     * @return email body
     *
     */
    EmailMessage composeEmail(Type templateType, Map<MessageTemplate.SubjectTag, String> subjectPlaceholderMap,
        Map<MessageTemplate.BodyTag, Object> bodyPlaceholderMap) throws EPublicationException;

    /**
     * <p>
     * Send email message.
     * </p>
     *
     * @param message
     *            message
     * @throws EPublicationException
     *
     */
    void send(EmailMessage message) throws EPublicationException;

    /**
     * <p>
     * Send bulk messages to given recipients.
     * </p>
     *
     * @param message
     *            message
     * @param recipients
     *            list of recipients
     * @throws EPublicationException
     *
     */
    void sendBulkMessage(EmailMessage message, List<String> recipients) throws EPublicationException;

}

/*
 * FILENAME
 *     UserRole.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.domain;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * User Role Enum.
 * </p>
 *
 * @author Manuja Jayamanna
 * @email manuja@hashcodesys.com
 **/
public enum UserRole
{
    VIEWER("Viewer"),
    USER("User"),
    SUPERVISOR("Supervisor"),
    SUPERUSER("Superuser");

    private String label;

    private UserRole(final String label)
    {
        this.label = label;
    }

    /**
     * <p>
     * Getter for label.
     * </p>
     * 
     * @return the label
     */
    public String getLabel()
    {
        return label;
    }

    /**
     * {@inheritDoc}
     *
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString()
    {
        return label;
    }
}

/*
 * FILENAME
 *     AmendmentRequestStateChangeListener.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.listeners;

import static com.hashcode.iwmi.publications.domain.PublicationStatus.NEED_AN_AMENDMENT;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.hashcode.iwmi.publications.domain.MessageTemplate;
import com.hashcode.iwmi.publications.domain.MessageTemplate.BodyTag;
import com.hashcode.iwmi.publications.domain.MessageTemplate.SubjectTag;
import com.hashcode.iwmi.publications.domain.PublicationItem;
import com.hashcode.iwmi.publications.domain.SystemUser;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;
import com.hashcode.iwmi.publications.services.PublicationItemService;
import com.hashcode.iwmi.publications.services.SystemUserService;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Request for amendment state change listener.
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
@Service
@Qualifier(value = "amendmentRequestStateChange")
@Transactional
public class AmendmentRequestStateChangeListener extends AbstractPublicationStateChangeListener
{
    private static final Logger log = LoggerFactory.getLogger(AmendmentRequestStateChangeListener.class);

    @Autowired
    private PublicationItemService publicationItemService;

    @Autowired
    private SystemUserService systemUserService;

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.listeners.PublicationStateChangeListener#onStateChange(java.lang.String,
     *      com.hashcode.iwmi.publications.domain.PublicationItem)
     */
    @Override
    public void onStateChange(String currentUser, PublicationItem publicationItem) throws EPublicationException
    {
        log.debug("Request for Amendment State changed, [ User : {}, Publication Item id : {}] ", currentUser,
            publicationItem.getId());
        if (publicationItem.getStatus() == NEED_AN_AMENDMENT)
        {
            publicationItem.setStatus(NEED_AN_AMENDMENT);
            publicationItemService.statusUpdate(publicationItem);
        }

        Map<MessageTemplate.SubjectTag, String> subjectMap = new HashMap<MessageTemplate.SubjectTag, String>();
        subjectMap.put(SubjectTag.SUBJECT_JOB_ID, publicationItem.getJobNumber());

        Map<MessageTemplate.BodyTag, Object> bodyMap = new HashMap<MessageTemplate.BodyTag, Object>();
        bodyMap.put(BodyTag.BODY_JOB_ID, publicationItem.getJobNumber());
        bodyMap.put(BodyTag.BODY_LINK_PARAM_1, linkURL + "/approval/page?id=" + publicationItem.getId());
        bodyMap.put(BodyTag.BODY_CONTENT_SET_1, publicationItem.getApproverComments().stream().findFirst().get().getComment());
        bodyMap.put(BodyTag.DATE, LocalDate.now(ZoneId.systemDefault()));
        bodyMap.put(BodyTag.DATE, LocalDate.now(ZoneId.systemDefault()));
        bodyMap.put(BodyTag.DATETIME, LocalDateTime.now(ZoneId.systemDefault()));

        SystemUser user = systemUserService.findUser(publicationItem.getCreatedBy());
        SystemUser officer = systemUserService.findUser(publicationItem.getRequesterName());
        bodyMap.put(BodyTag.BODY_TO_NAME, officer.getFirstName());
        bodyMap.put(BodyTag.BODY_CONTENT_SET_1, user.getFirstName() + " " + user.getLastName());
        bodyMap.put(BodyTag.BODY_CONTENT_SET_2, publicationItem.getApproverComments().stream().findFirst().get()
            .getComment());
        bodyMap.put(BodyTag.DATE, LocalDate.now(ZoneId.systemDefault()));
        bodyMap.put(BodyTag.DATETIME, LocalDateTime.now(ZoneId.systemDefault()));
        sendEmailAndNotifyOnDashboard(publicationItem, MessageTemplate.Type.SUPERVISOR_REQUEST_AMENDMENT_TO_OFFICER,
            subjectMap, bodyMap, officer);

        auditService.addAudit(
            currentUser,
            String.format("Supervisor Request for Amendment before Approve, Publication Item Id : %d",
                publicationItem.getId()));

        log.info("Request for Amendment changed done, [ User : {}, Publication Item id : {}] ", currentUser,
            publicationItem.getId());
    }

}

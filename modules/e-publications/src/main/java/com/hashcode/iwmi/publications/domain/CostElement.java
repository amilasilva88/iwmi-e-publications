/*
 * FILENAME
 *     CostElement.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Abstract Cost Center class for each cost item involve in publication costing
 * </p>
 *
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 **/
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class CostElement extends BaseModel
{
    private static final long serialVersionUID = 3435472948844500126L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    protected Long id;

    @Column(name = "no_of_copies")
    private int noOfCopies;

    @Column(name = "cost_per_unit")
    private double costPerUnit;

    @Column(name = "other_costs")
    private double otherCosts;

    @Column(name = "sub_total")
    private double subTotal;

    /**
     * <p>
     * Cost element type.
     * </p>
     */
    public abstract String getType();

    /**
     * <p>
     * Getter for id.
     * </p>
     * 
     * @return the id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * <p>
     * Getter for noOfCopies.
     * </p>
     * 
     * @return the noOfCopies
     */
    public int getNoOfCopies()
    {
        return noOfCopies;
    }

    /**
     * <p>
     * Setting value for noOfCopies.
     * </p>
     * 
     * @param noOfCopies
     *            the noOfCopies to set
     */
    public void setNoOfCopies(int noOfCopies)
    {
        this.noOfCopies = noOfCopies;
    }

    /**
     * <p>
     * Getter for costPerUnit.
     * </p>
     * 
     * @return the costPerUnit
     */
    public double getCostPerUnit()
    {
        return costPerUnit;
    }

    /**
     * <p>
     * Setting value for costPerUnit.
     * </p>
     * 
     * @param costPerUnit
     *            the costPerUnit to set
     */
    public void setCostPerUnit(double costPerUnit)
    {
        this.costPerUnit = costPerUnit;
    }

    /**
     * <p>
     * Getter for otherCosts.
     * </p>
     * 
     * @return the otherCosts
     */
    public double getOtherCosts()
    {
        return otherCosts;
    }

    /**
     * <p>
     * Setting value for otherCosts.
     * </p>
     * 
     * @param otherCosts
     *            the otherCosts to set
     */
    public void setOtherCosts(double otherCosts)
    {
        this.otherCosts = otherCosts;
    }

    /**
     * <p>
     * Getter for subTotal.
     * </p>
     * 
     * @return the subTotal
     */
    public double getSubTotal()
    {
        return subTotal;
    }

    /**
     * <p>
     * Setting value for subTotal.
     * </p>
     * 
     * @param subTotal
     *            the subTotal to set
     */
    public void setSubTotal(double subTotal)
    {
        this.subTotal = subTotal;
    }

    /**
     * {@inheritDoc}
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return String.format("CostElements [noOfCopies=%s, costPerUnit=%s, otherCosts=%s, subTotal=%s]", noOfCopies,
            costPerUnit, otherCosts, subTotal);
    }

}

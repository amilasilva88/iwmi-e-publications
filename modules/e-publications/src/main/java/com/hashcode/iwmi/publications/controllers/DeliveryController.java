/*
 * FILENAME
 *     CostEstimationController.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.controllers;

import static com.hashcode.iwmi.publications.controllers.InitiationController.ACTION_NEXT;
import static com.hashcode.iwmi.publications.domain.PublicationStatus.COMPLETED;
import static com.hashcode.iwmi.publications.domain.PublicationStatus.DELETED;
import static com.hashcode.iwmi.publications.domain.PublicationStatus.REJECTED;
import static com.hashcode.iwmi.publications.util.ApplicationUtils.addUserToModel;
import static com.hashcode.iwmi.publications.util.ApplicationUtils.createResponse;
import static com.hashcode.iwmi.publications.util.ApplicationUtils.getResponceDataMap;
import static com.hashcode.iwmi.publications.util.ApplicationUtils.setItemStatus;

import java.time.LocalDate;
import java.util.Map;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hashcode.iwmi.publications.domain.PublicationItem;
import com.hashcode.iwmi.publications.domain.SystemUser;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;
import com.hashcode.iwmi.publications.listeners.PublicationStateChangeListener;
import com.hashcode.iwmi.publications.services.PublicationItemService;
import com.hashcode.iwmi.publications.services.SystemUserService;
import com.hashcode.iwmi.publications.util.ApplicationUtils;
import com.hashcode.iwmi.publications.util.ResponseStatus;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Initiation Controller for Cost estimation.
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 **/
@Controller
@RequestMapping("/delivery")
public class DeliveryController
{
    private static final Logger log = LoggerFactory.getLogger(DeliveryController.class);

    @Autowired
    private PublicationItemService publicationItemService;
    
    @Autowired
    private SystemUserService userService;

    @Autowired
    @Qualifier(value = "deliveredStateChange")
    private PublicationStateChangeListener stateChangeListener;

    @RequestMapping("/page")
    public String pageLoad(@RequestParam("id") long id, Model model)
    {
        log.info("Request came to load the delivery page....");
        addUserToModel(model);
        model.addAttribute("id", id);
        return "job-delivering";
    }

    @RequestMapping(value = "/load/{id}", method = {
        RequestMethod.GET
    })
    @ResponseBody
    @Transactional
    public String loadData(@PathVariable("id") long id) throws EPublicationException
    {
        log.debug("Request came to load delivery data for publication [ Id :{}]", id);

        PublicationItem item = publicationItemService.findById(id);
        log.debug("Found Publication Item, Id :{}", item.getId());
        
        String username = ApplicationUtils.getCurrentUser().getUsername();
        SystemUser currentUser = userService.findUser(username);

        JSONObject data = new JSONObject();
        if (item != null)
        {
            data.accumulate("id", item.getId());
            String deliveryDate = null;
            if (null != item.getDeliveredDate())
            {
                deliveryDate = item.getDeliveredDate().toString();
            }
            data.accumulate("deliveredDate", deliveryDate);
            data.accumulate("deliveredComment", item.getDeliveredComment());
            setItemStatus(item, data);
        }
        
        boolean hasAccessPermission = checkAccessPermission(currentUser, item);
        log.debug("delivery data for publications, Permission, [ User :{}, can edit :{}]",
            currentUser.getUsername(), hasAccessPermission);
        data.put("canEdit", hasAccessPermission);
        log.debug("delivery data populated, [ JSON Data :{}]", data.toString());
        return data.toString();
    }
    
    public boolean checkAccessPermission(SystemUser currentUser, PublicationItem item)
    {
        if (item.getStatus() == REJECTED || item.getStatus() == DELETED || item.getStatus() == COMPLETED)
            return false;
        else if (currentUser.isSupervisor() || currentUser.isOfficer())
            return true;
        else
            return false;
    }

    @RequestMapping(value = "/saveData", method = {
        RequestMethod.POST, RequestMethod.PUT
    })
    @ResponseBody
    public String saveOrUpdateData(@RequestBody String data)
    {
        try
        {
            log.debug("Request came to save/update delivery data for publications");

            String username = ApplicationUtils.getCurrentUser().getUsername();

            JSONObject jsonData = new JSONObject(data);
            String action = jsonData.getString("action");
            long id = jsonData.getLong("id");
            PublicationItem item = publicationItemService.findById(id);
            item.setDeliveredComment(jsonData.getString("deliveredComment"));
            item.setDeliveredDate(LocalDate.parse(jsonData.getString("deliveredDate")));

            item.setLastModifiedBy(username);
            publicationItemService.update(item);
            
            if (action.equals(ACTION_NEXT))
            {
                stateChangeListener.onStateChange(username, item);
            }

            Map<String, Object> params = getResponceDataMap();
            params.put("id", item.getId());
            params.put("item_status", item.getStatus());
            params.put("data", "Publication delivering data saved successfully.");

            JSONObject jsonReponse = createResponse(ResponseStatus.SUCCESS, params);
            log.debug("JSON Response for load divisions, Response : {}", jsonReponse);
            return jsonReponse.toString();
        }
        catch (Exception e)
        {
            log.error("Error occurred in saving/update publication,", e);
            Map<String, Object> params = getResponceDataMap();
            params.put("error", "Error encountered in saving/update Publication");
            return createResponse(ResponseStatus.FAILED, params).toString();
        }
    }
}

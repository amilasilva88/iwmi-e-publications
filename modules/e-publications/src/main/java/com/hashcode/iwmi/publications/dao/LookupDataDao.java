/*
 * FILENAME
 *     LookupDataDao.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.dao;

import java.util.List;

import com.hashcode.iwmi.publications.domain.LookupData;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Data Access Object for Lookup data entity.
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 **/
public interface LookupDataDao
{
    /**
     * 
     * <p>
     * Persists the newInstance entity into database.
     * </p>
     * 
     * @param newInstance
     *            Entity to be saved
     * 
     */
    public <E extends LookupData> void create(E newInstance);

    /**
     * 
     * <p>
     * Updates the given inTransientObject.
     * </p>
     * 
     * @param inTransientObject
     *            Entity to be updated
     * @return the updated inTransientObject
     * 
     */
    public <E extends LookupData> E update(final E inTransientObject);

    /**
     * Removes an entity from persistent storage in the database.
     * 
     * @param persistentObject
     *            the object to be deleted
     */
    public <E extends LookupData> void delete(E persistentObject);

    /**
     * Retrieves an entity that was previously persisted to the database using the indicated id as primary key.
     * 
     * @param id
     *            the entity id
     * @return the entity with the given id
     * 
     */
    public <E extends LookupData> E findById(Class<E> clazz, Long id);

    /**
     * Retrieves an entity that was previously persisted to the database using the parent key.
     * 
     * @param id
     *            the entity id
     * @return the entity with the given id
     * 
     */
    public <E extends LookupData> List<E> findByParentId(Class<E> clazz, Long parentId);

    /**
     * 
     * <p>
     * Returns all entities from type <code>E</code> from the database.
     * </p>
     * 
     * @return A list of found entities
     * 
     */
    public <E extends LookupData> List<E> findAll(Class<E> clazz);

    /**
     * Return the total number of persisted entities of type <code>E</code>.
     * 
     * @return the total number of persisted entities of type <code>E</code>
     */
    <E extends LookupData> long countAll(Class<E> clazz);

}

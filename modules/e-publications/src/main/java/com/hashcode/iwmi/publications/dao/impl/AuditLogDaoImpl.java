/*
 * FILENAME
 *     AuditLogDaoImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.dao.impl;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.hashcode.iwmi.publications.dao.AuditLogDao;
import com.hashcode.iwmi.publications.domain.AuditLog;
import com.hashcode.iwmi.publications.util.ApplicationUtils;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * {@link AuditLogDao} implementation.
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 **/
@Repository
public class AuditLogDaoImpl extends GenericDaoImpl<AuditLog, Long> implements AuditLogDao
{
    private static final Logger log = LoggerFactory.getLogger(AuditLogDaoImpl.class);

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.dao.AuditLogDao#findLast30DaysActions(java.lang.String)
     */
    @Override
    public List<AuditLog> findLastNDaysActions(final String user, LocalDateTime fromDate, LocalDateTime toDate)
    {
        log.debug("findLastNDaysActions : [ User : {}, From :{}, To:{}]", user, fromDate, toDate);
        String username = (ApplicationUtils.isNotNullOrEmpty(user)) ? user : "%";

        String hqlString =
            "FROM " + AuditLog.class.getName() + " a WHERE a.username like :username AND "
                + " (a.createdDate >= :fromDate AND a.createdDate < :toDate) ORDER BY a.createdDate DESC ";

        TypedQuery<AuditLog> query = entityManager.createQuery(hqlString, AuditLog.class);
        query.setParameter("username", username);
        query.setParameter("fromDate", fromDate);
        query.setParameter("toDate", toDate);

        return query.getResultList();
    }

}

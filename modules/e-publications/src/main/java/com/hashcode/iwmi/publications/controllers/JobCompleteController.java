/*
 * FILENAME
 *     JobCompleteController.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.controllers;

import static com.hashcode.iwmi.publications.domain.PublicationStatus.COMPLETED;
import static com.hashcode.iwmi.publications.domain.PublicationStatus.COSTING;
import static com.hashcode.iwmi.publications.domain.PublicationStatus.DELETED;
import static com.hashcode.iwmi.publications.domain.PublicationStatus.REJECTED;
import static com.hashcode.iwmi.publications.util.ApplicationUtils.addUserToModel;
import static com.hashcode.iwmi.publications.util.ApplicationUtils.createResponse;
import static com.hashcode.iwmi.publications.util.ApplicationUtils.getResponceDataMap;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hashcode.iwmi.publications.domain.PublicationItem;
import com.hashcode.iwmi.publications.domain.PublicationStatus;
import com.hashcode.iwmi.publications.domain.SystemUser;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;
import com.hashcode.iwmi.publications.listeners.PublicationStateChangeListener;
import com.hashcode.iwmi.publications.services.PublicationItemService;
import com.hashcode.iwmi.publications.services.SystemUserService;
import com.hashcode.iwmi.publications.util.ApplicationUtils;
import com.hashcode.iwmi.publications.util.ResponseStatus;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 *  Job Complete Controller
 * <p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
@Controller
@RequestMapping("/jobComplete")
public class JobCompleteController
{
    private static final Logger log = LoggerFactory.getLogger(JobCompleteController.class);

    @Autowired
    private PublicationItemService publicationItemService;

    @Autowired
    @Qualifier(value = "completedStateChange")
    private PublicationStateChangeListener stateChangeListener;
    
    @Autowired
    private SystemUserService userService;
    
    @RequestMapping("/page")
    public String pageLoad(@RequestParam("id") long id, Model model)
    {
        log.info("Request came to load the status page....");
        addUserToModel(model);
        model.addAttribute("id", id);
        return "job-completion";
    }

    @RequestMapping(value = "/load/{id}", method = {
        RequestMethod.GET
    })
    @ResponseBody
    @Transactional
    public String loadData(@PathVariable("id") long id) throws EPublicationException
    {

        log.debug("Request came to load complete page data for publication [ Id :{}]", id);

        List<SystemUser> users = userService.findAll();
        log.debug("load availbale users, Total users : {}", users.size());

        PublicationItem item = publicationItemService.findById(id);
        PublicationStatus status = item.getStatus();
        
        String username = ApplicationUtils.getCurrentUser().getUsername();
        SystemUser currentUser = userService.findUser(username);
        
        JSONObject response = new JSONObject();
        response.accumulate("id", item.getId());
        response.accumulate("can_complete", checkAccessPermissionToModify(currentUser, item));
        response.accumulate("complete_done", status.equals(COMPLETED));
        response.accumulate("item_status", COMPLETED);
        
        JSONArray userArr = new JSONArray();
        users.stream().forEach(u -> {
            JSONObject user = new JSONObject();
            user.accumulate("email", u.getUsername());
            user.accumulate("display", u.getFirstName() + " " + u.getLastName());
            userArr.put(user);
        });
        response.accumulate("users", userArr);
     
        log.debug("Complete page data loaded for publication [ Id :{}]", id);
        return response.toString();
    }
    
    public boolean checkAccessPermissionToModify(SystemUser currentUser, PublicationItem item)
    {
        if (item.getStatus() == REJECTED || item.getStatus() == DELETED || item.getStatus() == COMPLETED)
        {
            return false;
        }
        else if (currentUser.isOfficer() && item.getStatus().equals(COSTING))
        {
            return true;
        }
        else
            return false;
    }

    @RequestMapping(value = "/confirm", method = {
        RequestMethod.POST, RequestMethod.PUT
    })
    @ResponseBody
    public String completeJob(@RequestBody String data)
    {
        try
        {
            log.debug("Request came to complete job for publications");

            String username = ApplicationUtils.getCurrentUser().getUsername();

            JSONObject jsonData = new JSONObject(data);
            long id = jsonData.getLong("id");
            boolean hasUsersToNotify = jsonData.has("notify_users");
            JSONArray notifyUsersArry = jsonData.getJSONArray("notify_users");
            
            Set<String> userEmails = new TreeSet<String>();
            if (hasUsersToNotify)
            {
                for (int i = 0; i < notifyUsersArry.length(); i++)
                {
                    userEmails.add(notifyUsersArry.getString(i));
                }
            }

            PublicationItem item = publicationItemService.findById(id);
          
            stateChangeListener.onStateChange(username, userEmails, item);
            log.info("Publication Item sucessfully completed, [ Item Id : {}]", item.getId());

            Map<String, Object> params = getResponceDataMap();
            params.put("id", item.getId());
            params.put("item_status", item.getStatus());
            params.put("data", "Publication status data saved successfully.");

            JSONObject jsonReponse = createResponse(ResponseStatus.SUCCESS, params);
            log.debug("JSON Response after completing Job, Response : {}", jsonReponse);
            return jsonReponse.toString();
        }
        catch (Exception e)
        {
            log.error("Error occurred in saving/update publication,", e);
            Map<String, Object> params = getResponceDataMap();
            params.put("error", "Error encountered in saving/update Publication");
            return createResponse(ResponseStatus.FAILED, params).toString();
        }
    }

}

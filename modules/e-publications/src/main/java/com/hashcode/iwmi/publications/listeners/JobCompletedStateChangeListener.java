/*
 * FILENAME
 *     JobCompletedStateChangeListener.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.listeners;

import static com.hashcode.iwmi.publications.domain.PublicationStatus.COMPLETED;
import static com.hashcode.iwmi.publications.domain.PublicationStatus.COSTING;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.hashcode.iwmi.publications.domain.MessageTemplate;
import com.hashcode.iwmi.publications.domain.MessageTemplate.BodyTag;
import com.hashcode.iwmi.publications.domain.MessageTemplate.SubjectTag;
import com.hashcode.iwmi.publications.domain.PublicationItem;
import com.hashcode.iwmi.publications.domain.SystemUser;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;
import com.hashcode.iwmi.publications.services.PublicationItemService;
import com.hashcode.iwmi.publications.services.SystemUserService;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Job Completed State change listener.
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
@Service
@Qualifier(value = "completedStateChange")
@Transactional
public class JobCompletedStateChangeListener extends AbstractPublicationStateChangeListener
{
    private static final Logger log = LoggerFactory.getLogger(JobCompletedStateChangeListener.class);

    @Autowired
    private PublicationItemService publicationItemService;

    @Autowired
    private SystemUserService systemUserService;

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.listeners.PublicationStateChangeListener#onStateChange(java.lang.String,
     *      com.hashcode.iwmi.publications.domain.PublicationItem)
     */
    @Override
    public void onStateChange(String currentUser, PublicationItem publicationItem) throws EPublicationException
    {
        log.debug("Job Completed State changed, [ User : {}, Publication Item id : {}] ", currentUser,
            publicationItem.getId());

        if (publicationItem.getStatus() == COSTING)
        {
            publicationItem.setStatus(COMPLETED);
            publicationItemService.statusUpdate(publicationItem);
        }

        Map<MessageTemplate.SubjectTag, String> subjectMap = new HashMap<MessageTemplate.SubjectTag, String>();
        subjectMap.put(SubjectTag.SUBJECT_JOB_ID, publicationItem.getJobNumber());
        subjectMap.put(SubjectTag.SUBJECT_TITLE, publicationItem.getProjectCode());
        subjectMap.put(SubjectTag.SUBJECT_OTHER_TEXT_1, publicationItem.getTypeOfWork());

        SystemUser user = systemUserService.findUser(publicationItem.getCreatedBy());

        Map<MessageTemplate.BodyTag, Object> bodyMap = new HashMap<MessageTemplate.BodyTag, Object>();
        bodyMap.put(BodyTag.BODY_JOB_ID, publicationItem.getJobNumber());
        bodyMap.put(BodyTag.BODY_TO_NAME, user.getFirstName());
        bodyMap.put(BodyTag.BODY_LINK_PARAM_1, linkURL + "/initiation/page?id=" + publicationItem.getId());
        bodyMap.put(BodyTag.DATE, LocalDate.now(ZoneId.systemDefault()));
        bodyMap.put(BodyTag.DATETIME, LocalDateTime.now(ZoneId.systemDefault()));

        sendEmailAndNotifyOnDashboard(publicationItem, MessageTemplate.Type.AFTER_JOB_COMPLETE_USER, subjectMap,
            bodyMap, user);

        auditService.addAudit(currentUser,
            String.format("Publication Job Completed, Publication Item Id : %d", publicationItem.getId()));

        log.info("Job Completed State changed done, [ User : {}, Publication Item id : {}] ", currentUser,
            publicationItem.getId());
    }

    @Override
    public void onStateChange(String currentUser, Set<String> notifyUsers, PublicationItem publicationItem)
        throws EPublicationException
    {
        log.debug("Job Completed State changed, [ User : {}, Publication Item id : {}] ", currentUser,
            publicationItem.getId());

        if (publicationItem.getStatus() == COSTING)
        {
            publicationItem.setStatus(COMPLETED);
            publicationItemService.statusUpdate(publicationItem);
        }

        Set<SystemUser> systemUsers = new TreeSet<SystemUser>();
        SystemUser user = systemUserService.findUser(publicationItem.getCreatedBy());
        systemUsers.add(user);
        SystemUser supervisor = systemUserService.findUser(publicationItem.getSupervisorName());
        systemUsers.add(supervisor);

        notifyUsers.stream().forEach(u -> {
            try
            {
                SystemUser sysUser = systemUserService.findUser(u);
                systemUsers.add(sysUser);
            }
            catch (Exception e)
            {
                log.error("Error occurred in fetching system user, ", e);
            }
        });

        systemUsers.stream().forEach(
            u -> {
                Map<MessageTemplate.SubjectTag, String> subjectMap = new HashMap<MessageTemplate.SubjectTag, String>();
                subjectMap.put(SubjectTag.SUBJECT_JOB_ID, publicationItem.getJobNumber());
                subjectMap.put(SubjectTag.SUBJECT_TITLE, publicationItem.getProjectCode());
                subjectMap.put(SubjectTag.SUBJECT_OTHER_TEXT_1, publicationItem.getTypeOfWork());

                Map<MessageTemplate.BodyTag, Object> bodyMap = new HashMap<MessageTemplate.BodyTag, Object>();
                bodyMap.put(BodyTag.BODY_JOB_ID, publicationItem.getJobNumber());
                bodyMap.put(BodyTag.BODY_TO_NAME, u.getFirstName());
                bodyMap.put(BodyTag.BODY_LINK_PARAM_1, linkURL + "/initiation/load/" + publicationItem.getId());
                bodyMap.put(BodyTag.DATE, LocalDate.now(ZoneId.systemDefault()));
                bodyMap.put(BodyTag.DATETIME, LocalDateTime.now(ZoneId.systemDefault()));

                try
                {
                    sendEmailAndNotifyOnDashboard(publicationItem, MessageTemplate.Type.AFTER_JOB_COMPLETE_USER,
                        subjectMap, bodyMap, u);
                }
                catch (Exception e)
                {
                    log.error("Error occurred in sending Email and Notify on Dashboard to user, ", e);
                }
            });

        auditService.addAudit(currentUser,
            String.format("Publication Job Completed, Publication Item Id : %d", publicationItem.getId()));

        log.info("Job Completed State changed done, [ User : {}, Publication Item id : {}] ", currentUser,
            publicationItem.getId());
    }

}

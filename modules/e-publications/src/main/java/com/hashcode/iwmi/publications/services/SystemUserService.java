/*
 * FILENAME
 *     SystemUserService.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.services;

import java.util.List;

import com.hashcode.iwmi.publications.domain.SystemUser;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * System User Service.
 * </p>
 *
 * @author Manuja Jayamanna
 * @email manuja@hashcodesys.com
 **/
public interface SystemUserService
{
    /**
     * <p>
     * Create new user.
     * </p>
     *
     * @param systemUser
     * @throws EPublicationException
     *
     */
    void createUser(SystemUser systemUser) throws EPublicationException;

    /**
     * <p>
     * Delete user.
     * </p>
     *
     * @param systemUser
     * @throws EPublicationException
     */
    void deleteUser(SystemUser systemUser) throws EPublicationException;

    /**
     * <p>
     * Update user.
     * </p>
     *
     * @param systemUser
     * @throws EPublicationException
     *
     */
    void updateUser(SystemUser systemUser) throws EPublicationException;

    /**
     * <p>
     * Find user by username
     * </p>
     *
     * @param username
     *            username
     * 
     * @return {@link SystemUser} instance
     *
     */
    SystemUser findUser(String username) throws EPublicationException;

    /**
     * <p>
     * Find user by email
     * </p>
     *
     * @param email
     *            email
     * 
     * @return {@link SystemUser} instance
     *
     */
    SystemUser findUserByEmail(String email) throws EPublicationException;

    /**
     * <p>
     * Find user by userId
     * </p>
     *
     * @param userId
     *            userId
     * 
     * @return {@link SystemUser} instance
     *
     */
    SystemUser findById(Long userId) throws EPublicationException;

    /**
     * <p>
     * Find all users.
     * </p>
     *
     * @return list of user instances
     * @throws EPublicationException
     */
    List<SystemUser> findAll() throws EPublicationException;
}

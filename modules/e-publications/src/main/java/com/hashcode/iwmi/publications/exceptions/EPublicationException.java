/*
 * FILENAME
 *     EPublicationDataException.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.exceptions;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Data Layer exceptions handled by this exception type
 * </p>
 * 
 * @author Amila Silva
 * @email amila@hashcodesys.com
 **/
public class EPublicationException extends Exception
{

    private static final long serialVersionUID = 4451001997054167548L;

    public EPublicationException()
    {
        super();
        // TODO Auto-generated constructor stub
    }

    public EPublicationException(String message, Throwable throwable)
    {
        super(message, throwable);
    }

    public EPublicationException(String message)
    {
        super(message);
    }

    /**
     * <p>
     * Add one-sentence summarising the constructor inputs here; this sentence should only contain one full-stop.
     * </p>
     * <p>
     * Add detailed HTML description of constructor here, including the following, where relevant:
     * <ul>
     * <li>Description of what the constuctor does and how it is done.</li>
     * <li>Details on which error conditions may occur.</li>
     * </ul>
     * </p>
     *
     * @param arg0
     **/
    public EPublicationException(Throwable throwable)
    {
        super(throwable);
    }
}

/*
 * FILENAME
 *     CostEstimationController.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.controllers;

import static com.hashcode.iwmi.publications.domain.PublicationStatus.COMPLETED;
import static com.hashcode.iwmi.publications.domain.PublicationStatus.DELETED;
import static com.hashcode.iwmi.publications.domain.PublicationStatus.REJECTED;
import static com.hashcode.iwmi.publications.util.ApplicationUtils.addUserToModel;
import static com.hashcode.iwmi.publications.util.ApplicationUtils.createResponse;
import static com.hashcode.iwmi.publications.util.ApplicationUtils.getResponceDataMap;
import static com.hashcode.iwmi.publications.util.ApplicationUtils.getString;

import java.io.ByteArrayOutputStream;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hashcode.iwmi.publications.domain.CostElement;
import com.hashcode.iwmi.publications.domain.DesigningCost;
import com.hashcode.iwmi.publications.domain.EditingCost;
import com.hashcode.iwmi.publications.domain.GraphicsCost;
import com.hashcode.iwmi.publications.domain.PrintingCost;
import com.hashcode.iwmi.publications.domain.ProofReadingCost;
import com.hashcode.iwmi.publications.domain.PublicationItem;
import com.hashcode.iwmi.publications.domain.RePrintingCost;
import com.hashcode.iwmi.publications.domain.SystemUser;
import com.hashcode.iwmi.publications.domain.TranslatingCost;
import com.hashcode.iwmi.publications.domain.TypeSettingCost;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;
import com.hashcode.iwmi.publications.exports.JobStatusReportPDF;
import com.hashcode.iwmi.publications.listeners.PublicationStateChangeListener;
import com.hashcode.iwmi.publications.services.PublicationItemService;
import com.hashcode.iwmi.publications.services.SystemUserService;
import com.hashcode.iwmi.publications.util.ApplicationUtils;
import com.hashcode.iwmi.publications.util.ResponseStatus;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Initiation Controller for Cost estimation.
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 **/
@Controller
@RequestMapping("/status")
public class StatusController
{
    private static final Logger log = LoggerFactory.getLogger(StatusController.class);

    @Autowired
    private PublicationItemService publicationItemService;

    @Autowired
    @Qualifier(value = "completedStateChange")
    private PublicationStateChangeListener stateChangeListener;

    @Autowired
    private JobStatusReportPDF jobStatusReportGenerator;
    
    @Autowired
    private SystemUserService userService;

    @RequestMapping("/page")
    public String pageLoad(@RequestParam("id") long id, Model model)
    {
        log.info("Request came to load the status page....");
        addUserToModel(model);
        model.addAttribute("id", id);
        return "job-status";
    }

    @RequestMapping(value = "/load/{id}", method = {
        RequestMethod.GET
    })
    @ResponseBody
    @Transactional
    public String loadData(@PathVariable("id") long id) throws EPublicationException
    {
        log.debug("Request came to load status data for publication [ Id :{}]", id);

        JSONObject data = null;
        PublicationItem item = publicationItemService.findById(id);
        log.debug("Found Publication Item, Id :{}", item.getId());
        
        String username = ApplicationUtils.getCurrentUser().getUsername();
        SystemUser currentUser = userService.findUser(username);

        String statusJsn = item.getPublicationStatusJson();
        data = (statusJsn == null) ? new JSONObject() : new JSONObject(statusJsn);

        JSONObject status = new JSONObject();
        status.put("id", item.getId());
        status.put("relatedProgram", getString(data, "relatedProgram"));
        status.put("typeOfJob", item.getType());
        status.put("series", item.getSeries());
        status.put("title", item.getTitle());
        status.put("author", getString(data, "author"));
        status.put("manuScriptReceiveDate", getString(data, "manuScriptReceiveDate"));
        status.put("noOfManuScriptPages", getString(data, "noOfManuScriptPages"));
        status.put("isbn_issn", getString(data, "isbn_issn"));
        status.put("requester", item.getRequesterName());
        status.put("supervisour", item.getSupervisorName());
        status.put("projectCode", item.getProjectCode());
        status.put("jobRequestDate", item.getCreatedDate());
        status.put("jobApprovedDate", item.getApprovedDate());
        status.put("processStartDate", getString(data, "processStartDate"));
        status.put("processEndDate", getString(data, "processEndDate"));
        status.put("estimatedCompletionDate", getString(data, "estimatedCompletionDate"));
        status.put("dateEnteredToSystem", item.getCompletionDate());
        status.put("estimatedCost", item.getTotalEstimatedCost());
        status.put("item_status", item.getStatus());

        for (CostElement costElement : item.getActualCostings())
        {
            switch (costElement.getType())
            {
                case "TYPE_SETTING_COST":
                    JSONObject typeSettingCostJsn = populateTypeSettingCost((TypeSettingCost) costElement, data);
                    status.put("TYPE_SETTING_COST", typeSettingCostJsn);
                    break;
                case "EDITING_COST":
                    JSONObject editCostJsn = populateEditingCost((EditingCost) costElement, data);
                    status.put("EDITING_COST", editCostJsn);
                    break;
                case "DESIGNING_COST":
                    JSONObject dsngCostJsn = populateDesingingCost((DesigningCost) costElement, data);
                    status.put("DESIGNING_COST", dsngCostJsn);
                    break;
                case "GRAPHICS_COST":
                    JSONObject grphCostJsn = populateGraphicCost((GraphicsCost) costElement, data);
                    status.put("GRAPHICS_COST", grphCostJsn);
                    break;
                case "PROOF_READ_COST":
                    JSONObject pReadCostJsn = populateProofReadCost((ProofReadingCost) costElement, data);
                    status.put("PROOF_READ_COST", pReadCostJsn);
                    break;
                case "TRANSLATING_COST":
                    JSONObject translatingCostJsn = populateTranslatingCost((TranslatingCost) costElement, data);
                    status.put("TRANSLATING_COST", translatingCostJsn);
                    break;
                case "PRINTING_COST":
                    JSONObject printingCostJsn = populatePrintingCost((PrintingCost) costElement, data);
                    status.put("PRINTING_COST", printingCostJsn);
                    break;
                case "REPRINTING_COST":
                    JSONObject rePrintingCostJsn = populateRePrintingCost((RePrintingCost) costElement, data);
                    status.put("REPRINTING_COST", rePrintingCostJsn);
                    break;
                default:
                    break;
            }
        }
        
        boolean hasAccessPermission = checkAccessPermission(currentUser, item);
        log.debug("Intital data for publications, Permission, [ User :{}, can edit :{}]",
            currentUser.getUsername(), hasAccessPermission);
        status.put("canEdit", hasAccessPermission);
        
        return status.toString();
    }
    
    public boolean checkAccessPermission(SystemUser currentUser, PublicationItem item)
    {
        if (item.getStatus() == REJECTED || item.getStatus() == DELETED || item.getStatus() == COMPLETED)
        {
            return false;
        }
        else if (currentUser.isOfficer())
        {
            return true;
        }
        else
            return false;
    }

    private JSONObject populateRePrintingCost(RePrintingCost cost, JSONObject data)
    {
        JSONObject jData = data.has("REPRINTING_COST") ? data.getJSONObject("REPRINTING_COST") : new JSONObject();
        jData.put("printer", getString(jData, "printer"));
        jData.put("quotation_date", getString(jData, "quotation_date"));
        jData.put("quotation_received_date", getString(jData, "quotation_received_date"));
        jData.put("sent_date", getString(jData, "sent_date"));
        jData.put("rate", cost.getCostPerUnit());
        jData.put("no_of_pages", cost.getNoOfCopies());
        jData.put("order_num", getString(jData, "order_num"));
        jData.put("total_cost", cost.getSubTotal());
        jData.put("est_end_date", getString(jData, "est_end_date"));
        jData.put("progress", getString(jData, "progress"));
        jData.put("machine_proof_expected_date", getString(jData, "machine_proof_expected_date"));
        jData.put("machine_proof_received_date", getString(jData, "machine_proof_received_date"));
        jData.put("machine_proof_approved_date", getString(jData, "machine_proof_approved_date"));
        jData.put("delay", getString(jData, "delay"));
        jData.put("actual_delivery", getString(jData, "actual_delivery"));
        return jData;
    }

    private JSONObject populatePrintingCost(PrintingCost cost, JSONObject data)
    {
        JSONObject jData = data.has("PRINTING_COST") ? data.getJSONObject("PRINTING_COST") : new JSONObject();
        jData.put("cost_type", "PRINTING_COST");
        jData.put("printer", getString(jData, "printer"));
        jData.put("quotation_date", getString(jData, "quotation_date"));
        jData.put("quotation_received_date", getString(jData, "quotation_received_date"));
        jData.put("sent_date", getString(jData, "sent_date"));
        jData.put("cost_per_unit", cost.getCostPerUnit());
        jData.put("no_of_pages", cost.getNoOfCopies());
        jData.put("order_num", getString(jData, "order_num"));
        jData.put("total_cost", cost.getSubTotal());
        jData.put("completion_date", getString(jData, "completion_date"));
        jData.put("progress", getString(jData, "progress"));
        jData.put("machine_proof_expected_date", getString(jData, "machine_proof_expected_date"));
        jData.put("machine_proof_received_date", getString(jData, "machine_proof_received_date"));
        jData.put("machine_proof_approved_date", getString(jData, "machine_proof_approved_date"));
        jData.put("delay", getString(jData, "delay"));
        jData.put("actual_delivery", getString(jData, "actual_delivery"));
        return jData;
    }

    private JSONObject populateTranslatingCost(TranslatingCost cost, JSONObject data)
    {
        JSONObject jData = data.has("TRANSLATING_COST") ? data.getJSONObject("TRANSLATING_COST") : new JSONObject();
        jData.put("cost_type", "TRANSLATING_COST");
        jData.put("contract_number", getString(jData, "contract_number"));
        jData.put("translator", getString(jData, "translator"));
        jData.put("cost_per_unit", cost.getCostPerUnit());
        jData.put("no_of_pages", cost.getNoOfCopies());
        jData.put("total_cost", cost.getSubTotal());
        jData.put("sent_date", getString(jData, "sent_date"));
        jData.put("est_receiving_date", getString(jData, "est_receiving_date"));
        jData.put("receiving_date", getString(jData, "receiving_date"));
        jData.put("progress", getString(jData, "progress"));
        jData.put("comments", getString(jData, "comments"));
        return jData;
    }

    private JSONObject populateProofReadCost(ProofReadingCost cost, JSONObject data)
    {
        JSONObject jData = data.has("PROOF_READ_COST") ? data.getJSONObject("PROOF_READ_COST") : new JSONObject();
        jData.put("cost_type", "PROOF_READ_COST");
        jData.put("reader", getString(jData, "reader"));
        jData.put("started_date", getString(jData, "started_date"));
        jData.put("est_end_date", getString(jData, "est_end_date"));
        jData.put("ended_date", getString(jData, "ended_date"));
        jData.put("progress", getString(jData, "progress"));
        jData.put("final_sent_to_author_date", getString(jData, "final_sent_to_author_date"));
        jData.put("final_sent_approved_date", getString(jData, "final_sent_approved_date"));
        jData.put("pre_press_in_date", getString(jData, "pre_press_in_date"));
        jData.put("est_pre_press_out_date", getString(jData, "est_pre_press_out_date"));
        jData.put("pre_press_out_date", getString(jData, "pre_press_out_date"));
        jData.put("pre_press_response", getString(jData, "pre_press_response"));
        jData.put("pre_press_progress", getString(jData, "pre_press_progress"));
        return jData;
    }

    private JSONObject populateGraphicCost(GraphicsCost cost, JSONObject data)
    {
        JSONObject jData = data.has("GRAPHICS_COST") ? data.getJSONObject("GRAPHICS_COST") : new JSONObject();
        jData.put("designer", getString(jData, "designer"));
        jData.put("rate", cost.getCostPerUnit());
        jData.put("designing_hours", getString(jData, "designing_hours"));
        jData.put("total_cost", cost.getSubTotal());
        jData.put("trimmed_size", getString(jData, "trimmed_size"));
        jData.put("web_version_date", getString(jData, "web_version_date"));
        jData.put("started_date", getString(jData, "started_date"));
        jData.put("est_end_date", getString(jData, "est_end_date"));
        jData.put("ended_date", getString(jData, "ended_date"));
        jData.put("progress", getString(jData, "progress"));
        return jData;
    }

    private JSONObject populateDesingingCost(DesigningCost cost, JSONObject data)
    {
        JSONObject jData = data.has("DESIGNING_COST") ? data.getJSONObject("DESIGNING_COST") : new JSONObject();
        jData.put("cost_type", "DESIGNING_COST");
        jData.put("designer", getString(jData, "designer"));
        jData.put("rate", cost.getCostPerUnit());
        jData.put("designing_hours", getString(jData, "designing_hours"));
        jData.put("total_cost", cost.getSubTotal());
        jData.put("trimmed_size", getString(jData, "trimmed_size"));
        jData.put("web_version_date", getString(jData, "web_version_date"));
        jData.put("started_date", getString(jData, "started_date"));
        jData.put("designing_out_date", getString(jData, "designing_out_date"));
        jData.put("ended_date", getString(jData, "ended_date"));
        jData.put("progress", getString(jData, "progress"));
        return jData;
    }

    private JSONObject populateTypeSettingCost(TypeSettingCost cost, JSONObject data)
    {
        JSONObject jData = data.has("TYPE_SETTING_COST") ? data.getJSONObject("TYPE_SETTING_COST") : new JSONObject();
        jData.put("cost_type", "TYPE_SETTING_COST");
        jData.put("ref_number", getString(jData, "ref_number"));
        jData.put("type_setter", getString(jData, "type_setter"));
        jData.put("cost_per_unit", cost.getCostPerUnit());
        jData.put("no_of_pages", cost.getNoOfCopies());
        jData.put("total_cost", cost.getSubTotal());
        jData.put("date_sent", getString(jData, "date_sent"));
        jData.put("started_date", getString(jData, "started_date"));
        jData.put("est_end_date", getString(jData, "est_end_date"));
        jData.put("ended_date", getString(jData, "ended_date"));
        jData.put("progress", getString(jData, "progress"));
        return jData;
    }

    private JSONObject populateEditingCost(EditingCost cost, JSONObject data)
    {
        JSONObject jData = data.has("EDITING_COST") ? data.getJSONObject("EDITING_COST") : new JSONObject();
        jData.put("cost_type", "EDITING_COST");
        jData.put("ref_number", getString(jData, "ref_number"));
        jData.put("editor", getString(jData, "editor"));
        jData.put("cost_per_unit", cost.getCostPerUnit());
        jData.put("no_of_pages", cost.getNoOfCopies());
        jData.put("total_cost", cost.getSubTotal());
        jData.put("date_sent", getString(jData, "date_sent"));
        jData.put("started_date", getString(jData, "started_date"));
        jData.put("est_end_date", getString(jData, "est_end_date"));
        jData.put("ended_date", getString(jData, "ended_date"));
        jData.put("progress", getString(jData, "progress"));
        jData.put("authors_approval_date", getString(jData, "authors_approval_date"));
        return jData;
    }
    

    @RequestMapping(value = "/saveData", method = {
        RequestMethod.POST, RequestMethod.PUT
    })
    @ResponseBody
    public String saveOrUpdateData(@RequestBody String data)
    {
        try
        {
            log.debug("Request came to save/update intital data for publications");

            String username = ApplicationUtils.getCurrentUser().getUsername();

            JSONObject jsonData = new JSONObject(data);
            long id = jsonData.getLong("id");
            //            String action = jsonData.getString("action");
            JSONObject jsonStatusData = jsonData.getJSONObject("statusData");
            String statusData = jsonStatusData.toString();

            PublicationItem item = publicationItemService.findById(id);
            item.setAuthor(jsonStatusData.getString("author"));
            item.setIsbn_issn(jsonStatusData.getString("isbn_issn"));
            item.setRelatedPrograme(jsonStatusData.getString("relatedProgram"));
            item.setTitle(jsonStatusData.getString("title"));
            item.setPublicationStatusJson(statusData);
            item.setLastModifiedBy(username);
            publicationItemService.update(item);

            Map<String, Object> params = getResponceDataMap();
            params.put("id", item.getId());
            params.put("item_status", item.getStatus());
            params.put("data", "Publication status data saved successfully.");

            JSONObject jsonReponse = createResponse(ResponseStatus.SUCCESS, params);
            log.debug("JSON Response for load divisions, Response : {}", jsonReponse);
            return jsonReponse.toString();
        }
        catch (Exception e)
        {
            log.error("Error occurred in saving/update publication,", e);
            Map<String, Object> params = getResponceDataMap();
            params.put("error", "Error encountered in saving/update Publication");
            return createResponse(ResponseStatus.FAILED, params).toString();
        }
    }

    
    /**
     * <p>
     * Download a file from document repository.
     * </p>
     *
     * @param response
     * @param value
     * @return
     * @throws EPublicationException
     *
     */
    @RequestMapping(value = "/export/{jobId}", method = RequestMethod.GET)
    public void exportJobStatus(@PathVariable long jobId, HttpServletResponse response) throws EPublicationException
    {
        log.debug("Export Job Status, Job Id :{}", jobId);

        try (ByteArrayOutputStream outputStream = jobStatusReportGenerator.generateReport(jobId))
        {
            response.setContentType("application/pdf");
            response.setHeader("Content-disposition", "attachment; filename=\"Publication-Status-" + jobId + ".pdf\"");
            response.setContentLength(outputStream.size());

            outputStream.writeTo(response.getOutputStream());
        }
        catch (Exception e)
        {
            log.error("Error occurred in writing a file to Output Stream, Job Id {}", jobId);
        }
    }
}

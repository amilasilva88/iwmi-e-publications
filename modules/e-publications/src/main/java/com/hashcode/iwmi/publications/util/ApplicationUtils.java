/*
 * FILENAME
 *     ApplicationUtils.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.util;

import static com.hashcode.iwmi.publications.domain.PublicationStatus.ESTIMATED;
import static com.hashcode.iwmi.publications.domain.PublicationStatus.NEED_AN_AMENDMENT;
import static com.hashcode.iwmi.publications.domain.PublicationStatus.REJECTED;

import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.KeySpec;
import java.text.MessageFormat;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;

import com.hashcode.iwmi.publications.config.PrincipalUser;
import com.hashcode.iwmi.publications.domain.PublicationItem;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Utility class all application context
 * </p>
 * 
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 **/
public class ApplicationUtils
{
    private static final Logger log = LoggerFactory.getLogger(ApplicationUtils.class);

    public static final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public static final String BUNDLE_NAME = "application";
    private static ResourceBundle resourceBundle = ResourceBundle.getBundle(BUNDLE_NAME);

    /**
     * <p>
     * Check for string value null or empty.
     * </p>
     *
     * @param value
     * @return boolean
     *
     */
    public static boolean isNotNullOrEmpty(String value)
    {
        return (value == null || value.isEmpty()) ? false : true;
    }

    /**
     * <p>
     * Check for string value is null or empty.
     * </p>
     *
     * @param value
     * @return boolean
     *
     */
    public static boolean isNullOrEmpty(String value)
    {
        return (value == null || value.isEmpty()) ? true : false;
    }

    /**
     * <p>
     * Get responce data map.
     * </p>
     *
     *
     * @return Map<String, Object>
     *
     */
    public static Map<String, Object> getResponceDataMap()
    {
        Map<String, Object> params = new HashMap<>();
        return params;
    }

    /**
     * 
     * <p>
     * Create a JSON response from given parameters.
     * </p>
     *
     * @param status
     *            status
     * @param values
     *            map of values
     * @return {@link JSONObject}
     *
     */
    public static JSONObject createResponse(ResponseStatus status, Map<String, Object> values)
    {
        JSONObject jsonResponse = new JSONObject();
        jsonResponse.put("status", status.getCode());
        jsonResponse.put("message", status.getMsg());

        if (values != null)
            values.forEach((key, value) -> {
                jsonResponse.put(key, value);
            });
        return jsonResponse;
    }

    /**
     * <p>
     * Resolve a message by a key and argument replacements.
     * </p>
     * 
     * @see MessageFormat#format(String, Object...)
     * @param key
     *            the message to look up
     * @param arguments
     *            optional message arguments
     * @return the resolved message
     **/
    public static String getConfiguration(final String key, final Object... arguments)
    {
        try
        {
            if (arguments != null)
                return MessageFormat.format(resourceBundle.getString(key), arguments);
            return resourceBundle.getString(key);
        }
        catch (MissingResourceException e)
        {
            log.error("Message key not found: " + key);
            return '!' + key + '!';
        }
    }

    /**
     * <p>
     * Get current context authentication.
     * </p>
     *
     * @return {@link Authentication}
     *
     */
    public static Authentication getContextAuth()
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth;
    }

    /**
     * <p>
     * Get current user.
     * </p>
     *
     * @return {@link Authentication}
     *
     */
    public static PrincipalUser getCurrentUser()
    {
        PrincipalUser user = (PrincipalUser) getContextAuth().getPrincipal();
        return user;
    }

    /**
     * <p>
     * Add user to the model.
     * </p>
     * 
     * @param model
     *            model
     *
     */
    public static void addUserToModel(Model model)
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null)
        {
            log.debug("authenticated user {}", auth.getName());
            model.addAttribute("username", auth.getName());
        }
    }

    private static byte[] salt = {
        (byte) 0xA9, (byte) 0x9B, (byte) 0xC8, (byte) 0x32, (byte) 0x56, (byte) 0x35, (byte) 0xE3, (byte) 0x03
    };
    // Iteration count
    private static int iterationCount = 19;
    private static String SECRET_KEY = "IWMI_e_pub!@#";

    public static String encryptPlainText(String plainText)
    {
        try
        {
            KeySpec keySpec = new PBEKeySpec(SECRET_KEY.toCharArray(), salt, iterationCount);
            SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);
            // Prepare the parameter to the ciphers
            AlgorithmParameterSpec paramSpec = new PBEParameterSpec(salt, iterationCount);

            //Enc process
            Cipher ecipher = Cipher.getInstance(key.getAlgorithm());
            ecipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);

            String charSet = "UTF-8";
            byte[] in = plainText.getBytes(charSet);
            byte[] out = ecipher.doFinal(in);
            //            String encStr = new sun.misc.BASE64Encoder().encode(out);
            String encStr = Base64.encodeBase64URLSafeString(out);
            return encStr;

        }
        catch (Exception e)
        {
        }
        return null;
    }

    public static String decryptText(String encryptedText)
    {
        try
        {
            KeySpec keySpec = new PBEKeySpec(SECRET_KEY.toCharArray(), salt, iterationCount);
            SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);

            // Prepare the parameter to the ciphers
            AlgorithmParameterSpec paramSpec = new PBEParameterSpec(salt, iterationCount);

            //Decryption process; same key will be used for decr
            Cipher dcipher = Cipher.getInstance(key.getAlgorithm());
            dcipher.init(Cipher.DECRYPT_MODE, key, paramSpec);

            byte[] enc = Base64.decodeBase64(encryptedText);
            byte[] utf8 = dcipher.doFinal(enc);
            String charSet = "UTF-8";
            String plainStr = new String(utf8, charSet);
            return plainStr;
        }
        catch (Exception e)
        {
        }
        return null;
    }

    public static String toDisplayCase(String s)
    {

        final String ACTIONABLE_DELIMITERS = " '-/"; // these cause the character following
                                                     // to be capitalized

        StringBuilder sb = new StringBuilder();
        boolean capNext = true;

        for (char c : s.toCharArray())
        {
            c = (capNext) ? Character.toUpperCase(c) : Character.toLowerCase(c);
            if (c == '_')
            {
                sb.append(' ');
            }
            else
            {
                sb.append(c);
            }
            capNext = (ACTIONABLE_DELIMITERS.indexOf((int) c) >= 0); // explicit cast not needed
        }
        return sb.toString();
    }

    public static String getString(JSONObject json, String key)
    {
        if (json.has(key))
        {
            return json.getString(key);
        }
        return "";
    }
    
    public static int getInt(JSONObject json, String key)
    {
        if (json.has(key))
        {
            return json.getInt(key);
        }
        return 0;
    }
    
    public static double getDouble(JSONObject json, String key)
    {
        if (json.has(key))
        {
            return json.getDouble(key);
        }
        return 0.0;
    }
    
    public static void setItemStatus(PublicationItem item, JSONObject data)
    {
        if (item.getStatus().equals(NEED_AN_AMENDMENT) || item.getStatus().equals(REJECTED))
        {
            data.accumulate("item_status", ESTIMATED);
        } else {
            data.accumulate("item_status", item.getStatus());
        }
    }

    public static void main(String[] args) throws Exception
    {
        String plain = "AP;amilasilva@gmail.com;7;";
        String enc = encryptPlainText(plain);
        System.out.println("Original text: " + plain);
        System.out.println("Encrypted text: " + enc);
        String encorder = enc;
        //            URLEncoder.encode(
        //                "Us1bpe77t9nYbInwtP62LRlPcE1BABPT4r/UyWZDH6UI8iClM3U2BcQgc61/215b9uVaJUk63vshbSgQLyWM7Z7CB6X52hQB",
        //                "ISO-8859-1");
        System.out.println(":::::::::::::::::; Encorder :" + encorder);
        String plainAfter = decryptText(encorder);
        System.out.println("Original text after decryption: " + plainAfter);
    }
}

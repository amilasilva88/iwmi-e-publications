/*
 * FILENAME
 *     LookupDaoImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the license agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.springframework.stereotype.Repository;

import com.hashcode.iwmi.publications.dao.LookupDataDao;
import com.hashcode.iwmi.publications.domain.LookupData;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Lookup Data DAO implementation
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 * @version $Id$
 **/
@Transactional
@Repository
public class LookupDaoImpl implements LookupDataDao
{

    protected EntityManager entityManager;

    /**
     * <p>
     * Setter for entityManager (injected by Spring configuration).
     * </p>
     * 
     * @param anEntityManager
     *            the entity manager for this Dao
     **/
    @PersistenceContext(type = PersistenceContextType.TRANSACTION)
    public void setEntityManager(final EntityManager anEntityManager)
    {
        this.entityManager = anEntityManager;
    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.dao.LookupDataDao#create(java.lang.Object)
     */
    @Override
    public <E extends LookupData> void create(E lookup)
    {
        this.entityManager.persist(lookup);

    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.dao.LookupDataDao#update(java.lang.Object)
     */
    @Override
    public <E extends LookupData> E update(E lookup)
    {
        return this.entityManager.merge(lookup);
    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.dao.LookupDataDao#delete(java.lang.Object)
     */
    @Override
    public <E extends LookupData> void delete(E lookup)
    {
        this.entityManager.remove(lookup);
    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.dao.LookupDataDao#findById(java.lang.Class, java.lang.Long)
     */
    @Override
    public <E extends LookupData> E findById(Class<E> clazz, Long id)
    {
        return this.entityManager.find(clazz, id);
    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.dao.LookupDataDao#findByParentId(java.lang.Class, java.lang.Long)
     */
    @Override
    public <E extends LookupData> List<E> findByParentId(Class<E> clazz, Long parentId)
    {
        TypedQuery<E> query =
            entityManager.createQuery("FROM " + clazz.getName() + " c WHERE c.parentId = :parentId ", clazz);
        query.setParameter("parentId", parentId);

        return query.getResultList();
    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.dao.LookupDataDao#findAll()
     */
    @Override
    public <E extends LookupData> List<E> findAll(Class<E> clazz)
    {
        TypedQuery<E> query = entityManager.createQuery("FROM " + clazz.getName() + "", clazz);
        return query.getResultList();
    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.dao.LookupDataDao#countAll()
     */
    @Override
    public <E extends LookupData> long countAll(Class<E> clazz)
    {
        Session session = (Session) entityManager.getDelegate();
        Criteria crit = session.createCriteria(clazz).setProjection(Projections.rowCount());

        return (Long) crit.list().get(0);
    }

}

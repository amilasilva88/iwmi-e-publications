/*
 * FILENAME
 *     DeliveredStateChangeListener.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.listeners;

import static com.hashcode.iwmi.publications.domain.PublicationStatus.APPROVED;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.hashcode.iwmi.publications.domain.MessageTemplate;
import com.hashcode.iwmi.publications.domain.MessageTemplate.BodyTag;
import com.hashcode.iwmi.publications.domain.MessageTemplate.SubjectTag;
import com.hashcode.iwmi.publications.domain.PublicationItem;
import com.hashcode.iwmi.publications.domain.PublicationStatus;
import com.hashcode.iwmi.publications.domain.SystemUser;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;
import com.hashcode.iwmi.publications.services.PublicationItemService;
import com.hashcode.iwmi.publications.services.SystemUserService;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Delivered state change lister.
 * </p>
 *
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
@Service
@Qualifier(value = "deliveredStateChange")
@Transactional
public class DeliveredStateChangeListener extends AbstractPublicationStateChangeListener
{
    private static final Logger log = LoggerFactory.getLogger(DeliveredStateChangeListener.class);

    @Autowired
    private PublicationItemService publicationItemService;

    @Autowired
    private SystemUserService systemUserService;

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.listeners.PublicationStateChangeListener#onStateChange(java.lang.String,
     *      com.hashcode.iwmi.publications.domain.PublicationItem)
     */
    @Override
    public void onStateChange(String currentUser, PublicationItem publicationItem) throws EPublicationException
    {
        log.debug("Delivered State changed, [ User : {}, Publication Item id : {}] ", currentUser,
            publicationItem.getId());

        if (publicationItem.getStatus() == APPROVED )
        {
            publicationItem.setStatus(PublicationStatus.DELIVERED);
            publicationItemService.statusUpdate(publicationItem);
        }
        

        Map<MessageTemplate.SubjectTag, String> subjectMap = new HashMap<MessageTemplate.SubjectTag, String>();
        subjectMap.put(SubjectTag.SUBJECT_JOB_ID, publicationItem.getJobNumber());
        subjectMap.put(SubjectTag.SUBJECT_TITLE, publicationItem.getProjectCode());
        subjectMap.put(SubjectTag.SUBJECT_OTHER_TEXT_1, publicationItem.getTypeOfWork());

        SystemUser user = systemUserService.findUser(publicationItem.getCreatedBy());
        Map<MessageTemplate.BodyTag, Object> bodyMap = new HashMap<MessageTemplate.BodyTag, Object>();
        bodyMap.put(BodyTag.BODY_JOB_ID, publicationItem.getJobNumber());
        bodyMap.put(BodyTag.BODY_TO_NAME, user.getFirstName());
        bodyMap.put(BodyTag.BODY_LINK_PARAM_1, linkURL + "/delivery/page?id=" + publicationItem.getId());
        bodyMap.put(BodyTag.DATE, LocalDate.now(ZoneId.systemDefault()));
        bodyMap.put(BodyTag.DATETIME, LocalDateTime.now(ZoneId.systemDefault()));

        sendEmailAndNotifyOnDashboard(publicationItem, MessageTemplate.Type.AFTER_DELIVERY_ACCEPTED_TO_USER,
            subjectMap, bodyMap, user);

        SystemUser supervisor = systemUserService.findUser(publicationItem.getSupervisorName());
        bodyMap.put(BodyTag.BODY_TO_NAME, supervisor.getFirstName());
        sendEmailAndNotifyOnDashboard(publicationItem, MessageTemplate.Type.AFTER_DELIVERY_ACCEPTED_TO_SUPERVISOR,
            subjectMap, bodyMap, supervisor);

        auditService
            .addAudit(
                currentUser,
                String.format("Publication Item related good delivered, Publication Item Id : %d",
                    publicationItem.getId()));

        log.info("Delivered State changed done, [ User : {}, Publication Item id : {}] ", currentUser,
            publicationItem.getId());

    }

}

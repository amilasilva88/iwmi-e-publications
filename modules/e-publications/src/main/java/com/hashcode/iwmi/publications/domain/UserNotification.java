/*
 * FILENAME
 *     Notification.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.domain;

import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.hashcode.iwmi.publications.util.LocalDateTimePersistenceConverter;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * User Notification entity
 * </p>
 *
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 **/
@Entity
@Table(name = "USER_NOTIFICATIONS")
public class UserNotification extends BaseModel
{
    private static final long serialVersionUID = 2811625143375557631L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "notified_time")
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    private LocalDateTime notifiedTime;

    @ManyToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private PublicationItem item;

    @Column(name = "message", length = 1000)
    private String message;

    @Column(name = "username", length = 250)
    private String username;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private NotificationStatus status;

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.domain.BaseModel#getId()
     */
    @Override
    public Long getId()
    {
        return this.id;
    }

    /**
     * <p>
     * Getter for notifiedTime.
     * </p>
     * 
     * @return the notifiedTime
     */
    public LocalDateTime getNotifiedTime()
    {
        return notifiedTime;
    }

    /**
     * <p>
     * Setting value for notifiedTime.
     * </p>
     * 
     * @param notifiedTime
     *            the notifiedTime to set
     */
    public void setNotifiedTime(LocalDateTime notifiedTime)
    {
        this.notifiedTime = notifiedTime;
    }

    /**
     * <p>
     * Getter for item.
     * </p>
     * 
     * @return the item
     */
    public PublicationItem getItem()
    {
        return item;
    }

    /**
     * <p>
     * Setting value for item.
     * </p>
     * 
     * @param item
     *            the item to set
     */
    public void setItem(PublicationItem item)
    {
        this.item = item;
    }

    /**
     * <p>
     * Getter for message.
     * </p>
     * 
     * @return the message
     */
    public String getMessage()
    {
        return message;
    }

    /**
     * <p>
     * Setting value for message.
     * </p>
     * 
     * @param message
     *            the message to set
     */
    public void setMessage(String message)
    {
        this.message = message;
    }

    /**
     * <p>
     * Getter for username.
     * </p>
     * 
     * @return the username
     */
    public String getUsername()
    {
        return username;
    }

    /**
     * <p>
     * Setting value for username.
     * </p>
     * 
     * @param username
     *            the username to set
     */
    public void setUsername(String username)
    {
        this.username = username;
    }

    /**
     * <p>
     * Getter for status.
     * </p>
     * 
     * @return the status
     */
    public NotificationStatus getStatus()
    {
        return status;
    }

    /**
     * <p>
     * Setting value for status.
     * </p>
     * 
     * @param status
     *            the status to set
     */
    public void setStatus(NotificationStatus status)
    {
        this.status = status;
    }

}

/*
 * FILENAME
 *     EmailMessageServiceImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.services.impl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.hashcode.iwmi.publications.dao.MessageTemplateDao;
import com.hashcode.iwmi.publications.domain.EmailMessage;
import com.hashcode.iwmi.publications.domain.MessageTemplate;
import com.hashcode.iwmi.publications.domain.MessageTemplate.Type;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;
import com.hashcode.iwmi.publications.services.EmailMessageService;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Implementation of the Email Message service.
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
@Service
@Transactional
public class EmailMessageServiceImpl implements EmailMessageService
{
    private static final Logger log = LoggerFactory.getLogger(EmailMessageServiceImpl.class);

    private ExecutorService executorService = Executors.newFixedThreadPool(20);

    @Autowired
    private MessageTemplateDao messageTemplateDao;

    @Value("${mail.server.host}")
    private String mailServerHost;

    @Value("${mail.server.port}")
    private String mailServerPort;

    @Value("${mail.server.user}")
    private String mailServerUser;

    @Value("${mail.server.password}")
    private String mailServerPassword;

    @Value("${mail.transport.protocol}")
    private String mailProtocol;

    @Value("${from.email.address}")
    private String systemFromAddress;

    private Session session = null;

    public void init()
    {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", mailServerHost);
        props.put("mail.smtp.port", mailServerPort);

        session = Session.getInstance(props, new javax.mail.Authenticator()
        {
            protected PasswordAuthentication getPasswordAuthentication()
            {
                return new PasswordAuthentication(mailServerUser, mailServerPassword);
            }
        });
    }

    public void cleanUp() throws Exception
    {
        executorService.shutdown();
    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.services.EmailMessageService#send(com.hashcode.iwmi.publications.domain.EmailMessage)
     */
    @Override
    public void send(final EmailMessage message) throws EPublicationException
    {
        executorService.execute(new Runnable()
        {

            @Override
            public void run()
            {
                try
                {
                    log.info("Email messenger, send message : {}", message.getRecipients());
                    if (session == null)
                    {
                        init();
                    }
                    Message mimeMessage = new MimeMessage(session);
                    mimeMessage.setFrom(new InternetAddress(systemFromAddress));
                    mimeMessage.setRecipients(Message.RecipientType.TO, message.getRecipientsList());
                    //mimeMessage.setRecipients(Message.RecipientType.CC, message.getCcList());
                    mimeMessage.setSentDate(Calendar.getInstance().getTime());
                    mimeMessage.setSubject(message.getSubject());
                    mimeMessage.setText(message.getMessage());

                    Transport.send(mimeMessage);

                    log.info("Email messenger : sent successfully, [ message :{}]", mimeMessage.getSubject());
                }
                catch (Exception e)
                {
                    log.error("Error occurred while sending a email message,", e);
                }

            }
        });
    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.services.EmailMessageService#sendBulkMessage(com.hashcode.iwmi.publications.domain.EmailMessage,
     *      java.util.List)
     */
    @Override
    public void sendBulkMessage(EmailMessage message, List<String> recipients) throws EPublicationException
    {
        executorService.execute(new Runnable()
        {

            @Override
            public void run()
            {
                try
                {
                    log.info("Email messenger : bulk send");
                    if (session == null)
                    {
                        init();
                    }

                    recipients.stream().forEach(i -> {

                        try
                        {
                            Message mimeMessage = new MimeMessage(session);
                            mimeMessage.setFrom(new InternetAddress(systemFromAddress));
                            mimeMessage.setRecipients(Message.RecipientType.TO, message.getRecipientsList());
                            //                    mimeMessage.setRecipients(Message.RecipientType.CC, message.getCcList());
                        mimeMessage.setSentDate(Calendar.getInstance().getTime());
                        mimeMessage.setSubject(message.getSubject());
                        mimeMessage.setText(message.getMessage());

                        Transport.send(mimeMessage);
                    }
                    catch (Exception e)
                    {
                        log.error("Error occurred while sending a email message,", e);
                    }
                }   );

                    log.info("Email messenger : bulk messages sent successfully, [ message :{} ]", message.getSubject());
                }
                catch (Exception e)
                {
                    log.error("Error occurred while sending a email message,", e);
                }

            }
        });
    }

    private String subjectText;
    private String bodyText;

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.services.EmailMessageService#composeEmailTemplate(com.hashcode.iwmi.publications.domain.MessageTemplate.Type,
     *      java.util.Map)
     */
    @Override
    public EmailMessage composeEmail(Type templateType, Map<MessageTemplate.SubjectTag, String> subjectPlaceholderMap,
        Map<MessageTemplate.BodyTag, Object> bodyPlaceholderMap) throws EPublicationException
    {
        try
        {

            log.debug("Compose email using {}", templateType);
            MessageTemplate template = messageTemplateDao.findMessageTemplateByType(templateType);
            subjectText = template.getSubject();
            bodyText = template.getMailBody();

            subjectPlaceholderMap.forEach((k, v) -> {
                subjectText = subjectText.replace(k.getKey(), v);
            });

            bodyPlaceholderMap.forEach((k, v) -> {

                switch (k)
                {
                    case BODY_TO_NAME:
                    case BODY_JOB_ID:
                    case BODY_HEADER_1:
                    case BODY_SUB_HEADER_1:
                    case BODY_SUB_HEADER_2:
                    case BODY_SUB_HEADER_3:
                    case BODY_CONTENT_SET_1:
                    case BODY_CONTENT_SET_2:
                    case BODY_CONTENT_SET_3:
                    case BODY_CONTENT_SET_4:
                    case BODY_CONTENT_SET_5:
                    case BODY_LINK_PARAM_1:
                    case BODY_LINK_PARAM_2:
                        bodyText = bodyText.replace(k.getKey(), (String) v);
                        log.info("::::::::::::::::::::" + k.getKey()+ "::::::::::::::: " + v + ":::");
                        break;

                    case DATE:
                        LocalDate date = (LocalDate) v;
//                        bodyText = bodyText.replaceAll(k.getKey(), date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
                        break;
                    case DATETIME:
                        LocalDateTime datetime = (LocalDateTime) v;
//                        bodyText = bodyText.replaceAll(k.getKey(), datetime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
                        break;

                    case BODY_LIST_TAB_1:

                        @SuppressWarnings("unchecked")
                        List<String> listTab1 = (List<String>) v;
                        StringBuffer buffList = new StringBuffer();
                        listTab1.stream().forEach(n -> {
                            buffList.append("\t").append(n).append("\n");
                        });
                        bodyText = bodyText.replace(k.getKey(), buffList.toString());
                        break;

                    case BODY_LIST_TAB_2:
                        List<String> listTab2 = (List<String>) v;
                        StringBuffer buffList2 = new StringBuffer();
                        listTab2.stream().forEach(n -> {
                            buffList2.append("\t\t").append(n).append("\n");
                        });
                        bodyText = bodyText.replace(k.getKey(), buffList2.toString());
                        break;
                }

            });

            EmailMessage emailMessage = new EmailMessage();
            emailMessage.setSubject(subjectText);
            emailMessage.setMessage(bodyText);

            return emailMessage;
        }
        catch (Exception e)
        {
            throw new EPublicationException("Error occurred while compose email, ", e);
        }
    }
}

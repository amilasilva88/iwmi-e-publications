/*
 * FILENAME
 *     AbstractPublicationStateChangeListener.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.listeners;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.hashcode.iwmi.publications.domain.EmailMessage;
import com.hashcode.iwmi.publications.domain.MessageTemplate;
import com.hashcode.iwmi.publications.domain.NotificationStatus;
import com.hashcode.iwmi.publications.domain.PublicationItem;
import com.hashcode.iwmi.publications.domain.SystemUser;
import com.hashcode.iwmi.publications.domain.UserNotification;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;
import com.hashcode.iwmi.publications.services.AuditLogService;
import com.hashcode.iwmi.publications.services.EmailMessageService;
import com.hashcode.iwmi.publications.services.PublicationItemService;
import com.hashcode.iwmi.publications.services.UserNotificationService;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Abstract Publication state change listener.
 * </p>
 *
 *
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
@Service
public abstract class AbstractPublicationStateChangeListener implements PublicationStateChangeListener
{
    private static final Logger log = LoggerFactory.getLogger(AbstractPublicationStateChangeListener.class);

    @Autowired
    private UserNotificationService userNotificationService;

    @Autowired
    private EmailMessageService emailMessageService; 
    
    @Autowired
    private PublicationItemService publicationItemService;

    @Autowired
    protected AuditLogService auditService;
    
    @Value("${ePub.app.url}")
    protected String linkURL;
    
    @Value("${ePub.app.base.url}")
    protected String appBaseURL;
    
    @Value("${export.file.location}")
    protected String exportFileLocation;
    
    @Value("${export.reports.url}")
    protected String exportReportUrl;
    

    @Transactional
    public void sendEmailAndNotifyOnDashboard(PublicationItem publicationItem, MessageTemplate.Type messageType,
        Map<MessageTemplate.SubjectTag, String> subjectMap, Map<MessageTemplate.BodyTag, Object> bodyMap,
        SystemUser user) throws EPublicationException
    {
        EmailMessage email = emailMessageService.composeEmail(messageType, subjectMap, bodyMap);
        email.setRecipients(user.getEmail());
        emailMessageService.send(email);
        
        publicationItem = publicationItemService.findById(publicationItem.getId());
        
        notifyOnUserDashboard(publicationItem, user, email);
        log.info(
            "Job Initiation State update send to and notified to dashboard , [ User : {}, Publication Item id : {}] ",
            user.getUsername(), publicationItem.getId());
    }

    @Transactional
    public void notifyOnUserDashboard(PublicationItem publicationItem, SystemUser user, EmailMessage email)
        throws EPublicationException
    {
        UserNotification userNotification = new UserNotification();
        userNotification.setItem(publicationItem);
        userNotification.setUsername(user.getUsername());
        userNotification.setMessage(email.getSubject());
        userNotification.setNotifiedTime(LocalDateTime.now());
        userNotification.setStatus(NotificationStatus.NEW);
        userNotificationService.addUserNotification(userNotification);
    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.listeners.PublicationStateChangeListener#onStateChange(java.lang.String, java.util.List, com.hashcode.iwmi.publications.domain.PublicationItem)
     */
    @Override
    public void onStateChange(String currentUser, Set<String> notifyUsers, PublicationItem publicationItem)
        throws EPublicationException
    {
        // TODO Auto-generated method stub
        
    }
    
}

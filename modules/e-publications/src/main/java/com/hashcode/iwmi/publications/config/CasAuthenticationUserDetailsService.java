/*
 * FILENAME
 *     TestCasAuthenticationUserDetailsService.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.config;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.hashcode.iwmi.publications.domain.SystemUser;
import com.hashcode.iwmi.publications.domain.UserRole;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;
import com.hashcode.iwmi.publications.services.SystemUserService;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Mock authentication user details service.
 * </p>
 *
 * @author Manuja Jayamanna
 * @email manuja@hashcodesys.com
 */
@SuppressWarnings("rawtypes")
public class CasAuthenticationUserDetailsService implements AuthenticationUserDetailsService
{

    @Autowired
    private SystemUserService userService;

    /**
     * {@inheritDoc}
     *
     * @see org.springframework.security.core.userdetails.AuthenticationUserDetailsService#loadUserDetails(org.springframework.security.core.Authentication)
     */
    @Transactional
    @Override
    public UserDetails loadUserDetails(Authentication token) throws UsernameNotFoundException
    {
        List<GrantedAuthority> authorities = new ArrayList<>();

        try
        {
            SystemUser systemUser = userService.findUser(token.getName());
            for (UserRole userRole : systemUser.getUserRoles())
            {
                authorities.add(new SimpleGrantedAuthority("ROLE_" + userRole.getLabel().toUpperCase()));
            }
            return new PrincipalUser(systemUser.getUsername(), "", systemUser.getFirstName() + " "
                + systemUser.getLastName(), systemUser.getEmail(), authorities);
        }
        catch (EPublicationException e)
        {
            throw new UsernameNotFoundException(token.getName());
        }
    }
}

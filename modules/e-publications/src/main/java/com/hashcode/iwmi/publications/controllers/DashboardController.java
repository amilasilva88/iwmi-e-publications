/*
 * FILENAME
 *     DashboardController.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.controllers;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hashcode.iwmi.publications.domain.PublicationItem;
import com.hashcode.iwmi.publications.domain.PublicationStatus;
import com.hashcode.iwmi.publications.domain.UserNotification;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;
import com.hashcode.iwmi.publications.services.PublicationItemService;
import com.hashcode.iwmi.publications.services.SystemUserService;
import com.hashcode.iwmi.publications.services.UserNotificationService;
import com.hashcode.iwmi.publications.util.ApplicationUtils;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Dashboard Controller.
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
@Controller
@RequestMapping("/dashboard")
public class DashboardController
{
    private static final Logger log = LoggerFactory.getLogger(DashboardController.class);

    @Autowired
    private UserNotificationService userNotificationService;

    @Autowired
    private PublicationItemService publicationItemService;

    @Autowired
    private SystemUserService userService;

    private Map<String, String> userNamesMap = new HashMap<String, String>();
    
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    @RequestMapping(value = "/notifications", method = {
        RequestMethod.GET
    })
    @ResponseBody
    public String loadNotifications() throws EPublicationException
    {
        String username = ApplicationUtils.getCurrentUser().getUsername();
        log.debug("Request came to load notifications for user : {}", username);
        List<UserNotification> userNotifications = userNotificationService.findUserNotificationByUser(username, 50);

        JSONArray jsonArray = new JSONArray();
        userNotifications.forEach((n) -> {
            JSONObject data = new JSONObject();
            data.put("notified_time", formatter.format(n.getNotifiedTime()));
            data.put("message", n.getMessage());
            data.put("publication_item_id", n.getItem().getId());
            data.put("status", n.getStatus());
            jsonArray.put(data);
        });

        JSONObject responseData = new JSONObject();
        responseData.put("no_records", userNotifications.size());
        responseData.put("notifications", jsonArray);

        log.debug("No of notifications found : {}", userNotifications.size());
        return responseData.toString();
    }

    @RequestMapping(value = "/publications", method = {
        RequestMethod.GET
    })
    @ResponseBody
    public String loadPublications() throws EPublicationException
    {
        String username = ApplicationUtils.getCurrentUser().getUsername();
        log.debug("Request came to load publications for user : {}", username);

        List<PublicationItem> publications = publicationItemService.findAll();

        JSONArray jsonArray = new JSONArray();

        publications.stream().filter(p -> p.getStatus() != PublicationStatus.DELETED).forEach(p -> {

            JSONObject data = new JSONObject();
            data.put("id", p.getId());
            data.put("type_of_work", p.getTypeOfWork());
            data.put("project_code", p.getProjectCode());
            data.put("required_delivery_date", p.getRequiredDeliveryDate());
            data.put("division", p.getDivision());
            data.put("status", p.getStatus());
            data.put("jobNumber", p.getJobNumber());
            data.put("series", p.getSeries());
            data.put("created_by", getName(p.getCreatedBy()));

            jsonArray.put(data);
        });

        JSONObject responseData = new JSONObject();
        responseData.put("no_records", publications.size());
        responseData.put("publication_items", jsonArray);

        log.debug("No of publications for display: {}", jsonArray.length());
        return responseData.toString();
    }

    private String getName(String username)
    {
        if (userNamesMap.isEmpty())
        {
            try
            {
                userService.findAll().forEach(u -> {
                    userNamesMap.put(u.getUsername(), u.getFirstName() + " " + u.getLastName());
                });
            }
            catch (EPublicationException e)
            {
                log.error("Error in loading the users,", e);
            }
        }
        String name = userNamesMap.get(username);
        return (name != null) ? name : "";
    }
}

/*
 * FILENAME
 *     PrincipalUser.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.config;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Principal User, Current logged in user
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
public class PrincipalUser extends User
{

    private static final long serialVersionUID = 271494748512701260L;

    private String email;
    private String displayName;

    /**
     * Calls the more complex constructor with all boolean arguments set to {@code true}.
     */
    public PrincipalUser(String username, String password, String displayName, String email,
        Collection<? extends GrantedAuthority> authorities)
    {
        super(username, password, authorities);
        this.displayName = displayName;
        this.email = email;
    }

    /**
     * <p>
     * Getter for email.
     * </p>
     * 
     * @return the email
     */
    public String getEmail()
    {
        return email;
    }

    /**
     * <p>
     * Setting value for email.
     * </p>
     * 
     * @param email
     *            the email to set
     */
    public void setEmail(String email)
    {
        this.email = email;
    }

    /**
     * <p>
     * Getter for displayName.
     * </p>
     * 
     * @return the displayName
     */
    public String getDisplayName()
    {
        return displayName;
    }

    /**
     * <p>
     * Setting value for displayName.
     * </p>
     * 
     * @param displayName
     *            the displayName to set
     */
    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }

}

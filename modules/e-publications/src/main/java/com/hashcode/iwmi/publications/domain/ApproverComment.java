/*
 * FILENAME
 *     ApproverComment.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.domain;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.hashcode.iwmi.publications.util.LocalDateTimePersistenceConverter;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Approver Comments Entity.
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 * @version $Id$
 **/
@Entity
@Table(name = "APPROVER_COMMENTS")
public class ApproverComment extends BaseModel
{
    private static final long serialVersionUID = -3197025791295586844L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "approved_time")
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    private LocalDateTime approvedTime;

    @Column(name = "comment", length = 1000)
    private String comment;

    @ManyToOne
    @JoinColumn(name = "publication_item_id")
    private PublicationItem publicationItem;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private PublicationStatus status;

    @Column(name = "approver_name")
    private String approver;

    /**
     * <p>
     * Getter for approvedTime.
     * </p>
     * 
     * @return the approvedTime
     */
    public LocalDateTime getApprovedTime()
    {
        return approvedTime;
    }

    /**
     * <p>
     * Setting value for approvedTime.
     * </p>
     * 
     * @param approvedTime
     *            the approvedTime to set
     */
    public void setApprovedTime(LocalDateTime approvedTime)
    {
        this.approvedTime = approvedTime;
    }

    /**
     * <p>
     * Getter for comment.
     * </p>
     * 
     * @return the comment
     */
    public String getComment()
    {
        return comment;
    }

    /**
     * <p>
     * Setting value for comment.
     * </p>
     * 
     * @param comment
     *            the comment to set
     */
    public void setComment(String comment)
    {
        this.comment = comment;
    }

    /**
     * <p>
     * Getter for publicationItem.
     * </p>
     * 
     * @return the publicationItem
     */
    public PublicationItem getPublicationItem()
    {
        return publicationItem;
    }

    /**
     * <p>
     * Setting value for publicationItem.
     * </p>
     * 
     * @param publicationItem
     *            the publicationItem to set
     */
    public void setPublicationItem(PublicationItem publicationItem)
    {
        this.publicationItem = publicationItem;
    }

    /**
     * <p>
     * Setting value for id.
     * </p>
     * 
     * @param id
     *            the id to set
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.domain.BaseModel#getId()
     */
    @Override
    public Long getId()
    {
        return id;
    }

    /**
     * <p>
     * Getter for status.
     * </p>
     * 
     * @return the status
     */
    public PublicationStatus getStatus()
    {
        return status;
    }

    /**
     * <p>
     * Setting value for status.
     * </p>
     * 
     * @param status
     *            the status to set
     */
    public void setStatus(PublicationStatus status)
    {
        this.status = status;
    }

    /**
     * <p>
     * Getter for approver.
     * </p>
     * 
     * @return the approver
     */
    public String getApprover()
    {
        return approver;
    }

    /**
     * <p>
     * Setting value for approver.
     * </p>
     * 
     * @param approver
     *            the approver to set
     */
    public void setApprover(String approver)
    {
        this.approver = approver;
    }

}

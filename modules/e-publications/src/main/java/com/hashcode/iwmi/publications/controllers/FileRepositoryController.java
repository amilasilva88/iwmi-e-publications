/*
 * FILENAME
 *     FileRepositoryController.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.controllers;

import static com.hashcode.iwmi.publications.util.ApplicationUtils.createResponse;
import static com.hashcode.iwmi.publications.util.ApplicationUtils.getResponceDataMap;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.hashcode.iwmi.publications.domain.Document;
import com.hashcode.iwmi.publications.domain.PublicationItem;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;
import com.hashcode.iwmi.publications.services.DocumentService;
import com.hashcode.iwmi.publications.services.PublicationItemService;
import com.hashcode.iwmi.publications.util.ResponseStatus;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * File Repository Controller for e-Publication files in repository and uploading files.
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
@Controller
@RequestMapping("/file_repo")
public class FileRepositoryController
{
    private static final Logger log = LoggerFactory.getLogger(FileRepositoryController.class);

    @Value("${document.repo.file.location}")
    private String docRepoLocation;

    @Value("${app.document.repo.url}")
    private String appDocRepoUrl;

    @Autowired
    private PublicationItemService publicationItemService;

    @Autowired
    private DocumentService documentService;

    /**
     * <p>
     * Uploaded files receives here.
     * </p>
     * 
     * @throws EPublicationException
     */
    @Transactional
    @RequestMapping(value = "/upload", method = {
        RequestMethod.POST, RequestMethod.PUT
    })
    public @ResponseBody String upload(MultipartHttpServletRequest request, HttpServletResponse response)
        throws EPublicationException
    {
        log.info("Upload request received");

        long pubItemId = Long.valueOf(request.getParameter("e_pub_id"));
        log.info("Upload request received pubItemId: " + pubItemId);
        Iterator<String> itr = request.getFileNames();
        MultipartFile mpf = null;
        PublicationItem publicationItem = publicationItemService.findById(pubItemId);

        boolean isSuccess = true;
        while (itr.hasNext())
        {
            mpf = request.getFile(itr.next());
            log.info("uploaded file details [ Filename :{}]", mpf.getOriginalFilename());

            Document file = new Document();
            file.setFileName(mpf.getOriginalFilename());
            file.setFileSize(mpf.getSize() / (1024.0 * 1024.0));
            file.setFileType(mpf.getContentType());
            file.setPublicationItem(publicationItem);

            try
            {
                file.setData(mpf.getBytes());

                String path = docRepoLocation + File.separator + pubItemId;
                File docPath = new File(path);
                if (!docPath.exists())
                {
                    docPath.mkdirs();
                }

                file.setFileLocation(path + File.separator + mpf.getOriginalFilename());
                file.setFileUrl(appDocRepoUrl + pubItemId + "/" + mpf.getOriginalFilename());
                FileCopyUtils.copy(mpf.getBytes(), new FileOutputStream(file.getFileLocation()));

//                documentService.saveDocument(file);
                publicationItem.getAttachments().add(file);
                isSuccess = true;
            }
            catch (IOException e)
            {
                isSuccess = false;
                log.error("Error occurred in uploading files,", e);
                break;
            }
        }

        publicationItemService.update(publicationItem);
        
        Map<String, Object> params = getResponceDataMap();
        params.put("id", pubItemId);
        params.put("data", "Documents upload " + (isSuccess ? "successfull" : "failed"));
        JSONObject jsonReponse = createResponse((isSuccess) ? ResponseStatus.SUCCESS : ResponseStatus.FAILED, params);
        log.debug("JSON Response for load divisions, Response : {}", jsonReponse);
        return jsonReponse.toString();
    }

    /**
     * <p>
     * Download a file from document repository.
     * </p>
     *
     * @param response
     * @param value
     * @return
     * @throws EPublicationException
     *
     */
    @RequestMapping(value = "/download/{fileId}", method = RequestMethod.GET)
    public void downloadFileById(@PathVariable long fileId, HttpServletResponse response) throws EPublicationException
    {
        log.debug("File download request, File Id :{}", fileId);
        Document document = documentService.findById(fileId);
        if (document != null)
        {
            response.setContentType(document.getFileType());
            response.setHeader("Content-disposition", "attachment; filename=\"" + document.getFileName() + "\"");

            try (FileInputStream is = new FileInputStream(document.getFileLocation());
                OutputStream outStream = response.getOutputStream())
            {
                FileCopyUtils.copy(is, response.getOutputStream());
            }
            catch (Exception e)
            {
                log.error("Error occurred in writing a file to Output Stream :{}", fileId);
            }
        }
        else
        {
            log.info("No document found for Id :{}", fileId);
        }
    }
}

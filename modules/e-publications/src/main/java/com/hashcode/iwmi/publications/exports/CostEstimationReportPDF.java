/*
 * FILENAME
 *     CostEstimationReportPDF.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.exports;

import static com.hashcode.iwmi.publications.util.ApplicationUtils.toDisplayCase;

import java.io.ByteArrayOutputStream;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hashcode.iwmi.publications.domain.CostElement;
import com.hashcode.iwmi.publications.domain.PublicationItem;
import com.hashcode.iwmi.publications.services.PublicationItemService;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * CostEstimation Report PDF.
 * </p>
 *
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
@Service
public class CostEstimationReportPDF extends AbstractReportPDF
{
    private static final Logger log = LoggerFactory.getLogger(CostEstimationReportPDF.class);

    @Autowired
    private PublicationItemService publicationService;

    @Override
    @Transactional
    public ByteArrayOutputStream generateReport(Long publicationItemId)
    {
        try
        {
            long startTime = System.currentTimeMillis();
            log.info("Cost Estimation Report Generation started, Item id :{}", publicationItemId);

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            Document iTextPdf = new Document(PageSize.A4);
            PdfWriter.getInstance(iTextPdf, outputStream);
            iTextPdf.open();

            PublicationItem item = publicationService.findById(publicationItemId);
            PdfPTable documentTable = createDocumentTable();
            documentTitle("Cost Estimation For Publication Item\n     ", TITLE_BOLD, documentTable);

            PdfPTable jobDetailTable = createJobDetailTable(item);
            documentTable.addCell(createCellNoBorder(jobDetailTable, 1));
            documentTable.addCell(emptyCellNoBorder(1));

            double totalOverHead = 0.0, totalEstimation = 0.0;
            boolean hasElements = false;
            for (CostElement element : item.getCostEstimations())
            {
                PdfPTable elementDetailTable = createElementDetailTable(element);
                documentTable.addCell(createCellNoBorder(elementDetailTable, 2));

                totalOverHead += element.getOtherCosts();
                totalEstimation += element.getSubTotal();
                hasElements = true;
            }

            if (hasElements)
            {
                PdfPTable totalTable = createTotalTable(totalOverHead, totalEstimation);
                documentTable.addCell(createCellNoBorder(totalTable, 2));
            }

            iTextPdf.add(documentTable);
            iTextPdf.close();

            long endTime = System.currentTimeMillis();

            log.info("Time taken to create PDF :" + (endTime - startTime) + " milis");

            return outputStream;
        }
        catch (Exception e)
        {
            log.error("Error occurred while generating the PDF report,", e);
            return null;
        }

    }

    private PdfPTable createElementDetailTable(CostElement element) throws Exception
    {
        PdfPTable elementTable = new PdfPTable(2);
        elementTable.setWidthPercentage(100);
        elementTable.setSpacingBefore(5);
        elementTable.setSpacingAfter(5);

        PdfPCell elementTitleCell = new PdfPCell(new Phrase(toDisplayCase(element.getType()), BOLD));
        elementTitleCell.setColspan(2);
        elementTitleCell.setBackgroundColor(LIGHT_BLUE_PDF_TABLE_HEADER);
        elementTitleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        elementTable.addCell(elementTitleCell);

        PdfPTable field = fieldTableForValues(LABEL_BOLD, NORMAL, "    No of pages/copies:", element.getNoOfCopies());
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Cost per unit:", element.getCostPerUnit());
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Other costs:", element.getOtherCosts());
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Sub total:", element.getOtherCosts());
        elementTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_2));
        elementTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_2));

        return elementTable;
    }

    private PdfPTable createTotalTable(double totalOverHead, double totalEstimate) throws Exception
    {
        PdfPTable totalTable = new PdfPTable(2);
        totalTable.setWidthPercentage(100);
        totalTable.setSpacingBefore(5);
        totalTable.setSpacingAfter(5);

        PdfPTable field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Total overhead:", totalOverHead);
        totalTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_1));
        totalTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_1));
        field = fieldTableForValues(LABEL_BOLD, NORMAL, "    Total Estimate:", totalEstimate);
        totalTable.addCell(createCellNoBorder(field, 1, LIGHT_GREY_PDF_TABLE_BG_1));
        totalTable.addCell(emptyCellNoBorder(1, LIGHT_GREY_PDF_TABLE_BG_1));

        return totalTable;
    }

    private PdfPTable createJobDetailTable(PublicationItem item) throws Exception
    {

        PdfPTable jobDetailTable = new PdfPTable(2);
        jobDetailTable.setWidthPercentage(100);
        jobDetailTable.setSpacingBefore(5);
        jobDetailTable.setSpacingAfter(5);

        PdfPCell jobTitleCell = new PdfPCell(new Phrase("Job Details", BOLD));
        jobTitleCell.setColspan(2);
        jobTitleCell.setBackgroundColor(LIGHT_BLUE_PDF_TABLE_HEADER);
        jobTitleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        jobDetailTable.addCell(jobTitleCell);

        PdfPTable field = fieldTable(LABEL_BOLD, NORMAL, "   Job Number:", item.getJobNumber());
        jobDetailTable.addCell(createCellNoBorder(field, 1));
        jobDetailTable.addCell(emptyCellNoBorder(1));
        field = fieldTable(LABEL_BOLD, NORMAL, "   Job Type:", item.getType());
        jobDetailTable.addCell(createCellNoBorder(field, 1));
        field = fieldTable(LABEL_BOLD, NORMAL, "   Series:", item.getSeries());
        jobDetailTable.addCell(createCellNoBorder(field, 1));
        field = fieldTable(LABEL_BOLD, NORMAL, "   Print Run:", item.getPrintRun());
        jobDetailTable.addCell(createCellNoBorder(field, 1));
        field = fieldTable(LABEL_BOLD, NORMAL, "   Project Code:", item.getProjectCode());
        jobDetailTable.addCell(createCellNoBorder(field, 1));

        return jobDetailTable;
    }

    private PdfPTable generateTable(List<String> headerNames, List<List<Object>> dataRows)
    {
        PdfPTable table = new PdfPTable(headerNames.size());
        table.setWidthPercentage(98);
        table.setSpacingBefore(5);
        table.setSpacingAfter(5);

        PdfPCell tableCell;

        // header rows
        for (String header : headerNames)
        {
            tableCell = new PdfPCell(new Phrase(header.toUpperCase(), BOLD));
            tableCell.setBackgroundColor(SKY_BLUE_PDF_TABLE_HEADER);
            tableCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(tableCell);
        }

        int count = 0;
        // Data rows
        for (List<Object> dataRow : dataRows)
        {

            for (Object cellVal : dataRow)
            {
                tableCell = new PdfPCell(new Phrase(String.valueOf(cellVal), NORMAL));

                if (count % 2 == 1)
                {
                    tableCell.setBackgroundColor(ODD_ROW);
                }
                else
                {
                    tableCell.setBackgroundColor(EVEN_ROW);
                }

                tableCell.setHorizontalAlignment(Element.ALIGN_LEFT | Element.ALIGN_JUSTIFIED);
                table.addCell(tableCell);
            }
            count++;
        }
        return table;
    }

}

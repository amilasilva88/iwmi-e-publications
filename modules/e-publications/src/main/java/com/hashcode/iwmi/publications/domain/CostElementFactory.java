/*
 * FILENAME
 *     CostElementFactory.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.domain;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Cost Element Factory.
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
public final class CostElementFactory
{

    /**
     * <p>
     * Get Cost Element by Type.
     * </p>
     *
     * @param type
     *            cost element type
     * @return {@link CostElement}
     *
     */
    public static CostElement getCostElement(String type)
    {
        switch (type)
        {
            case "DESIGNING_COST":
                return new DesigningCost();
            case "EDITING_COST":
                return new EditingCost();
            case "GRAPHICS_COST":
                return new GraphicsCost();
            case "OPERATIONAL_COST":
                return new OperationalCost();
            case "PRINTING_COST":
                return new PrintingCost();
            case "PROOF_READ_COST":
                return new ProofReadingCost();
            case "REPRINTING_COST":
                return new RePrintingCost();
            case "TRANSLATING_COST":
                return new TranslatingCost();
            case "TYPE_SETTING_COST":
                return new TypeSettingCost();
            default:
                return null;
        }

    }

}

/*
 * FILENAME
 *     PublicationItemServiceImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.services.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hashcode.iwmi.publications.dao.PublicationItemDao;
import com.hashcode.iwmi.publications.domain.PublicationItem;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;
import com.hashcode.iwmi.publications.services.PublicationItemService;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Implementation of the {@link PublicationItemService}.
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
@Service
@Transactional
public class PublicationItemServiceImpl implements PublicationItemService
{
    private static Logger log = LoggerFactory.getLogger(PublicationItemServiceImpl.class);

    @Autowired
    private PublicationItemDao publicationItemDao;

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.services.PublicationItemService#save(com.hashcode.iwmi.publications.domain.PublicationItem)
     */
    @Override
    public Long save(PublicationItem publicationItem) throws EPublicationException
    {
        publicationItemDao.create(publicationItem);
        return publicationItem.getId();
    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.services.PublicationItemService#update(com.hashcode.iwmi.publications.domain.PublicationItem)
     */
    @Override
    public Long update(PublicationItem publicationItem) throws EPublicationException
    {
        publicationItemDao.update(publicationItem);
        return publicationItem.getId();
    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.services.PublicationItemService#findById(java.lang.Long)
     */
    @Override
    public PublicationItem findById(Long id) throws EPublicationException
    {
        PublicationItem item = publicationItemDao.findById(id);
        return item;
    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.services.PublicationItemService#findAll()
     */
    @Override
    public List<PublicationItem> findAll() throws EPublicationException
    {
        List<PublicationItem> items = publicationItemDao.findAll();
        return items;
    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.services.PublicationItemService#statusUpdate(com.hashcode.iwmi.publications.domain.PublicationItem)
     */
    @Override
    public void statusUpdate(PublicationItem publicationItem) throws EPublicationException
    {
        publicationItemDao.statusUpdate(publicationItem);
        log.info("Publication Item status updated, Publication Item : {}", publicationItem.getId());
    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.services.PublicationItemService#findByIds(java.util.List)
     */
    @Override
    public List<PublicationItem> findByIds(List<Long> ids) throws EPublicationException
    {
        return publicationItemDao.findByIds(ids);
    }

}

/*
 * FILENAME
 *     AuditLogDao.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.dao;

import java.time.LocalDateTime;
import java.util.List;

import com.hashcode.iwmi.publications.domain.AuditLog;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * DAO for Audit Log
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
public interface AuditLogDao extends GenericDao<AuditLog, Long>
{

    /**
     * <p>
     * Find the audit logs for last N days by given user or all the users.
     * </p>
     *
     * @param user
     *            username if null will fecth data for all the users
     * @param fromDate
     *            date range starts from
     * @param toDate
     *            date range ends from
     * @return list of {@li AuditLog}
     *
     */
    List<AuditLog> findLastNDaysActions(final String user, LocalDateTime fromDate, LocalDateTime toDate);
}

/*
 * FILENAME
 *     PublicationItem.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.domain;

import static com.hashcode.iwmi.publications.util.SearchEngineManager.AUTHOR;
import static com.hashcode.iwmi.publications.util.SearchEngineManager.COMPLTETION_DATE;
import static com.hashcode.iwmi.publications.util.SearchEngineManager.CREATED_BY;
import static com.hashcode.iwmi.publications.util.SearchEngineManager.DESC_OF_WORK;
import static com.hashcode.iwmi.publications.util.SearchEngineManager.DIVISION;
import static com.hashcode.iwmi.publications.util.SearchEngineManager.ID;
import static com.hashcode.iwmi.publications.util.SearchEngineManager.ISBN_ISSN;
import static com.hashcode.iwmi.publications.util.SearchEngineManager.JOB_NUMBER;
import static com.hashcode.iwmi.publications.util.SearchEngineManager.OTHER_INFO;
import static com.hashcode.iwmi.publications.util.SearchEngineManager.PRINT_RUN;
import static com.hashcode.iwmi.publications.util.SearchEngineManager.PROJECT_CODE;
import static com.hashcode.iwmi.publications.util.SearchEngineManager.RELATED_PROG;
import static com.hashcode.iwmi.publications.util.SearchEngineManager.REQUESTER;
import static com.hashcode.iwmi.publications.util.SearchEngineManager.SERIES;
import static com.hashcode.iwmi.publications.util.SearchEngineManager.STATUS;
import static com.hashcode.iwmi.publications.util.SearchEngineManager.SUPERVISOR_NAME;
import static com.hashcode.iwmi.publications.util.SearchEngineManager.TITLE;
import static com.hashcode.iwmi.publications.util.SearchEngineManager.TYPE;
import static com.hashcode.iwmi.publications.util.SearchEngineManager.TYPE_OF_WORK;
import static com.hashcode.iwmi.publications.util.SearchEngineManager.YEAR;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.hashcode.iwmi.publications.util.LocalDatePersistenceConverter;
import com.hashcode.solrjx.Indexable;
import com.hashcode.solrjx.IndexableDataType;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Publication Item entity class.
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 **/
@Entity
@Table(name = "PUBLICATION_ITEMS")
public class PublicationItem extends BaseModel implements Serializable, Searchable
{
    private static final long serialVersionUID = -7985835330374622360L;

    @Indexable(fieldName = ID, id = true)
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Indexable(fieldName = TYPE_OF_WORK, type = IndexableDataType.TEXT)
    @Column(name = "type_of_work", length = 500)
    private String typeOfWork;

    @Indexable(fieldName = DESC_OF_WORK, type = IndexableDataType.TEXT)
    @Column(name = "desc_of_work", length = 1000)
    private String descOfWork;

    @Indexable(fieldName = PRINT_RUN, type = IndexableDataType.TEXT)
    @Column(name = "print_run", length = 10)
    private String printRun;

    @Indexable(fieldName = PROJECT_CODE, type = IndexableDataType.TEXT)
    @Column(name = "project_code")
    private String projectCode;

    @Column(name = "req_delivery_date")
    @Convert(converter = LocalDatePersistenceConverter.class)
    private LocalDate requiredDeliveryDate;

    @Indexable(fieldName = COMPLTETION_DATE, type = IndexableDataType.DATE)
    @Column(name = "completion_date")
    @Convert(converter = LocalDatePersistenceConverter.class)
    private LocalDate completionDate;

    @Indexable(fieldName = OTHER_INFO, type = IndexableDataType.TEXT)
    @Column(name = "other_info", length = 500)
    private String otherInfo;

    @Indexable(fieldName = REQUESTER, type = IndexableDataType.TEXT)
    @Column(name = "requester_name")
    private String requesterName;

    @Indexable(fieldName = DIVISION, type = IndexableDataType.TEXT)
    @Column(name = "division")
    private String division;

    @Indexable(fieldName = SUPERVISOR_NAME, type = IndexableDataType.TEXT)
    @Column(name = "supervisor_name")
    private String supervisorName;

    @Indexable(fieldName = JOB_NUMBER, type = IndexableDataType.TEXT)
    @Column(name = "job_number")
    private String jobNumber;

    @Indexable(fieldName = TYPE, type = IndexableDataType.TEXT)
    @Column(name = "type")
    private String type;

    @Indexable(fieldName = SERIES, type = IndexableDataType.TEXT)
    @Column(name = "series")
    private String series;

    @Indexable(fieldName = TITLE, type = IndexableDataType.TEXT)
    @Column(name = "title")
    private String title;

    @Column(name = "approved_date")
    private LocalDate approvedDate;

    @Column(name = "delivered_date")
    private LocalDate deliveredDate;

    @Column(name = "delivered_comment")
    private String deliveredComment;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "PUBLICATIONS_APPROVER_COMMENTS")
    private List<ApproverComment> approverComments;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "PUBLICATIONS_COST_ESTIMATIONS")
    private List<CostElement> costEstimations;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "PUBLICATIONS_ACTUAL_COSTINGS")
    private List<CostElement> actualCostings;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "PUBLICATIONS_ATTACHMENTS")
    private List<Document> attachments;

    @Indexable(fieldName = STATUS, type = IndexableDataType.TEXT)
    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private PublicationStatus status;

    @Indexable(fieldName = CREATED_BY, type = IndexableDataType.TEXT)
    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "total_estimated_cost")
    private Double totalEstimatedCost;

    @Column(name = "total_actual_cost")
    private Double totalActualCost;

    @Column(name = "pub_status_json", length = 10000)
    private String publicationStatusJson;

    @Indexable(fieldName = RELATED_PROG, type = IndexableDataType.TEXT)
    private String relatedPrograme;

    @Indexable(fieldName = AUTHOR, type = IndexableDataType.TEXT)
    private String author;

    @Indexable(fieldName = ISBN_ISSN, type = IndexableDataType.TEXT)
    private String isbn_issn;

    /**
     * <p>
     * Get Year from date.
     * </p>
     *
     * @return year
     *
     */
    @Indexable(fieldName = YEAR, type = IndexableDataType.INT)
    public int getYear()
    {
        if (requiredDeliveryDate != null)
        {
            return this.requiredDeliveryDate.getYear();
        }
        return 2015;
    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.domain.BaseModel#getId()
     */
    @Override
    public Long getId()
    {
        return this.id;
    }

    /**
     * <p>
     * Getter for typeOfWork.
     * </p>
     * 
     * @return the typeOfWork
     */
    public String getTypeOfWork()
    {
        return typeOfWork;
    }

    /**
     * <p>
     * Setting value for typeOfWork.
     * </p>
     * 
     * @param typeOfWork
     *            the typeOfWork to set
     */
    public void setTypeOfWork(String typeOfWork)
    {
        this.typeOfWork = typeOfWork;
    }

    /**
     * <p>
     * Getter for descOfWork.
     * </p>
     * 
     * @return the descOfWork
     */
    public String getDescOfWork()
    {
        return descOfWork;
    }

    /**
     * <p>
     * Setting value for descOfWork.
     * </p>
     * 
     * @param descOfWork
     *            the descOfWork to set
     */
    public void setDescOfWork(String descOfWork)
    {
        this.descOfWork = descOfWork;
    }

    /**
     * <p>
     * Getter for printRun.
     * </p>
     * 
     * @return the printRun
     */
    public String getPrintRun()
    {
        return printRun;
    }

    /**
     * <p>
     * Setting value for printRun.
     * </p>
     * 
     * @param printRun
     *            the printRun to set
     */
    public void setPrintRun(String printRun)
    {
        this.printRun = printRun;
    }

    /**
     * <p>
     * Getter for projectCode.
     * </p>
     * 
     * @return the projectCode
     */
    public String getProjectCode()
    {
        return projectCode;
    }

    /**
     * <p>
     * Setting value for projectCode.
     * </p>
     * 
     * @param projectCode
     *            the projectCode to set
     */
    public void setProjectCode(String projectCode)
    {
        this.projectCode = projectCode;
    }

    /**
     * <p>
     * Getter for requiredDeliveryDate.
     * </p>
     * 
     * @return the requiredDeliveryDate
     */
    public LocalDate getRequiredDeliveryDate()
    {
        return requiredDeliveryDate;
    }

    /**
     * <p>
     * Setting value for requiredDeliveryDate.
     * </p>
     * 
     * @param requiredDeliveryDate
     *            the requiredDeliveryDate to set
     */
    public void setRequiredDeliveryDate(LocalDate requiredDeliveryDate)
    {
        this.requiredDeliveryDate = requiredDeliveryDate;
    }

    /**
     * <p>
     * Getter for otherInfo.
     * </p>
     * 
     * @return the otherInfo
     */
    public String getOtherInfo()
    {
        return otherInfo;
    }

    /**
     * <p>
     * Setting value for otherInfo.
     * </p>
     * 
     * @param otherInfo
     *            the otherInfo to set
     */
    public void setOtherInfo(String otherInfo)
    {
        this.otherInfo = otherInfo;
    }

    /**
     * <p>
     * Getter for requesterName.
     * </p>
     * 
     * @return the requesterName
     */
    public String getRequesterName()
    {
        return requesterName;
    }

    /**
     * <p>
     * Setting value for requesterName.
     * </p>
     * 
     * @param requesterName
     *            the requesterName to set
     */
    public void setRequesterName(String requesterName)
    {
        this.requesterName = requesterName;
    }

    /**
     * <p>
     * Getter for division.
     * </p>
     * 
     * @return the division
     */
    public String getDivision()
    {
        return division;
    }

    /**
     * <p>
     * Setting value for division.
     * </p>
     * 
     * @param division
     *            the division to set
     */
    public void setDivision(String division)
    {
        this.division = division;
    }

    /**
     * <p>
     * Getter for supervisorName.
     * </p>
     * 
     * @return the supervisorName
     */
    public String getSupervisorName()
    {
        return supervisorName;
    }

    /**
     * <p>
     * Setting value for supervisorName.
     * </p>
     * 
     * @param supervisorName
     *            the supervisorName to set
     */
    public void setSupervisorName(String supervisorName)
    {
        this.supervisorName = supervisorName;
    }

    /**
     * <p>
     * Getter for status.
     * </p>
     * 
     * @return the status
     */
    public PublicationStatus getStatus()
    {
        return status;
    }

    /**
     * <p>
     * Setting value for status.
     * </p>
     * 
     * @param status
     *            the status to set
     */
    public void setStatus(PublicationStatus status)
    {
        this.status = status;
    }

    /**
     * <p>
     * Getter for approvedDate.
     * </p>
     * 
     * @return the approvedDate
     */
    public LocalDate getApprovedDate()
    {
        return approvedDate;
    }

    /**
     * <p>
     * Setting value for approvedDate.
     * </p>
     * 
     * @param approvedDate
     *            the approvedDate to set
     */
    public void setApprovedDate(LocalDate approvedDate)
    {
        this.approvedDate = approvedDate;
    }

    /**
     * <p>
     * Getter for jobNumber.
     * </p>
     * 
     * @return the jobNumber
     */
    public String getJobNumber()
    {
        return jobNumber;
    }

    /**
     * <p>
     * Setting value for jobNumber.
     * </p>
     * 
     * @param jobNumber
     *            the jobNumber to set
     */
    public void setJobNumber(String jobNumber)
    {
        this.jobNumber = jobNumber;
    }

    /**
     * <p>
     * Getter for type.
     * </p>
     * 
     * @return the type
     */
    public String getType()
    {
        return type;
    }

    /**
     * <p>
     * Setting value for type.
     * </p>
     * 
     * @param type
     *            the type to set
     */
    public void setType(String type)
    {
        this.type = type;
    }

    /**
     * <p>
     * Getter for series.
     * </p>
     * 
     * @return the series
     */
    public String getSeries()
    {
        return series;
    }

    /**
     * <p>
     * Setting value for series.
     * </p>
     * 
     * @param series
     *            the series to set
     */
    public void setSeries(String series)
    {
        this.series = series;
    }

    /**
     * <p>
     * Getter for title.
     * </p>
     * 
     * @return the title
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * <p>
     * Setting value for title.
     * </p>
     * 
     * @param title
     *            the title to set
     */
    public void setTitle(String title)
    {
        this.title = title;
    }

    /**
     * <p>
     * Getter for approverComments.
     * </p>
     * 
     * @return the approverComments
     */
    public List<ApproverComment> getApproverComments()
    {
        if (approverComments == null)
        {
            approverComments = new ArrayList<ApproverComment>();
        }
        return approverComments;
    }

    /**
     * <p>
     * Setting value for approverComments.
     * </p>
     * 
     * @param approverComments
     *            the approverComments to set
     */
    public void setApproverComments(List<ApproverComment> approverComments)
    {
        this.approverComments = approverComments;
    }

    /**
     * <p>
     * Getter for costEstimations.
     * </p>
     * 
     * @return the costEstimations
     */
    public List<CostElement> getCostEstimations()
    {
        if (costEstimations == null)
        {
            costEstimations = new ArrayList<CostElement>();
        }
        return costEstimations;
    }

    /**
     * <p>
     * Setting value for costEstimations.
     * </p>
     * 
     * @param costEstimations
     *            the costEstimations to set
     */
    public void setCostEstimations(List<CostElement> costEstimations)
    {
        this.costEstimations = costEstimations;
    }

    /**
     * <p>
     * Getter for actualCostings.
     * </p>
     * 
     * @return the actualCostings
     */
    public List<CostElement> getActualCostings()
    {
        if (actualCostings == null)
        {
            actualCostings = new ArrayList<CostElement>();
        }
        return actualCostings;
    }

    /**
     * <p>
     * Setting value for actualCostings.
     * </p>
     * 
     * @param actualCostings
     *            the actualCostings to set
     */
    public void setActualCostings(List<CostElement> actualCostings)
    {
        this.actualCostings = actualCostings;
    }

    /**
     * <p>
     * Getter for completionDate.
     * </p>
     * 
     * @return the completionDate
     */
    public LocalDate getCompletionDate()
    {
        return completionDate;
    }

    /**
     * <p>
     * Setting value for completionDate.
     * </p>
     * 
     * @param completionDate
     *            the completionDate to set
     */
    public void setCompletionDate(LocalDate completionDate)
    {
        this.completionDate = completionDate;
    }

    /**
     * <p>
     * Getter for attachments.
     * </p>
     * 
     * @return the attachments
     */
    public List<Document> getAttachments()
    {
        if (attachments == null)
        {
            attachments = new ArrayList<Document>();
        }
        return attachments;
    }

    /**
     * <p>
     * Setting value for attachments.
     * </p>
     * 
     * @param attachments
     *            the attachments to set
     */
    public void setAttachments(List<Document> attachments)
    {
        this.attachments = attachments;
    }

    /**
     * <p>
     * Getter for createdBy.
     * </p>
     * 
     * @return the createdBy
     */
    public String getCreatedBy()
    {
        return createdBy;
    }

    /**
     * <p>
     * Setting value for createdBy.
     * </p>
     * 
     * @param createdBy
     *            the createdBy to set
     */
    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    public LocalDate getDeliveredDate()
    {
        return deliveredDate;
    }

    public void setDeliveredDate(LocalDate deliveredDate)
    {
        this.deliveredDate = deliveredDate;
    }

    public String getDeliveredComment()
    {
        return deliveredComment;
    }

    public void setDeliveredComment(String deliveredComment)
    {
        this.deliveredComment = deliveredComment;
    }

    /**
     * <p>
     * Getter for publicationStatusJson.
     * </p>
     * 
     * @return the publicationStatusJson
     */
    public String getPublicationStatusJson()
    {
        return publicationStatusJson;
    }

    /**
     * <p>
     * Setting value for publicationStatusJson.
     * </p>
     * 
     * @param publicationStatusJson
     *            the publicationStatusJson to set
     */
    public void setPublicationStatusJson(String publicationStatusJson)
    {
        this.publicationStatusJson = publicationStatusJson;
    }

    /**
     * <p>
     * Getter for totalEstimatedCost.
     * </p>
     * 
     * @return the totalEstimatedCost
     */
    public Double getTotalEstimatedCost()
    {
        return totalEstimatedCost;
    }

    /**
     * <p>
     * Setting value for totalEstimatedCost.
     * </p>
     * 
     * @param totalEstimatedCost
     *            the totalEstimatedCost to set
     */
    public void setTotalEstimatedCost(Double totalEstimatedCost)
    {
        this.totalEstimatedCost = totalEstimatedCost;
    }

    /**
     * <p>
     * Getter for totalActualCost.
     * </p>
     * 
     * @return the totalActualCost
     */
    public Double getTotalActualCost()
    {
        return totalActualCost;
    }

    /**
     * <p>
     * Setting value for totalActualCost.
     * </p>
     * 
     * @param totalActualCost
     *            the totalActualCost to set
     */
    public void setTotalActualCost(Double totalActualCost)
    {
        this.totalActualCost = totalActualCost;
    }

    /**
     * <p>
     * Getter for author.
     * </p>
     * 
     * @return the author
     */
    public String getAuthor()
    {
        return author;
    }

    /**
     * <p>
     * Setting value for author.
     * </p>
     * 
     * @param author
     *            the author to set
     */
    public void setAuthor(String author)
    {
        this.author = author;
    }

    /**
     * <p>
     * Getter for isbn_issn.
     * </p>
     * 
     * @return the isbn_issn
     */
    public String getIsbn_issn()
    {
        return isbn_issn;
    }

    /**
     * <p>
     * Setting value for isbn_issn.
     * </p>
     * 
     * @param isbn_issn
     *            the isbn_issn to set
     */
    public void setIsbn_issn(String isbn_issn)
    {
        this.isbn_issn = isbn_issn;
    }

    /**
     * <p>
     * Getter for relatedPrograme.
     * </p>
     * 
     * @return the relatedPrograme
     */
    public String getRelatedPrograme()
    {
        return relatedPrograme;
    }

    /**
     * <p>
     * Setting value for relatedPrograme.
     * </p>
     * 
     * @param relatedPrograme
     *            the relatedPrograme to set
     */
    public void setRelatedPrograme(String relatedPrograme)
    {
        this.relatedPrograme = relatedPrograme;
    }

}

/*
 * FILENAME
 *     SearchEngineManager.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.util;

import static java.util.Collections.emptyList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hashcode.iwmi.publications.domain.Searchable;
import com.hashcode.solrjx.util.SolrDocumentParser;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Solr search engine manager.
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
public final class SearchEngineManager
{
    private final static Logger log = LoggerFactory.getLogger(SearchEngineManager.class);
    private final static String SOLR_SERVER_URL = ApplicationUtils.getConfiguration("solr.server.url");

    public static final String ID = "id";
    public static final String TYPE_OF_WORK = "type_of_work";
    public static final String DESC_OF_WORK = "desc_of_work";
    public static final String PRINT_RUN = "print_run";
    public static final String PROJECT_CODE = "project_code";
    public static final String REQUESTER = "requester";
    public static final String DIVISION = "division";
    public static final String SUPERVISOR_NAME = "supervisorName";
    public static final String JOB_NUMBER = "job_number";
    public static final String TYPE = "type";
    public static final String SERIES = "series";
    public static final String TITLE = "title";
    public static final String YEAR = "year";
    public static final String AUTHOR = "author";
    public static final String STATUS = "status";
    public static final String RELATED_PROG = "related_prog";
    public static final String ISBN_ISSN = "isbn_issn";
    public static final String COMPLTETION_DATE = "completion_date";
    public static final String OTHER_INFO = "other_info";
    public static final String CREATED_BY = "created_by";

    /**
     * <p>
     * Get Solr server connection.
     * </p>
     *
     * @return {@link HttpSolrClient}
     */
    private static HttpSolrClient getConnection() throws Exception
    {
        try
        {
            HttpSolrClient client = new HttpSolrClient(SOLR_SERVER_URL);
            log.info("SOLR Server connection : {} ", client.ping().getStatus());
            return client;
        }
        catch (Exception e)
        {
            log.error("Error encountered in connecting to Solr");
        }
        return null;
    }

    public static <T> void indexer(T data)
    {
        try
        {
            log.debug("Solr Indexer : indexing single object");
            HttpSolrClient client = getConnection();
            if (client == null)
            {
                log.warn("No search server configured");
                return;
            }

            if (data instanceof Searchable)
            {
                List<SolrInputDocument> docs = SolrDocumentParser.parseDocs(Arrays.asList(data));
                client.add(docs);
                client.commit();
                log.debug("Solr Indexer indexed.");
            }
        }
        catch (Exception e)
        {
            log.error("Error encountered in indexing data, [Object : {}]", data, e);
        }
    }

    public static <T> void indexer(List<T> dataList)
    {
        try
        {
            log.debug("Solr Indexer : indexing...");
            HttpSolrClient client = getConnection();
            if (client == null)
            {
                log.warn("No search server configured");
                return;
            }
            List<SolrInputDocument> docs = SolrDocumentParser.parseDocs(dataList);
            client.add(docs);
            client.commit();
            log.debug("Solr Indexer indexed. [ No items : {}]", docs.size());
        }
        catch (Exception e)
        {
            log.error("Error encountered in indexing data,", dataList.size(), e);
        }
    }

    public static <T> void deleteIndex(Long id)
    {
        try
        {
            log.debug("Solr remove from index [ Id :{}]", id);
            HttpSolrClient client = getConnection();
            if (client == null)
            {
                log.warn("No search server configured");
                return;
            }
            client.deleteById(String.valueOf(id));
            client.commit();
            log.debug("Solr index removed.");
        }
        catch (Exception e)
        {
            log.error("Error encountered in deleting data,", e);
        }
    }

    public static <T> void delete(List<Long> ids) throws Exception
    {
        try
        {
            log.debug("Solr remove from index [ Ids :{}]", ids);
            HttpSolrClient client = getConnection();
            if (client == null)
            {
                log.warn("No search server configured");
                return;
            }
            ids.stream().forEach((n) -> {
                try
                {
                    client.deleteById(String.valueOf(n));
                }
                catch (Exception e)
                {
                    log.error("Error encountered in removing index :", e);
                }

            });

            client.commit();
            log.debug("Solr index removed.");
        }
        catch (Exception e)
        {
            log.error("Error encountered in deleting data,", e);
        }
    }

    /**
     * <p>
     * Search in solr.
     * </p>
     *
     * @param query
     *            query to search
     * @param filterQuery
     *            filter query by fields
     * @return list of Ids
     */
    public static List<Long> search(String query, String[] filterQuery)
    {
        List<Long> ids = new ArrayList<>();
        try
        {
            HttpSolrClient client = getConnection();
            if (client == null)
            {
                log.warn("No search server configured");
                return emptyList();
            }
            SolrQuery solrQuery = new SolrQuery();
            solrQuery.setQuery(query);
            solrQuery.addFilterQuery(filterQuery);
            solrQuery.setFields(ID);
            solrQuery.setSort(ID, ORDER.asc);
            solrQuery.setStart(0);

            log.info("Solr Query : {} ", solrQuery.toString());
            
            QueryResponse response = client.query(solrQuery);
            SolrDocumentList results = response.getResults();
            for (int i = 0; i < results.size(); ++i)
            {
               ids.add(Long.valueOf((String)results.get(i).getFieldValue(ID)));
            }
            log.info("Solr Ids: {} ", ids);
        }
        catch (Exception e)
        {
            log.error("Error encountered in Searching data,", e);
        }
        return ids;
    }

}

/*
 * FILENAME
 *     CostEstimationDoneStateChangeListener.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.listeners;

import static com.hashcode.iwmi.publications.domain.PublicationStatus.ESTIMATED;
import static com.hashcode.iwmi.publications.domain.PublicationStatus.INITIALISED;
import static com.hashcode.iwmi.publications.domain.PublicationStatus.NEED_AN_AMENDMENT;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.net.URLEncoder;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.hashcode.iwmi.publications.domain.MessageTemplate;
import com.hashcode.iwmi.publications.domain.MessageTemplate.BodyTag;
import com.hashcode.iwmi.publications.domain.MessageTemplate.SubjectTag;
import com.hashcode.iwmi.publications.domain.PublicationItem;
import com.hashcode.iwmi.publications.domain.SystemUser;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;
import com.hashcode.iwmi.publications.exports.CostEstimationReportPDF;
import com.hashcode.iwmi.publications.services.PublicationItemService;
import com.hashcode.iwmi.publications.services.SystemUserService;
import com.hashcode.iwmi.publications.util.ApplicationUtils;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Cost Estimation Done State change listener.
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
@Service
@Qualifier(value = "costEstimationStateChange")
public class CostEstimationDoneStateChangeListener extends AbstractPublicationStateChangeListener
{
    private static final Logger log = LoggerFactory.getLogger(CostEstimationDoneStateChangeListener.class);

    @Autowired
    private PublicationItemService publicationItemService;

    @Autowired
    private SystemUserService systemUserService;
    
    @Autowired
    private CostEstimationReportPDF costEstimationReport;

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.listeners.PublicationStateChangeListener#onStateChange(java.lang.String,
     *      com.hashcode.iwmi.publications.domain.PublicationItem)
     */
    @Transactional
    @Override
    public void onStateChange(String currentUser, PublicationItem publicationItem) throws EPublicationException
    {
        log.debug("Cost Estimation Done State changed, [ User : {}, Publication Item id : {}] ", currentUser,
            publicationItem.getId());

        if (publicationItem.getStatus() == INITIALISED || publicationItem.getStatus() == NEED_AN_AMENDMENT)
        {
            publicationItem.setStatus(ESTIMATED);
            publicationItemService.statusUpdate(publicationItem);

        }

        Map<MessageTemplate.SubjectTag, String> subjectMap = new HashMap<MessageTemplate.SubjectTag, String>();
        subjectMap.put(SubjectTag.SUBJECT_JOB_ID, publicationItem.getJobNumber());
        subjectMap.put(SubjectTag.SUBJECT_TITLE, publicationItem.getProjectCode());
        subjectMap.put(SubjectTag.SUBJECT_OTHER_TEXT_1, publicationItem.getTypeOfWork());

        SystemUser user = systemUserService.findUser(publicationItem.getCreatedBy());
        Map<MessageTemplate.BodyTag, Object> bodyMap = new HashMap<MessageTemplate.BodyTag, Object>();
        bodyMap.put(BodyTag.BODY_JOB_ID, publicationItem.getJobNumber());
        bodyMap.put(BodyTag.BODY_TO_NAME, user.getFirstName());
        bodyMap.put(BodyTag.BODY_LINK_PARAM_1, linkURL + "/costEstimation/page?id=" + publicationItem.getId());
        bodyMap.put(BodyTag.DATE, LocalDate.now(ZoneId.systemDefault()));
        bodyMap.put(BodyTag.DATETIME, LocalDateTime.now(ZoneId.systemDefault()));

        sendEmailAndNotifyOnDashboard(publicationItem, MessageTemplate.Type.AFTER_COST_ESITMATION_TO_USER, subjectMap,
            bodyMap, user);

        String costEstimationFileName = "cost_estimation_" +publicationItem.getJobNumber() + "_" + ".pdf";
        ByteArrayOutputStream exportReportStream = costEstimationReport.generateReport(publicationItem.getId());
        
        try (FileChannel rwChannel =
            new RandomAccessFile(exportFileLocation + File.separator + costEstimationFileName, "rw").getChannel();
            OutputStream out = Channels.newOutputStream(rwChannel))
        {
            out.write(exportReportStream.toByteArray());
            out.flush();
            
            SystemUser supervisor = systemUserService.findUser(publicationItem.getSupervisorName());
            // <AP/REJ>;<item_ID>;<user_email>;<length>;
            String approvedCode = "AP;" + publicationItem.getId() + ";" + supervisor.getEmail() + ";";
            approvedCode += approvedCode + approvedCode.length() + ";";
            
            String rejectedCode = "REJ;" + publicationItem.getId() + ";" + supervisor.getEmail() + ";";
            rejectedCode += rejectedCode + rejectedCode.length() + ";";
            
            String approvedEncryptedCode = ApplicationUtils.encryptPlainText(approvedCode);
            String rejectedEncryptedCode = ApplicationUtils.encryptPlainText(rejectedCode);
            bodyMap.put(BodyTag.BODY_TO_NAME, supervisor.getFirstName());
            bodyMap.put(BodyTag.BODY_CONTENT_SET_1, supervisor.getFirstName());
            bodyMap.put(BodyTag.BODY_CONTENT_SET_2,  linkURL + "/approval/directApprove/" + approvedEncryptedCode);
            bodyMap.put(BodyTag.BODY_CONTENT_SET_3, linkURL +  "/approval/directApprove/" + rejectedEncryptedCode);
            bodyMap.put(BodyTag.BODY_CONTENT_SET_4, exportReportUrl + URLEncoder.encode(costEstimationFileName, "UTF-8"));
//            sendEmailAndNotifyOnDashboard(publicationItem, MessageTemplate.Type.AFTER_COST_ESITMATION_TO_SUPERVISOR,
//                subjectMap, bodyMap, supervisor);
            
            bodyMap.put(BodyTag.BODY_LINK_PARAM_1, linkURL + "/approval/page?id=" + publicationItem.getId());
            sendEmailAndNotifyOnDashboard(publicationItem, MessageTemplate.Type.TO_JOB_APPROVAL, subjectMap, bodyMap, supervisor);

        }
        catch (IOException e)
        {
            log.error("Error occurred during export report writing to file ", e);
        }
        auditService.addAudit(currentUser,
            String.format("Cost estimation completed, Publication Item Id : %d", publicationItem.getId()));

        log.info("Cost Estimation  State changed done, [ User : {}, Publication Item id : {}] ", currentUser,
            publicationItem.getId());
    }

}

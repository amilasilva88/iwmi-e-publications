/*
 * FILENAME
 *     AuditLogServiceImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.services.impl;

import java.time.LocalDateTime;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hashcode.iwmi.publications.dao.AuditLogDao;
import com.hashcode.iwmi.publications.domain.AuditLog;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;
import com.hashcode.iwmi.publications.services.AuditLogService;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Audit Log Service Implementation
 * </p>
 *
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
@Service
@Transactional
public class AuditLogServiceImpl implements AuditLogService
{

    private static final Logger log = LoggerFactory.getLogger(AuditLogServiceImpl.class);

    @Autowired
    private AuditLogDao auditlogDao;

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.services.AuditLogService#addAudit(java.lang.String, java.lang.String)
     */
    @Override
    public Long addAudit(String user, String userAction) throws EPublicationException
    {
        try
        {
            AuditLog audit = new AuditLog();
            audit.setUsername(user);
            audit.setUserAction(userAction);
            auditlogDao.create(audit);
            log.debug("User action added to the Audit Log");

            return audit.getId();
        }
        catch (Exception e)
        {
            throw new EPublicationException("Error occurred while creating audit log", e);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.services.AuditLogService#findLastNDaysLogs(java.lang.String, int)
     */
    @Override
    public List<AuditLog> findLastNDaysLogs(String userName, int noOfDaysBack) throws EPublicationException
    {
        log.debug("find Lastest Audit Logs upto {} days for user :{}", noOfDaysBack, userName);

        LocalDateTime fromDate = LocalDateTime.now().withHour(0).withMinute(0).withSecond(0).minusDays(noOfDaysBack);
        LocalDateTime toDate = LocalDateTime.now().withHour(0).withMinute(0).withSecond(0).plusDays(1);

        List<AuditLog> auditLogs = auditlogDao.findLastNDaysActions(userName, fromDate, toDate);

        if (auditLogs.isEmpty())
            log.info("No Audit logs found for user :{}, No of days :{}", userName, noOfDaysBack);
        else
            log.debug("Audit logs found for user :{}, auditLogs :{}", userName, auditLogs.size());

        return auditLogs;
    }

}

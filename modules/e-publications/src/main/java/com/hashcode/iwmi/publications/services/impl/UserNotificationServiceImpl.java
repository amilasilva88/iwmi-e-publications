/*
 * FILENAME
 *     UserNotificationServiceImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.services.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hashcode.iwmi.publications.dao.UserNotificationDao;
import com.hashcode.iwmi.publications.domain.UserNotification;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;
import com.hashcode.iwmi.publications.services.UserNotificationService;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * User Notification implementation.
 * </p>
 *
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
@Service
public class UserNotificationServiceImpl implements UserNotificationService
{
    private static final Logger log = LoggerFactory.getLogger(UserNotificationServiceImpl.class);

    @Autowired
    private UserNotificationDao userNotificationDao;

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.services.UserNotificationService#addUserNotification(com.hashcode.iwmi.publications.domain.UserNotification)
     */
    @Transactional
    @Override
    public Long addUserNotification(UserNotification userNotification) throws EPublicationException
    {
        try
        {
            log.debug("Add new User Notification");
            userNotificationDao.create(userNotification);
            log.debug("New User Notification saved successfully");

            return userNotification.getId();
        }
        catch (Exception e)
        {
            throw new EPublicationException("Error occurred while add new user notification,", e);
        }

    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.services.UserNotificationService#getUserNotificationByUser(java.lang.String,
     *      int)
     */
    @Transactional
    @Override
    public List<UserNotification> findUserNotificationByUser(final String username, final int limit)
        throws EPublicationException
    {
        try
        {
            log.debug("Find User Notifications By user [ User  : {}]", username);
            List<UserNotification> notifications = userNotificationDao.findUserNotificationByUser(username, limit);

            if (notifications.isEmpty())
                log.info("No User notifications found");
            else
                log.info("{} user notifications loaded for {}", notifications.size(), username);
            log.debug("New User Notification saved successfully");

            return notifications;
        }
        catch (Exception e)
        {
            throw new EPublicationException("Error occurred while finding user notification,", e);
        }
    }

}

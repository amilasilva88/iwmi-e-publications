/*
 * FILENAME
 *     LookupDataController.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.controllers;

import static com.hashcode.iwmi.publications.util.ApplicationUtils.createResponse;
import static com.hashcode.iwmi.publications.util.ApplicationUtils.getResponceDataMap;

import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hashcode.iwmi.publications.domain.DivisionsLookup;
import com.hashcode.iwmi.publications.domain.PublicationTypeLookup;
import com.hashcode.iwmi.publications.domain.RequestersLookup;
import com.hashcode.iwmi.publications.domain.SupervisorsLookup;
import com.hashcode.iwmi.publications.services.LookupDataService;
import com.hashcode.iwmi.publications.util.ResponseStatus;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Look up data Controller for retrieve lookup data.
 * </p>
 *
 * @author Amila Silva
 *
 * @version $Id$
 **/
@Controller
@RequestMapping("/lookup")
public class LookupDataController
{
    private static final Logger log = LoggerFactory.getLogger(LookupDataController.class);

    @Autowired
    private LookupDataService lookupDataService;

    @RequestMapping(value = "/loadDivisions", method = {
        RequestMethod.GET
    })
    @ResponseBody
    public String loadDivisions(Model model)
    {
        try
        {
            JSONArray dataArray = new JSONArray();
            log.debug("Request came to fetch division data");
            List<DivisionsLookup> lookups = lookupDataService.findAllLookupData(DivisionsLookup.class);
            lookups.stream().forEachOrdered(n -> {
                JSONObject data = new JSONObject();
                data.put("id", n.getId());
                data.put("display", n.getDisplay());
                data.put("value", n.getValue());
                dataArray.put(data);
            });

            Map<String, Object> params = getResponceDataMap();
            params.put("data", dataArray);

            JSONObject jsonReponse = createResponse(ResponseStatus.SUCCESS, params);
            log.debug("JSON Response for load divisions, Response : {}", jsonReponse);
            return jsonReponse.toString();
        }
        catch (Exception e)
        {
            log.error("Error occurred in load divisions,", e);
            Map<String, Object> params = getResponceDataMap();
            params.put("error", "Error encountered in Division data loading");
            return createResponse(ResponseStatus.FAILED, params).toString();
        }
    }

    @RequestMapping(value = "/loadPublicationTypes", method = {
        RequestMethod.GET
    })
    @ResponseBody
    public String loadPublicationTypes(Model model)
    {
        try
        {
            JSONArray dataArray = new JSONArray();
            log.debug("Request came to fetch publication type data");
            List<PublicationTypeLookup> lookups = lookupDataService.findAllLookupData(PublicationTypeLookup.class);
            lookups.stream().forEachOrdered(n -> {
                JSONObject data = new JSONObject();
                data.put("id", n.getId());
                data.put("display", n.getDisplay());
                data.put("value", n.getValue());
                dataArray.put(data);
            });

            Map<String, Object> params = getResponceDataMap();
            params.put("data", dataArray);

            JSONObject jsonReponse = createResponse(ResponseStatus.SUCCESS, params);
            log.debug("JSON Response for load publication types, Response : {}", jsonReponse);
            return jsonReponse.toString();
        }
        catch (Exception e)
        {
            log.error("Error occurred in load publication types,", e);
            Map<String, Object> params = getResponceDataMap();
            params.put("error", "Error encountered in Publication Types data loading");
            return createResponse(ResponseStatus.FAILED, params).toString();
        }
    }

    @RequestMapping(value = "/loadRequesters", method = {
        RequestMethod.GET
    })
    @ResponseBody
    public String loadRequesters(Model model)
    {
        try
        {
            JSONArray dataArray = new JSONArray();
            log.debug("Request came to fetch Requesters data");
            List<RequestersLookup> lookups = lookupDataService.findAllLookupData(RequestersLookup.class);
            lookups.stream().forEachOrdered(n -> {
                JSONObject data = new JSONObject();
                data.put("id", n.getId());
                data.put("display", n.getDisplay());
                data.put("value", n.getValue());
                dataArray.put(data);
            });

            Map<String, Object> params = getResponceDataMap();
            params.put("data", dataArray);

            JSONObject jsonReponse = createResponse(ResponseStatus.SUCCESS, params);
            log.debug("JSON Response for load Requesters , Response : {}", jsonReponse);
            return jsonReponse.toString();
        }
        catch (Exception e)
        {
            log.error("Error occurred in load Requesters,", e);
            Map<String, Object> params = getResponceDataMap();
            params.put("error", "Error encountered in Requesters data loading");
            return createResponse(ResponseStatus.FAILED, params).toString();
        }
    }

    @RequestMapping(value = "/loadSupervisors", method = {
        RequestMethod.GET
    })
    @ResponseBody
    public String loadSupervisors(Model model)
    {
        try
        {
            JSONArray dataArray = new JSONArray();
            log.debug("Request came to fetch supervisors data");
            List<SupervisorsLookup> lookups = lookupDataService.findAllLookupData(SupervisorsLookup.class);
            lookups.stream().forEachOrdered(n -> {
                JSONObject data = new JSONObject();
                data.put("id", n.getId());
                data.put("display", n.getDisplay());
                data.put("value", n.getValue());
                dataArray.put(data);
            });

            Map<String, Object> params = getResponceDataMap();
            params.put("data", dataArray);

            JSONObject jsonReponse = createResponse(ResponseStatus.SUCCESS, params);
            log.debug("JSON Response for load supervisors , Response : {}", jsonReponse);
            return jsonReponse.toString();
        }
        catch (Exception e)
        {
            log.error("Error occurred in load supervisors,", e);
            Map<String, Object> params = getResponceDataMap();
            params.put("error", "Error encountered in Requesters data loading");
            return createResponse(ResponseStatus.FAILED, params).toString();
        }
    }

}

/*
 * FILENAME
 *     DocumentServiceImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.services.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hashcode.iwmi.publications.dao.DocumentDao;
import com.hashcode.iwmi.publications.domain.Document;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;
import com.hashcode.iwmi.publications.services.DocumentService;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * DocumentService Implementation.
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
@Service
@Transactional
public class DocumentServiceImpl implements DocumentService
{

    private static final Logger log = LoggerFactory.getLogger(DocumentServiceImpl.class);

    @Autowired
    private DocumentDao documentDao;

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.services.DocumentService#saveDocument(com.hashcode.iwmi.publications.domain.Document)
     */
    @Override
    public Long saveDocument(Document document) throws EPublicationException
    {
        try
        {
            log.debug("Save Document");
            documentDao.create(document);

            log.debug("Document saved successfully, [ Id:{}]", document.getId());
            return document.getId();
        }
        catch (Exception e)
        {
            throw new EPublicationException("Error occurred while saving the document.", e);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.services.DocumentService#deleteDocument(com.hashcode.iwmi.publications.domain.Document)
     */
    @Override
    public void deleteDocument(Document document) throws EPublicationException
    {
        try
        {
            log.debug("Delete Document");
            documentDao.delete(document);

            log.debug("Document deleted successfully, [ Id:{}]", document.getId());
        }
        catch (Exception e)
        {
            throw new EPublicationException("Error occurred while deleting the document.", e);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.services.DocumentService#deleteDocumentsAttachedToPublication(java.lang.Long)
     */
    @Override
    public void deleteDocumentsAttachedToPublication(Long publicationId) throws EPublicationException
    {
        try
        {
            log.debug("Save Document");

            List<Document> documents = documentDao.findDocumentsByPublicationItem(publicationId);

            documents.stream().forEach(d -> {
                documentDao.delete(d);
                //TODO remove from the disk
            });

            log.debug("Documents deleted successfully, [ Publication Id:{}]", publicationId);
        }
        catch (Exception e)
        {
            throw new EPublicationException("Error occurred while deleting the documents for publication id.", e);
        }

    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.services.DocumentService#findById(java.lang.Long)
     */
    @Override
    public Document findById(Long fileId) throws EPublicationException
    {
        try
        {
            log.debug("Find a Document Id: {}", fileId);
            Document document = documentDao.findById(fileId);

            log.debug("Find a Document Id: {}", document.getFileName());
            return document;
        }
        catch (Exception e)
        {
            throw new EPublicationException("Error occurred while find the document by id.", e);
        }

    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.services.DocumentService#findDocumentsByPublicationItem(java.lang.Long)
     */
    @Override
    public List<Document> findDocumentsByPublicationItem(Long publicationId) throws EPublicationException
    {
        try
        {
            log.debug("Find documents by publication item, [Id : {}]", publicationId);

            List<Document> documents = documentDao.findDocumentsByPublicationItem(publicationId);
            log.debug("No of attached documents for publication Item , [ fileName: {}, Docs  : {}]", publicationId,
                documents.size());
            return documents;
        }
        catch (Exception e)
        {
            throw new EPublicationException("Error occurred while find documents by Publication item.", e);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.services.DocumentService#findByFileName(java.lang.String)
     */
    @Override
    public List<Document> findByFileName(String fileName) throws EPublicationException
    {
        try
        {
            log.debug("Find documents by file name, [ Name : {}]", fileName);

            List<Document> documents = documentDao.findByFileName(fileName);
            log.debug("No of attached documents by filename, [ fileName: {}, Docs  : {}]", fileName, documents.size());
            return documents;
        }
        catch (Exception e)
        {
            throw new EPublicationException("Error occurred while find documents by Publication item.", e);
        }
    }

}

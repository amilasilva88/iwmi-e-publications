/*
 * FILENAME
 *     TypeSettingCost.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Cost Element for Type Setting
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 **/
@Entity
@Table(name = "TYPE_SETTING_COST")
public class TypeSettingCost extends CostElement
{

    private static final long serialVersionUID = -5523813859939887533L;

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.domain.CostElement#getType()
     */
    @Override
    public String getType()
    {
        return "TYPE_SETTING_COST";
    }

}

/*
 * FILENAME
 *     UserDataController.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.transaction.Transactional;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hashcode.iwmi.publications.domain.SystemUser;
import com.hashcode.iwmi.publications.domain.UserRole;
import com.hashcode.iwmi.publications.domain.UserRoleDto;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;
import com.hashcode.iwmi.publications.services.SystemUserService;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * User data controller.
 * </p>
 *
 * @author Manuja
 *
 * @version $Id$
 */
@Controller
public class UserDataController
{
    private static final Logger log = LoggerFactory.getLogger(UserDataController.class);

    @Autowired
    private SystemUserService userService;

    @Transactional
    @RequestMapping(value = "/editUser", method = {
        RequestMethod.GET
    })
    public String editUser(@RequestParam String userId, Model model) throws NumberFormatException,
        EPublicationException
    {
        log.info("Request came to edit user {}", userId);
        SystemUser systemUser = userService.findById(Long.parseLong(userId));
        model.addAttribute("systemUser", systemUser);

        List<UserRole> userSelectedRoles = systemUser.getUserRoles();
        List<UserRoleDto> optionRoles = new ArrayList<UserRoleDto>();
        for (UserRole userRole : UserRole.values())
        {
            UserRoleDto userRoleDto = new UserRoleDto();
            userRoleDto.setUserRole(userRole);
            userRoleDto.setSelected(userSelectedRoles.contains(userRole));
            optionRoles.add(userRoleDto);
        }
        model.addAttribute("optionRoles", optionRoles);
        return "edit-user";
    }

    @Transactional
    @RequestMapping(value = "/saveUserForm", method = {
        RequestMethod.POST
    })
    public String saveUser(@RequestParam String userId, @RequestParam String email, @RequestParam String firstName,
        @RequestParam String lastName, @RequestParam UserRole[] userRoles, Model model)
    {
        log.debug("Request came to save user id : {} ", userId);
        try
        {
            SystemUser systemUser = userService.findById(Long.parseLong(userId));
            systemUser.setEmail(email);
            systemUser.setFirstName(firstName);
            systemUser.setLastName(lastName);
            List<UserRole> userRolesList = new ArrayList<UserRole>(Arrays.asList(userRoles));
            systemUser.setUserRoles(userRolesList);

            userService.updateUser(systemUser);

            model.addAttribute("message", "Successfully Saved");
            log.info("Saved user, [ Id : {} ] ", systemUser.getId());
        }
        catch (Exception e)
        {
            log.error("Error occurred during user saving, ", e);
            model.addAttribute("message", "Error occurred during the save");
        }
        return "search-users";
    }

    @Transactional
    @RequestMapping(value = "/checkEmail", method = {
        RequestMethod.POST
    })
    @ResponseBody
    public String checkEmail(@RequestBody String requestString) throws EPublicationException
    {
        log.info("Check email request received {}", requestString);
        JSONObject emailJson = new JSONObject(requestString);
        String email = emailJson.getString("email");
        String userId = emailJson.getString("userId");

        SystemUser user = userService.findUserByEmail(email);
        JSONObject userData = new JSONObject();
        if (user == null || user.getId().equals(Long.parseLong(userId)))
        {
            userData.put("status", "true");
        }
        else
        {
            userData.put("status", "false");
        }

        log.info("Response for checke mail {}", userData.toString());

        return userData.toString();
    }

    @RequestMapping("/searchUsersForm")
    public String userForm(Model model)
    {
        log.debug("Request came to add user");
        return "search-users";
    }

    @Transactional
    @RequestMapping(value = "/searchUsers", method = {
        RequestMethod.POST
    })
    @ResponseBody
    public String searchUsers(@RequestBody String searchCriteria) throws EPublicationException
    {
        log.info("Search users");
        return searchUsersRepository();
    }

    @Transactional
    @RequestMapping(value = "/deleteUser", method = {
        RequestMethod.POST
    })
    @ResponseBody
    public String deleteUser(@RequestBody String searchCriteria) throws EPublicationException
    {
        log.info("Delete users");
        JSONObject searchCriteriaJson = new JSONObject(searchCriteria);
        Long idToDelete = searchCriteriaJson.getLong("id");

        SystemUser user = userService.findById(idToDelete);
        userService.deleteUser(user);
        userService.findAll();

        return searchUsersRepository();
    }

    private String searchUsersRepository() throws EPublicationException
    {
        List<SystemUser> users = userService.findAll();
        JSONArray userArray = new JSONArray();
        users.stream().forEach((level) -> {
            JSONObject userData = new JSONObject();
            userData.put("id", level.getId());
            userData.put("email", level.getEmail());
            userData.put("firstName", level.getFirstName());
            userData.put("lastName", level.getLastName());
            userData.put("username", level.getUsername());
            userData.put("roles", getRoles(level.getUserRoles()));
            userArray.put(userData);
        });
        log.info("User search, Response : {}", userArray.length());
        return userArray.toString();
    }

    private String getRoles(List<UserRole> userRoles)
    {
        StringBuffer rolesString = new StringBuffer();
        for (UserRole userRole : userRoles)
        {
            if (rolesString.length() != 0)
            {
                rolesString.append(", ");
            }
            rolesString.append(userRole.getLabel());
        }

        return rolesString.toString();
    }

}

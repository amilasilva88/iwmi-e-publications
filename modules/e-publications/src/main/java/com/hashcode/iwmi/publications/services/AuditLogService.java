/*
 * FILENAME
 *     AuditLogService.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.services;

import java.util.List;

import com.hashcode.iwmi.publications.domain.AuditLog;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Audit Log service
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
public interface AuditLogService
{

    /**
     * <p>
     * Add audit message.
     * </p>
     *
     * @param user
     *            current user
     * @param userAction
     *            user action with the system
     * @throws EPublicationException
     *
     */
    Long addAudit(String user, String userAction) throws EPublicationException;

    /**
     * <p>
     * Find the latest audit records for the given user upto specified no of days back.
     * </p>
     *
     * @param userName
     *            user name to serach for data, if null or empty will fetch all users audit logs
     * @param noOfDaysBack
     *            no of days that to go backword to fetch the data
     * @return list of {@link AuditLog}
     * @throws EPublicationException
     *
     */
    List<AuditLog> findLastNDaysLogs(String userName, int noOfDaysBack) throws EPublicationException;

}

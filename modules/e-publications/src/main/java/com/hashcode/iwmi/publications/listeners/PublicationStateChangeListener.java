/*
 * FILENAME
 *     PublicationStateChangeListener.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.listeners;

import java.util.Set;

import com.hashcode.iwmi.publications.domain.PublicationItem;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Publication Item state change listener
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
public interface PublicationStateChangeListener
{

    /**
     * <p>
     * This will triggers in an event of PublicationItem state change
     * </p>
     *
     * @param currentUser
     * @param publicationItem
     * @throws EPublicationException
     *
     */
    void onStateChange(String currentUser, PublicationItem publicationItem) throws EPublicationException;
    
    /**
     * <p>
     * This will triggers in an event of PublicationItem state change
     * </p>
     *
     * @param currentUser
     * @param publicationItem
     * @throws EPublicationException
     *
     */
    void onStateChange(String currentUser, Set<String> notifyUsers, PublicationItem publicationItem) throws EPublicationException;
}

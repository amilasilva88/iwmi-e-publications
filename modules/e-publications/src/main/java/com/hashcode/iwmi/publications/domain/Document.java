/*
 * FILENAME
 *     DocumentRecord.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Document Repository entry details.
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 **/
@Entity
@Table(name = "DOCUMENTS")
public class Document extends BaseModel
{
    private static final long serialVersionUID = -1815289340821351208L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "publicationItem_id")
    private PublicationItem publicationItem;

    @Column(name = "comments", length = 1000)
    private String comments;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "file_location")
    private String fileLocation;

    @Column(name = "file_url")
    private String fileUrl;

    @Column(name = "file_md5")
    private String fileMd5;

    @Column(name = "file_type")
    private String fileType;

    @Column(name = "file_size")
    private Double fileSize;

    @Transient
    private byte[] data;

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.domain.BaseModel#getId()
     */
    @Override
    public Long getId()
    {
        return id;
    }

    /**
     * <p>
     * Getter for data.
     * </p>
     * 
     * @return the data
     */
    public byte[] getData()
    {
        return data;
    }

    /**
     * <p>
     * Setting value for data.
     * </p>
     * 
     * @param data
     *            the data to set
     */
    public void setData(byte[] data)
    {
        this.data = data;
    }

    /**
     * <p>
     * Getter for fileName.
     * </p>
     * 
     * @return the fileName
     */
    public String getFileName()
    {
        return fileName;
    }

    /**
     * <p>
     * Setting value for fileName.
     * </p>
     * 
     * @param fileName
     *            the fileName to set
     */
    public void setFileName(String fileName)
    {
        this.fileName = fileName;
    }

    /**
     * <p>
     * Getter for fileLocation.
     * </p>
     * 
     * @return the fileLocation
     */
    public String getFileLocation()
    {
        return fileLocation;
    }

    /**
     * <p>
     * Setting value for fileLocation.
     * </p>
     * 
     * @param fileLocation
     *            the fileLocation to set
     */
    public void setFileLocation(String fileLocation)
    {
        this.fileLocation = fileLocation;
    }

    /**
     * <p>
     * Getter for fileMd5.
     * </p>
     * 
     * @return the fileMd5
     */
    public String getFileMd5()
    {
        return fileMd5;
    }

    /**
     * <p>
     * Setting value for fileMd5.
     * </p>
     * 
     * @param fileMd5
     *            the fileMd5 to set
     */
    public void setFileMd5(String fileMd5)
    {
        this.fileMd5 = fileMd5;
    }

    /**
     * <p>
     * Getter for fileType.
     * </p>
     * 
     * @return the fileType
     */
    public String getFileType()
    {
        return fileType;
    }

    /**
     * <p>
     * Setting value for fileType.
     * </p>
     * 
     * @param fileType
     *            the fileType to set
     */
    public void setFileType(String fileType)
    {
        this.fileType = fileType;
    }

    /**
     * <p>
     * Getter for publicationItem.
     * </p>
     * 
     * @return the publicationItem
     */
    public PublicationItem getPublicationItem()
    {
        return publicationItem;
    }

    /**
     * <p>
     * Setting value for publicationItem.
     * </p>
     * 
     * @param publicationItem
     *            the publicationItem to set
     */
    public void setPublicationItem(PublicationItem publicationItem)
    {
        this.publicationItem = publicationItem;
    }

    /**
     * <p>
     * Getter for comments.
     * </p>
     * 
     * @return the comments
     */
    public String getComments()
    {
        return comments;
    }

    /**
     * <p>
     * Setting value for comments.
     * </p>
     * 
     * @param comments
     *            the comments to set
     */
    public void setComments(String comments)
    {
        this.comments = comments;
    }

    /**
     * <p>
     * Getter for fileSize.
     * </p>
     * 
     * @return the fileSize
     */
    public Double getFileSize()
    {
        return fileSize;
    }

    /**
     * <p>
     * Setting value for fileSize.
     * </p>
     * 
     * @param fileSize
     *            the fileSize to set
     */
    public void setFileSize(Double fileSize)
    {
        this.fileSize = fileSize;
    }

    /**
     * <p>
     * Getter for fileUrl.
     * </p>
     * 
     * @return the fileUrl
     */
    public String getFileUrl()
    {
        return fileUrl;
    }

    /**
     * <p>
     * Setting value for fileUrl.
     * </p>
     * 
     * @param fileUrl
     *            the fileUrl to set
     */
    public void setFileUrl(String fileUrl)
    {
        this.fileUrl = fileUrl;
    }
    
    

}

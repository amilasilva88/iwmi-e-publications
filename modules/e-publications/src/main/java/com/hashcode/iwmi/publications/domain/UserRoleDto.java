/*
 * FILENAME
 *     UserRoleDto.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.domain;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * User role dto.
 * </p>
 *
 *
 * @author Manuja Jayamanna
 * @email manuja@hashcodesys.com
 **/
public class UserRoleDto
{
    private UserRole userRole;
    private boolean selected;

    /**
     * <p>
     * Getter for userRole.
     * </p>
     * 
     * @return the userRole
     */
    public UserRole getUserRole()
    {
        return userRole;
    }

    /**
     * <p>
     * Setting value for userRole.
     * </p>
     * 
     * @param userRole
     *            the userRole to set
     */
    public void setUserRole(UserRole userRole)
    {
        this.userRole = userRole;
    }

    /**
     * <p>
     * Getter for selected.
     * </p>
     * 
     * @return the selected
     */
    public boolean isSelected()
    {
        return selected;
    }

    /**
     * <p>
     * Setting value for selected.
     * </p>
     * 
     * @param selected
     *            the selected to set
     */
    public void setSelected(boolean selected)
    {
        this.selected = selected;
    }
}

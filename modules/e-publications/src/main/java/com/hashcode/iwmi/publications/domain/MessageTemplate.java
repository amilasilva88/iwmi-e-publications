/*
 * FILENAME
 *     MessageTemplates.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * All system level Message Templates manage by this entity.
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
@Entity
@Table(name = "MESSAGE_TEMPLATES")
public class MessageTemplate extends BaseModel
{
    private static final long serialVersionUID = -4305769904048254255L;

    public enum Type
    {
        JOB_INITIATION_DONE,
        TO_COST_ESTIMATION,
        AFTER_COST_ESITMATION_TO_USER,
        AFTER_COST_ESITMATION_TO_SUPERVISOR,
        TO_JOB_APPROVAL_USER,
        TO_JOB_APPROVAL,
        SUPERVISOR_APPROVED_TO_USER,
        SUPERVISOR_APPROVED_TO_OFFICER,
        SUPERVISOR_REJECT_TO_USER,
        SUPERVISOR_REJECT_TO_OFFICER,
        SUPERVISOR_REQUEST_AMENDMENT_TO_OFFICER,
        AFTER_DELIVERY_ACCEPTED_TO_USER,
        AFTER_DELIVERY_ACCEPTED_TO_SUPERVISOR,
        AFTER_COSTING_TO_USER,
        AFTER_COSTING_TO_SUPERVISOR,
        TO_STATUS_UPDATE,
        AFTER_STATUS_UPDATE,
        AFTER_JOB_COMPLETE_USER;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "mail_type")
    private Type type;

    @Column(name = "mail_subject")
    private String subject;

    @Column(name = "mail_body", length = 2000)
    private String mailBody;

    /**
     * <p>
     * Getter for type.
     * </p>
     * 
     * @return the type
     */
    public Type getType()
    {
        return type;
    }

    /**
     * <p>
     * Setting value for type.
     * </p>
     * 
     * @param type
     *            the type to set
     */
    public void setType(Type type)
    {
        this.type = type;
    }

    /**
     * <p>
     * Getter for subject.
     * </p>
     * 
     * @return the subject
     */
    public String getSubject()
    {
        return subject;
    }

    /**
     * <p>
     * Setting value for subject.
     * </p>
     * 
     * @param subject
     *            the subject to set
     */
    public void setSubject(String subject)
    {
        this.subject = subject;
    }

    /**
     * <p>
     * Getter for mailBody.
     * </p>
     * 
     * @return the mailBody
     */
    public String getMailBody()
    {
        return mailBody;
    }

    /**
     * <p>
     * Setting value for mailBody.
     * </p>
     * 
     * @param mailBody
     *            the mailBody to set
     */
    public void setMailBody(String mailBody)
    {
        this.mailBody = mailBody;
    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.domain.BaseModel#getId()
     */
    @Override
    public Long getId()
    {
        return this.id;
    }

    public enum BodyTag
    {
        BODY_TO_NAME("{to_name}"),
        BODY_JOB_ID("{job_id}"),
        BODY_HEADER_1("{header_1}"),
        BODY_SUB_HEADER_1("{sub_header_1}"),
        BODY_SUB_HEADER_2("{sub_header_2}"),
        BODY_SUB_HEADER_3("{sub_header_3}"),
        BODY_CONTENT_SET_1("{content_1}"),
        BODY_CONTENT_SET_2("{content_2}"),
        BODY_CONTENT_SET_3("{content_3}"),
        BODY_CONTENT_SET_4("{content_4}"),
        BODY_CONTENT_SET_5("{content_5}"),
        BODY_LINK_PARAM_1("{link_param_1}"),
        BODY_LINK_PARAM_2("{link_param_2}"),
        BODY_LIST_TAB_1("{list_1}"),
        BODY_LIST_TAB_2("{list_2}"),
        DATE("{date}"),
        DATETIME("{date_time}");

        private String key;

        private BodyTag(String key)
        {
            this.key = key;
        }

        /**
         * <p>
         * Getter for key.
         * </p>
         * 
         * @return the key
         */
        public String getKey()
        {
            return key;
        }
    }

    public enum SubjectTag
    {

        SUBJECT_JOB_ID("{job_id}"),
        SUBJECT_TITLE("{title}"),
        SUBJECT_OTHER_TEXT_1("{other1}"),
        SUBJECT_OTHER_TEXT_2("{other1}");

        private String key;

        private SubjectTag(String key)
        {
            this.key = key;
        }

        /**
         * <p>
         * Getter for key.
         * </p>
         * 
         * @return the key
         */
        public String getKey()
        {
            return key;
        }
    }

}

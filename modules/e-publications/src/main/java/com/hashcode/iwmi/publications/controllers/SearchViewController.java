/*
 * FILENAME
 *     SearchViewController.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.controllers;

import static com.hashcode.iwmi.publications.util.ApplicationUtils.addUserToModel;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.net.URLEncoder;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hashcode.iwmi.publications.domain.PublicationItem;
import com.hashcode.iwmi.publications.domain.PublicationStatus;
import com.hashcode.iwmi.publications.exports.JobDetailReportExcel;
import com.hashcode.iwmi.publications.services.PublicationItemService;
import com.hashcode.iwmi.publications.util.SearchEngineManager;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * E-Publication related search requests handle by this controller.
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
@Controller
@RequestMapping("/search")
public class SearchViewController
{
    private static final Logger log = LoggerFactory.getLogger(SearchViewController.class);

    @Autowired
    private PublicationItemService publicationItemService;

    @Autowired
    private JobDetailReportExcel jobDetailsExcelReport;

    @Value("${export.file.location}")
    protected String exportFileLocation;

    @Value("${export.reports.url}")
    protected String exportReportUrl;

    @RequestMapping("/page")
    public String pageLoad(Model model)
    {
        log.info("Request came to load the search page....");
        addUserToModel(model);
        return "search";
    }

    /**
     * <p>
     * Find Items by id.
     * </p>
     *
     * <ul>
     * fields :[ {field = "", keyword = ""} , {field = "", keyword = ""} , {field = "", keyword = ""} , {field = "",
     * keyword = ""}]
     * </ul>
     *
     * @param data
     * @return
     *
     */
    @RequestMapping(value = "/criteria", method = {
        RequestMethod.GET, RequestMethod.POST
    })
    @ResponseBody
    public String findItems(@RequestBody String searchCriteria)
    {
        try
        {
            JSONObject criteria = new JSONObject(searchCriteria);
            JSONArray fields = criteria.getJSONArray("fields");

            String[] filterQuery = new String[fields.length()];
            for (int i = 0; i < fields.length(); i++)
            {
                JSONObject field = fields.getJSONObject(i);

                String fieldName = field.getString("field");
                String keyword = field.getString("keyword");
                filterQuery[i] = fieldName + ":" + keyword;
            }

            List<Long> itemIds = SearchEngineManager.search("*:*", filterQuery);
            List<PublicationItem> publications = new ArrayList<PublicationItem>();
            if (!itemIds.isEmpty())
            {
                publications = publicationItemService.findByIds(itemIds);
            }

            JSONArray jsonArray = new JSONArray();

            publications.stream().filter(p -> p.getStatus() != PublicationStatus.DELETED).forEach(p -> {

                JSONObject data = new JSONObject();
                data.put("id", p.getId());
                data.put("type_of_work", p.getTypeOfWork());
                data.put("project_code", p.getProjectCode());
                data.put("required_delivery_date", p.getRequiredDeliveryDate());
                data.put("division", p.getDivision());
                data.put("status", p.getStatus());
                data.put("jobNumber", p.getJobNumber());
                data.put("series", p.getSeries());
                data.put("created_by", p.getCreatedBy());

                jsonArray.put(data);
            });

            JSONObject responseData = new JSONObject();
            responseData.put("no_records", publications.size());
            responseData.put("publication_items", jsonArray);
            log.debug("No of publications for display: {}", jsonArray.length());
            return responseData.toString();
        }
        catch (Exception e)
        {
            log.error("Error occurred in Search items,", e);
        }
        return "";
    }

    /**
     * <p>
     * Find Items by id.
     * </p>
     *
     * <ul>
     * fields :[ {field = "", keyword = ""} , {field = "", keyword = ""} , {field = "", keyword = ""} , {field = "",
     * keyword = ""}]
     * </ul>
     *
     * @param data
     * @return
     *
     */
    @RequestMapping(value = "/exportTable", method = {
        RequestMethod.GET, RequestMethod.POST
    })
    @ResponseBody
    @Transactional
    public String exportTable(@RequestBody String searchCriteria, HttpServletResponse response)
    {
        try
        {
            log.info("Export to excel, Criteria : {}", searchCriteria);
            JSONObject criteria = new JSONObject(searchCriteria);
            JSONArray fields = criteria.getJSONArray("fields");

            String[] filterQuery = new String[fields.length()];
            for (int i = 0; i < fields.length(); i++)
            {
                JSONObject field = fields.getJSONObject(i);

                String fieldName = field.getString("field");
                String keyword = field.getString("keyword");
                filterQuery[i] = fieldName + ":" + keyword;
            }

            List<Long> itemIds = SearchEngineManager.search("*:*", filterQuery);
            List<PublicationItem> publications = new ArrayList<PublicationItem>();
            if (!itemIds.isEmpty())
            {
                publications = publicationItemService.findByIds(itemIds);
            }

            //            try (ByteArrayOutputStream outputStream = jobDetailsExcelReport.generateReport(publications))
            //            {
            //                response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            //                response.setHeader("Content-disposition", "attachment; filename=\"Publication-Item-Details.xlsx\"");
            //                response.setContentLength(outputStream.size());
            //
            //                outputStream.writeTo(response.getOutputStream());
            //                outputStream.flush();

            String itemDetailsExcel = "e-Publication-Item-Details_" + System.currentTimeMillis() + ".xlsx";
            try (ByteArrayOutputStream outputStream = jobDetailsExcelReport.generateReport(publications);
                FileChannel rwChannel =
                    new RandomAccessFile(exportFileLocation + File.separator + itemDetailsExcel, "rw").getChannel();
                OutputStream out = Channels.newOutputStream(rwChannel))
            {
                out.write(outputStream.toByteArray());
                out.flush();

                String filelink = exportReportUrl + URLEncoder.encode(itemDetailsExcel, "UTF-8");

                return filelink;
            }
            catch (IOException e)
            {
                log.error("Error occurred during export report writing to file ", e);
            }
            log.info("Data written to response, Content Type : {}", response.getContentType());
        }
        catch (Exception e)
        {
            log.error("Error occurred in writing a job details to excel", e);
        }
        return "";
    }
}

/*
 * FILENAME
 *     MessageTemplateDaoImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.dao.impl;

import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.hashcode.iwmi.publications.dao.MessageTemplateDao;
import com.hashcode.iwmi.publications.domain.MessageTemplate;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Implementation for MessageTemplaceDao.
 * </p>
 *
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
@Repository
public class MessageTemplateDaoImpl extends GenericDaoImpl<MessageTemplate, Long> implements MessageTemplateDao
{

    private static final Logger log = LoggerFactory.getLogger(MessageTemplateDaoImpl.class);

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.dao.MessageTemplateDao#findMessageTemplateByType(java.lang.String)
     */
    @Override
    public MessageTemplate findMessageTemplateByType(MessageTemplate.Type templateType)
    {
        log.debug("Find Message Template by name :{}", templateType);

        String queryText = "SELECT m FROM " + MessageTemplate.class.getName() + " m  WHERE m.type = :templateType";

        TypedQuery<MessageTemplate> query = entityManager.createQuery(queryText, MessageTemplate.class);
        query.setParameter("templateType", templateType);

        MessageTemplate messageTemplate = query.getSingleResult();
        log.debug("Found Message Template by Type :{}, [ Results :{}]", messageTemplate.getType());
        return messageTemplate;
    }

}

/*
 * FILENAME
 *     ApprovedStateChangeListener.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2015 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.listeners;

import static com.hashcode.iwmi.publications.domain.PublicationStatus.APPROVED;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.hashcode.iwmi.publications.domain.MessageTemplate;
import com.hashcode.iwmi.publications.domain.MessageTemplate.BodyTag;
import com.hashcode.iwmi.publications.domain.MessageTemplate.SubjectTag;
import com.hashcode.iwmi.publications.domain.PublicationItem;
import com.hashcode.iwmi.publications.domain.SystemUser;
import com.hashcode.iwmi.publications.exceptions.EPublicationException;
import com.hashcode.iwmi.publications.services.PublicationItemService;
import com.hashcode.iwmi.publications.services.SystemUserService;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Approved state change listener.
 * </p>
 * 
 * @author Amila Silva
 * @email amila@hashcodesys.com
 *
 **/
@Service
@Qualifier(value = "approvedStateChange")
@Transactional
public class ApprovedStateChangeListener extends AbstractPublicationStateChangeListener
{

    private static final Logger log = LoggerFactory.getLogger(ApprovedStateChangeListener.class);

    @Autowired
    private PublicationItemService publicationItemService;

    @Autowired
    private SystemUserService systemUserService;

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.publications.listeners.PublicationStateChangeListener#onStateChange(java.lang.String,
     *      com.hashcode.iwmi.publications.domain.PublicationItem)
     */
    @Override
    public void onStateChange(String currentUser, PublicationItem item) throws EPublicationException
    {
        log.debug("Approved State changed, [ User : {}, Publication Item id : {}] ", currentUser, item.getId());

        Map<MessageTemplate.SubjectTag, String> subjectMap = new HashMap<MessageTemplate.SubjectTag, String>();
        subjectMap.put(SubjectTag.SUBJECT_JOB_ID, item.getJobNumber());

        Map<MessageTemplate.BodyTag, Object> bodyMap = new HashMap<MessageTemplate.BodyTag, Object>();
        bodyMap.put(BodyTag.BODY_JOB_ID, item.getJobNumber());
        bodyMap.put(BodyTag.BODY_LINK_PARAM_1, linkURL + "/approval/page?id=" + item.getId());
        bodyMap.put(BodyTag.BODY_CONTENT_SET_1, item.getApproverComments().stream().findFirst().get().getComment());
        bodyMap.put(BodyTag.DATE, LocalDate.now(ZoneId.systemDefault()));
        bodyMap.put(BodyTag.DATETIME, LocalDateTime.now(ZoneId.systemDefault()));

        SystemUser user = systemUserService.findUser(item.getRequesterName());
        SystemUser officer = systemUserService.findUser(item.getCreatedBy());

        if (item.getStatus() == APPROVED)
        {
            item.setStatus(APPROVED);
            publicationItemService.statusUpdate(item);
        }

        item.setStatus(APPROVED);
        publicationItemService.statusUpdate(item);

        bodyMap.put(BodyTag.BODY_TO_NAME, user.getFirstName());
        sendEmailAndNotifyOnDashboard(item, MessageTemplate.Type.SUPERVISOR_APPROVED_TO_USER, subjectMap, bodyMap, user);

        bodyMap.put(BodyTag.BODY_TO_NAME, officer.getFirstName());
        sendEmailAndNotifyOnDashboard(item, MessageTemplate.Type.SUPERVISOR_APPROVED_TO_OFFICER, subjectMap, bodyMap,
            officer);

        auditService
            .addAudit(currentUser, String.format("Supervisor Approved, Publication Item Id : %d", item.getId()));

        log.info("Approved changed done, [ User : {}, Publication Item id : {}] ", currentUser, item.getId());
    }

}

var JobStatus = {
		INITIATION: 0,
		NEW: 0,
		INITIALISED: 1,
		ESTIMATED: 2,
		APPROVED: 3,
		DELIVERED: 4,
		COSTING: 5,
		Status: 6,
		COMPLETED: 6
};
var ACTION = {
	NEXT: "NEXT",
	SAVE: "SAVE"
};
var JobLibrary = {
	setDisableAllInput: function(isDisabledValue) {
		if (isDisabledValue) {
			$("input").attr("DISABLED", isDisabledValue);
			$("textarea").attr("DISABLED", isDisabledValue);
			$("select").attr("DISABLED", isDisabledValue);
			$("button").attr("DISABLED", isDisabledValue);	
			$('.calendar').unbind("click");
		}
	},
	setItemStatus: function (itemStatus, currentPage) {
		$(".col-lg-2.horizontal-step").removeClass("active")
		//$(".col-lg-2.horizontal-step").eq(0).addClass("active").addClass("active")
		
		if (null != itemStatus) {
			var itemStatusRawvalue = (JobStatus[itemStatus] == JobStatus["COSTING"]) ? JobStatus["Status"] : JobStatus[itemStatus]
			for (var i = 0; i<= itemStatusRawvalue; i++) {
				$(".col-lg-2.horizontal-step").eq(i).addClass("active");
				if (i==currentPage){
					$(".col-lg-2.horizontal-step").eq(i).addClass("current");
				}
			}
			if (currentPage != JobStatus.Status) {
//				$('#btnNext').attr('disabled','disabled');
				if (currentPage < itemStatusRawvalue) {
//					$('#btnNext').removeAttr('disabled','disabled');
				}
			}
		}
	},
	showJobActionStatus: function(isSuccess) {
		var actionClass = "alert alert-danger alert-dismissible hidden";
		var actionTitle = "Fail !";
		var actionDescription = " Sorry, unable to save your job.";
		if (isSuccess) {
			actionClass = "alert alert-success alert-dismissible hidden";
			actionTitle = "Success !";
			actionDescription = " Your job has been saved successfully."
		}
		$("#action-alert").removeClass().addClass(actionClass);
		$("#action-alert > #message-title").html(actionTitle);
		$("#action-alert > #message-description").html(actionDescription);
		$("#action-alert").removeClass("hidden").addClass("fade in");
	},
	showJobActionStatusWithMesage: function(isSuccess, message) {
		var actionClass = "alert alert-danger alert-dismissible hidden";
		var actionTitle = "Fail !";
		var actionDescription = " "+message;
		if (isSuccess) {
			actionClass = "alert alert-success alert-dismissible hidden";
			actionTitle = "Success !";
		}
		$("#action-alert").removeClass().addClass(actionClass);
		$("#action-alert > #message-title").html(actionTitle);
		$("#action-alert > #message-description").html(actionDescription);
		$("#action-alert").removeClass("hidden").addClass("fade in");
	},
	clearJobActionStatus: function() {
		$("#action-alert").removeClass().addClass("hidden");
	}
}
$(document).ready(function() {
	$('#modalSubmitInfo').on('hidden.bs.modal', function () {
		window.location.href = "../home/dashboard";
	});
	$(document).on('click','.horizontal-step.active',function() {
		location.href = $(this).attr("value");
	});
	$('.input-group.date').datepicker({
		format : "yyyy-mm-dd",
		todayBtn : "linked",
		autoclose : true,
		todayHighlight : true
	});

	$('.input-group.date').datepicker('setDate', new Date());
});

function postRequest(url, postData) {
	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	var responseData;
	$
			.ajax({
				type : 'POST',
				contentType : "application/json; charset=utf-8",
				beforeSend : function(jqXHR, settings) {
					jqXHR.setRequestHeader(header, token);
				},
				dataType : "json",
				url : url,
				data : JSON.stringify(postData),
				success : function(data) {
					responseData = data;
				},
				error : function(xhr, status) {
					alert("Something went wrong during the data loading for table, Please try again"
							+ status);
				},
				async : false
			});
	return responseData;
}
function postRequestForExcel(url, postData) {
	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	var responseData;
	
	$.ajax({
				type : 'POST',
				contentType : "application/json; charset=utf-8",
				beforeSend : function(jqXHR, settings) {
					jqXHR.setRequestHeader(header, token);
				},
				url : url,
				data : JSON.stringify(postData),
				success : function(downloadUrl) {
					  
					var a = document.createElement("a");
                    
                    if (typeof a.download === "undefined") {
                        window.location = downloadUrl;
                    } else {
                        a.href = downloadUrl;
//                        a.download = filename;
                        document.body.appendChild(a);
                        a.click();
                    }
				},
				error : function(xhr, status) {
					alert("Something went wrong during the data loading for table, Please try again"
							+ status);
				},
				async : false
			});
	return responseData;
}

function getRequest(url) {
	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	var responseData;
	$.ajax({
		url : url,
		type : 'GET',
		dataType : "json",
		beforeSend : function(jqXHR, settings) {
			jqXHR.setRequestHeader(header, token);
		},
		success : function(data) {
			responseData = data;
		},
		error : function(xhr, status) {
			alert("Something went wrong during the data loading for table, Please try again "
					+ status);
		},
		async : false
	});
	return responseData;
}
function getRequestWithRequestData(url,getData) {
	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	var responseData;
	$.ajax({
		url : url,
		type : 'GET',
		dataType : "json",
		data : JSON.stringify(getData),
		beforeSend : function(jqXHR, settings) {
			jqXHR.setRequestHeader(header, token);
		},
		success : function(data) {
			responseData = data;
		},
		error : function(xhr, status) {
			alert("Something went wrong during the data loading for table, Please try again "
					+ status);
		},
		async : false
	});
	return responseData;
}
function getFormatedFloatValue(value) {
	var formattedValue = ""+value;
	formattedValue = formattedValue.replace(/^0+/, '');
	formattedValue = Number(formattedValue);
	return formattedValue;
}

$(function() {

	$('#side-menu').metisMenu();

});

// Loads the correct sidebar on window load,
// collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
	$(window)
			.bind(
					"load resize",
					function() {
						topOffset = 50;
						width = (this.window.innerWidth > 0) ? this.window.innerWidth
								: this.screen.width;
						if (width < 768) {
							$('div.navbar-collapse').addClass('collapse')
							topOffset = 100; // 2-row-menu
						} else {
							$('div.navbar-collapse').removeClass('collapse')
						}

						height = (this.window.innerHeight > 0) ? this.window.innerHeight
								: this.screen.height;
						height = height - topOffset;
						if (height < 1)
							height = 1;
						if (height > topOffset) {
							$("#page-wrapper").css("min-height",
									(height) + "px");
						}
					})
})

/**
 * 
 * Get today's date in YYYY-MM-DD format
 * 
 * @returns {String}
 */
function todayDate() {
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth() + 1; // January is 0!

	var yyyy = today.getFullYear();
	if (dd < 10) {
		dd = '0' + dd
	}
	if (mm < 10) {
		mm = '0' + mm
	}
	return today = yyyy + '-' + mm + '-' + dd;
}

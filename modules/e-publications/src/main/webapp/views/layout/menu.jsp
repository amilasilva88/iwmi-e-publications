 <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
 <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <img src="../../images/epublishing-banner.png"/>
                <!-- <a class="navbar-brand" href="#">IWMI e-Publications v1.0</a> -->
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    	<sec:authentication var="principal" property="principal" />
                        <i class="fa fa-user fa-fw"></i>${principal.username} <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <!-- <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li> -->
                        <!-- <li class="divider"></li> -->
                        <li><form action="/e-publications/logout" name="logoutform" method="POST"><input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"><a href="#" onclick="logoutform.submit();"><i class="fa fa-user fa-fw"></i>Logout</a></form>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->
            
             <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="../home/dashboard"><i class="fa fa-dashboard fa-fw"></i>Dashboard</a>
                        </li>
                        <li>
                            <a href="../search/page"><i class="fa fa-search fa-fw"></i>Search</a>
                        </li>
                        <!--  <li>
                            <a href="#"><i class="fa fa-search fa-fw"></i>Menu 2<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#">Levels 1</a>
                                </li>
                                <li>
                                    <a href="#">Levels 2</a>
                                </li>
                            </ul>
                        </li> -->
                        <sec:authorize ifAllGranted="ROLE_SUPERUSER">
                        <li>
                            <a href="#"><i class="fa fa-user fa-fw"></i>System Users<span class="fa arrow"></span></a>
                           <!--  <ul class="nav nav-second-level">
                                <li>
                                     <a href="../searchUsersForm">Users</a>
                                </li>
                            </ul> -->
                        </li>
                        </sec:authorize>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
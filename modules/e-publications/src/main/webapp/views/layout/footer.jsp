
<div class="row">
	<div class="col-lg-12">
		<div class="text-center">
			<span class="text-muted" style="font-size: 12px; text-align: center;"><i
				class="fa fa-copyright fa-fw"></i> 2015 hashCode. All Rights Reserved</span>
		</div>

		<div class="text-center">
			<!-- Social Tags -->
			<span>
			   <a href="http://www.facebook.com/IWMIonFB"><i class="fa fa-facebook-square fa-lg"></i></a> 
			</span> 
			<span>
			   <a href="https://twitter.com/IWMI_Water_News"><i class="fa fa-twitter-square fa-lg"></i></a>
			</span> 
			<span>
			   <a href="https://plus.google.com/114126873075728022721/posts"><i class="fa fa-google-plus-square fa-lg"></i></a>
			</span> 
			<span>
			   <a href="http://www.youtube.com/iwmimedia"><i class="fa fa-youtube-square fa-lg"></i></a>
			</span> 
			<span>
			   <a href="http://www.linkedin.com/company/international-water-management-institute-iwmi"><i class="fa fa-linkedin-square fa-lg"></i></a>
		    </span> 
		    <span>
		       <a href="http://www.flickr.com/photos/iwmi"><i class="fa fa-flickr fa-lg"></i></a> 
		    </span>
		</div>
	</div>
	<!-- /.col-lg-12 -->
</div>


<!-- /#wrapper -->

	<!-- jQuery Version 1.11.0 -->
	<script src="../../javascript/bootstrap/js/jquery-1.11.0.js"></script>
	<script src="../../javascript/bootstrap/js/jquery-ui.min.js"></script>
	
	<!-- Bootstrap Core JavaScript -->
	<script src="../../javascript/bootstrap/js/bootstrap.min.js"></script>
	<script src="../../javascript/bootstrap/js/bootstrap-datepicker.js"></script>
	
	<!-- Metis Menu Plugin JavaScript -->
	<script src="../../javascript/bootstrap/js/plugins/metisMenu/metisMenu.min.js"></script>
	
	<!-- DataTables JavaScript -->
	<script src="../../javascript/bootstrap/js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="../../javascript/bootstrap/js/plugins/dataTables/dataTables.bootstrap.js"></script>
	
	<!-- Chart JavaScript -->
	<script src="../../javascript/bootstrap/js/plugins/chart/d3.min.js"></script>
	<script src="../../javascript/bootstrap/js/plugins/chart/nv.d3.min.js"></script>
	<script src="../../javascript/bootstrap/js/plugins/chart/hashCode.charts.js"></script>
	
	<!-- Download JavaScript -->
	<script src="../../javascript/bootstrap/js/plugins/download/jquery.fileDownloads.js"></script>
	
	<!-- Validation JavaScript -->
	<!-- <script type="text/javascript" src="../../javascript/bootstrap/js/plugins/validator/bootstrapValidator.min.js"></script> -->

	
	<script type="text/javascript" src="../../javascript/bootstrap/js/plugins/validator/bootstrapvalidator.min.js"></script>
<!--
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>
-->	<script type="text/javascript" src="../../javascript/bootstrap/js/bootbox.min.js"></script>
	
	<!-- Custom Theme JavaScript -->
	<script src="../../javascript/e-publications-app.js"></script>
	

<script type="text/javascript">
/*
	function postRequest(posturl, postData) {
		var responseData;

		$.ajax({
			type : 'POST',
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			url : posturl,
			data : JSON.stringify(postData),
			success : function(data) {
				responseData = data;
			},
			error : function(xhr, status) {
				//alert("Something went wrong during the data loading for table, Please try again");
			},
			async : false
		});

		return responseData;
	}
*/
	function prepareTableData(response, postUrl) {
		var rows = {};
		var dataofTable = [];
		for (var i = 0; i < response.length; i++) {
			var responseRow = {
				date : response[i]['date'],
				level : response[i]['level'],
				measuringPoint : response[i]['measuringPoint'],
				comment : response[i]['comment'],
				action : '<a href="#" onClick="deleteLevel('
						+ response[i]['id'] + ', \'' + postUrl + '\')">Delete</a>'
			};
			dataofTable.push(responseRow);
		}
		rows["rows"] = dataofTable;
		return rows;
	}

	function deleteLevel(idValue, postUrl) {
		bootbox.dialog({
			message : "Are you sure want to delete selected water level ?",
			title : "Confirm",
			buttons : {
				success : {
					label : "Cancel",
					className : "btn-success",
					callback : function() {
					}
				},
				danger : {
					label : "Ok",
					className : "btn-danger",
					callback : function() {

						var measurePointId = $("#measuringPoint").val();
						var recordDate = $("#recordDate").val();

						var reqData = {
							measurePointId : measurePointId,
							recordDate : recordDate,
							id : idValue
						};
						var response = postRequest(postUrl, reqData);
						var tableData = prepareTableData(response, postUrl);
						renderDataTable(tableData);
					}
				}
			}
		});
	}
</script>

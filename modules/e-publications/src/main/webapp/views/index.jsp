<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">

<jsp:include page="layout/header.jsp" />
<body>

	<div id="wrapper">
		<jsp:include page="layout/menu.jsp" />

		<!-- Page Content -->
		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h2 class="page-header">Dashboard</h2>
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<div class="row">
				<div class="col-lg-12">
					<jsp:include page="content/index-body.jsp" />
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<jsp:include page="layout/footer.jsp" />

	<script type="text/javascript">
	
	$(document).ready(function() {
		var responsePublications = getRequest('../dashboard/publications');
		var tablePublicationsData = preparePublicationItemsData(responsePublications);
		renderDataPublicationsTable(tablePublicationsData, "publication-items-data-table");
		
		var responseNotifications = getRequest('../dashboard/notifications');
		var tableNotificationData = prepareNotificationsData(responseNotifications);
		renderDataNotificationsTable(tableNotificationData);
		
		$("#btnAddnew").click(loadNewJob);
	});

	function loadNewJob(){
		window.location.href = '../initiation/new';
	}

	function preparePublicationItemsData(response) {
		
		var publicationItems = response['publication_items'];
		var rows = {};
		var dataofTable = [];
		for (var i = 0; i < publicationItems.length; i++) {
			var responseRow = {
					typeOfWork : (publicationItems[i]['type_of_work']) ? publicationItems[i]['type_of_work'] : "",	
					projectCode : (publicationItems[i]['project_code']) ? publicationItems[i]['project_code'] : "",	
					requiredDeliveryDate : (publicationItems[i]['required_delivery_date'])? publicationItems[i]['required_delivery_date'] : "",
					division : (publicationItems[i]['division']) ? publicationItems[i]['division'] : "",
					status : (publicationItems[i]['status']) ? publicationItems[i]['status'] : "",
					jobNumber : (publicationItems[i]['jobNumber']) ? publicationItems[i]['jobNumber'] : "",
					createdBy : (publicationItems[i]['created_by']) ? publicationItems[i]['created_by'] : "",
					action : '<a href="../initiation/page?id=' + publicationItems[i]['id']+ '">Edit</a> | <a href="../status/page?id=' + publicationItems[i]['id']+ '">Status</a>'
			};
			dataofTable.push(responseRow);
		}
		rows["rows"] = dataofTable;
		return rows;
	}
	function renderDataPublicationsTable(dataSet) {
		$('#publication-items-data-table').DataTable({
			"bAutoWidth" : false,
			"bJQueryUI" : true,
			"bDestroy" : true,
			"order": [[ 4, "desc" ]],
			"aaData" : dataSet.rows,
			"aoColumns" : [
			/* {
				"sTitle" : "Type of work",
				"mData" : "typeOfWork",
			},  */
			
			{
				"sTitle" : "Requested by",
				"mData" : "createdBy"
			}, 
			{
				"sTitle" : "Job number",
				"mData" : "jobNumber"
			} ,
			{
				"sTitle" : "Project code",
				"mData" : "projectCode"
			}, 
			{
				"sTitle" : "Division",
				"mData" : "division"
			},
			{
				"sTitle" : "Delivery date",
				"mData" : "requiredDeliveryDate"
			},
			{
				"sTitle" : "Status",
				"mData" : "status"
			},
			{
				"sTitle" : "Action",
				"mData" : "action",
				"sWidth" : "120px"
			} ]
		});
	}
	function prepareNotificationsData(response, itemType) {
		
		var publicationItems = response['notifications'];
		var rows = {};
		var dataofTable = [];
		for (var i = 0; i < publicationItems.length; i++) {
			var responseRow = {
					dateAndTime : (publicationItems[i]['notified_time']) ? publicationItems[i]['notified_time'] : "",	
					status : (publicationItems[i]['status']) ? publicationItems[i]['status'] : "",	
					message : (publicationItems[i]['message']) ? publicationItems[i]['message'] : "",
					action : '<a href="../initiation/page?id=' + publicationItems[i]['publication_item_id']+ '">View</a>'
			};
			dataofTable.push(responseRow);
		}
		rows["rows"] = dataofTable;
		return rows;
	}
	function renderDataNotificationsTable(dataSet) {
		$('#notification-items-data-table').DataTable({
			"bAutoWidth" : false,
			"bJQueryUI" : true,
			"bDestroy" : true,
			"order": [[ 0, "desc" ]],
			"aaData" : dataSet.rows,
			"aoColumns" : [ {
				"sTitle" : "Date",
				"mData" : "dateAndTime",
			}, 
			{
				"sTitle" : "Message",
				"mData" : "message"
			}, {
				"sTitle" : "Action",
				"mData" : "action",
				"sWidth" : "120px"
			} ]
		});
		
	}
	</script>
</body>
</html>

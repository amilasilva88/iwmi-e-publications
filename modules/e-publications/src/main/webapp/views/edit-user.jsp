<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">

<jsp:include page="layout/header.jsp" />
<body>

	<div id="wrapper">
		<jsp:include page="layout/menu.jsp" />

		<!-- Page Content -->
		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h2 class="page-header">Users</h2>
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<div class="row">
				<div class="col-lg-12">
					<jsp:include page="content/edit-user-body.jsp" />
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<jsp:include page="layout/footer.jsp" />
	<script type="text/javascript">
		$(document).ready(function() {
			validateAndSubmitForm();
		});

		function validateAndSubmitForm() {
			$('#formAddSystemUser')
					.bootstrapValidator(
							{
								feedbackIcons : {
									valid : 'glyphicon glyphicon-ok',
									invalid : 'glyphicon glyphicon-remove',
									validating : 'glyphicon glyphicon-refresh'
								},
								fields : {
									email : {
										validators : {
											notEmpty : {
												message : 'The email address is required and cannot be empty'
											},
											emailAddress : {
												message : 'The email address is not a valid'
											},
											callback : {
												callback : function(value,
														validator, $field) {
													var reqData = {
														email : $("#email")
																.val(),
														userId : $("#userId").val()
													}
													var response = postRequest(
															'./checkEmail',
															reqData);
													if (response.status == 'false') {
														return {
															valid : false,
															message : 'Email address already taken'
														};
													}

													return true;
												}
											}
										}
									},
									firstName : {
										validators : {
											notEmpty : {
												message : 'First name is required'
											}
										}
									},
									lastName : {
										validators : {
											notEmpty : {
												message : 'Last name is required'
											}
										}

									},
									userRoles : {
										validators : {
											notEmpty : {
												message : 'Please select at least one role'
											}
										}

									}
								}
							}).on(
							'success.form.bv',
							function(e) {
													
								$('formAddSystemUser').submit();
							});
		}
	</script>
</body>
</html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="panel panel-default">
	<div class="panel-heading">Search Data</div>
	<c:if test="${not empty message}">
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert"
				aria-hidden="true">&times;</button>
			<label>${message}</label>
			<c:remove var="message"/>
		</div>
	</c:if>
	<div class="panel-body">
		<div class="row">
			<div class="col-lg-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover"
						id="predicted-water-data-table">
						<thead>
							<tr>
								<th>Username</th>
								<th>Email</th>
								<th>First Name</th>
								<th>Last Name</th>
								<th>Roles</th>
								<th>Action</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div style="padding:10">
	<!-- <div id="success-alert" class="alert alert-warning alert-dismissible hidden" role="alert"> -->
	<div id="action-alert" class="alert alert-dismissible hidden" role="alert">
		<button type="button" class="close" data-dismiss="alert"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<strong id="message-title"></strong><span id="message-description"></span> 
	</div>
</div>
	
<!-- ----------- Next action ----------------- -->
<!-- Modal -->
<div id="modalSubmitInfo" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <h4 class="modal-title"><center>Your request has been submitted successfully</center></h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal" data-href="../../home/dashboard" data-window="external">OK</button> 
        <!-- <a href="../home/dashboard" class="btn close_link" data-dismiss="modal" role="button" data-window="external">Close</a> -->
      </div>
    </div>
  </div>
</div>
<!-- ------------ ------------- ------------- -->
<div class="row">
	<div class="col-lg-2 horizontal-step" value="../initiation/page?id=${id}" style="width: 14.25%" >
		<h5>Initiation</h5>
	</div>
	<div class="col-lg-2 horizontal-step" value="../costEstimation/page?id=${id}" style="width: 14.25%" >
		<h5>Estimation</h5>
	</div>
	<div class="col-lg-2 horizontal-step" value="../approval/page?id=${id}" style="width: 14.25%" >
		<h5>Approval</h5>
	</div>
	<div class="col-lg-2 horizontal-step" value="../delivery/page?id=${id}" style="width: 14.25%" >
		<h5>Delivering</h5>
	</div>
	<div class="col-lg-2 horizontal-step" value="../actual_costing/page?id=${id}" style="width: 14.25%">
		<h5>Actual costing</h5>
	</div>
	<div class="col-lg-2 horizontal-step" value="../status/page?id=${id}" style="width: 14.25%">
		<h5>Job status</h5>
	</div>
	<div class="col-lg-2 horizontal-step" value="../jobComplete/page?id=${id}" style="width: 14.5%">
		<h5>Completion</h5>
	</div>
</div>
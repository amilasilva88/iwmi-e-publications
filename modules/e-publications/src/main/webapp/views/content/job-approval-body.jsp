<div class="col-lg-12 content-body">
	<jsp:include page="navigation/job-navigation-menu.jsp" />
	<div id="pageStepApproval" class="row stepContainer">
		<form role="form" id="formJobApproval">
			<div class="col-lg-6">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Status</span>
					  	<select class="form-control"  id="selectStatus"  name="selectStatus" required>
					  		<option value="">-- Select ---</option>
					  		<option value="APPROVED">Approve</option>
					  		<option value="NEED_AN_AMENDMENT">Need an amendment</option>
					  		<option value="REJECTED">Reject</option>
					  	</select>
					</div>
				</div>
			</div>
			<div class="col-lg-12"></div>
			<div class="col-lg-12">
				<div class="form-group textarea">
					<div class="input-group">
						<span class="input-group-addon">Comments</span>
						<textarea class="form-control" id="textAreaApproverComment" name="textAreaApproverComment" rows="5"
							maxlength="500"></textarea>
					</div>
				</div>
			</div>
			<div class="col-lg-12"><hr></div>
			<div class="col-lg-6">
				<div class="form-group" >
					<button type="button" class="btn btn-default" id="btnReset" name="btnReset">Reset</button>
					<button type="button" class="btn btn-primary" id="btnSave" name="btnSave">Save</button>
				</div>
			</div>
			<div class="col-lg-6" style="text-align: right;">
				<div class="form-group" >
					<button type="button" class="btn btn-success" id="btnNext" name="btnNext">Submit</button>
				</div>
			</div>
			<input type="submit" class="superHide" action="" id="hiddenJobId" name="hiddenJobId" value="${id}"  />
		</form>
	</div>
</div>

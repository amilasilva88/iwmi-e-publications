<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="panel panel-default">
	<div class="panel-heading">Edit User</div>
	<div id="pageAddSystemUser" class="row stepContainer">
		<form role="form" id="formAddSystemUser" action="./saveUserForm" method="POST">
		<input type="hidden" id="userId" name="userId" value="${systemUser.id}" />
		<input type="hidden" name="_csrf" id="_csrf" value="${_csrf.token}">
		<input type="hidden" name="_csrf_header" id="_csrf_header" value="${_csrf.headerName}">
		
		<div class="col-lg-12">
			<div class="col-lg-6">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Email Address</span><input
							class="form-control" placeholder="E-mail" name="email" id="email"
							type="email" value="${systemUser.email}" autofocus>
					</div>
				</div>
			</div>
			</div>
			<div class="col-lg-12">
			<div class="col-lg-6">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">First Name</span> <input
							class="form-control" placeholder="First Name" id="firstName"
							name="firstName" value="${systemUser.firstName}" />
					</div>
				</div>
			</div>
			</div>
			<div class="col-lg-12">
			<div class="col-lg-6">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Last Name</span> <input
							class="form-control" placeholder="Last Name" id="lastName"
							name="lastName" value="${systemUser.lastName}" />
					</div>
				</div>
			</div>
			</div>
			<div class="col-lg-12">
				<div class="col-lg-6">
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon">User Roles</span> <select
								class="form-control" id="userRoles" name="userRoles" multiple>
								<c:forEach items="${optionRoles}" var="optionRole">
									<c:choose>
										<c:when test="${optionRole.selected}">
											<option value="${optionRole.userRole}" selected="selected">${optionRole.userRole.label}</option>
										</c:when>
										<c:otherwise>
											<option value="${optionRole.userRole}">${optionRole.userRole.label}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div style="clear:both;">&nbsp;</div>
			<div class="col-lg-12">
				<hr>
			</div>
			<div class="col-lg-12">
			<div class="col-lg-6">
				<div class="form-group">
					<button type="submit" id="submitBtn" class="btn btn-default">Save</button>
					<button type="reset" class="btn btn-default">Clear</button>
				</div>
			</div>
			</div>
		</form>
	</div>
</div>

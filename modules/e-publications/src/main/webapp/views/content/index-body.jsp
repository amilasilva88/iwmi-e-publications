<div class="row">
	<div class="row">
		<div class="col-lg-12">
			<!-- 
			<button type="button" class="btn btn-default pull-right" id="btnAddnew" style="padding: 2px 14px; font-size:13px;">
				<i class="fa fa-newspaper-o"></i>&nbsp;New
			</button>
			 -->
			<button type="button" class="btn btn-primary btn-lg pull-left" id="btnAddnew" style="padding: 6px 82px;"><i class="fa fa-newspaper-o"></i>&nbsp; Add Job</button>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<hr/>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
				   <div class="row">
				   		<!-- 
				      <div class="col-lg-10">
				         Publications
				      </div>
				      <div class="col-lg-2">
				         <button type="button" class="btn btn-default pull-right" id="btnAddnew" style="padding: 2px 14px; font-size:13px;">
							<i class="fa fa-newspaper-o"></i>&nbsp;New
						 </button>
				      </div>
				       -->
				       <div class="col-lg-12">
				         Publication requests
				      </div>
				   </div>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12 content-body">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover" id="publication-items-data-table">
									<thead>
										<tr>
											<!-- <th>Type of work</th> -->	
											<th>Requested by</th>
											<th>Job number</th>
											<th>Project code</th>
											<th>Division</th>
											<th>Delivery date</th>
											<th>Status</th>
											<th>Action</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
   <div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">Notifications</div>
	
				<div class="panel-body">
					<div class="row">
						<!-- 
						<div class="col-lg-12">
						    <svg id="todayWaterLevel_Chart" style="height:450px;"></svg>
						</div>
						 -->
						<div class="col-lg-12 content-body">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover" id="notification-items-data-table">
									<thead>
										<tr>
											<th width="15%">Date</th>	
											<th width="75%">Message</th>
											<th width="15%">Action</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</div>


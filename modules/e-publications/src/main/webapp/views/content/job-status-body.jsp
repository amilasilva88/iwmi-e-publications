<div class="col-lg-12 content-body">
	<jsp:include page="navigation/job-navigation-menu.jsp" />
	<div id="pageStepInitiation" class="row stepContainer">
		<form role="form" id="formJobStatus">
			<!-- -------- ---------- -------- -->
			<!-- --------FORM STATUS -------- -->
			<!-- <div class="col-lg-12" style="text-align: right;">
				<div class="form-group" >
					<button type="button" class="btn btn-default" id="btnNext">Next >></button>
					<button type="button" class="btn btn-success" name="btnConfirm" data-toggle="modal" data-target="#modalUsersList">Confirm</button>
					<button type="button" class="btn btn-success" name="btnConfirm">Confirm</button>
				</div>
			</div> -->
			<div class="col-lg-6">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Job number</span>
					  	<input type="text" class="form-control" id="textJobNumber" name="textJobNumber" value="1234-49038-12" READONLY DISABLED>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Related program</span>
					  	<input type="text" class="form-control" id="textRelatedProgram" maxlength="50">
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Type of the job</span>
					  	<input type="text" class="form-control" id="textTypeOfTheJob" READONLY DISABLED>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Series</span>
					  	<input type="text" class="form-control" id="textSeries" READONLY DISABLED>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Title</span>
					  	<input type="text" class="form-control" id="textTitle" >
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Author</span>
						<input type='text' class="form-control" id="textAuthor" name="textAuthor" />
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Manus scrip received date</span>
					  	<div class='input-group date' id='datetimepicker1'>
							<input type='text' class="form-control"  id="textManuScripReceiveDate" name="textManuScripReceiveDate" />
							<span class="input-group-addon calendar">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Number of manuscript pages</span>
					  	<input type="text" class="form-control" id="textNoOfManuscriptPages" maxlength="10" value="0">
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">ISBN-ISSN</span>
					  	<input type="text" class="form-control" id="textISBN-ISSN" maxlength="20">
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Supervisor</span>
					  	<input type="text" class="form-control" id="textSupervisor" READONLY DISABLED>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Admin</span>
					  	<input type="text" class="form-control" id="textAdmin" READONLY DISABLED>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Project code</span>
					  	<input type="text" class="form-control" id="textProjectCode" READONLY DISABLED>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Job requested date</span>
					  	<div class='input-group'>
							<input type='text' class="form-control" id="textJobRequestedDate" READONLY DISABLED/>
							<span class="input-group-addon calendar">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Job approve date</span>
					  	<div class='input-group'>
							<input type='text' class="form-control" id="textJobApproveDate" READONLY DISABLED/>
							<span class="input-group-addon calendar">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Process started date</span>
					  	<div class='input-group date'>
							<input type='text' class="form-control" id="textProcessStartedDate" />
							<span class="input-group-addon calendar">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
				</div>
			</div><div class="col-lg-6">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Estimated job completing date</span>
					  	<div class='input-group date'>
							<input type='text' class="form-control" id="textEstimatedJobCompletingDate" />
							<span class="input-group-addon calendar">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Process ended date</span>
					  	<div class='input-group date'>
							<input type='text' class="form-control" id="textProcessEndedDate" />
							<span class="input-group-addon calendar">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Date entered into the system</span>
					  	<div class='input-group date'>
							<input type='text' class="form-control" id="textSystemEnteredDate" name="textDateOfCompletion"/>
							<span class="input-group-addon calendar">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Estimated cost</span>
					  	<input type="text" class="form-control" id="textEstimatedCost" READONLY DISABLED>
					</div>
				</div>
			</div>
			
			<div id="operationCostContainer" class="col-lg-12" style="float:right">
				<div class="col-lg-12 costSection costEstimateContainer off" id="operationCostEditing" data-type="EDITING_COST">
					<div class="operationCostItem">
						<h4>Editing</h4>
						<table class="table table-condensed" >
							<tr>
								<td>Consultancy reference number</td>
								<td><input type="text" id="textEConsultancyReferenceNumber" name="textEConsultancyReferenceNumber" ></td>
							</tr>
							<tr>
								<td>Editor</td>
								<td><input type="text"  id="textEEditor" name="textEEditor" ></td>
							</tr>
							<tr>
								<td>Rate</td>
								<td><input type="text" class="amount" id="textERate" name="textERate" DISABLED READONLY ></td>
							</tr>
							<tr>
								<td>No of pages</td>
								<td><input type="text" class="amount" id="textENoOfPages" name="textENoOfPages" DISABLED READONLY></td>
							</tr>
							<tr>
								<td>Editing cost</td>
								<td><input type="text" class="amount" id="textEEditingCost" name="textEEditingCost" DISABLED READONLY></td>
							</tr>
							<tr>
								<td>Date sent for editing</td>
								<td>
									<div class='input-group date'>
										<input type='text' class="form-control" id="textEDateSentForEditing" name="textEDateSentForEditing"/>
										<span class="input-group-addon calendar">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<td>Started date</td>
								<td>
									<div class='input-group date'>
										<input type='text' class="form-control" id="textEDateStarted" name="textEDateStarted"/>
										<span class="input-group-addon calendar">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<td>Estimated ending date</td>
								<td>
									<div class='input-group date'>
										<input type='text' class="form-control" id="textEEstimatedEndingDate" name="textEEstimatedEndingDate"/>
										<span class="input-group-addon calendar">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<td>Ended date</td>
								<td>
									<div class='input-group date'>
										<input type='text' class="form-control" id="textEEndedDate" name="textEEndedDate"/>
										<span class="input-group-addon calendar">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<td>Progress</td>
								<td>
									<textarea class="form-control" id="textAreaEProgress" name="textAreaEProgress" rows="3"
							maxlength="100"></textarea>
								</td>
							</tr>
							<tr>
								<td>Manuscript sent for author's approval</td>
								<td>
									<div class='input-group date'>
										<input type='text' class="form-control" id="textEDateSentAuthorApprove" name="textEDateSentAuthorApprove"/>
										<span class="input-group-addon calendar">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</td>
							</tr>
						</table>
					</div>
				</div>
				<div class="col-lg-12 costSection costEstimateContainer off" id="operationCostProofRead" data-type="PROOF_READ_COST">
                    <div class="operationCostItem">
                        <h4>Proofreading</h4>
                        <table class="table table-condensed" >
                            <tr>
                                <td>Proof reader</td>
                                <td><input type="text" id="textPRProofReader" name="textPRProofReader"></td>
                            </tr>
                            <tr>
                                <td>Started date</td>
                                <td>
                                    <div class='input-group date'>
                                        <input type='text' class="form-control" id="textPRDateStarted" name="textPRDateStarted"/>
                                        <span class="input-group-addon calendar">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>Estimated ending date</td>
                                <td>
                                    <div class='input-group date'>
                                        <input type='text' class="form-control" id="textPRDateEstimatedEnd" name="textPRDateEstimatedEnd"/>
                                        <span class="input-group-addon calendar">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>Ended date</td>
                                <td>
                                    <div class='input-group date'>
                                        <input type='text' class="form-control"  id="textPRDateEnd" name="textPRDateEnd"/>
                                        <span class="input-group-addon calendar">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>Progress</td>
                                <td>
                                    <textarea class="form-control" id="textAreaPRProgress" name="textAreaPRProgress" rows="3" maxlength="100"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td>Final proof sent to author</td>
                                <td>
                                    <div class='input-group date'>
                                        <input type='text' class="form-control" id="textPRDateSentAuthor" name="textPRDateSentAuthor" />
                                        <span class="input-group-addon calendar">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>Final proof approved date</td>
                                <td>
                                    <div class='input-group date'>
                                        <input type='text' class="form-control" id="textPRDateApproved" name="textPRDateApproved" />
                                        <span class="input-group-addon calendar">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>Pre-press in date</td>
                                <td>
                                    <div class='input-group date'>
                                        <input type='text' class="form-control" id="textPRDatePrePress" name="textPRDatePrePress"/>
                                        <span class="input-group-addon calendar">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>Estimated pre-press out date</td>
                                <td>
                                    <div class='input-group date'>
                                        <input type='text' class="form-control"  id="textPRDatePrePressEstimatedOut" name="textPRDatePrePressEstimatedOut"/>
                                        <span class="input-group-addon calendar">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>Pre-press out date</td>
                                <td>
                                    <div class='input-group date'>
                                        <input type='text' class="form-control" id="textPRDatePrePressOut" name="textPRDatePrePressOut"/>
                                        <span class="input-group-addon calendar">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>Pre-press response</td>
                                <td>
                                    <textarea class="form-control" id="textAreaPRDatePrePressResponse" name="textAreaPRDatePrePressResponse" rows="3" maxlength="100"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td>Pre-press progress</td>
                                <td>
                                    <textarea class="form-control" id="textAreaPRDatePrePressProgress" name="textAreaPRDatePrePressProgress"  rows="3" maxlength="100"></textarea>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
				<div class="col-lg-12 costSection costEstimateContainer off" id="operationCostDesigning"  data-type="DESIGNING_COST">
					<div class="operationCostItem">
						<h4>Designing</h4>
						<table class="table table-condensed" >
							<tr>
								<td>Designer</td>
								<td><input type="text" id="textDDesigner" name="textDDesigner"></td>
							</tr>
							<tr>
								<td>Rate</td>
								<td><input type="text" class="amount" id="textDRate" name="textDRate" DISABLED READONLY></td>
							</tr>
							<tr>
								<td>Designing hours</td>
								<td><input type="text" class="amount" id="textDHours" name="textDHours" ></td>
							</tr>
							<tr>
								<td>Designing cost</td>
								<td><input type="text" class="amount" id="textDCost" name="textDCost" DISABLED READONLY></td>
							</tr>
							<tr>
								<td>Trimmed size</td>
								<td><input type="text" class="amount" id="textDTrimmedSize" name="textDTrimmedSize"  DISABLED READONLY></td>
							</tr>
							<tr>
								<td>Web version</td>
								<td>
									<div class='input-group date'>
										<input type='text' class="form-control"  id="textDWebVersion" name="textDWebVersion" />
										<span class="input-group-addon calendar">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<td>Started date</td>
								<td>
									<div class='input-group date'>
										<input type='text' class="form-control"  id="textDDateStarted" name="textDDateStarted"/>
										<span class="input-group-addon calendar">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<td>Estimated designing out date</td>
								<td>
									<div class='input-group date'>
										<input type='text' class="form-control" id="textDDateDesigningOut" name="textDDateDesigningOut"/>
										<span class="input-group-addon calendar">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<td>Ended date</td>
								<td>
									<div class='input-group date'>
										<input type='text' class="form-control" id="textDDateEnded" name="textDDateEnded"/>
										<span class="input-group-addon calendar">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<td>Progress</td>
								<td>
									<textarea class="form-control" id="textAreaDProgress" name="textAreaDProgress" rows="3" maxlength="100"></textarea>
								</td>
							</tr>
						</table>
					</div>
				</div>
				<div class="col-lg-12 costSection costEstimateContainer off" id="operationCostGraphics"  data-type="GRAPHICS_COST">
					<div class="operationCostItem">
						<h4>Graphics</h4>
						<table class="table table-condensed" >
							<tr>
								<td>Designer</td>
								<td><input type="text" id="textGDesigner" name="textGDesigner"></td>
							</tr>
							<tr>
								<td>Rate</td>
								<td><input type="text" class="amount" id="textGRate" name="textGRate" DISABLED READONLY></td>
							</tr>
							<tr>
								<td>Designing hours</td>
								<td><input type="text" class="amount" id="textGDHours" name="textGDHours" ></td>
							</tr>
							<tr>
								<td>Designing cost</td>
								<td><input type="text" class="amount" id="textGDCost" name="textGDCost" DISABLED READONLY></td>
							</tr>
							<tr>
								<td>Trimmed size</td>
								<td><input type="text" class="amount" id="textGTrimmedSize" name="textGTrimmedSize"  DISABLED READONLY></td>
							</tr>
							<tr>
								<td>Web version</td>
								<td>
									<div class='input-group date'>
										<input type='text' class="form-control"  id="textGWebVersion" name="textGWebVersion" />
										<span class="input-group-addon calendar">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<td>Started date</td>
								<td>
									<div class='input-group date'>
										<input type='text' class="form-control"  id="textGDateStarted" name="textGDateStarted"/>
										<span class="input-group-addon calendar">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<td>Estimated designing out date</td>
								<td>
									<div class='input-group date'>
										<input type='text' class="form-control" id="textGDateDesigningOut" name="textGDateDesigningOut"/>
										<span class="input-group-addon calendar">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<td>Ended date</td>
								<td>
									<div class='input-group date'>
										<input type='text' class="form-control" id="textGDateEnded" name="textGDateEnded"/>
										<span class="input-group-addon calendar">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<td>Progress</td>
								<td>
									<textarea class="form-control" id="textAreaGProgress" name="textAreaGProgress" rows="3" maxlength="100"></textarea>
								</td>
							</tr>
						</table>
					</div>
				</div>
				<div class="col-lg-12 costSection costEstimateContainer off" id="operationCostTypeSetting" data-type="TYPE_SETTING_COST">
					<div class="operationCostItem">
						<h4>Type setting</h4>
						<table class="table table-condensed" >
							<tr>
								<td>Consultancy reference number</td>
								<td><input type="text" id="textTSConsultancyRefNo" name="textTSConsultancyRefNo"></td>
							</tr>
							<tr>
								<td>Typesetter</td>
								<td><input type="text"  id="textTSTypesetter" name="textTSTypesetter"></td>
							</tr>
							<tr>
								<td>Rate</td>
								<td><input type="text" class="amount" id="textTSRate" name="textTSRate" DISABLED READONLY></td>
							</tr>
							<tr>
								<td>No of pages</td>
								<td><input type="text" class="amount" id="textTSNoOfPages" name="textTSNoOfPages" DISABLED READONLY></td>
							</tr>
							<tr>
								<td>Typesetting cost</td>
								<td><input type="text" class="amount" id="textTSTypeSettingCost" name="textTSTypeSettingCost"  DISABLED READONLY></td>
							</tr>
							<tr>
								<td>Date sent for editing</td>
								<td>
									<div class='input-group date'>
										<input type='text' class="form-control" id="textTSDateSentEditing" name="textTSDateSentEditing" />
										<span class="input-group-addon calendar">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<td>Started date</td>
								<td>
									<div class='input-group date'>
										<input type='text' class="form-control"  id="textTSDateStarted" name="textTSDateStarted"/>
										<span class="input-group-addon calendar">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<td>Estimated ending date</td>
								<td>
									<div class='input-group date'>
										<input type='text' class="form-control"  id="textTSDateEstimateEnd" name="textTSDateEstimateEnd"/>
										<span class="input-group-addon calendar">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<td>Ended date</td>
								<td>
									<div class='input-group date'>
										<input type='text' class="form-control" id="textTSDateEnd" name="textTSDateEnd"/>
										<span class="input-group-addon calendar">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<td>Progress</td>
								<td>
									<textarea class="form-control" id="textAreaTSProgress" name="textAreaTSProgress" rows="3" maxlength="100"></textarea>
								</td>
							</tr>
						</table>
					</div>
				</div>
				
				<div class="col-lg-12 costSection costEstimateContainer off" id="operationCostPrinting" data-type="PRINTING_COST">
					<div class="operationCostItem">
						<h4>Printing</h4>
						<table class="table table-condensed" >
							<tr>
								<td>Printer</td>
								<td><input type="text" id="textPPrinter" name="textPPrinter" ></td>
							</tr>
							<tr>
								<td>Quotation called date</td>
								<td>
									<div class='input-group date'>
										<input type='text' class="form-control"  id="textPQuatationCalledDate" name="textPQuatationCalledDate" />
										<span class="input-group-addon calendar">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<td>Quotations received date</td>
								<td>
									<div class='input-group date'>
										<input type='text' class="form-control" id="textPQuatationReceivedDate" name="textPQuatationReceivedDate" />
										<span class="input-group-addon calendar">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<td>Sent date for printing</td>
								<td>
									<div class='input-group date'>
										<input type='text' class="form-control" id="textPSentPrintingDate" name="textPSentPrintingDate"/>
										<span class="input-group-addon calendar">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<td>Rate</td>
								<td><input type="text" class="amount" id="textPRate" name="textPRate" DISABLED READONLY></td>
							</tr>
							<tr>
								<td>Number of pages</td>
								<td><input type="text" class="amount" id="textPNoOfPages" name="textPNoOfPages" DISABLED READONLY></td>
							</tr>
							<tr>
								<td>Purchase order number</td>
								<td><input type="text" id="textPPurchaseOrderNo" name="textPPurchaseOrderNo" ></td>
							</tr>
							<tr>
								<td>Print cost</td>
								<td><input type="text" class="amount" id="textPCost" name="textPCost" DISABLED READONLY></td>
							</tr>	
							<tr>
								<td>Estimated date of completion</td>
								<td>
									<div class='input-group date'>
										<input type='text' class="form-control" id="textPDateOfCompletion" name="textPDateOfCompletion" />
										<span class="input-group-addon calendar">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<td>Progress</td>
								<td>
									<textarea class="form-control" id="textAreaPProgress" name="textAreaPProgress" rows="3" maxlength="100"></textarea>
								</td>
							</tr>
							<tr>
								<td>Machine proof expected date</td>
								<td>
									<div class='input-group date'>
										<input type='text' class="form-control"  id="textPMachineProofExpectedDate" name="textPMachineProofExpectedDate" />
										<span class="input-group-addon calendar">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<td>Machine proof received date</td>
								<td>
									<div class='input-group date'>
										<input type='text' class="form-control"  id="textPMachineProofReceivedDate" name="textPMachineProofReceivedDate" />
										<span class="input-group-addon calendar">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<td>Machine proof approved date</td>
								<td>
									<div class='input-group date'>
										<input type='text' class="form-control" id="textPMachineProofApprovedDate" name="textPMachineProofApprovedDate"/>
										<span class="input-group-addon calendar">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<td>Delay cause</td>
								<td>
									<textarea class="form-control" id="textAreaPDelayCause" name="textAreaPDelayCause" rows="5" maxlength="200"></textarea>
								</td>
							</tr>
							<tr>
								<td>Actual delivery</td>
								<td>
									<div class='input-group date'>
										<input type='text' class="form-control" id="textPDateActualDelivery" name="textPDateActualDelivery" />
										<span class="input-group-addon calendar">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</td>
							</tr>
						</table>
					</div>
				</div>
				<div class="col-lg-12 costSection costEstimateContainer off" id="operationCostReprinting" data-type="REPRINTING_COST">
					<div class="operationCostItem">
						<h4>Reprinting</h4>
						<table class="table table-condensed" >
							<tr>
								<td>Printer</td>
								<td><input type="text" id="textRPPrinter" name="textRPPrinter" ></td>
							</tr>
							<tr>
								<td>Quotation called date</td>
								<td>
									<div class='input-group date'>
										<input type='text' class="form-control"  id="textRPQuatationCalledDate" name="textRPQuatationCalledDate" />
										<span class="input-group-addon calendar">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<td>Quotations received date</td>
								<td>
									<div class='input-group date'>
										<input type='text' class="form-control" id="textRPQuatationReceivedDate" name="textRPQuatationReceivedDate" />
										<span class="input-group-addon calendar">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<td>Sent date for printing</td>
								<td>
									<div class='input-group date'>
										<input type='text' class="form-control" id="textRPSentPrintingDate" name="textRPSentPrintingDate"/>
										<span class="input-group-addon calendar">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<td>Rate</td>
								<td><input type="text" class="amount" id="textRPRate" name="textRPRate" DISABLED READONLY></td>
							</tr>
							<tr>
								<td>Number of pages</td>
								<td><input type="text" class="amount" id="textRPNoOfPages" name="textRPNoOfPages" DISABLED READONLY></td>
							</tr>
							<tr>
								<td>Purchase order number</td>
								<td><input type="text" id="textRPPurchaseOrderNo" name="textRPPurchaseOrderNo" ></td>
							</tr>
							<tr>
								<td>Print cost</td>
								<td><input type="text" class="amount" id="textRPCost" name="textRPCost" DISABLED READONLY></td>
							</tr>	
							<tr>
								<td>Estimated date of completion</td>
								<td>
									<div class='input-group date'>
										<input type='text' class="form-control" id="textRPDateOfCompletion" name="textRPDateOfCompletion" />
										<span class="input-group-addon calendar">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<td>Progress</td>
								<td>
									<textarea class="form-control" id="textAreaRPProgress" name="textAreaRPProgress" rows="3" maxlength="100"></textarea>
								</td>
							</tr>
							<tr>
								<td>Machine proof expected date</td>
								<td>
									<div class='input-group date'>
										<input type='text' class="form-control"  id="textRPMachineProofExpectedDate" name="textRPMachineProofExpectedDate" />
										<span class="input-group-addon calendar">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<td>Machine proof received date</td>
								<td>
									<div class='input-group date'>
										<input type='text' class="form-control"  id="textRPMachineProofReceivedDate" name="textRPMachineProofReceivedDate" />
										<span class="input-group-addon calendar">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<td>Machine proof approved date</td>
								<td>
									<div class='input-group date'>
										<input type='text' class="form-control" id="textRPMachineProofApprovedDate" name="textRPMachineProofApprovedDate"/>
										<span class="input-group-addon calendar">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<td>Delay cause</td>
								<td>
									<textarea class="form-control" id="textAreaRPDelayCause" name="textAreaRPDelayCause" rows="5" maxlength="200"></textarea>
								</td>
							</tr>
							<tr>
								<td>Actual delivery</td>
								<td>
									<div class='input-group date'>
										<input type='text' class="form-control" id="textRPDateActualDelivery" name="textRPDateActualDelivery" />
										<span class="input-group-addon calendar">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</td>
							</tr>
						</table>
					</div>
				</div>
				<div class="col-lg-12 costSection costEstimateContainer off" id="operationCostTranslating" data-type="TRANSLATING_COST">
					<div class="operationCostItem">
						<h4>Translating</h4>
						<table class="table table-condensed" >
							<tr>
								<td>Contract number</td>
								<td><input type="text" id="textTContractNumber" name="textTContractNumber" ></td>
							</tr>
							<tr>
								<td>Translator</td>
								<td><input type="text" id="textTTranslator" name="textTTranslator"></td>
							</tr>
							<tr>
								<td>Rate</td>
								<td><input type="text" class="amount" id="textTRate" name="textTRate" DISABLED READONLY></td>
							</tr>
							<tr>
								<td>No of pages</td>
								<td><input type="text" class="amount" id="textTNoOfPages" name="textTNoOfPages" DISABLED READONLY></td>
							</tr>
							<tr>
								<td>Cost</td>
								<td><input type="text" class="amount" id="textTCost" name="textTCost" DISABLED READONLY></td>
							</tr>
							<tr>
								<td>Date sent for translating</td>
								<td>
									<div class='input-group date'>
										<input type='text' class="form-control" id="textTDateSentTranslating" name="textTDateSentTranslating" />
										<span class="input-group-addon calendar">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<td>Estimated date of receiving translation</td>
								<td>
									<div class='input-group date'>
										<input type='text' class="form-control" id="textTDateEstimatedReceivingTranslating" name="textTDateEstimatedReceivingTranslating"/>
										<span class="input-group-addon calendar">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<td>Date received translated</td>
								<td>
									<div class='input-group date'>
										<input type='text' class="form-control" id="textTDateReceivingTranslating" name="textTDateReceivingTranslating"/>
										<span class="input-group-addon calendar">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<td>Progress</td>
								<td>
									<textarea class="form-control" id="textAreaTProgress" name="textAreaTProgress" rows="3" maxlength="100"></textarea>
								</td>
							</tr>
							<tr>
								<td>Comments</td>
								<td>
									<textarea class="form-control" id="textAreaTComments" name="textAreaTComments" rows="5" maxlength="200"></textarea>
								</td>
							</tr>
						</table>
					</div>
				</div>

			</div>
			<div class="col-lg-12"><hr></div>
			<div class="col-lg-6">
				<div class="form-group" >
					<button type="button" class="btn btn-default" id="btnReset">Reset</button>
					<button type="button" class="btn btn-primary" id="btnSave">Save</button>
					<button type="button" class="btn btn-info" id="btnExportPDF"><i class="fa fa-file-pdf-o"></i>&nbsp;Export To PDF</button>
				</div>
			</div>
			<div class="col-lg-6" style="text-align: right;">
				<div class="form-group" >
					<button type="button" class="btn btn-success" id="btnNext" name="btnNext">Next >></button>
				</div>
			</div>
			<!-- <div class="col-lg-12">
				<div class="form-group" >
					<button type="button" class="btn btn-warning" name="btnConfirm">Confirm</button>
				</div>
			</div> -->

			<!-- --------FORM STATUS -------- -->
			<!-- -------- ---------- -------- -->
			<input type="submit" class="superHide" action="" id="hiddenJobId" name="hiddenJobId" value="${id}" />
		</form>
	</div>
</div>


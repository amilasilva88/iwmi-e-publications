<style>
#fileUploadingDiv {
	position: fixed;
	left:0; 
	top:0;
	width: 100%;
	height: 100%; 
	background: rgba(206, 206, 206, 0.5); 
	z-index: 1000;
	padding: 100px 0;
}
.off {
	display: none;
	visibility: hidden;
}
.on {
	display: block;
	visibility: visible;
}
#fileUploadingDiv .loading-container {
	padding: 20px 50px; 
	width:210px; 
	height: 70px; 
	background: rgba(255, 255, 255,0.5); 
	border: 1px solid #0B0B61; 
	clear: both; 
	border-radius: 20px;
}
</style>
<div id="fileUploadingDiv" class="off">
	<span style="position: fixed; left: 50%; top: 50%;">
		<div class="loading-container">
			<div style="float: center; height: 30px;width: 110px;">
				<img style="position: absolute; left: 85px;width:30px; height:30px;" src="../../images/uploading.gif"/>
			</div>
			<div style="clear:both"> </div>
		</div>
	</span>
</div>
<div class="col-lg-12 content-body">
	<jsp:include page="navigation/job-navigation-menu.jsp" />
	<div id="pageStepComplete" class="row stepContainer">
		<form role="form" id="formJobStatus">
			<div class="col-lg-12 hidden" id="listUsers">
				<div class="row col-lg-12">
	       			<h4 class="modal-title" id="myModalLabel">Select users to send job Completion notification</h4>
	    		</div>
	    		<div class="row col-lg-12"> <br></div>
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover" id="notify-users-data-table">
						<thead>
							<tr>
								<th>Name</th>	
								<th>Email</th>
								<th>Select</th>
							</tr>
						</thead>
					</table>
				</div>
				<div class="row col-lg-12"> <br></div>
				<div class="col-lg-12">
				  <div class="form-group" >
				      <i class="fa fa-exclamation-triangle alert-danger"></i> Press 'Confirm' to close the job. You will not be able to edit any information once confirmed
				  </div>   
				</div>
				<div class="col-lg-12"><hr></div>
				<div class="col-lg-6">
					<div class="form-group" >
						<button type="button" class="btn btn-default" id="btnReset">Reset</button>
					</div>
				</div>
				<div class="col-lg-6" style="text-align: right;">
					<div class="form-group" >
						<button type="button" class="btn btn-success" name="btnComplete" id="btnComplete">Confirm</button>
					</div>
				</div>
				<!-- --------FORM STATUS -------- -->
				<!-- -------- ---------- -------- -->
				<input type="submit" class="superHide" action="" id="hiddenJobId" name="hiddenJobId" value="${id}" />
			</div>
			<!-- -------------- --------------- -->
			<!-- ------------ Modal ----------- -->
			<!-- <div class="modal fade" id="modalNotAllowUsersList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"> -->
			<div id="contentForInvalid" class="row hidden">
	      		<div class="col-lg-12">
	      			<div class="col-lg-12">
	      				<div class="form-group" >
	      				    <div class="alert alert-warning">
						       <strong><i class="fa fa-exclamation-triangle"></i></strong>
						       <span>This Publication Job has been already completed</span>
						     </div>	
						</div>
					</div>
				</div>
			</div>						
		</form>
	</div>
</div>


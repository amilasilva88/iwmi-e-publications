<style>
#fileUploadingDiv {
	position: fixed;
	left:0; 
	top:0;
	width: 100%;
	height: 100%; 
	background: rgba(206, 206, 206, 0.5); 
	z-index: 1000;
	padding: 100px 0;
}
.off {
	display: none;
	visibility: hidden;
}
.on {
	display: block;
	visibility: visible;
}
#fileUploadingDiv .loading-container {
	padding: 20px 50px; 
	width:210px; 
	height: 100px; 
	background: rgba(255, 255, 255,0.5); 
	border: 1px solid #0B0B61; 
	clear: both; 
	border-radius: 20px;
}
#tableUploadedFiles {
	border: 1px solid #DDD;
}

</style>
<div id="fileUploadingDiv" class="off">
	<span style="position: fixed; left: 50%; top: 50%;">
		<div class="loading-container">
			<div style="float: center; height: 30px;width: 110px;">
				<img style="position: absolute; left: 85px;width:30px; height:30px;" src="../../images/uploading.gif"/>
			</div>
			<div style="float: left; height: 30px; width: 110px;text-align: center; font-weight: bold">File uploading...</div>
			<div style="clear:both"> </div>
		</div>
	</span>
</div>
<div class="col-lg-12 content-body">
	<jsp:include page="navigation/job-navigation-menu.jsp" />
	<div id="pageStepInitiation" class="row stepContainer">
		<form role="form" id="formJobInitiation" enctype="multipart/form-data" action="../file_repo/upload" method="post">
		    <div class="col-lg-6">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Requested by</span>
					  	<input type="text" class="form-control" id="textCreatedBy" name="textCreatedBy" READONLY>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Type of work *</span> 
						<select class="form-control" id="selectTypeOfWork" name="selectTypeOfWork" required>
					  		<option value="">-- Select --</option>
							<option value="Annual Report">Annual Report</option>
							<option value="Advertisement">Advertisement</option>
							<option value="Agreements/Proposal">Agreements/Proposal</option>
							<option value="Banner/Backdrop">Banner/Backdrop</option>
							<option value="Brochure">Brochure</option>
							<option value="Book">Book</option>
							<option value="Business card">Business card</option>
							<option value="CD Stickers">CD Stickers</option>
							<option value="CD Jackets">CD Jackets</option>
							<option value="Certificates">Certificates</option>
							<option value="Financial statement">Financial statement</option>
							<option value="Journal Article/paper">Journal Article/paper</option>
							<option value="Letterhead">Letter head</option>
							<option value="Internal Name card">Internal Name card</option>
							<option value="Poster">Poster</option>
							<option value="Policy brief">Policy brief</option>
							<option value="Proceeding">Proceeding</option>
							<option value="Research Report">Research Report</option>
							<option value="Working Paper">Working Paper</option>
							<option value="Other">Other</option>
					  	</select>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Title</span> 
						<input type="text" class="form-control" id="textTitle" name="textTitle" required maxlength="24">
					</div>
				</div>
			</div>
			<div class="col-lg-12">
				<div class="form-group textarea">
					<div class="input-group">
						<span class="input-group-addon">Description of work</span>
						<textarea class="form-control" id="textDescOfWork" name="textDescOfWork" rows="5"
							maxlength="500"></textarea>
					</div>
				</div>
			</div>
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading" style="background-color: #EEE !important;">
					   <div class="row">
					   	 <div class="col-lg-12">Services required</div>
					   </div>
					</div>
					<div class="panel-body">
						<div class="col-lg-12 group-check" id="costCategorySections">
						    <div class="col-lg-3">
			                    <div class="form-group">
			                        <div class="input-group">
			                            <span class="input-group-addon">
			                                <input type="checkbox" id="checkboxEditing"  name="checkboxCostEstimation" value="EDITING_COST"  detailContainer="editingEstimateContainer"> 
			                            </span>
			                            <label class="form-control" for="checkboxEditing">Editing</label>
			                        </div><!-- /input-group -->
			                    </div>
			                </div>
			                <div class="col-lg-3">
			                    <div class="form-group">
			                        <div class="input-group">
			                            <span class="input-group-addon">
			                                <input type="checkbox" id="checkboxProofReading"  name="checkboxCostEstimation" value="PROOF_READ_COST"  detailContainer="proofReadingEstimateContainer"> 
			                            </span>
			                            <label class="form-control" for="checkboxProofReading">Proof reading</label>
			                        </div><!-- /input-group -->
			                    </div>
			                </div>
			                <div class="col-lg-3">
			                    <div class="form-group">
			                        <div class="input-group">
			                            <span class="input-group-addon">
			                                <input type="checkbox" id="checkboxDesigning"  name="checkboxCostEstimation" value="DESIGNING_COST"  detailContainer="designingEstimateContainer"> 
			                            </span>
			                            <label class="form-control" for="checkboxDesigning">Designing</label>
			                        </div><!-- /input-group -->
			                    </div>
			                </div>
			                <div class="col-lg-3">
			                    <div class="form-group">
			                        <div class="input-group">
			                            <span class="input-group-addon">
			                                <input type="checkbox" id="checkboxGraphicsScanning"  name="checkboxCostEstimation" value="GRAPHICS_COST"  detailContainer="graphicsScanningEstimateContainer"> 
			                            </span>
			                            <label class="form-control" for="checkboxGraphicsScanning">Graphics</label>
			                        </div><!-- /input-group -->
			                    </div>
			                </div>
							<div class="col-lg-3">
								<div class="form-group">
									<div class="input-group">
									    <span class="input-group-addon">
									    	<input type="checkbox" id="checkboxTypesetting"  name="checkboxCostEstimation" value="TYPE_SETTING_COST" detailContainer="typesettingEstimateContainer"> 
									    </span>
									    <label class="form-control" for="checkboxTypesetting">Typesetting/Layout</label>
									</div><!-- /input-group -->
								</div>
							</div>
							<div class="col-lg-3">
								<div class="form-group">
									<div class="input-group">
									    <span class="input-group-addon">
									    	<input type="checkbox" id="checkboxPrinting" name="checkboxCostEstimation" value="PRINTING_COST"  detailContainer="printingEstimateContainer"> 
									    </span>
									    <label class="form-control" for="checkboxPrinting">Printing</label>
									</div><!-- /input-group -->
								</div>
							</div>
							<div class="col-lg-3">
								<div class="form-group">
									<div class="input-group">
									    <span class="input-group-addon">
									    	<input type="checkbox" id="checkboxReprinting" name="checkboxCostEstimation" value="REPRINTING_COST"  detailContainer="reprintingEstimateContainer"> 
									    </span>
									    <label class="form-control" for="checkboxReprinting">Reprinting</label>
									</div><!-- /input-group -->
								</div>
							</div>
			                <div class="col-lg-3">
			                    <div class="form-group">
			                        <div class="input-group">
			                            <span class="input-group-addon">
			                                <input type="checkbox" id="checkboxTranslating" name="checkboxCostEstimation" value="TRANSLATING_COST"  detailContainer="translatingEstimateContainer"> 
			                            </span>
			                            <label class="form-control" for="checkboxTranslating">Translating</label>
			                        </div><!-- /input-group -->
			                    </div>
			                </div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Print run</span>
					  	<input type="text" class="form-control" id="textPrintRun"  name="textPrintRun">
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Project code *</span>
					  	<input type="text" class="form-control" id="textProjectCode" name="textProjectCode" placeholder="XXX-XX-XX-XXX-XX-XXXX">
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Required delivery date *</span>
					  	<div class='input-group date'>
							<input type='text' class="form-control" id="textRequiredDeliveryDate" name="textRequiredDeliveryDate" />
							<span class="input-group-addon calendar">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Division *</span>
					  	<select class="form-control"  id="selectDivision" name="selectDivision">
					  		<option value="">-- Select --</option>
					  	</select>
					</div>
				</div>	
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Supervisor *</span>
					  	<select class="form-control"  id="selectAdminName" name="selectAdminName">
					  		<option value="">-- Select --</option>
					  	</select>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">e-Publishing Officer *</span>
					  	<select class="form-control"  id="selectSupervisorName" name="selectSupervisorName" required>
					  	    <option value="">-- Select --</option>
					  	</select>
					</div>
				</div>
			</div>
			
			<div class="col-lg-12">
				<div class="form-group textarea">
					<div class="input-group">
						<span class="input-group-addon">Other information</span>
					  	<textarea class="form-control" id="textOtherInformation" name="textOtherInformation" rows="2" maxlength="200"></textarea>
					</div>
				</div>
			</div>






			<!-- -------------------- -->

			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading" style="background-color: #EEE !important;">
					   <div class="row">
					   	 <div class="col-lg-12">Source files</div>
					   </div>
					</div>
					<div class="panel-body">
						<div class="col-lg-12">
							<div class="col-lg-6"> 
								<div class="table-responsive">
									<table class="table table-striped" id="tableUploadedFiles">
										<thead>
											<tr>
												<th>Files</th>
							                </tr>
			              				</thead>
			              				<tbody>
			                				<tr>
			                  					<td>No files uploaded</td>
							                </tr>
			              				</tbody>
			            			</table>
			          			</div>
			          		</div>
						</div>
						<div class="col-lg-12" id="attachmentContainer">
							<div id="container-attachments">
								<div class="col-lg-6">
									<div class="form-group">
										<div class="input-group">
											<div class="col-lg-11">
										    	<input type="file" name="resource_files[]" class="btn btn-default btn-file" >
										    </div>
										    <div class="col-lg-1">
												<a class="close">X</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<div class="input-group">
											<div class="col-lg-11">
										    	<input type="file" name="resource_files[]" class="btn btn-default btn-file" >
										    </div>
										    <div class="col-lg-1">
												<a class="close">X</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<!-- <a href="javascript:void(0)" id="aMoreAttachments">More attachments</a> -->
									<button type="button" class="btn btn-default" id="aMoreAttachments">More attachments</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- -------------------- -->















			<div class="col-lg-12"><hr></div>
			<div class="col-lg-6">
				<div class="form-group" >
					<button type="reset" class="btn btn-default" id="btnReset" name="btnReset">Reset</button>
					<button type="button" class="btn btn-primary" id="btnSave" name="btnSave" >Save</button>
				</div>
			</div>
			<div class="col-lg-6" style="text-align: right;">
				<div class="form-group" >
					<button type="button" class="btn btn-success" id="btnNext" name="btnNext">Submit</button>
				</div>
			</div>
			<input type="submit" class="superHide" action="" id="hiddenJobId" name="hiddenJobId" value="-1" />
		</form>
	</div>
</div>


<style>
#fileUploadingDiv {
	position: fixed;
	left:0; 
	top:0;
	width: 100%;
	height: 100%; 
	background: rgba(206, 206, 206, 0.5); 
	z-index: 1000;
	padding: 100px 0;
}
.off {
	display: none;
	visibility: hidden;
}
.on {
	display: block;
	visibility: visible;
}
#fileUploadingDiv .loading-container {
	padding: 20px 50px; 
	width:210px; 
	height: 100px; 
	background: rgba(255, 255, 255,0.5); 
	border: 1px solid #0B0B61; 
	clear: both; 
	border-radius: 20px;
}
</style>

<div id="fileUploadingDiv" class="off">
	<span style="position: fixed; left: 50%; top: 50%;">
		<div class="loading-container">
			<div style="float: center; height: 30px;width: 110px;">
				<img style="position: absolute; left: 85px;width:30px; height:30px;" src="../../images/uploading.gif"/>
			</div>
			<div style="float: left; height: 30px; width: 110px;text-align: center; font-weight: bold">File uploading...</div>
			<div style="clear:both"> </div>
		</div>
	</span>
</div>
<div class="col-lg-12 content-body">
	<jsp:include page="navigation/job-navigation-menu.jsp" />
	<div id="pageStepActualCosting" class="row stepContainer">
		<form role="form" id="formJobActualCosting" enctype="multipart/form-data" action="../file_repo/upload" method="post">
			<div class="col-lg-6">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Date of completion *</span>
					  	<div class='input-group date'>
							<input type='text' class="form-control" id="textDateOfCompletion" name="textDateOfCompletion" READONLY/>
							<span class="input-group-addon calendar">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
				</div>
			</div>
			<!-- 
			<div class="col-lg-12">
				<h5>Attachments</h5>
			</div>
			<div class="col-lg-12">
				<div class="col-lg-6"> 
					<div class="table-responsive">
						<table class="table table-striped" id="tableUploadedFiles">
							<thead>
								<tr>
									<th>Files</th>
				                </tr>
              				</thead>
              				<tbody>
                				<tr>
                  					<td>No files uploaded</td>
				                </tr>
              				</tbody>
            			</table>
          			</div>
          		</div>
			</div>
			<div class="col-lg-12" id="attachmentContainer">
				<div id="container-attachments">
					<div class="col-lg-6">
						<div class="form-group">
							<div class="input-group">
								<div class="col-lg-11">
							    	<input type="file" name="resource_files[]" class="btn btn-default btn-file" >
							    </div>
							    <div class="col-lg-1">
									<a class="close">�</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<div class="input-group">
								<div class="col-lg-11">
							    	<input type="file" name="resource_files[]" class="btn btn-default btn-file" >
							    </div>
							    <div class="col-lg-1">
									<a class="close">�</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-12">
					<div class="form-group">
						<button type="button" class="btn btn-default" id="aMoreAttachments">More attachments</button>
					</div>
				</div>
			</div>
			 -->
			<div class="col-lg-12 group-check" id="costCategorySections">
			    <div class="col-lg-3">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <input type="checkbox" id="checkboxEditing"  name="checkboxCostEstimation" value="EDITING_COST"  detailContainer="editingEstimateContainer"> 
                            </span>
                            <label class="form-control" for="checkboxEditing">Editing</label>
                        </div><!-- /input-group -->
                    </div>
                </div>
			    <div class="col-lg-3">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <input type="checkbox" id="checkboxProofReading"  name="checkboxCostEstimation" value="PROOF_READ_COST"  detailContainer="proofReadingEstimateContainer"> 
                            </span>
                            <label class="form-control" for="checkboxProofReading">Proof reading</label>
                        </div><!-- /input-group -->
                    </div>
                </div>
				<div class="col-lg-3">
					<div class="form-group">
						<div class="input-group">
						    <span class="input-group-addon">
						    	<input type="checkbox" id="checkboxDesigning"  name="checkboxCostEstimation" value="DESIGNING_COST"  detailContainer="designingEstimateContainer"> 
						    </span>
						    <label class="form-control" for="checkboxDesigning">Designing</label>
						</div><!-- /input-group -->
					</div>
				</div>
				<div class="col-lg-3">
					<div class="form-group">
						<div class="input-group">
						    <span class="input-group-addon">
						    	<input type="checkbox" id="checkboxGraphicsScanning"  name="checkboxCostEstimation" value="GRAPHICS_COST"  detailContainer="graphicsScanningEstimateContainer"> 
						    </span>
						    <label class="form-control" for="checkboxGraphicsScanning">Graphics</label>
						</div><!-- /input-group -->
					</div>
				</div>
				<div class="col-lg-3">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <input type="checkbox" id="checkboxTypesetting"  name="checkboxCostEstimation" value="TYPE_SETTING_COST" detailContainer="typesettingEstimateContainer"> 
                            </span>
                            <label class="form-control" for="checkboxTypesetting">Typesetting/Layout</label>
                        </div><!-- /input-group -->
                    </div>
                </div>
				<div class="col-lg-3">
					<div class="form-group">
						<div class="input-group">
						    <span class="input-group-addon">
						    	<input type="checkbox" id="checkboxPrinting" name="checkboxCostEstimation" value="PRINTING_COST"  detailContainer="printingEstimateContainer"> 
						    </span>
						    <label class="form-control" for="checkboxPrinting">Printing</label>
						</div><!-- /input-group -->
					</div>
				</div>
				<div class="col-lg-3">
					<div class="form-group">
						<div class="input-group">
						    <span class="input-group-addon">
						    	<input type="checkbox" id="checkboxReprinting" name="checkboxCostEstimation" value="REPRINTING_COST"  detailContainer="reprintingEstimateContainer"> 
						    </span>
						    <label class="form-control" for="checkboxReprinting">Reprinting</label>
						</div><!-- /input-group -->
					</div>
				</div>
				<div class="col-lg-3">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <input type="checkbox" id="checkboxTranslating" name="checkboxCostEstimation" value="TRANSLATING_COST"  detailContainer="translatingEstimateContainer"> 
                            </span>
                            <label class="form-control" for="checkboxTranslating">Translating</label>
                        </div><!-- /input-group -->
                    </div>
                </div>
			</div>
			<div id="operationCostContainer" class="col-lg-12" style="float:right">
			    <div class="col-lg-12  costSection costEstimateContainer off" id="editingEstimateContainer">
                    <div class="operationCostItem">
                        <h4>Editing</h4>
                        <table class="table table-condensed" >
                            <tr>
                                <td>No of pages</td>
                                <td><input type="text" class="amount" id="textNumberOfPagesE"  name="textNumberOfPages"></td>
                            </tr>
                            <tr>
                                <td>Cost per unit</td>
                                <td><input type="text" class="amount" id="textCostPerUnitE" name="textCostPerUnit"></td>
                            </tr>
                            <tr>
                                <td>Other costs</td>
                                <td><input type="text" class="amount" id="textOtherCostE" name="textOtherCost" overhead="0"></td>
                            </tr>
                            <tr>
                                <td>Sub total</td>
                                <td><input type="text" class="amount" id="textSubTotalE" name="textSubTotal" DISABLED></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-lg-12 costSection costEstimateContainer off" id="proofReadingEstimateContainer">
                    <div class="operationCostItem">
                        <h4>Proof reading</h4>
                        <table class="table table-condensed" >
                            <tr>
                                <td>No of pages</td>
                                <td><input type="text" class="amount" id="textNumberOfPagesPR"  name="textNumberOfPages"></td>
                            </tr>
                            <tr>
                                <td>Cost per unit</td>
                                <td><input type="text" class="amount" id="textCostPerUnitPR" name="textCostPerUnit"></td>
                            </tr>
                            <tr>
                                <td>Other costs</td>
                                <td><input type="text" class="amount" id="textOtherCostPR" name="textOtherCost" overhead="0"></td>
                            </tr>
                            <tr>
                                <td>Sub total</td>
                                <td><input type="text" class="amount" id="textSubTotalPR" name="textSubTotal" DISABLED></td>
                            </tr>
                        </table>
                    </div>
                </div>
				<div class="col-lg-12  costSection costEstimateContainer off" id="designingEstimateContainer">
					<div class="operationCostItem">
						<h4>Designing</h4>
						<table class="table table-condensed" >
							<tr>
								<td>No of pages</td>
								<td><input type="text" class="amount" id="textNumberOfPagesD"  name="textNumberOfPages"></td>
							</tr>
							<tr>
								<td>Cost per unit</td>
								<td><input type="text" class="amount" id="textCostPerUnitD" name="textCostPerUnit"></td>
							</tr>
							<tr>
								<td>Other costs</td>
								<td><input type="text" class="amount" id="textOtherCostD" name="textOtherCost" overhead="0"></td>
							</tr>
							<tr>
								<td>Sub total</td>
								<td><input type="text" class="amount" id="textSubTotalD" name="textSubTotal" DISABLED></td>
							</tr>
						</table>
					</div>
				</div>
				<div class="col-lg-12  costSection costEstimateContainer off" id="graphicsScanningEstimateContainer">
					<div class="operationCostItem">
						<h4>Graphics</h4>
						<table class="table table-condensed" >
							<tr>
								<td>No of pages</td>
								<td><input type="text" class="amount" id="textNumberOfPagesGS"  name="textNumberOfPages"></td>
							</tr>
							<tr>
								<td>Cost per unit</td>
								<td><input type="text" class="amount" id="textCostPerUnitGS" name="textCostPerUnit"></td>
							</tr>
							<tr>
								<td>Other costs</td>
								<td><input type="text" class="amount" id="textOtherCostGS" name="textOtherCost" overhead="0"></td>
							</tr>
							<tr>
								<td>Sub total</td>
								<td><input type="text" class="amount" id="textSubTotalGS" name="textSubTotal" DISABLED></td>
							</tr>
						</table>
					</div>
				</div>
				<div class="col-lg-12 costSection costEstimateContainer off" id="typesettingEstimateContainer">
                    <div class="operationCostItem">
                        <h4>Typesetting/Layout</h4>
                        <table class="table table-condensed" >
                            <tr>
                                <td>No of pages</td>
                                <td><input type="text" class="amount" id="textNumberOfPagesTS"  name="textNumberOfPages"></td>
                            </tr>
                            <tr>
                                <td>Cost per unit</td>
                                <td><input type="text" class="amount" id="textCostPerUnitTS" name="textCostPerUnit"></td>
                            </tr>
                            <tr>
                                <td>Other costs</td>
                                <td><input type="text" class="amount" id="textOtherCostTS" name="textOtherCost" overhead="0"></td>
                            </tr>
                            <tr>
                                <td>Sub total</td>
                                <td><input type="text" class="amount" id="textSubTotalTS" name="textSubTotal" DISABLED></td>
                            </tr>
                        </table>
                    </div>
                </div>
				<div class="col-lg-12  costSection costEstimateContainer off" id="printingEstimateContainer">
					<div class="operationCostItem">
						<h4>Printing</h4>
						<table class="table table-condensed" >
							<tr>
								<td>No of pages/copies</td>
								<td><input type="text" class="amount" id="textNumberOfPagesP"  name="textNumberOfPages"></td>
							</tr>
							<tr>
								<td>Cost per unit</td>
								<td><input type="text" class="amount" id="textCostPerUnitP" name="textCostPerUnit"></td>
							</tr>
							<tr>
								<td>Other costs</td>
								<td><input type="text" class="amount" id="textOtherCostP" name="textOtherCost" overhead="0"></td>
							</tr>
							<tr>
								<td>Sub total</td>
								<td><input type="text" class="amount" id="textSubTotalP" name="textSubTotal" DISABLED></td>
							</tr>
						</table>
					</div>
				</div>
				<div class="col-lg-12  costSection costEstimateContainer off" id="reprintingEstimateContainer">
					<div class="operationCostItem">
						<h4>Reprinting</h4>
						<table class="table table-condensed" >
							<tr>
								<td>No of pages/copies</td>
								<td><input type="text" class="amount" id="textNumberOfPagesRP"  name="textNumberOfPages"></td>
							</tr>
							<tr>
								<td>Cost per unit</td>
								<td><input type="text" class="amount" id="textCostPerUnitRP" name="textCostPerUnit" ></td>
							</tr>
							<tr>
								<td>Other costs</td>
								<td><input type="text" class="amount" id="textOtherCostRP" name="textOtherCost" overhead="0" ></td>
							</tr>
							<tr>
								<td>Sub total</td>
								<td><input type="text" class="amount" id="textSubTotalRP" name="textSubTotal" DISABLED></td>
							</tr>
						</table>
					</div>
				</div>
				<div class="col-lg-12  costSection costEstimateContainer off" id="translatingEstimateContainer">
                    <div class="operationCostItem">
                        <h4>Translating</h4>
                        <table class="table table-condensed" >
                            <tr>
                                <td>No of pages</td>
                                <td><input type="text" class="amount" id="textNumberOfPagesT"  name="textNumberOfPages"></td>
                            </tr>
                            <tr>
                                <td>Cost per unit</td>
                                <td><input type="text" class="amount" id="textCostPerUnitT" name="textCostPerUnit"></td>
                            </tr>
                            <tr>
                                <td>Other costs</td>
                                <td><input type="text" class="amount" id="textOtherCostT" name="textOtherCost" overhead="0"></td>
                            </tr>
                            <tr>
                                <td>Sub total</td>
                                <td><input type="text" class="amount" id="textSubTotalT" name="textSubTotal" DISABLED></td>
                            </tr>
                        </table>
                    </div>
                </div>
				<div class="col-lg-12 costEstimateContainer on" id="totalEstimateContainer">
					<div class="operationCostItem">
						<table class="table table-condensed" >
							<tr>
								<td>Total overhead</td>
								<td><input type="text" class="amount" id="textTotalOverheadCost" name="textTotalOverheadCost" DISABLED></td>
							</tr>
							<tr>
								<td>Total estimation</td>
								<td><input type="text" class="amount" id="textTotalAllSubCost"  name="textTotalAllSubCost" DISABLED></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="col-lg-12"><hr></div>
			<div class="col-lg-6">
				<div class="form-group" >
					<button type="button" class="btn btn-default" id="btnReset">Reset</button>
					<button type="button" class="btn btn-primary" id="btnSave">Save</button>
				</div>
			</div>
			<div class="col-lg-6" style="text-align: right;">
				<div class="form-group" >
					<button type="button" class="btn btn-success" id="btnNext">Submit</button>
				</div>
			</div>
			<input type="submit" class="superHide" action="" id="hiddenJobId" name="hiddenJobId" value="${id}" />
		</form>
		<div class="modal fade" id="modalBusy" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="row">
				    <div class="col-md-3 col-sm-4">
				      <p style="background: white">
				        <i class="fa fa-spinner fa-spin fa-3x fa-fw margin-bottom"></i>
				      </p>
				    </div>
				</div>
		  	</div>
		</div>
		
	</div>
</div>
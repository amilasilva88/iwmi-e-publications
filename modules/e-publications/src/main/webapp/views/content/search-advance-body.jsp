<div class="col-lg-12 content-body">
	<div class="panel panel-default">
		<div class="panel-heading">Advance Search</div>
		<div class="panel-body">
			<div id="pageStepDelivery" class="row stepContainer">
				<form role="form" id="formSearchAdvance">
					<div class="col-lg-4">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Job number</span>
								<input type='text' class="form-control" id="textJobNumber" name="textJobNumber" field="job_number_t"/>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Project code</span>
								<select class="form-control" id="selectProjectCode" name="selectProjectCode" field="project_code_t">
							  		<option value="">--Select---</option>
							  	</select>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Year</span>
								<select class="form-control" id="selectYear" name="selectYear" field="year_i">
							  		<option value="">--Select--</option>
							  	</select>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Series</span>
								<input type='text' class="form-control" id="textSeries" name="textSeries" field="series_t"/>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">ISBN-ISSN</span>
								<input type='text' class="form-control" id="textISBNISSN" name="textISBNISSN" field="isbn_issn_t"/>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Type of the job</span>
								<select class="form-control" id="selectTypeOfJob" name="selectTypeOfJob" field="type_of_work_t">
							  		<option value="">--Select--</option>
							  		<option value="Annual Report">Annual Report</option>
									<option value="Advertisement">Advertisement</option>
									<option value="Agreements/Proposal">Agreements/Proposal</option>
									<option value="Banner/Backdrop">Banner/Backdrop</option>
									<option value="Brochure">Brochure</option>
									<option value="Book">Book</option>
									<option value="Business card">Business card</option>
									<option value="CD Stickers">CD Stickers</option>
									<option value="CD Jackets">CD Jackets</option>
									<option value="Certificates">Certificates</option>
									<option value="Financial statement">Financial statement</option>
									<option value="Journal Article/paper">Journal Article/paper</option>
									<option value="Letterhead">Letter head</option>
									<option value="Internal Name card">Internal Name card</option>
									<option value="Poster">Poster</option>
									<option value="Policy brief">Policy brief</option>
									<option value="Proceeding">Proceeding</option>
									<option value="Research Report">Research Report</option>
									<option value="Working Paper">Working Paper</option>
									<option value="Other">Other</option>
							  	</select>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Title</span>
								<input type='text' class="form-control" id="textTitle" name="textTitle" field="title_t"/>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Author</span>
								<input type='text' class="form-control" id="textAuthor" name="textAuthor" field="author_t"/>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Related program</span>
								<input type='text' class="form-control" id="textRelatedProgram" name="textRelatedProgram" field="related_program_t"/>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Status</span>
								<select class="form-control" id="selectStatus" name="selectStatus" field="status_t">
							  		<option value="">--Select--</option>
							  		<option value="NEW">New</option>
							  		<option value="INITIALISED">Initialized</option>
							  		<option value="ESTIMATED">Estimated</option>
							  		<option value="APPROVED">Approved</option>
							  		<option value="NEED_AN_AMENDMENT">Need An Amendment</option>
							  		<option value="REJECTED">Rejected</option>
							  		<option value="DELIVERED">Delivered</option>
							  		<option value="COSTING">Costing</option>
							  		<option value="COMPLETED">Completed</option>
							  		<!-- <option value="DELETED">Deleted</option> -->
							  	</select>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Supervisor's name</span>
								<select class="form-control" id="selectSupervisor" name="selectSupervisor" field="supervisorName_t">
							  		<option value="">---Select---</option>
							  	</select>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">e-Publishing Officer</span>
								<select class="form-control" id="selectRequester" name="selectRequester" field="requester_t">
							  		<option value="">---Select---</option>
							  	</select>
							</div>
						</div>
					</div>
					<div class="col-lg-12"><hr></div>
					<div class="col-lg-12">
						<div class="form-group" >
							<button type="button" class="btn btn-default" id="btnSearch"><i class="fa fa-search-o"></i>&nbsp;Search</button>
							<button type="button" class="btn btn-success" id="btnExcelExport"><i class="fa fa-file-excel-o"></i>&nbsp;Export Results to Excel</button>
						</div>
					</div>
				</form>
				<div class="row" style="padding:0 10px">
					<div class="col-lg-12 content-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="publication-items-data-table">
								<thead>
									<tr>
										<th>Type of work</th>	
										<th>Project code</th>
										<th>Required delivery date</th>
										<th>Division</th>
										<th>Status</th>
										<th>Job number</th>
										<th>Series</th>
										<th>Created by</th>
										<th>Action</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-lg-12 content-body">
	<jsp:include page="navigation/job-navigation-menu.jsp" />
	<div id="pageStepDelivery" class="row stepContainer">
		<form role="form" id="formJobDelivery">
			<div class="col-lg-12">
				<div class="form-group textarea">
					<div class="input-group">
						<span class="input-group-addon">Comments</span>
						<textarea class="form-control" id="textAreaDeliveryComment" name="textAreaDeliveryComment" rows="5"
							maxlength="500"></textarea>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Date</span>
					  	<div class='input-group date' id='datetimepickerDelivery'>
							<input type='text' class="form-control" id="textDeliveryDate" name="textDeliveryDate"/>
							<span class="input-group-addon calendar">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-12"><hr></div>
			<div class="col-lg-6">
				<div class="form-group" >
					<button type="button" class="btn btn-default" id="btnReset">Reset</button>
					<button type="button" class="btn btn-primary" id="btnSave">Save</button>
				</div>
			</div>
			<div class="col-lg-6" style="text-align: right;">
				<div class="form-group" >
					<button type="button" class="btn btn-success" id="btnNext">Submit</button>
				</div>
			</div>
			<input type="submit" class="superHide" action="" id="hiddenJobId" name="hiddenJobId" value="${id}" />
		</form>
	</div>
</div>

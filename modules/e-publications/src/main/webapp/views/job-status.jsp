<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">

<jsp:include page="layout/header.jsp" />
<body>

	<div id="wrapper">
		<jsp:include page="layout/menu.jsp" />

		<!-- Page Content -->
		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h2 class="page-header">Status</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<jsp:include page="content/job-status-body.jsp" />
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->
	<jsp:include page="layout/footer.jsp" />
	<script type="text/javascript">
	$(function() {
		$('#costCategorySections input[type=checkbox]').each(function () {
			var contentId = $(this).attr('detailContainer');//+"EstimateContainer";
			$("#"+contentId).removeClass("off");
			$("#"+contentId).removeClass("on");
			if (this.checked) { 
				$("#"+contentId).addClass("on");
			} else {
				$("#"+contentId+" input[type=text]").val("");
				$("#"+contentId).addClass("off");
			}
		});
		
		loadDataForId(${id});
		
		$("#btnSave").click(validateAndSaveData);
		$("#btnExportPDF").click(downloadeResource);
		$("#btnNext").click(goToNext);
		
		
	});
	function downloadeResource() {
		var jobId = $("#hiddenJobId").val();	
		window.location.href='../status/export/'+jobId
	}
	function goToNext() {
		var jobId = $("#hiddenJobId").val();	
		window.location.href='../jobComplete/page?id='+jobId
	}
	function submitFormNext() {
		submitForm(ACTION.NEXT)
	}
	function submitFormSave() {
		submitForm(ACTION.SAVE)
	}
	function submitForm(action) {
		$('#hiddenJobId').attr("action", action)
		var event = jQuery.Event( "click" );
		$( "#hiddenJobId" ).trigger(event);
	}
	function loadDataForId(itemId) {
		var itemStatus = null;
		
		if (null != itemId) {
			var sectionData = null;
			sectionData = getRequest("./load/"+itemId);
			if ((sectionData != null) || (sectionData != ""))  {
				if (null != sectionData['canEdit']) {
					var isDisable = !sectionData['canEdit'];
					JobLibrary.setDisableAllInput(isDisable);
				}
				if ((null != sectionData['item_status']) && (sectionData['item_status'] !="")) {
					itemStatus = sectionData['item_status'];
				}
				$('#hiddenJobId').val(itemId);
				if ((null != sectionData['id']) && (sectionData['id'] !="")) {
					$('#hiddenJobId').val(sectionData['id']);
				}
				
				if ((null != sectionData['jobNumber']) && (sectionData['jobNumber'] !="")) {
					$('#textJobNumber').val(sectionData['jobNumber']);
				}
				if ((null != sectionData['relatedProgram']) && (sectionData['relatedProgram'] !="")) {
					$('#textRelatedProgram').val(sectionData['relatedProgram']);
				} 
				if ((null != sectionData['typeOfJob']) && (sectionData['typeOfJob'] !="")) {
					$('#textTypeOfTheJob').val(sectionData['typeOfJob']);
				}
				if ((null != sectionData['series']) && (sectionData['series'] !="")) {
					$('#textSeries').val(sectionData['series']);
				}
				if ((null != sectionData['title']) && (sectionData['title'] !="")) {
					$('#textTitle').val(sectionData['title']);
				}
				if ((null != sectionData['author']) && (sectionData['author'] !="")) {
					$('#textAuthor').val(sectionData['author']);
				}
				if ((null != sectionData['manuScriptReceiveDate']) && (sectionData['manuScriptReceiveDate'] !="")) {
					$('#textManuScripReceiveDate').val(sectionData['manuScriptReceiveDate']);
				} 
				if ((null != sectionData['noOfManuScriptPages']) && (sectionData['noOfManuScriptPages'] !="")) {
					$('#textNoOfManuscriptPages').val(sectionData['noOfManuScriptPages']);
				}
				if ((null != sectionData['isbn_issn']) && (sectionData['isbn_issn'] !="")) {
					$('#textISBN-ISSN').val(sectionData['isbn_issn']);
				} 
				if ((null != sectionData['requester']) && (sectionData['requester'] !="")) {
					$('#textSupervisor').val(sectionData['requester']);
				}
				if ((null != sectionData['supervisour']) && (sectionData['supervisour'] !="")) {
					$('#textAdmin').val(sectionData['supervisour']);
				} 
				if ((null != sectionData['projectCode']) && (sectionData['projectCode'] !="")) {
					$('#textProjectCode').val(sectionData['projectCode']);
				}
				if ((null != sectionData['jobRequestDate']) && (sectionData['jobRequestDate'] !="")) {
					$('#textJobRequestedDate').val(sectionData['jobRequestDate']);
				} 
				if ((null != sectionData['jobApprovedDate']) && (sectionData['jobApprovedDate'] !="")) {
					$('#textJobApproveDate').val(sectionData['jobApprovedDate']);
				}
				if ((null != sectionData['processStartDate']) && (sectionData['processStartDate'] !="")) {
					$('#textProcessStartedDate').val(sectionData['processStartDate']);
				} 
				if ((null != sectionData['estimatedCompletionDate']) && (sectionData['estimatedCompletionDate'] !="")) {
					$('#textEstimatedJobCompletingDate').val(sectionData['estimatedCompletionDate']);
				}
				if ((null != sectionData['processEndDate']) && (sectionData['processEndDate'] !="")) {
					$('#textProcessEndedDate').val(sectionData['processEndDate']);
				} 
				if ((null != sectionData['systemEnteredDate']) && (sectionData['systemEnteredDate'] !="")) {
					$('#textSystemEnteredDate').val(sectionData['systemEnteredDate']);
				}
				if ((null != sectionData['estimatedCost']) && (sectionData['estimatedCost'] !="")) {
					$('#textEstimatedCost').val(sectionData['estimatedCost']);
				}
				/*
				if ((null != sectionData['costElements']) && (sectionData['costElements'].length>0)) {
					var costElements = sectionData['costElements'];
					for (var i=0; i<costElements.length;i++) {
						var tmpCostElement = costElements[i];
						switch (tmpCostElement['cost_type']) {
							case "TYPE_SETTING_COST":
								setDetailCostTypeSettingData(tmpCostElement);
								break;
							case "EDITING_COST":
								setDetailCostEditingData(tmpCostElement);
								break;
							case "DESIGNING_COST":
								setDetailCostDesigningData(tmpCostElement);
								break;
							case "PROOF_READ_COST":
								setDetailCostProofReadingData(tmpCostElement);
								break;
							case "TRANSLATING_COST":
								setDetailCostTranslatingData(tmpCostElement);
								break;
							case "PRINTING_COST":
								setDetailCostPrintingData(tmpCostElement);
								break;
						}
					}
				}
				*/
				$("#operationCostEditing").removeClass("on");
				$("#operationCostEditing").addClass("off");
				$("#operationCostTypeSetting").removeClass("on");
				$("#operationCostTypeSetting").addClass("off");
				$("#operationCostProofRead").removeClass("on");
				$("#operationCostProofRead").addClass("off");
				$("#operationCostDesigning").removeClass("on");
				$("#operationCostDesigning").addClass("off");
				$("#operationCostPrinting").removeClass("on");
				$("#operationCostPrinting").addClass("off");
				$("#operationCostTranslating").removeClass("on");
				$("#operationCostTranslating").addClass("off");
				console.log(JSON.stringify(sectionData))
				if (null != sectionData['TYPE_SETTING_COST']) {
					$("#operationCostTypeSetting").removeClass("off");
					$("#operationCostTypeSetting").addClass("on");
					setDetailCostTypeSettingData(sectionData['TYPE_SETTING_COST']);
				}
				if (null != sectionData['EDITING_COST']) {
					$("#operationCostEditing").removeClass("off");
					$("#operationCostEditing").addClass("on");
					setDetailCostEditingData(sectionData['EDITING_COST']);
				}
				if (null != sectionData['DESIGNING_COST']) {
					$("#operationCostDesigning").removeClass("off");
					$("#operationCostDesigning").addClass("on");
					setDetailCostDesigningData(sectionData['DESIGNING_COST']);
				}
				if (null != sectionData['GRAPHICS_COST']) {
					$("#operationCostGraphics").removeClass("off");
					$("#operationCostGraphics").addClass("on");
					setDetailCostGraphicsData(sectionData['GRAPHICS_COST']);
				}
				if (null != sectionData['PROOF_READ_COST']) {
					$("#operationCostProofRead").removeClass("off");
					$("#operationCostProofRead").addClass("on");
					setDetailCostProofReadingData(sectionData['PROOF_READ_COST']);
				}
				if (null != sectionData['TRANSLATING_COST']) {
					$("#operationCostTranslating").removeClass("off");
					$("#operationCostTranslating").addClass("on");
					setDetailCostTranslatingData(sectionData['TRANSLATING_COST']);
				}
				if (null != sectionData['PRINTING_COST']) {
					$("#operationCostPrinting").removeClass("off");
					$("#operationCostPrinting").addClass("on");
					setDetailCostPrintingData(sectionData['PRINTING_COST']);
				}
				if (null != sectionData['REPRINTING_COST']) {
					$("#operationCostReprinting").removeClass("off");
					$("#operationCostReprinting").addClass("on");
					setDetailCostReprintingData(sectionData['REPRINTING_COST']);
				}
			}
		}
		JobLibrary.setItemStatus(itemStatus, JobStatus.COSTING);
	}
	function saveAndGotoNext() {
		var response = validateAndSave();
		var isSuccess = (response['status'] == '1000');
		JobLibrary.showJobActionStatus(isSuccess);
		if (isSuccess) {
			$("#hiddenJobId").val(response['id']);	
			window.location.href='../costEstimation/page?id=' + response['id']
		}
	}
	function validateAndSaveData() {
		$('#hiddenJobId').attr("action", ACTION.SAVE);
		var response = validateAndSave();
		var isSuccess = (response['status'] == '1000');
		JobLibrary.showJobActionStatus(isSuccess);
		if(isSuccess) {
			$("#hiddenJobId").val(response['id']);	
		 	JobLibrary.setItemStatus(response['item_status'], JobStatus.COSTING);
		}
	}
	
	function validateAndSave() {
		var hiddenJobId = $("#hiddenJobId").val();
		var textRelatedProgram = $("#textRelatedProgram").val();
		var textTitle = $("#textTitle").val();
		var textAuthor = $("#textAuthor").val();
		var textManuScripReceiveDate = $("#textManuScripReceiveDate").val();
		var textNoOfManuscriptPages = $("#textNoOfManuscriptPages").val();
		var textISBNISSN = $("#textISBN-ISSN").val();
		var textProcessStartedDate = $("#textProcessStartedDate").val();
		var textEstimatedJobCompletingDate = $("#textEstimatedJobCompletingDate").val();
		var textProcessEndedDate = $("#textProcessEndedDate").val();
		var textSystemEnteredDate = $("#textSystemEnteredDate").val();
		var costElementsData = [];
		var userAction = $("#hiddenJobId").attr("action");		
		var postData = {
				action: userAction,
				id:hiddenJobId,
				title: textTitle,
				relatedProgram: textRelatedProgram,
				author: textAuthor,
				manuScriptReceiveDate: textManuScripReceiveDate,
				noOfManuScriptPages: textNoOfManuscriptPages,
				isbn_issn: textISBNISSN,
				processStartDate: textProcessStartedDate,
				estimatedCompletionDate: textEstimatedJobCompletingDate,
				systemEnteredDate: textSystemEnteredDate,
				processEndDate: textProcessEndedDate,
				//costElements: costElementsData,
			};
		$('.costSection.costEstimateContainer.on').each(function() {
			switch ($(this).attr("data-type")) {
				case "TYPE_SETTING_COST":
					var dataCostElement = getDetailCostTypeSettingData();
					postData["TYPE_SETTING_COST"]=dataCostElement;
					costElementsData.push(dataCostElement);
					break;
				case "EDITING_COST":
					var dataCostElement = getDetailCostEditingData();
					postData["EDITING_COST"]=dataCostElement;
					costElementsData.push(dataCostElement);
					break;
				case "DESIGNING_COST":
					var dataCostElement = getDetailCostDesigningData();
					postData["DESIGNING_COST"]=dataCostElement;
					costElementsData.push(dataCostElement);
					break;
				case "GRAPHICS_COST":
					var dataCostElement = getDetailCostGraphicsData();
					postData["GRAPHICS_COST"]=dataCostElement;
					costElementsData.push(dataCostElement);
					break;
				case "PROOF_READ_COST":
					var dataCostElement = getDetailCostProofReadingData();
					postData["PROOF_READ_COST"]=dataCostElement;
					costElementsData.push(dataCostElement);
					break;
				case "TRANSLATING_COST":
					var dataCostElement = getDetailCostTranslatingData();
					postData["TRANSLATING_COST"]=dataCostElement;
					costElementsData.push(dataCostElement);
					break;
				case "PRINTING_COST":
					var dataCostElement = getDetailCostPrintingData();
					postData["PRINTING_COST"]=dataCostElement;
					costElementsData.push(dataCostElement);
					break;
				case "REPRINTING_COST":
					var dataCostElement = getDetailCostReprintingData();
					postData["REPRINTING_COST"]=dataCostElement;
					costElementsData.push(dataCostElement);
					break;
			}
		});
		//TODO- validation check
		
		var formatedPostData = {
				id:hiddenJobId,
				statusData:postData
		}
		var postUrl = "../status/saveData";
		var result = postRequest(postUrl, formatedPostData);
		return result;
	};
	
	function getDetailCostTypeSettingData() {
		var textTSConsultancyRefNo = $("#textTSConsultancyRefNo").val();
		var textTSTypesetter = $("#textTSTypesetter").val();
		//----------- Readonly ---------------
		var textTSRate = $("#textTSRate").val();
		var textTSNoOfPages = $("#textTSNoOfPages").val();
		var textTSTypeSettingCost = $("#textTSTypeSettingCost").val();
		//------------ ------ ----------------
		var textTSDateSentEditing = $("#textTSDateSentEditing").val();
		var textTSDateStarted = $("#textTSDateStarted").val();
		var textTSDateEstimateEnd = $("#textTSDateEstimateEnd").val();
		var textTSDateEnd = $("#textTSDateEnd").val();
		var textAreaTSProgress = $("#textAreaTSProgress").val();
		var typeSettingData = {
			cost_type:"TYPE_SETTING_COST",
			ref_number: textTSConsultancyRefNo,
			type_setter: textTSTypesetter,
			cost_per_unit: textTSRate,
			no_of_pages: textTSNoOfPages,
			total_cost: textTSTypeSettingCost,
			date_sent: textTSDateSentEditing,
			started_date: textTSDateStarted,
			est_end_date: textTSDateEstimateEnd,
			ended_date: textTSDateEnd,
			progress: textAreaTSProgress
		};
		return typeSettingData;
	};
	function setDetailCostTypeSettingData(data) {
		if ((null != data['type_setter']) && (data['type_setter'] !="")) {
			$("#textTSTypesetter").val(data['type_setter']);
		}
		if ((null != data['ref_number']) && (data['ref_number'] !="")) {
			$("#textTSConsultancyRefNo").val(data['ref_number']);
		}
		//----------- Readonly ---------------
		if ((null != data['cost_per_unit']) && (data['cost_per_unit'] !="")) {
			$("#textTSRate").val(data['cost_per_unit']);
		}
		if ((null != data['no_of_pages']) && (data['no_of_pages'] !="")) {
			$("#textTSNoOfPages").val(data['no_of_pages']);
		}
		if ((null != data['total_cost']) && (data['total_cost'] !="")) {
			$("#textTSTypeSettingCost").val(data['total_cost']);
		}
		//------------ ------ ----------------
		if ((null != data['date_sent']) && (data['date_sent'] !="")) {
			$("#textTSDateSentEditing").val(data['date_sent']);
		}
		if ((null != data['started_date']) && (data['started_date'] !="")) {
			$("#textTSDateStarted").val(data['started_date']);
		}
		if ((null != data['est_end_date']) && (data['est_end_date'] !="")) {
			$("#textTSDateEstimateEnd").val(data['est_end_date']);
		}
		if ((null != data['ended_date']) && (data['ended_date'] !="")) {
			$("#textTSDateEnd").val(data['ended_date']);
		}
		if ((null != data['progress']) && (data['progress'] !="")) {
			$("#textAreaTSProgress").val(data['progress']);
		}
	};
	function getDetailCostEditingData() {
		var textEConsultancyReferenceNumber = $("#textEConsultancyReferenceNumber").val();
		var textEEditor = $("#textEEditor").val();
		// --------  Readonly -------------
		var textERate = $("#textERate").val();
		var textENoOfPages = $("#textENoOfPages").val();
		var textEEditingCost = $("#textEEditingCost").val();
		// --------  -------- -------------
		var textEDateSentForEditing = $("#textEDateSentForEditing").val();
		var textEDateStarted = $("#textEDateStarted").val();
		var textEEstimatedEndingDate = $("#textEEstimatedEndingDate").val();
		var textEEndedDate = $("#textEEndedDate").val();
		var textAreaEProgress = $("#textAreaEProgress").val();
		var textEDateSentAuthorApprove = $("#textEDateSentAuthorApprove").val();
		
		var editingData = {
			cost_type:"EDITING_COST",
			ref_number: textEConsultancyReferenceNumber,
			editor: textEEditor,
			cost_per_unit: textERate,
			no_of_pages: textENoOfPages,
			total_cost: textEEditingCost,
			date_sent: textEDateSentForEditing,
			started_date: textEDateStarted,
			est_end_date: textEEstimatedEndingDate,
			ended_date: textEEndedDate,
			progress: textAreaEProgress,
			authors_approval_date: textEDateSentAuthorApprove
		};
		return editingData;
	};
	function setDetailCostEditingData(data) {
		if ((null != data['ref_number']) && (data['ref_number'] !="")) {
			$("#textEConsultancyReferenceNumber").val(data['ref_number']);
		}
		if ((null != data['editor']) && (data['editor'] !="")) {
			$("#textEEditor").val(data['editor']);
		}
		// --------  Readonly -------------
		if ((null != data['cost_per_unit']) && (data['cost_per_unit'] !="")) {
			$("#textERate").val(data['cost_per_unit']);
		}
		if ((null != data['no_of_pages']) && (data['no_of_pages'] !="")) {
			$("#textENoOfPages").val(data['no_of_pages']);
		}
		if ((null != data['total_cost']) && (data['total_cost'] !="")) {
			$("#textEEditingCost").val(data['total_cost']);
		}
		// --------  -------- -------------
		if ((null != data['date_sent']) && (data['date_sent'] !="")) {
			$("#textEDateSentForEditing").val(data['date_sent']);
		}
		if ((null != data['started_date']) && (data['started_date'] !="")) {
			$("#textEDateStarted").val(data['started_date']);
		}
		if ((null != data['est_end_date']) && (data['est_end_date'] !="")) {
			$("#textEEstimatedEndingDate").val(data['est_end_date']);
		}
		if ((null != data['ended_date']) && (data['ended_date'] !="")) {
			$("#textEEndedDate").val(data['ended_date']);
		}
		if ((null != data['progress']) && (data['progress'] !="")) {
			$("#textAreaEProgress").val(data['progress']);
		}
		if ((null != data['authors_approval_date']) && (data['authors_approval_date'] !="")) {
			$("#textEDateSentAuthorApprove").val(data['authors_approval_date']);
		}
	};
	function getDetailCostDesigningData() {	
		var textDDesigner = $("#textDDesigner").val();
		var textDHours = $("#textDHours").val();
		// -------- Readonly ----------
		var textDRate = $("#textDRate").val();
		var textDCost = $("#textDCost").val();
		var textDTrimmedSize = $("#textDTrimmedSize").val();
		// -------- -------- -----------
		var textDWebVersion = $("#textDWebVersion").val();
		var textDDateStarted = $("#textDDateStarted").val();
		var textDDateDesigningOut = $("#textDDateDesigningOut").val();
		var textDDateEnded = $("#textDDateEnded").val();
		var textAreaDProgress = $("#textAreaDProgress").val();
		var dataDesigning = {
				cost_type: "DESIGNING_COST",
				designer: textDDesigner,
				designing_hours: textDHours,
				rate: textDRate,
				total_cost: textDCost,
				trimmed_size: textDTrimmedSize,
				web_version_date: textDWebVersion,
				started_date: textDDateStarted,
				designing_out_date: textDDateDesigningOut,
				ended_date: textDDateEnded,
				progress: textAreaDProgress
		};
		return dataDesigning;
	};
	function setDetailCostDesigningData(data) {	
		if ((null != data['designer']) && (data['designer'] !="")) { 
			$("#textDDesigner").val(data['designer']);
		}
		if ((null != data['designing_hours']) && (data['designing_hours'] !="")) { 
			$("#textDHours").val(data['designing_hours']);
		}
		// -------- Readonly ----------
		if ((null != data['rate']) && (data['rate'] !="")) { 
			$("#textDRate").val(data['rate']);
		}
		if ((null != data['total_cost']) && (data['total_cost'] !="")) { 
			$("#textDCost").val(data['total_cost']);
		}
		if ((null != data['trimmed_size']) && (data['trimmed_size'] !="")) { 
			$("#textDTrimmedSize").val(data['trimmed_size']);
		}
		// -------- -------- -----------
		if ((null != data['web_version_date']) && (data['web_version_date'] !="")) { 
			$("#textDWebVersion").val(data['web_version_date']);
		}
		if ((null != data['started_date']) && (data['started_date'] !="")) { 
			$("#textDDateStarted").val(data['started_date']);
		}
		if ((null != data['designing_out_date']) && (data['designing_out_date'] !="")) { 
			$("#textDDateDesigningOut").val(data['designing_out_date']);
		}
		if ((null != data['ended_date']) && (data['ended_date'] !="")) { 
			$("#textDDateEnded").val(data['ended_date']);
		}
		if ((null != data['progress']) && (data['progress'] !="")) { 
			$("#textAreaDProgress").val(data['progress']);
		}
	};
	//---------------------------------
	
	
	

	
	function getDetailCostGraphicsData() {	
		var textGDesigner = $("#textGDesigner").val();
		var textGDHours = $("#textGDHours").val();
		// -------- Readonly ----------
		var textGRate = $("#textGRate").val();
		var textGDCost = $("#textGDCost").val();
		var textGTrimmedSize = $("#textGTrimmedSize").val();
		// -------- -------- -----------
		var textGWebVersion = $("#textGWebVersion").val();
		var textGDateStarted = $("#textGDateStarted").val();
		var textGDateDesigningOut = $("#textGDateDesigningOut").val();
		var textGDateEnded = $("#textGDateEnded").val();
		var textAreaGProgress = $("#textAreaGProgress").val();
		var dataDesigning = {
				cost_type: "GRAPHICS_COST",
				designer: textGDesigner,
				designing_hours: textGDHours,
				rate: textGRate,
				total_cost: textGDCost,
				trimmed_size: textGTrimmedSize,
				web_version_date: textGWebVersion,
				started_date: textGDateStarted,
				designing_out_date: textGDateDesigningOut,
				ended_date: textGDateEnded,
				progress: textAreaGProgress
		};
		return dataDesigning;
	};
	function setDetailCostGraphicsData(data) {	
		if ((null != data['designer']) && (data['designer'] !="")) { 
			$("#textGDesigner").val(data['designer']);
		}
		if ((null != data['designing_hours']) && (data['designing_hours'] !="")) { 
			$("#textGDHours").val(data['designing_hours']);
		}
		// -------- Readonly ----------
		if ((null != data['rate']) && (data['rate'] !="")) { 
			$("#textGRate").val(data['rate']);
		}
		if ((null != data['total_cost']) && (data['total_cost'] !="")) { 
			$("#textGDCost").val(data['total_cost']);
		}
		if ((null != data['trimmed_size']) && (data['trimmed_size'] !="")) { 
			$("#textGTrimmedSize").val(data['trimmed_size']);
		}
		// -------- -------- -----------
		if ((null != data['web_version_date']) && (data['web_version_date'] !="")) { 
			$("#textGWebVersion").val(data['web_version_date']);
		}
		if ((null != data['started_date']) && (data['started_date'] !="")) { 
			$("#textGDateStarted").val(data['started_date']);
		}
		if ((null != data['designing_out_date']) && (data['designing_out_date'] !="")) { 
			$("#textGDateDesigningOut").val(data['designing_out_date']);
		}
		if ((null != data['ended_date']) && (data['ended_date'] !="")) { 
			$("#textGDateEnded").val(data['ended_date']);
		}
		if ((null != data['progress']) && (data['progress'] !="")) { 
			$("#textAreaGProgress").val(data['progress']);
		}
	};
	//---------------------------------
	function getDetailCostProofReadingData() {
		var textPRProofReader = $("#textPRProofReader").val();
		var textPRDateStarted = $("#textPRDateStarted").val();
		var textPRDateEstimatedEnd = $("#textPRDateEstimatedEnd").val();
		var textPRDateEnd = $("#textPRDateEnd").val();
		var textAreaPRProgress = $("#textAreaPRProgress").val();
		var textPRDateSentAuthor = $("#textPRDateSentAuthor").val();
		var textPRDateApproved = $("#textPRDateApproved").val();
		var textPRDatePrePress = $("#textPRDatePrePress").val();
		var textPRDatePrePressEstimatedOut = $("#textPRDatePrePressEstimatedOut").val();
		var textPRDatePrePressOut = $("#textPRDatePrePressOut").val();
		var textAreaPRDatePrePressResponse = $("#textAreaPRDatePrePressResponse").val();
		var textAreaPRDatePrePressProgress = $("#textAreaPRDatePrePressProgress").val();
		var dataPR = {
				cost_type:"PROOF_READ_COST",
				reader: textPRProofReader,
				started_date: textPRDateStarted,
				est_end_date: textPRDateEstimatedEnd,
				ended_date: textPRDateEnd,
				progress: textAreaPRProgress,
				final_sent_to_author_date: textPRDateSentAuthor,
				final_sent_approved_date: textPRDateApproved,
				pre_press_in_date: textPRDatePrePress,
				est_pre_press_out_date: textPRDatePrePressEstimatedOut,
				pre_press_out_date: textPRDatePrePressOut,
				pre_press_response: textAreaPRDatePrePressResponse,
				pre_press_progress: textAreaPRDatePrePressProgress
		};
		return dataPR;
	};
	function setDetailCostProofReadingData(data) {
		if ((null != data['reader']) && (data['reader'] !="")) {
			$("#textPRProofReader").val(data['reader']);
		}
		if ((null != data['started_date']) && (data['started_date'] !="")) {
			$("#textPRDateStarted").val(data['started_date']);
		}
		if ((null != data['est_end_date']) && (data['est_end_date'] !="")) {
			$("#textPRDateEstimatedEnd").val(data['est_end_date']);
		}
		if ((null != data['ended_date']) && (data['ended_date'] !="")) {
			$("#textPRDateEnd").val(data['ended_date']);
		}
		if ((null != data['progress']) && (data['progress'] !="")) {
			$("#textAreaPRProgress").val(data['progress']);
		}
		if ((null != data['final_sent_to_author_date']) && (data['final_sent_to_author_date'] !="")) {
			$("#textPRDateSentAuthor").val(data['final_sent_to_author_date']);
		}
		if ((null != data['final_sent_approved_date']) && (data['final_sent_approved_date'] !="")) {
			$("#textPRDateApproved").val(data['final_sent_approved_date']);
		}
		if ((null != data['pre_press_in_date']) && (data['pre_press_in_date'] !="")) {
			$("#textPRDatePrePress").val(data['pre_press_in_date']);
		}
		if ((null != data['est_pre_press_out_date']) && (data['est_pre_press_out_date'] !="")) {
			$("#textPRDatePrePressEstimatedOut").val(data['est_pre_press_out_date']);
		}
		if ((null != data['pre_press_out_date']) && (data['pre_press_out_date'] !="")) {
			$("#textPRDatePrePressOut").val(data['pre_press_out_date']);
		}
		if ((null != data['pre_press_response']) && (data['pre_press_response'] !="")) {
			$("#textAreaPRDatePrePressResponse").val(data['pre_press_response']);
		}
		if ((null != data['pre_press_progress']) && (data['pre_press_progress'] !="")) {
			$("#textAreaPRDatePrePressProgress").val(data['pre_press_progress']);
		}
	};
	function getDetailCostTranslatingData() {	
		var textTContractNumber = $("#textTContractNumber").val();
		var textTTranslator = $("#textTTranslator").val();
		// ---------- Readonly -------------
		var textTRate = $("#textTRate").val();
		var textTNoOfPages = $("#textTNoOfPages").val();
		var textTCost = $("#textTCost").val();
		// ----------- ------- --------------
		var textTDateSentTranslating = $("#textTDateSentTranslating").val();
		var textTDateEstimatedReceivingTranslating = $("#textTDateEstimatedReceivingTranslating").val();
		var textTDateReceivingTranslating = $("#textTDateReceivingTranslating").val();
		var textAreaTProgress = $("#textAreaTProgress").val();
		var textAreaTComments = $("#textAreaTComments").val();
		var dataTranslating = {
				cost_type: "TRANSLATING_COST",
				contract_number: textTContractNumber,
				translator: textTTranslator,
				cost_per_unit: textTRate,
				no_of_pages: textTNoOfPages,
				total_cost: textTCost,
				sent_date: textTDateSentTranslating,
				est_receiving_date: textTDateEstimatedReceivingTranslating,
				receiving_date: textTDateReceivingTranslating,
				progress: textAreaTProgress,
				comments: textAreaTComments
		};
		return dataTranslating;
	};
	function setDetailCostTranslatingData(data) {	
		if ((null != data['contract_number']) && (data['contract_number'] !="")) {
			$("#textTContractNumber").val(data['contract_number']);
		}
		if ((null != data['translator']) && (data['translator'] !="")) {
			$("#textTTranslator").val(data['translator']);
		}
		// ---------- Readonly -------------
		if ((null != data['cost_per_unit']) && (data['cost_per_unit'] !="")) {
			$("#textTRate").val(data['cost_per_unit']);
		}
		if ((null != data['no_of_pages']) && (data['no_of_pages'] !="")) {
			$("#textTNoOfPages").val(data['no_of_pages']);
		}
		if ((null != data['total_cost']) && (data['total_cost'] !="")) {
			$("#textTCost").val(data['total_cost']);
		}
		// ----------- ------- --------------
		if ((null != data['sent_date']) && (data['sent_date'] !="")) {
			$("#textTDateSentTranslating").val(data['sent_date']);
		}
		if ((null != data['est_receiving_date']) && (data['est_receiving_date'] !="")) {
			$("#textTDateEstimatedReceivingTranslating").val(data['est_receiving_date']);
		}
		if ((null != data['receiving_date']) && (data['receiving_date'] !="")) {
			$("#textTDateReceivingTranslating").val(data['receiving_date']);
		}
		if ((null != data['progress']) && (data['progress'] !="")) {
			$("#textAreaTProgress").val(data['progress']);
		}
		if ((null != data['comments']) && (data['comments'] !="")) {
			$("#textAreaTComments").val(data['comments']);
		}
	};
	function getDetailCostPrintingData() {			
		var textPPrinter = $("#textPPrinter").val();
		var textPQuatationCalledDate = $("#textPQuatationCalledDate").val();
		var textPQuatationReceivedDate = $("#textPQuatationReceivedDate").val();
		var textPSentPrintingDate = $("#textPSentPrintingDate").val();
		//------- Readonly -------------
		var textPRate = $("#textPRate").val();
		var textPNoOfPages = $("#textPNoOfPages").val();
		var textPCost = $("#textPCost").val();
		//------- -------- -------------
		var textPPurchaseOrderNo = $("#textPPurchaseOrderNo").val();
		var textPDateOfCompletion = $("#textPDateOfCompletion").val();
		var textAreaPProgress = $("#textAreaPProgress").val();
		var textPMachineProofExpectedDate = $("#textPMachineProofExpectedDate").val();
		var textPMachineProofReceivedDate = $("#textPMachineProofReceivedDate").val();
		var textPMachineProofApprovedDate = $("#textPMachineProofApprovedDate").val();
		var textAreaPDelayCause = $("#textAreaPDelayCause").val();
		var textPDateActualDelivery = $("#textPDateActualDelivery").val();
		var dataPrinting = {
				cost_type: "PRINTING_COST",
				printer: textPPrinter,
				quotation_date: textPQuatationCalledDate,
				quotation_received_date: textPQuatationReceivedDate,
				sent_date: textPSentPrintingDate,
				cost_per_unit: textPRate,
				no_of_pages: textPNoOfPages,
				total_cost: textPCost,
				order_num: textPPurchaseOrderNo,
				completion_date: textPDateOfCompletion,
				progress: textAreaPProgress,
				machine_proof_expected_date: textPMachineProofExpectedDate,
				machine_proof_received_date: textPMachineProofReceivedDate,
				machine_proof_approved_date: textPMachineProofApprovedDate,
				delay: textAreaPDelayCause,
				actual_delivery: textPDateActualDelivery
		};
		return dataPrinting;
	};
	function setDetailCostPrintingData(data) {			
		if ((null != data['printer']) && (data['printer'] !="")) {
			$("#textPPrinter").val(data['printer']);
		}
		if ((null != data['quotation_date']) && (data['quotation_date'] !="")) { 
			$("#textPQuatationCalledDate").val(data['quotation_date']);
		}
		if ((null != data['quotation_received_date']) && (data['quotation_received_date'] !="")) { 
			$("#textPQuatationReceivedDate").val(data['quotation_received_date']);
		}
		if ((null != data['sent_date']) && (data['sent_date'] !="")) { 
			$("#textPSentPrintingDate").val(data['sent_date']);
		}
		//------- Readonly -------------
		if ((null != data['cost_per_unit']) && (data['cost_per_unit'] !="")) { 
			$("#textPRate").val(data['cost_per_unit']);
		}
		if ((null != data['no_of_pages']) && (data['no_of_pages'] !="")) { 
			$("#textPNoOfPages").val(data['no_of_pages']);
		}
		if ((null != data['total_cost']) && (data['total_cost'] !="")) { 
			$("#textPCost").val(data['total_cost']);
		}
		//------- -------- -------------
		if ((null != data['order_num']) && (data['order_num'] !="")) { 
			$("#textPPurchaseOrderNo").val(data['order_num']);
		}
		if ((null != data['completion_date']) && (data['completion_date'] !="")) {
			$("#textPDateOfCompletion").val(data['completion_date']);
		}
		if ((null != data['progress']) && (data['progress'] !="")) {
			$("#textAreaPProgress").val(data['progress']);
		}
		if ((null != data['machine_proof_expected_date']) && (data['machine_proof_expected_date'] !="")) {
			$("#textPMachineProofExpectedDate").val(data['machine_proof_expected_date']);
		}
		if ((null != data['machine_proof_received_date']) && (data['machine_proof_received_date'] !="")) {
			$("#textPMachineProofReceivedDate").val(data['machine_proof_received_date']);
		}
		if ((null != data['machine_proof_approved_date']) && (data['machine_proof_approved_date'] !="")) {
			$("#textPMachineProofApprovedDate").val(data['machine_proof_approved_date']);
		}
		if ((null != data['delay']) && (data['delay'] !="")) {
			$("#textAreaPDelayCause").val(data['delay']);
		}
		if ((null != data['actual_delivery']) && (data['actual_delivery'] !="")) {
			$("#textPDateActualDelivery").val(data['actual_delivery']);
		}
	};
	function getDetailCostReprintingData() {			
		var textRPPrinter = $("#textRPPrinter").val();
		var textRPQuatationCalledDate = $("#textRPQuatationCalledDate").val();
		var textRPQuatationReceivedDate = $("#textRPQuatationReceivedDate").val();
		var textRPSentPrintingDate = $("#textRPSentPrintingDate").val();
		//------- Readonly -------------
		var textRPRate = $("#textRPRate").val();
		var textRPNoOfPages = $("#textRPNoOfPages").val();
		var textRPCost = $("#textRPCost").val();
		//------- -------- -------------
		var textRPPurchaseOrderNo = $("#textRPPurchaseOrderNo").val();
		var textRPDateOfCompletion = $("#textRPDateOfCompletion").val();
		var textAreaRPProgress = $("#textAreaRPProgress").val();
		var textRPMachineProofExpectedDate = $("#textRPMachineProofExpectedDate").val();
		var textRPMachineProofReceivedDate = $("#textRPMachineProofReceivedDate").val();
		var textRPMachineProofApprovedDate = $("#textRPMachineProofApprovedDate").val();
		var textAreaRPDelayCause = $("#textAreaRPDelayCause").val();
		var textRPDateActualDelivery = $("#textRPDateActualDelivery").val();
		var dataPrinting = {
				cost_type: "REPRINTING_COST",
				printer: textRPPrinter,
				quotation_date: textRPQuatationCalledDate,
				quotation_received_date: textRPQuatationReceivedDate,
				sent_date: textRPSentPrintingDate,
				cost_per_unit: textRPRate,
				no_of_pages: textRPNoOfPages,
				total_cost: textRPCost,
				order_num: textRPPurchaseOrderNo,
				completion_date: textRPDateOfCompletion,
				progress: textAreaRPProgress,
				machine_proof_expected_date: textRPMachineProofExpectedDate,
				machine_proof_received_date: textRPMachineProofReceivedDate,
				machine_proof_approved_date: textRPMachineProofApprovedDate,
				delay: textAreaRPDelayCause,
				actual_delivery: textRPDateActualDelivery
		};
		return dataPrinting;
	};
	function setDetailCostReprintingData(data) {			
		if ((null != data['printer']) && (data['printer'] !="")) {
			$("#textRPPrinter").val(data['printer']);
		}
		if ((null != data['quotation_date']) && (data['quotation_date'] !="")) { 
			$("#textRPQuatationCalledDate").val(data['quotation_date']);
		}
		if ((null != data['quotation_received_date']) && (data['quotation_received_date'] !="")) { 
			$("#textRPQuatationReceivedDate").val(data['quotation_received_date']);
		}
		if ((null != data['sent_date']) && (data['sent_date'] !="")) { 
			$("#textRPSentPrintingDate").val(data['sent_date']);
		}
		//------- Readonly -------------
		if ((null != data['cost_per_unit']) && (data['cost_per_unit'] !="")) { 
			$("#textRPRate").val(data['cost_per_unit']);
		}
		if ((null != data['no_of_pages']) && (data['no_of_pages'] !="")) { 
			$("#textRPNoOfPages").val(data['no_of_pages']);
		}
		if ((null != data['total_cost']) && (data['total_cost'] !="")) { 
			$("#textRPCost").val(data['total_cost']);
		}
		//------- -------- -------------
		if ((null != data['order_num']) && (data['order_num'] !="")) { 
			$("#textRPPurchaseOrderNo").val(data['order_num']);
		}
		if ((null != data['completion_date']) && (data['completion_date'] !="")) {
			$("#textRPDateOfCompletion").val(data['completion_date']);
		}
		if ((null != data['progress']) && (data['progress'] !="")) {
			$("#textAreaRPProgress").val(data['progress']);
		}
		if ((null != data['machine_proof_expected_date']) && (data['machine_proof_expected_date'] !="")) {
			$("#textRPMachineProofExpectedDate").val(data['machine_proof_expected_date']);
		}
		if ((null != data['machine_proof_received_date']) && (data['machine_proof_received_date'] !="")) {
			$("#textRPMachineProofReceivedDate").val(data['machine_proof_received_date']);
		}
		if ((null != data['machine_proof_approved_date']) && (data['machine_proof_approved_date'] !="")) {
			$("#textRPMachineProofApprovedDate").val(data['machine_proof_approved_date']);
		}
		if ((null != data['delay']) && (data['delay'] !="")) {
			$("#textAreaRPDelayCause").val(data['delay']);
		}
		if ((null != data['actual_delivery']) && (data['actual_delivery'] !="")) {
			$("#textRPDateActualDelivery").val(data['actual_delivery']);
		}
	};
	
	/**
	function onShowModal() {
		JobLibrary.clearJobActionStatus()
		var hiddenJobId = $('#hiddenJobId').val();
		var usersUrl = "../status/completeStatePage/"+hiddenJobId;
		var result = getRequest(usersUrl);
		var isCanComplete = result['can_complete'];
		if (isCanComplete) {
			var users = result.users
			var tableData = prepareUsersData(users);
			renderDataTable(tableData);
			$('#modalUsersList').modal('toggle');
		} else {
			$('#modalNotAllowUsersList').modal('toggle');
		}
		
	};
	function prepareUsersData(users) {	
		var rows = {};
		var dataofTable = [];
		for (var i = 0; i < users[0].length; i++) {
			var nameValue = (users[0][i]['display']) ? users[0][i]['display'] : "";
			var emailValue = (users[0][i]['email']) ? users[0][i]['email'] : "";
			var responseRow = {
					name : nameValue,	
					email : emailValue,	
					select : '<input type="checkbox" name="checkboxNotifyUser" display="'+nameValue+'" email="'+emailValue+'" />'
			};
			dataofTable.push(responseRow);
		}
		rows["rows"] = dataofTable;
		return rows;
	}
	function renderDataTable(dataSet) {
		$('#notify-users-data-table').DataTable({
			"bAutoWidth" : false,
			"bJQueryUI" : true,
			"bDestroy" : true,
			"searching": true,
			"aaData" : dataSet.rows,
			"aoColumns" : [ {
				"sTitle" : "Name",
				"mData" : "name",
			}, {
				"sTitle" : "Email",
				"mData" : "email"
			}, {
				"sTitle" : "Select",
				"mData" : "select",
			} ]
		});
	}
	function confirmStatusOfItem() {
		JobLibrary.clearJobActionStatus()
		var hiddenJobId = $('#hiddenJobId').val();
		var selectedUsers = $('input[name="checkboxNotifyUser"]:checked');
		var notifyUsers = [];
		$( "input[name='checkboxNotifyUser']:checked" ).each(function( index ) {
			var email = $(this).attr("email");
			notifyUsers.push(email)
		});
		if(notifyUsers.length > 0) {
			var formatedPostData = {
					id: hiddenJobId,
					notify_users: notifyUsers
			}
			var postUrl = "../status/completeJob";
			var response = postRequest(postUrl, formatedPostData);
			var isSuccess = (response['status'] == '1000');
			var message = (isSuccess) ? "Your job has been confirmed successfully." : "Sorry, unable to confirm job status."
			JobLibrary.showJobActionStatusWithMesage(isSuccess, message);
		}
		$('#modalUsersList').modal('toggle');
	}
	function onDismissModal() {
		$( "input[name='checkboxNotifyUser']").attr("CHECKED",false);
	}
	*/
	</script>
</body>
</html>

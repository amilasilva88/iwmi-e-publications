<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">

<jsp:include page="layout/header.jsp" />
<body>

	<div id="wrapper">
		<jsp:include page="layout/menu.jsp" />

		<!-- Page Content -->
		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h2 class="page-header">Approval</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<jsp:include page="content/job-approval-body.jsp" />
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<jsp:include page="layout/footer.jsp" />
	<script type="text/javascript">
	var PAGE = {
		STATUS: JobStatus.ESTIMATED,
		URLNEXT: "delivery"
	}
	$(function() {
		loadDataForId(${id});
		
		$("#btnSave").click(validateAndSaveData);
		$("#btnNext").click(saveAndGotoNext);
		/*
		$("#btnSave").click(submitFormSave);
		$("#btnNext").click(submitFormNext);
		validateForm()
		*/
	});
	function submitFormNext() {
		submitForm(ACTION.NEXT)
	}
	function submitFormSave() {
		submitForm(ACTION.SAVE)
	}
	function submitForm(action) {
		$('#hiddenJobId').attr("action", action)
		var event = jQuery.Event( "click" );
		$( "#hiddenJobId" ).trigger(event);
	}
	function loadDataForId(itemId) {
		var itemStatus = null;
		if (null != itemId) {
			var sectionData = null;
			sectionData = getRequest("./load/"+itemId);
			if ((sectionData != null) || (sectionData != ""))  {
				if (null != sectionData['canEdit']) {
					var isDisable = !sectionData['canEdit'];
					JobLibrary.setDisableAllInput(isDisable);
				}
				if ((null != sectionData['item_status']) && (sectionData['item_status'] !="")) {
					itemStatus = sectionData['item_status'];
				}
				if ((null != sectionData['id']) && (sectionData['id'] !="")) {
					$('#hiddenJobId').val(sectionData['id']);
				}
				if ((null != sectionData['approverComment']) && (sectionData['approverComment'] !="")) {
					$('#textAreaApproverComment').val(sectionData['approverComment']);
				}
				if ((null != sectionData['status']) && (sectionData['status'] !="")) {
					$('#selectStatus').val(sectionData['status']).attr("selected", "selected");
				}
			}
		}
		JobLibrary.setItemStatus(itemStatus, JobStatus.ESTIMATED);
	}
	function setItemStatus(statusOfItem) {
		var navItems = $(".col-lg-2.horizontal-step");
		$(".col-lg-2.horizontal-step").removeClass("active")
		for (var i = 0; i<= statusOfItem; i++) {
			$(".col-lg-2.horizontal-step").eq(i).addClass("active")
		}
		$('#btnNext').attr('disabled','disabled');
		if (JobStatus.Approval < statusOfItem) {
			$('#btnNext').removeAttr('disabled','disabled');
		}
	}
	function saveAndGotoNext() {
		var response = validateAndSave();
		var isSuccess = (response['status'] == '1000');
		JobLibrary.showJobActionStatus(isSuccess);
		if (isSuccess) {
			$("#hiddenJobId").val(response['id']);	
			//window.location.href = '../delivery/page?id=' + response['id'];
			$("#modalSubmitInfo").modal("toggle");
		}
	}

	function validateAndSaveData() {
		var response = validateAndSave();
		var isSuccess = (response['status'] == '1000');
		JobLibrary.showJobActionStatus(isSuccess);
		if (isSuccess) {
			$("#hiddenJobId").val(response['id']);	
			JobLibrary.setItemStatus(response['item_status'], JobStatus.ESTIMATED);
		}
	}

	function validateAndSave() {
		
		var hiddenJobId = $("#hiddenJobId").val();
		var textAreaApproverComment = $("#textAreaApproverComment").val();
		var selectStatus = $("#selectStatus").val();
		var userAction = $("#hiddenJobId").attr("action");		
		// validation check
		var postData = {
			action: userAction,
			id:hiddenJobId,
			approverComment: textAreaApproverComment,
			status : selectStatus
		};
		var postUrl = "../approval/saveData";
		var result = postRequest(postUrl, postData);
		return result;
	};
	function validateForm() {
    	var buttonAction= $("#hiddenJobId").attr("action")
    	var response = validateAndSave();
		var isSuccess = ((typeof response != 'undefined') && (typeof response['status'] != 'undefined') && (response['status'] == '1000'));
		JobLibrary.showJobActionStatus(isSuccess);
		if (isSuccess) {
			if (buttonAction == ACTION.NEXT) {
				$("#hiddenJobId").val(response['id']);	
				window.location.href='../'+PAGE.URLNEXT+'/page?id=' + response['id'];
			} else {
				$("#hiddenJobId").val(response['id']);	
	 			JobLibrary.setItemStatus(response['item_status'], PAGE.STATUS);
			}
		}		        
	}
	</script>
</body>
</html>

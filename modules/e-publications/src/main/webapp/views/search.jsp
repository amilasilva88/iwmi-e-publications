<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">

<jsp:include page="layout/header.jsp" />
<style>
	.input-group-addon {
		width: 100px;
	}
</style>
<body>

	<div id="wrapper">
		<jsp:include page="layout/menu.jsp" />

		<!-- Page Content -->
		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<jsp:include page="content/search-advance-body.jsp" />
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
			<jsp:include page="content/search-result-body.jsp" />
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->
	<jsp:include page="layout/footer.jsp" />
	<script type="text/javascript">
	$(function() {
		var currentYear = new Date().getFullYear();
		for(var i=currentYear-5;i<=currentYear+5;i++) {
			$("#selectYear").append($('<option></option>').val(i).html(i));
		}
		var requesters = getRequest("../lookup/loadRequesters");
		populateDropdown("selectRequester", requesters);

		var supervisours = getRequest("../lookup/loadSupervisors");
		populateDropdown("selectSupervisor", supervisours);
		
		$("#btnSearch").click(getSearchResult);
		//$(".view_search_item").live('click', viewSearchItem);
		$( document ).on( "click", "a.view_search_item", viewSearchItem);
		$("#btnExcelExport").click(downloadeExcelReport);
	});
	function downloadeExcelReport() {
		var postData = getSearchRequestData()
		var postUrl = "../search/exportTable";
		//$("#formSearchAdvance").submit();
		var result = postRequestForExcel(postUrl, postData);
		//var objectUrl = URL.createObjectURL(result);
	   	//window.open(objectUrl);
	}
	function populateDropdown(selectorId, responceData) {
		if (responceData.data.length > 0) {
			for (var i = 0; i < responceData.data.length; i++) {
				var tmpItem = responceData.data[i];
				$("#" + selectorId).append(
						$('<option></option>').val(tmpItem['value']).html(
								tmpItem['display']));
			}
		}
	}
	function getSearchRequestData() {
		var textJobNumber = $.trim($('#textJobNumber').val());
		$('#textJobNumber').val(textJobNumber);
		var textRelatedProgram = $.trim($('#textRelatedProgram').val());
		$('#textRelatedProgram').val(textRelatedProgram);
		var textSeries = $.trim($('#textSeries').val());
		$('#textSeries').val(textSeries);
		var textTitle = $.trim($('#textTitle').val());
		$('#textTitle').val(textTitle);
		var textAuthor = $.trim($('#textAuthor').val());
		$('#textAuthor').val(textAuthor);
		var textISBNISSN = $.trim($('#textISBNISSN').val());
		$('#textISBNISSN').val(textISBNISSN);
		var selectTypeOfJob = $.trim($('#selectTypeOfJob').val());
		var selectRequester = $.trim($('#selectRequester').val());
		var selectSupervisor = $.trim($('#selectSupervisor').val());
		var selectProjectCode = $.trim($('#selectProjectCode').val());
		var selectStatus = $.trim($('#selectStatus').val());
		var selectInvolver = $.trim($('#selectInvolver').val());
		var selectYear = $.trim($('#selectYear').val());
		
		var requestData  = [];
		
		if (textJobNumber && (textJobNumber!="")) {
			requestData.push({field: $('#textJobNumber').attr('field'), keyword: textJobNumber});
		}
		if (textRelatedProgram && (textRelatedProgram!="")) {
			requestData.push({field: $('#textRelatedProgram').attr('field'), keyword: textRelatedProgram});
		}
		if (textSeries && (textSeries!="")) {
			requestData.push({field: $('#textSeries').attr('field'), keyword: textSeries});
		}
		if (textTitle && (textTitle!="")) {
			requestData.push({field: $('#textTitle').attr('field'), keyword: textTitle});
		}
		if (textAuthor && (textAuthor!="")) {
			requestData.push({field: $('#textAuthor').attr('field'), keyword: textAuthor});
		}
		if (textISBNISSN && (textISBNISSN!="")) {
			requestData.push({field: $('#textISBNISSN').attr('field'), keyword: textISBNISSN});
		}
		if (selectTypeOfJob && (selectTypeOfJob!="")) {
			requestData.push({field: $('#selectTypeOfJob').attr('field'), keyword: selectTypeOfJob});
		}
		if (selectRequester && (selectRequester!="")) {
			requestData.push({field: $('#selectRequester').attr('field'), keyword: selectRequester});
		}
		if (selectSupervisor && (selectSupervisor!="")) {
			requestData.push({field: $('#selectSupervisor').attr('field'), keyword: selectSupervisor});
		}
		if (selectProjectCode && (selectProjectCode!="")) {
			requestData.push({field: $('#selectProjectCode').attr('field'), keyword: selectProjectCode});
		}
		if (selectStatus && (selectStatus!="")) {
			requestData.push({field: $('#selectStatus').attr('field'), keyword: selectStatus});
		}
		if (selectInvolver && (selectInvolver!="")) {
			requestData.push({field: $('#selectInvolver').attr('field'), keyword: selectInvolver});
		}
		if (selectYear && (selectYear!="")) {
			requestData.push({field: $('#selectYear').attr('field'), keyword: selectYear});
		}
		var postData = {
				fields: requestData
		}
		return  postData
	}
	function getSearchResult() {
		var postData = getSearchRequestData()
		var postUrl = "../search/criteria";
		var result = postRequest(postUrl, postData);
		var tableData = prepareSearchData(result);
		renderDataTable(tableData);
	}

	function prepareSearchData(response) {
		var publicationItems = response.publication_items;
		var rows = {};
		var dataofTable = [];
		for (var i = 0; i < publicationItems.length; i++) {
			var responseRow = {
					typeOfWork : (publicationItems[i]['type_of_work']) ? publicationItems[i]['type_of_work'] : "",	
					projectCode : (publicationItems[i]['project_code']) ? publicationItems[i]['project_code'] : "",	
					requiredDeliveryDate : (publicationItems[i]['required_delivery_date']) ? publicationItems[i]['required_delivery_date'] : "",
					division : (publicationItems[i]['division']) ? publicationItems[i]['division'] : "",
					status : (publicationItems[i]['status']) ? publicationItems[i]['status'] : "",
					jobNumber : (publicationItems[i]['jobNumber']) ? publicationItems[i]['jobNumber'] : "",
					series : (publicationItems[i]['series']) ? publicationItems[i]['series'] : "",
					createdBy : (publicationItems[i]['created_by']) ? publicationItems[i]['created_by'] : "",
					//action : '<a class="view_search_item" href="javascript:void(0)" role="view" data-id="' + publicationItems[i]['id']+ '" >View</a>'
					action : '<a href="../initiation/page?id='+publicationItems[i]['id']+'" target="_blank" >View</a> | <a href="../status/export/'+publicationItems[i]['id']+'" >PDF</a>'
			};
			dataofTable.push(responseRow);
		}
		rows["rows"] = dataofTable;
		return rows;
	}
	
	function renderDataTable(dataSet) {
		$('#publication-items-data-table').DataTable({
			"bAutoWidth" : false,
			"bJQueryUI" : true,
			"bDestroy" : true,
			"searching": false,
			"order": [[ 2, "desc" ]],
			"aaData" : dataSet.rows,
			"aoColumns" : [ {
				"sTitle" : "Type of work",
				"mData" : "typeOfWork",
			}, {
				"sTitle" : "Project code",
				"mData" : "projectCode"
			}, {
				"sTitle" : "Delivery date",
				"mData" : "requiredDeliveryDate"
			}, {
				"sTitle" : "Division",
				"mData" : "division"
			}, {
				"sTitle" : "Status",
				"mData" : "status"
			},{
				"sTitle" : "Job number",
				"mData" : "jobNumber"
			} ,{
				"sTitle" : "Series",
				"mData" : "series"
			} ,{
				"sTitle" : "Created by",
				"mData" : "createdBy"
			}, {
				"sTitle" : "Action",
				"mData" : "action",
				"sWidth" : "80px"
			} ]
		});
	}
	
	function viewSearchItem() {
		$('#myModal').removeData('bs.modal');
        $('#myModal').modal({remote: '../initiation/page?id=' + $(this).attr("data-id")});
        $('#myModal').modal('show');
	};
	
	</script>
</body>
</html>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">

<jsp:include page="layout/header.jsp" />
<body>
	<div id="wrapper">
		<jsp:include page="layout/menu.jsp" />

		<!-- Page Content -->
		<div id="page-wrapper"> 
			<div class="row">
				<div class="col-lg-12">
					<h2 class="page-header">Initiation</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<jsp:include page="content/job-initiation-body.jsp" />
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->
	</div>
	<!-- /#wrapper -->

	<jsp:include page="layout/footer.jsp" />

	<script type="text/javascript">
		$(function() {	

			var divisions = getRequest("../lookup/loadDivisions");
			populateDropdown("selectDivision", divisions);
			
			var supervisors = getRequest("../lookup/loadRequesters");
			populateDropdown("selectSupervisorName", supervisors);

			var admins = getRequest("../lookup/loadSupervisors");
			populateDropdown("selectAdminName", admins);
			loadDataForId(${id});
			
			/*
			$("#btnSave").click(validateAndSaveData);
			$("#btnNext").click(saveAndGotoNext);
			*/
			$("#btnSave").click(submitFormSave);
			$("#btnNext").click(submitFormNext);
			validateForm();
			
			$("#aMoreAttachments").click(setMoreAttachementFields);
			$('#container-attachments').on('click', '.close', closeAttachmentField);
			
		});
		
		function closeAttachmentField() {
			var elements = $("#container-attachments .col-lg-6");
			var removedElementIndex = elements.index($(this).parent(".col-lg-1").parent(".input-group").parent('.form-group').parent(".col-lg-6"));
			filesList[""+removedElementIndex+""] = null;
			if (elements.length > 2) {
				//filesList.splice(indexValueOfArray,1);
				$(this).parent(".col-lg-1").parent(".input-group").parent('.form-group').parent(".col-lg-6").remove();
			} else {
				$(this).parent(".col-lg-1").siblings(".col-lg-11").children(".btn-file").val("");
			}
		}
		function resetAllAttachFields() {
			var elements = $("#container-attachments .col-lg-6");
			$("#container-attachments .btn-file").val("");
			for(var i=2; i<elements.length; i++) {
				elements[i].remove();
			}
		}
		function setMoreAttachementFields() {
			var ele = '<div class="col-lg-6">'+
				'<div class="form-group">'+
					'<div class="input-group">'+
						'<div class="col-lg-11">'+
			    			'<input type="file" name="resource_files[]" class="btn btn-default btn-file">'+
			    		'</div>'+
			    		'<div class="col-lg-1">'+
							'<a class="close">X</a>'+
						'</div>'+
			    	'</div>'+
				'</div>'+
			'</div>';
			$("#container-attachments").append(ele);
		}
		function submitFormNext() {
			submitForm(ACTION.NEXT)
		}
		function submitFormSave() {
			submitForm(ACTION.SAVE)
		}
		function submitForm(action) {
			// var submitForm = $("#formJobInitiation").submit();
			// $('#hiddenJobId').trigger("click");
			$('#hiddenJobId').attr("action", action)
			var event = jQuery.Event( "click" );
			$( "#hiddenJobId" ).trigger( event );

		}
		function loadDataForId(itemId) {
			var itemStatus = 'INITIATION';
			resetAllAttachFields();
			if (null != itemId) {
				var sectionData = null;
				sectionData = getRequest("./load/"+itemId);
				if ((sectionData != null) || (sectionData != ""))  {
					if (null != sectionData['canEdit']) {
						var isDisable = !sectionData['canEdit'];
						JobLibrary.setDisableAllInput(isDisable);
					}
					if ((null != sectionData['item_status']) && (sectionData['item_status'] !="")) {
						itemStatus = sectionData['item_status'];
					}
					if ((null != sectionData['id']) && (sectionData['id'] !="")) {
						$('#hiddenJobId').val(sectionData['id']);
					}
					if ((null != sectionData['title']) && (sectionData['title'] !="")) {
						$('#textTitle').val(sectionData['title']);
					}
					if ((null != sectionData['typeOfWork']) && (sectionData['typeOfWork'] !="")) {
						$('#selectTypeOfWork').val(sectionData['typeOfWork']).attr("selected", "selected");
					}
					if ((null != sectionData['descOfWork']) && (sectionData['descOfWork'] !="")) {
						$('#textDescOfWork').val(sectionData['descOfWork']);
					}
					if ((null != sectionData['printRun']) && (sectionData['printRun'] !="")) {
						$('#textPrintRun').val(sectionData['printRun']);
					}
					if ((null != sectionData['projectCode']) && (sectionData['projectCode'] !="")) {
						$('#textProjectCode').val(sectionData['projectCode']);
					}
					if ((null != sectionData['requiredDeliveryDate']) && (sectionData['requiredDeliveryDate'] !="")) {
						$('#textRequiredDeliveryDate').val(sectionData['requiredDeliveryDate']);
					}
					if ((null != sectionData['requesterName']) && (sectionData['requesterName'] !="")) {
						$('#selectSupervisorName').val(sectionData['requesterName']).attr("selected", "selected");
					}
					if ((null != sectionData['division']) && (sectionData['division'] !="")) {
						$('#selectDivision').val(sectionData['division']).attr("selected", "selected");
					}
					if ((null != sectionData['supervisorName']) && (sectionData['supervisorName'] !="")) {
						$('#selectAdminName').val(sectionData['supervisorName']).attr("selected", "selected");
					}
					if ((null != sectionData['created_by']) && (sectionData['created_by'] !="")) {
						$('#textCreatedBy').val(sectionData['created_by']);
					}
					if ((null != sectionData['otherInfo']) && (sectionData['otherInfo'] !="")) {
						$('#textOtherInformation').val(sectionData['otherInfo']);
					}
					if ((null != sectionData['costElements']) && (sectionData['costElements'].length>0)) {
						var costElements = sectionData['costElements'];
						for (var i=0;i<costElements.length;i++) {
							var costElement = costElements[i];
							var costType = costElement['cost_type'];
							$('#costCategorySections input[type=checkbox][value='+costType+']').attr("CHECKED","true");
						}
					}
					if ((null != sectionData['docs']) && (sectionData['docs'].length>0)) {
						//$('#tableUploadedFiles tr:last').remove()
						$('#tableUploadedFiles tr:gt(0)').remove();
						var documents = sectionData['docs'];
						for (var i=0;i<documents.length;i++) {
							var doc = documents[i];
							var doc_file_name = doc['doc_file_name'];
							var doc_id = doc['doc_id'];
							var doc_file_url = doc['doc_file_url'];
							$('#tableUploadedFiles tr:last').after('<tr><td><a href="'+doc_file_url+'">'+doc_file_name+'</a></td></tr>');
						}
					}
				}
			}
			JobLibrary.setItemStatus(itemStatus, JobStatus.INITIATION);
		}
		
		function populateDropdown(selectorId, responceData) {
			if (responceData.data.length > 0) {
				for (var i = 0; i < responceData.data.length; i++) {
					var tmpItem = responceData.data[i];
					$("#" + selectorId).append(
							$('<option></option>').val(tmpItem['value']).html(
									tmpItem['display']));
				}
			}
		}
		function saveAndGotoNext() {
			var response = validateAndSave();
			var isSuccess = (response['status'] == '1000');
			JobLibrary.showJobActionStatus(isSuccess);
			if (isSuccess) {
				$("#hiddenJobId").val(response['id']);	
				window.location.href='../costEstimation/page?id=' + response['id']
			}
		}
		function validateAndSaveData() {
			$('#action-alert').removeClass('hidden').addClass('fade in');
			$('#action-alert').removeClass();
			
			var isValid = validateForm()
			if (isValid) {
				var response = validateAndSave();
				var isSuccess = (response['status'] == '1000');
				JobLibrary.showJobActionStatus(isSuccess);
				if (isSuccess) {
					$("#hiddenJobId").val(response['id']);	
				 	JobLibrary.setItemStatus(response['item_status'], JobStatus.INITIATION);
				}
			}
		}
		var filesList = [];
		$(document).on('change','input[type=file]',prepareUpload);
		// Grab the files and set them to our variable
		function prepareUpload(event) {
			var elements = $("#container-attachments .col-lg-6");
			var changedElementIndex = elements.index($(this).parent(".col-lg-11").parent(".input-group").parent('.form-group').parent(".col-lg-6"));
			filesList[""+changedElementIndex+""] =  event.target.files;
		}
		// Catch the form submit and upload the files
		function uploadFiles() {
			//event.stopPropagation(); // Stop stuff happening
		    //event.preventDefault(); // Totally stop stuff happening
			// START A LOADING SPINNER HERE
		    // Create a formdata object and add the files
		    //var formData = new FormData(this);
		    var formData = new FormData();
		    var i = 0;
		    var responseData = [];
			for(key in filesList) {
		    	var tmpFile = filesList[key];
		    	if(tmpFile != null) {
		    		$.each(tmpFile, function(key, value) {	    	
			    		formData.append(i, value);
			    	});
		    		i = i + 1;
		    	}
		    }
			if (i > 0) {
		    	$("#fileUploadingDiv").removeClass("off").addClass("on");
			    var hiddenJobId = $("#hiddenJobId").val();
				var postUrl = '../file_repo/upload';
				formData.append('e_pub_id', hiddenJobId);
				var token = $("meta[name='_csrf']").attr("content");
				var header = $("meta[name='_csrf_header']").attr("content");
				$.ajax({
			        url: postUrl,
			        type: 'POST',
			        data: formData,
			        cache: false,
			        contentType : "application/json; charset=utf-8",
					beforeSend : function(jqXHR, settings) {
						jqXHR.setRequestHeader(header, token);
					},
					dataType : "json",
			        enctype: 'multipart/form-data',
			        processData: false, // Don't process the files
			        contentType: false,  // Set content type to false as jQuery will tell the server its a query string request
			        success: function(data, textStatus, jqXHR) {
			        	$("#fileUploadingDiv").removeClass("on").addClass("off");
			        	responseData = data;
			            if (typeof data.error === 'undefined') {
			            } else {
			            }
			        },
			        error: function(jqXHR, textStatus, errorThrown) {
			        	$("#fileUploadingDiv").removeClass("on").addClass("off");	    		
			        	formData = [];
			            // Handle errors here
			            // STOP LOADING SPINNER
			        },
			        async : false
			    });
		    } else {
		    	responseData["status"] = "1000";
		    }
			return responseData;
		}
		function validateAndSave() {
			//----------- Upload file ---------------
			var isCompleteUpload = false
			if ((filesList != null) && (filesList.length > 0)) {
				var uploadResponce = uploadFiles();
				isCompleteUpload = ((typeof uploadResponce !== 'undefined') && (uploadResponce['status'] == "1000"))
			} else {
				isCompleteUpload = true;
			}
			//----------- ----------- ---------------
			var hiddenJobId = $("#hiddenJobId").val();
			var selectTypeOfWork = $("#selectTypeOfWork").val();
			var textTitle = $("#textTitle").val();
			var textDescOfWork = $("#textDescOfWork").val();
			var textPrintRun = $("#textPrintRun").val();
			var textProjectCode = $("#textProjectCode").val();
			var textRequiredDeliveryDate = $("#textRequiredDeliveryDate").val();
			var selectRequesterName = $("#selectSupervisorName").val();
			var selectDivision = $("#selectDivision").val();
			var selectSupervisorName = $("#selectAdminName").val();
			var textOtherInformation = $("#textOtherInformation").val();
			var userAction = $("#hiddenJobId").attr("action");
			var costElementsData = [];
			$('#costCategorySections input[type=checkbox]').each(function() {
				if ($(this).is(":checked")) {
					costElementsData.push({
						"cost_type" : $(this).val(),
						"no_of_pages" : 0,
						"cost_per_unit" : 0,
						"other_costs" : 0
					});
				}
			});
			var postData = {
				action: userAction,
				id : hiddenJobId,
				title: textTitle,
				typeOfWork : selectTypeOfWork,
				descOfWork : textDescOfWork,
				printRun : textPrintRun,
				projectCode : textProjectCode,
				requiredDeliveryDate : textRequiredDeliveryDate,
				requesterName : selectRequesterName,
				division : selectDivision,
				supervisorName : selectSupervisorName,
				otherInfo : textOtherInformation,
				costElements : costElementsData
			};
			var postUrl = "../initiation/saveData";
			var response = postRequest(postUrl, postData);
			return response;
		}
		
		function validateForm() {
			$('#formJobInitiation').bootstrapValidator({
				/**
				feedbackIcons: {
		            valid: 'glyphicon-ok',
		            invalid: 'glyphicon-remove',
		            validating: 'glyphicon-refresh'
        		},
				*/
				framework: 'bootstrap',
				feedbackIcons: {
		            valid: 'glyphicon glyphicon-ok',
		            invalid: 'glyphicon glyphicon-remove',
		            validating: 'glyphicon glyphicon-refresh'
        		},
				submitHandler: function(validator, form, submitButton) {
		        	var buttonAction= $(submitButton).attr("action")
		        	var response = validateAndSave();
					var isSuccess = ((typeof response != 'undefined') && (typeof response['status'] != 'undefined') && (response['status'] == '1000'));
					if (isSuccess) {
						if (buttonAction == ACTION.NEXT) {
							$("#hiddenJobId").val(response['id']);	
							//window.location.href='../costEstimation/page?id=' + response['id']
							$("#modalSubmitInfo").modal("toggle");
						} else {
							JobLibrary.showJobActionStatus(isSuccess);
							if ($("#hiddenJobId").val() != response['id']) {
								window.location.href='../initiation/page?id=' + response['id']
							} else {
								$("#hiddenJobId").val(response['id']);	
								JobLibrary.setItemStatus(response['item_status'], JobStatus.INITIATION);
								loadDataForId(response['id']);
							}
						}
					}
		        },/*
		        disabled: false,
		        err: {
		            container: 'tooltip'
		        },
		        submitButtons: 'button[type="submit"]',*/
		        fields : {
					selectTypeOfWork: {
						validators : {
							notEmpty : {
								message : 'This field is required'
							}
						}
					},
					textDescOfWork: {
						validators : {
							stringLength: {
		                        max: 500,
		                        message: 'Description of work must be less than 500 characters'
		                    }
						}
					},/*
					textPrintRun : {
						validators : {
							notEmpty : {
								message : 'This field is required'
							}
							,
                    		numeric: {
                        		message: 'Print run must be a number'
                    		},
                    		stringLength: {
		                        max: 4,
		                        min:4,
		                        message: 'Print run must be 4 characters'
		                    }
						}
					},*/
					textProjectCode: {
						validators : {
							notEmpty : {
								message : 'This field is required'
							},
                    		regexp: {
		                        regexp: /^\d{3}-\d{2}-\d{2}-[a-zA-Z0-9-]{3}-[a-zA-Z0-9-]{2}-[a-zA-Z0-9-]{4}$/,
		                        message: 'Format: eg:012-02-03-IFP-PR-M552'
		                    }
						}
					},
					selectSupervisorName: {
						validators : {
							notEmpty : {
								message : 'This field is required'
							}
						}
					},
					selectDivision: {
						validators : {
							notEmpty : {
								message : 'This field is required'
							}
						}
					},
					selectAdminName: {
						validators : {
							notEmpty : {
								message : 'This field is required'
							}
						}
					},
					textRequiredDeliveryDate: {
		                validators: {
		                    date: {
		                        format: 'YYYY-MM-DD',
		                        message: 'This field is required'
		                    }
		                }
		            },
					textOtherInformation: {
						validators : {
							stringLength: {
		                        max: 200,
		                        message: 'Other information must be less than 200 characters'
		                    }
						}
					}
				}
			});
		}
	</script>
</body>
</html>

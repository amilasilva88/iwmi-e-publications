<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">

<jsp:include page="layout/header.jsp" />
<body>

	<div id="wrapper">
		<jsp:include page="layout/menu.jsp" />
		<!-- Page Content -->
		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h2 class="page-header">Actual costing</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<jsp:include page="content/job-actual-costing-body.jsp" />
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<jsp:include page="layout/footer.jsp" />
	<script src="http://malsup.github.com/jquery.form.js"></script>
	<script type="text/javascript">
	var PAGE = {
		STATUS: JobStatus.DELIVERED,
		URLNEXT: "status"
	}
	$(function() {

		$("#aMoreAttachments").click(function() {
			var ele = '<div class="col-lg-6">'+
				'<div class="form-group">'+
					'<div class="input-group">'+
						'<div class="col-lg-11">'+
			    			'<input type="file" name="resource_files[]" class="btn btn-default btn-file">'+
			    		'</div>'+
			    		'<div class="col-lg-1">'+
							'<a class="close">x</a>'+
						'</div>'+
			    	'</div>'+
				'</div>'+
			'</div>';
			$("#container-attachments").append(ele);
		});
		$('#container-attachments').on('click', '.close', function() {
			var elements = $("#container-attachments .col-lg-6");
			if (elements.length>2) {
				$(this).parent(".col-lg-1").parent(".input-group").parent('.form-group').parent(".col-lg-6").remove();
				
			} else {
				$(this).parent(".col-lg-1").siblings(".col-lg-11").children(".btn-file").val("");
			}
		});
		
		$('#costCategorySections input[type=checkbox]').click(function () {
			var contentId = $(this).attr('detailContainer');
			
			$("#"+contentId).removeClass("off");
			$("#"+contentId).removeClass("on");
			if (this.checked) { 
				$("#"+contentId).slideDown( "fast", function() {
					$("#"+contentId).addClass("on");
				});
			} else {
				$("#"+contentId).slideDown( "fast", function() {
					$("#"+contentId).addClass("off");
				});
				$("#"+contentId+" input[name=textNumberOfPages]").val("0");
				$("#"+contentId+" input[name=textCostPerUnit]").val("0");
				$("#"+contentId+" input[name=textOtherCost]").val("0");
				$("#"+contentId+" input[name=textOtherCost]").attr("overhead","0");
				$("#"+contentId+" input[name=textSubTotal]").val("0");
			}
			calculateTotalEstimate();
		});
		$(".costSection.costEstimateContainer input[type=text]").keyup(calculateTotalEstimate);
		loadDataForId(${id});
		
		$("#btnSave").click(validateAndSaveData);
		$("#btnNext").click(saveAndGotoNext);
	});
	
	function submitForm(action) {
		$('#hiddenJobId').attr("action", action);
		var event = jQuery.Event( "click" );
		$( "#hiddenJobId" ).trigger(event);
	}
	function loadDataForId(itemId) {
		var itemStatus = null;
		if (null != itemId) {
			var sectionData = null;
			sectionData = getRequest("./load/"+itemId);
			if ((sectionData != null) || (sectionData != ""))  {
				if (null != sectionData['canEdit']) {
					var isDisable = !sectionData['canEdit'];
					JobLibrary.setDisableAllInput(isDisable);
				}
				if ((null != sectionData['item_status']) && (sectionData['item_status'] !="")) {
					itemStatus = sectionData['item_status'];
				}
				if ((null != sectionData['id']) && (sectionData['id'] !="")) {
					$('#hiddenJobId').val(sectionData['id']);
				}
				if ((null != sectionData['completionDate']) && (sectionData['completionDate'] !="")) {
					$('#textDateOfCompletion').val(sectionData['completionDate']);
				}
				if ((null != sectionData['costElements']) && (sectionData['costElements'].length>0)) {
					var costElements = sectionData['costElements'];
					for (var i=0;i<costElements.length;i++) {
						var costElement = costElements[i];
						var costType = costElement['cost_type'];
						$('#costCategorySections input[type=checkbox][value='+costType+']').attr("CHECKED","true");
						var contentId = $('#costCategorySections input[type=checkbox][value='+costType+']').attr('detailContainer');
						$("#"+contentId+" input[name=textNumberOfPages]").val(costElement['no_of_pages']);
						$("#"+contentId+" input[name=textCostPerUnit]").val(costElement['cost_per_unit']);
						var costForPages = parseFloat(costElement['no_of_pages']) * parseFloat(costElement['cost_per_unit']);
						var overhead = costForPages * ((parseFloat(costElement['other_costs'])*1.0)/ 100);
						$("#"+contentId+" input[name=textOtherCost]").val(costElement['other_costs']);
						$("#"+contentId+" input[name=textOtherCost]").attr("overhead",overhead);
					}
				}
				if ((null != sectionData['docs']) && (sectionData['docs'].length>0)) {
						//$('#tableUploadedFiles tr:last').remove()
						$('#tableUploadedFiles tr:gt(0)').remove();
						var documents = sectionData['docs'];
						for (var i=0;i<documents.length;i++) {
							var doc = documents[i];
							var doc_file_name = doc['doc_file_name'];
							var doc_id = doc['doc_id'];
							var doc_file_url = doc['doc_file_url'];
							$('#tableUploadedFiles tr:last').after('<tr><td><a href="'+doc_file_url+'">'+doc_file_name+'</a></td></tr>');
						}
				//TODO - Set data 
				}
			}
		}
		$('#costCategorySections input[type=checkbox]').each(function () {
			var contentId = $(this).attr('detailContainer');
			$("#"+contentId).removeClass("off");
			$("#"+contentId).removeClass("on");
			if (this.checked) { 
				$("#"+contentId).addClass("on");
			} else {
				$("#"+contentId+" input[name=textNumberOfPages]").val("0");
				$("#"+contentId+" input[name=textCostPerUnit]").val("0");
				$("#"+contentId+" input[name=textOtherCost]").val("0");
				$("#"+contentId+" input[name=textOtherCost]").attr("overhead","0");
				$("#"+contentId).addClass("off");
			}
			var noOfUnits = parseFloat($("#"+contentId+" input[name=textNumberOfPages]").val());
			var costPerUnit = parseFloat($("#"+contentId+" input[name=textCostPerUnit]").val());
			var otherCost = parseFloat($("#"+contentId+" input[name=textOtherCost]").val());
			var costForPages = parseFloat(costPerUnit)*parseFloat(noOfUnits);
			var overhead = costForPages * ((parseFloat(otherCost)*1.0)/ 100);
			$("#"+contentId+" input[name=textOtherCost]").attr("overhead",overhead);
			var subTotal = (costForPages) + overhead;
			$("#"+contentId+" input[name=textSubTotal]").val(subTotal);
		});
		calculateTotalEstimate();
		JobLibrary.setItemStatus(itemStatus, PAGE.STATUS);
		
	}
	
	function calculateTotalEstimate() {
		var sectionId = $(this).parent().parent().parent().parent().parent().parent(".costEstimateContainer.on").attr("id");
		
		var noOfPages = $("#"+sectionId+" input[name=textNumberOfPages]").val();
		noOfPages = getFormatedFloatValue(noOfPages);
		noOfPages = (!isNaN(noOfPages) && (noOfPages == parseFloat(noOfPages).toString()))?parseFloat(noOfPages):0;
		
		var unitCost = $("#"+sectionId+" input[name=textCostPerUnit]").val();
		unitCost = getFormatedFloatValue(unitCost);
		unitCost = (!isNaN(unitCost) && (unitCost == parseFloat(unitCost).toString()))?parseFloat(unitCost):0;
			
		var totalPagesCost = (noOfPages * unitCost);
		var otherCost = $("#"+sectionId+" input[name=textOtherCost]").val();
		otherCost = getFormatedFloatValue(otherCost);
		otherCost = (!isNaN(otherCost) && (otherCost == parseFloat(otherCost).toString()))?parseFloat(otherCost):0;
		var overheadCost = (totalPagesCost * (otherCost/100));
		$("#"+sectionId+" input[name=textOtherCost]").attr("overhead", overheadCost);

		var subCost = totalPagesCost+overheadCost;
		$("#"+sectionId+" input[name=textSubTotal]").val(subCost);
		var totalOtherCost = 0;
		var totalESTCost = 0;
		$(".costSection.costEstimateContainer input[name=textSubTotal]").each(function() {
			totalESTCost += isNaN(parseInt($(this).val()))?0:parseFloat($(this).val());
		});
		totalESTCost = (isNaN(totalESTCost))?0:totalESTCost;
		$("#textTotalAllSubCost").val(totalESTCost);
		var totalOtherCost = 0;
		$(".costSection.costEstimateContainer input[name=textOtherCost]").each(function() {
			totalOtherCost += isNaN(parseFloat($(this).attr("overhead")))?0:parseFloat($(this).attr("overhead"));
		});
		totalESTCost = (isNaN(totalOtherCost))?0:totalOtherCost;
		$("#textTotalOverheadCost").val(totalOtherCost);
	}

	function saveAndGotoNext() {
		$('#hiddenJobId').attr("action", ACTION.NEXT);
		var response = validateAndSave();
		var isSuccess = (response['status'] == '1000');
		JobLibrary.showJobActionStatus(isSuccess);
		if (isSuccess) {
			filesList = [];
			$("#hiddenJobId").val(response['id']);	
			//window.location.href = '../'+PAGE.URLNEXT+'/page?id=' + response['id'];
			$("#modalSubmitInfo").modal("toggle");
		}
	}

	function validateAndSaveData() {
		//$('#modalBusy').modal('toggle');
		$('#hiddenJobId').attr("action", ACTION.SAVE);
		var response = validateAndSave();
		var isSuccess = (response['status'] == '1000');
		JobLibrary.showJobActionStatus(isSuccess);
		if (isSuccess) {
			filesList = [];
			$("#hiddenJobId").val(response['id']);	
			loadDataForId(response['id']);
			JobLibrary.setItemStatus(response['item_status'], PAGE.STATUS);
		}
	}
	
	function validateAndSave() {
		//validation check
		/*
		var isCompleteUpload = false
		$("#fileUploadingDiv").removeClass("off").addClass("on");
		if ((filesList != null) && (filesList.length > 0)) {
			var uploadResponce = uploadFiles(event);
			isCompleteUpload = ((typeof uploadResponce !== 'undefined') && (uploadResponce['status'] == "1000"))
		} else {
			isCompleteUpload = true;
		} 
		*/
		//if (isCompleteUpload) {
			$("input[type=file]").val("");
			filesList = [];
			var hiddenJobId = $("#hiddenJobId").val();
			var textDateOfCompletion = $("#textDateOfCompletion").val();
			var costElementsData = [];
			$('#costCategorySections input[type=checkbox]').each(function() {
				if ($( this ).is(":checked")) {
					var contentId = $(this).attr('detailContainer');
					var noOfPages = parseInt($("#"+contentId+" input[name=textNumberOfPages]").val());
					var unitCost = parseFloat($("#"+contentId+" input[name=textCostPerUnit]").val());
					var otherCost = parseFloat($("#"+contentId+" input[name=textOtherCost]").val());
					var costType = $(this).val();
					if(!isNaN(noOfPages) && !isNaN(noOfPages) && !isNaN(noOfPages)) {
						costElementsData.push({ "cost_type":costType, "no_of_pages":noOfPages, "cost_per_unit":unitCost, "other_costs":otherCost});
					} else {
						return;
					}
				}
			});
			var userAction = $("#hiddenJobId").attr("action");			
			var postData = {
				action: userAction,
				id:hiddenJobId,
				completionDate: textDateOfCompletion,
				costElements: costElementsData
			};
			var postUrl = "../actual_costing/saveData";
			var result = postRequest(postUrl, postData);
			//$("#fileUploadingDiv").removeClass("on").addClass("off");
			return result;
			/*
		} else {
			$("#fileUploadingDiv").removeClass("on").addClass("off");
			return null;
		}
		*/
	}
	
	$('#upload-form').ajaxForm({
        success: function(msg) {
            alert("File has been uploaded successfully");
        },
        error: function(msg) {
            alert("Couldn't upload file");
        }
    });
	var filesList = [];
	$(document).on('change','input[type=file]',prepareUpload);
	$('#btnUpload').on('click', uploadFiles);
	
	// Grab the files and set them to our variable
	function prepareUpload(event) {
		//files = event.target.files;
		filesList.push( event.target.files);
	}
	
	// Catch the form submit and upload the files
	function uploadFiles(event) {
		event.stopPropagation(); // Stop stuff happening
	    event.preventDefault(); // Totally stop stuff happening
		// START A LOADING SPINNER HERE
	    // Create a formdata object and add the files
	    var formData = new FormData(this);
	    for(var i = 0; i<filesList.length; i++) {
	    	var tmpFile = filesList[i];
	    	$.each(tmpFile, function(key, value) {	    	
		    	formData.append(i, value);
		    });
	    }
	    var hiddenJobId = $("#hiddenJobId").val();
		var postUrl = '../file_repo/upload';
		formData.append('e_pub_id', hiddenJobId);
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");
		var responseData;
		$.ajax({
	        url: postUrl,
	        type: 'POST',
	        data: formData,
	        cache: false,
	        contentType : "application/json; charset=utf-8",
			beforeSend : function(jqXHR, settings) {
				jqXHR.setRequestHeader(header, token);
			},
			dataType : "json",
	        enctype: 'multipart/form-data',
	        processData: false, // Don't process the files
	        contentType: false,  // Set content type to false as jQuery will tell the server its a query string request
	        success: function(data, textStatus, jqXHR) {
	        	//$("#fileUploadingDiv").removeClass("on").addClass("off");
	        	responseData = data;
	            if (typeof data.error === 'undefined') {
	            } else {
	            }
	        },
	        error: function(jqXHR, textStatus, errorThrown) {
	        	//$("#fileUploadingDiv").removeClass("on").addClass("off");	    		
	        	formData = [];
	            // Handle errors here
	            // STOP LOADING SPINNER
	        },
	        async : false
	    });
		return responseData;
	}
	</script>
</body>
</html>

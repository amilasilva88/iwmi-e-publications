<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">

<jsp:include page="layout/header.jsp" />
<body>

	<div id="wrapper">
		<jsp:include page="layout/menu.jsp" />

		<!-- Page Content -->
		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h2 class="page-header">Cost estimation</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<jsp:include page="content/job-cost-estimation-body.jsp" />
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<jsp:include page="layout/footer.jsp" />
	<script type="text/javascript">
	var PAGE = {
		STATUS: JobStatus.INITIALISED,
		URLNEXT: "approval"
	}
	$(function() {
		$('#costCategorySections input[type=checkbox]').click(clickActionChangeCostItem);
		$(".costSection.costEstimateContainer input[type=text]").keyup(calculateTotalEstimate);
		//$(".costSection.costEstimateContainer.on input[type=text]").live('keyup',calculateTotalEstimate);
		loadDataForId(${id});
		/*
		$("#btnSave").click(validateAndSaveData);
		$("#btnNext").click(saveAndGotoNext);
		*/
		$("#btnSave").click(submitFormSave);
		$("#btnNext").click(submitFormNext);
		validateForm()
	});
	function submitFormNext() {
		submitForm(ACTION.NEXT)
	}
	function submitFormSave() {
		submitForm(ACTION.SAVE)
	}
	function submitForm(action) {
		$('#hiddenJobId').attr("action", action)
		var event = jQuery.Event( "click" );
		$( "#hiddenJobId" ).trigger(event);
	}
	function clickActionChangeCostItem() {
		var contentId = $(this).attr('detailContainer');
		
		$("#"+contentId).removeClass("off");
		$("#"+contentId).removeClass("on");
		if (this.checked) { 
			$("#"+contentId).slideDown( "fast", function() {
				$("#"+contentId).addClass("on");
			});
		} else {
			$("#"+contentId).slideDown( "fast", function() {
				$("#"+contentId).addClass("off");
			});
			$("#"+contentId+" input[name=textNumberOfPages]").val("0");
			$("#"+contentId+" input[name=textCostPerUnit]").val("0");
			$("#"+contentId+" input[name=textOtherCost]").val("0");
			$("#"+contentId+" input[name=textOtherCost]").attr("overhead","0");
			$("#"+contentId+" input[name=textSubTotal]").val("0");
		}
		calculateTotalEstimate();
	}
	
	function calculateTotalEstimate() {
		var sectionId = $(this).parent().parent().parent().parent().parent().parent(".costEstimateContainer.on").attr("id");
		
		var noOfPages = $("#"+sectionId+" input[name=textNumberOfPages]").val();
		noOfPages = getFormatedFloatValue(noOfPages);
		noOfPages = (!isNaN(noOfPages) && (noOfPages == parseFloat(noOfPages).toString()))?parseFloat(noOfPages):0;
		
		var unitCost = $("#"+sectionId+" input[name=textCostPerUnit]").val();
		unitCost = getFormatedFloatValue(unitCost);
		unitCost = (!isNaN(unitCost) && (unitCost == parseFloat(unitCost).toString()))?parseFloat(unitCost):0;
		var totalPagesCost = (noOfPages * unitCost);
		var otherCost = $("#"+sectionId+" input[name=textOtherCost]").val();
		otherCost = getFormatedFloatValue(otherCost);
		otherCost = (!isNaN(otherCost) && (otherCost == parseFloat(otherCost).toString()))?parseFloat(otherCost):0;
		var overheadCost = (totalPagesCost * (otherCost/100));
		$("#"+sectionId+" input[name=textOtherCost]").attr("overhead", overheadCost);

		var subCost = totalPagesCost+overheadCost;
		$("#"+sectionId+" input[name=textSubTotal]").val(subCost);
		var totalOtherCost = 0;
		var totalESTCost = 0;
		$(".costSection.costEstimateContainer input[name=textSubTotal]").each(function() {
			totalESTCost += isNaN(parseInt($(this).val()))?0:parseFloat($(this).val());
		});
		totalESTCost = (isNaN(totalESTCost))?0:totalESTCost;
		$("#textTotalAllSubCost").val(totalESTCost);
		var totalOtherCost = 0;
		$(".costSection.costEstimateContainer input[name=textOtherCost]").each(function() {
			totalOtherCost += isNaN(parseFloat($(this).attr("overhead")))?0:parseFloat($(this).attr("overhead"));
		});
		totalESTCost = (isNaN(totalOtherCost))?0:totalOtherCost;
		$("#textTotalOverheadCost").val(totalOtherCost);
	}

	function saveAndGotoNext() {
		var response = validateAndSave();
		var isSuccess = (response['status'] == '1000');
		JobLibrary.showJobActionStatus(isSuccess);
		if (isSuccess) {
			$("#hiddenJobId").val(response['id']);	
			window.location.href='../'+PAGE.URLNEXT+'/page?id=' + response['id'];
		}
	}

	function validateAndSaveData() {
		var response = validateAndSave();
		var isSuccess = (response['status'] == '1000');
		JobLibrary.showJobActionStatus(isSuccess);
		if (isSuccess) {
			$("#hiddenJobId").val(response['id']);	
			JobLibrary.setItemStatus(response['item_status'], PAGE.STATUS);
		}
	}
	
	function validateAndSave() {
		var hiddenJobId = $("#hiddenJobId").val();
		var textJobNumber = $("#textJobNumber").val();
		var selectTypeOfJob = $("#selectTypeOfJob").val();
		var textSeries = $("#textSeries").val();
		var textCreatedBy = $("#textCreatedBy").val();
		
		var costElementsData = [];
		$('#costCategorySections input[type=checkbox]').each(function() {
			if ($( this ).is(":checked")) {
				var contentId = $(this).attr('detailContainer');
				var noOfPages = parseInt($("#"+contentId+" input[name=textNumberOfPages]").val());
				var unitCost = parseFloat($("#"+contentId+" input[name=textCostPerUnit]").val());
				var otherCost = parseFloat($("#"+contentId+" input[name=textOtherCost]").val());
				if(!isNaN(noOfPages) && !isNaN(noOfPages) && !isNaN(noOfPages)) {
					costElementsData.push({ "cost_type":$(this).val(), "no_of_pages":noOfPages, "cost_per_unit":unitCost, "other_costs":otherCost});
				} else {
					return;
				}
			}
		});
		//TODO- validation check
		var userAction = $("#hiddenJobId").attr("action");			
		var postData = {
			action: userAction,
			id:hiddenJobId,
			requesterName:textCreatedBy,
			jobNumber: textJobNumber,
			type: selectTypeOfJob,
			series: textSeries,
			costElements: costElementsData
		};
		var postUrl = "../costEstimation/saveData";
		var result = postRequest(postUrl, postData);
		return result;
	};

	
	function loadDataForId(itemId) {
		var itemStatus = null;
		if (null != itemId) {
			var sectionData = null;
			sectionData = getRequest("./load/"+itemId);
			if ((sectionData != null) || (sectionData != ""))  {
				if (null != sectionData['canEdit']) {
					var isDisable = !sectionData['canEdit'];
					JobLibrary.setDisableAllInput(isDisable);
				}
				if ((null != sectionData['item_status']) && (sectionData['item_status'] !="")) {
					itemStatus = sectionData['item_status'];
				}
				if ((null != sectionData['id']) && (sectionData['id'] !="")) {
					$('#hiddenJobId').val(sectionData['id']);
				}
				if ((null != sectionData['requesterName']) && (sectionData['requesterName'] !="")) {
					$('#textCreatedBy').val(sectionData['requesterName']);
				}
				if ((null != sectionData['jobNumber']) && (sectionData['jobNumber'] !="")) {
					$('#textJobNumber').val(sectionData['jobNumber']);
				}
				if ((null != sectionData['type']) && (sectionData['type'] !="")) {
					$('#selectTypeOfJob').val(sectionData['type']).attr("selected", "selected");
				}
				if ((null != sectionData['series']) && (sectionData['series'] !="")) {
					$('#textSeries').val(sectionData['series']);
				}
				if ((null != sectionData['costElements']) && (sectionData['costElements'].length>0)) {
					var costElements = sectionData['costElements'];
					for (var i=0;i<costElements.length;i++) {
						var costElement = costElements[i];
						var costType = costElement['cost_type'];
						$('#costCategorySections input[type=checkbox][value='+costType+']').attr("CHECKED","true");
						var contentId = $('#costCategorySections input[type=checkbox][value='+costType+']').attr('detailContainer');
						$("#"+contentId+" input[name=textNumberOfPages]").val(costElement['no_of_pages']);
						$("#"+contentId+" input[name=textCostPerUnit]").val(costElement['cost_per_unit']);
						var costForPages = parseFloat(costElement['no_of_pages'])*parseFloat(costElement['cost_per_unit']);
						var overhead = costForPages * ((parseFloat(costElement['other_costs'])*1.0)/ 100);
						$("#"+contentId+" input[name=textOtherCost]").val(costElement['other_costs']);
						$("#"+contentId+" input[name=textOtherCost]").attr("overhead",overhead);
					}
				}
			}
		}
		$('#costCategorySections input[type=checkbox]').each(function () {
			var contentId = $(this).attr('detailContainer');
			$("#"+contentId).removeClass("off");
			$("#"+contentId).removeClass("on");
			if (this.checked) { 
				$("#"+contentId).addClass("on");
			} else {
				$("#"+contentId+" input[name=textNumberOfPages]").val("0");
				$("#"+contentId+" input[name=textCostPerUnit]").val("0");
				$("#"+contentId+" input[name=textOtherCost]").val("0");
				$("#"+contentId+" input[name=textOtherCost]").attr("overhead","0");
				$("#"+contentId).addClass("off");
			}
			/*
			var noOfUnits = parseFloat($("#"+contentId+" input[name=textNumberOfPages]").val());
			var costPerUnit = parseFloat($("#"+contentId+" input[name=textCostPerUnit]").val());
			var otherCost = parseFloat($("#"+contentId+" input[name=textOtherCost]").val());
			var subTotal = ((costPerUnit*noOfUnits) + otherCost)*1.0;
			*/
			var noOfUnits = $("#"+contentId+" input[name=textNumberOfPages]").val();
			var costPerUnit = $("#"+contentId+" input[name=textCostPerUnit]").val();
			var otherCost = $("#"+contentId+" input[name=textOtherCost]").val();
			var costForPages = parseFloat(costPerUnit)*parseFloat(noOfUnits);
			var overhead = costForPages * ((parseFloat(otherCost)*1.0)/ 100);
			$("#"+contentId+" input[name=textOtherCost]").attr("overhead",overhead);
			var subTotal = (costForPages) + overhead;
			$("#"+contentId+" input[name=textSubTotal]").val(subTotal);
		});
		calculateTotalEstimate();
		JobLibrary.setItemStatus(itemStatus, PAGE.STATUS);		
	}
	
	function setItemStatus(statusOfItem) {
		var navItems = $(".col-lg-2.horizontal-step");
		$(".col-lg-2.horizontal-step").removeClass("active")
		for (var i = 0; i<= statusOfItem; i++) {
			$(".col-lg-2.horizontal-step").eq(i).addClass("active")
		}
		$('#btnNext').attr('disabled','disabled');
		if (JobStatus.CostEstimation < statusOfItem) {
			$('#btnNext').removeAttr('disabled','disabled');
		}
	}
	function validateForm() {
			$('form').bootstrapValidator({
				framework: 'bootstrap',
				feedbackIcons: {
		            valid: 'glyphicon glyphicon-ok',
		            invalid: 'glyphicon glyphicon-remove',
		            validating: 'glyphicon glyphicon-refresh'
        		},
				submitHandler: function(validator, form, submitButton) {
		        	var buttonAction= $(submitButton).attr("action")
		        	var response = validateAndSave();
					var isSuccess = ((typeof response != 'undefined') && (typeof response['status'] != 'undefined') && (response['status'] == '1000'));
					JobLibrary.showJobActionStatus(isSuccess);
					if (isSuccess) {
						if (buttonAction == ACTION.NEXT) {
							$("#hiddenJobId").val(response['id']);	
							//window.location.href='../'+PAGE.URLNEXT+'/page?id=' + response['id'];
							$("#modalSubmitInfo").modal("toggle");
						} else {
							$("#hiddenJobId").val(response['id']);	
				 			JobLibrary.setItemStatus(response['item_status'], PAGE.STATUS);
						}
					}
		        },
		        fields : {
					selectTypeOfJob: {
						validators : {
							notEmpty : {
								message : 'This field is required'
							}
						}
					}
				}
			});
		};
	</script>
</body>
</html>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">

<jsp:include page="layout/header.jsp" />
<body>

	<div id="wrapper">
		<jsp:include page="layout/menu.jsp" />

		<!-- Page Content -->
		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h2 class="page-header">Delivery</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<jsp:include page="content/job-delivering-body.jsp" />
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->
	<jsp:include page="layout/footer.jsp" />
	<script type="text/javascript">
	var PAGE = {
		STATUS: JobStatus.APPROVED,
		URLNEXT: "actual_costing"
	}
	$(function() {
		loadDataForId(${id});
		
		$("#btnSave").click(validateAndSaveData);
		$("#btnNext").click(saveAndGotoNext);
		/*
		$("#btnSave").click(submitFormSave);
		$("#btnNext").click(submitFormNext);
		validateForm()
		*/
	});
	function submitFormNext() {
		submitForm(ACTION.NEXT)
	}
	function submitFormSave() {
		submitForm(ACTION.SAVE)
	}
	function submitForm(action) {
		$('#hiddenJobId').attr("action", action)
		var event = jQuery.Event( "click" );
		$( "#hiddenJobId" ).trigger(event);
	}
	function loadDataForId(itemId) {
		var itemStatus = null;
		if (null != itemId) {
			var sectionData = null;
			sectionData = getRequest("./load/"+itemId);
			if ((sectionData != null) || (sectionData != ""))  {
				if (null != sectionData['canEdit']) {
					var isDisable = !sectionData['canEdit'];
					JobLibrary.setDisableAllInput(isDisable);
				}
				if ((null != sectionData['item_status']) && (sectionData['item_status'] !="")) {
					itemStatus = sectionData['item_status'];
				}
				if ((null != sectionData['id']) && (sectionData['id'] !="")) {
					$('#hiddenJobId').val(sectionData['id']);
				}
				if ((null != sectionData['deliveredComment']) && (sectionData['deliveredComment'] !="")) {
					$('#textAreaDeliveryComment').val(sectionData['deliveredComment']);
				}
				if ((null != sectionData['deliveredDate']) && (sectionData['deliveredDate'] !="")) {
					$('#textDeliveryDate').val(sectionData['deliveredDate']);
				}
			}
		}
		JobLibrary.setItemStatus(itemStatus, PAGE.STATUS);
	}
	function saveAndGotoNext() {
		$("#hiddenJobId").attr("action", ACTION.NEXT);
		var response = validateAndSave();
		var isSuccess = (response['status'] == '1000');
		JobLibrary.showJobActionStatus(isSuccess);
		if (isSuccess) {
			$("#hiddenJobId").val(response['id']);	
			//window.location.href='../'+PAGE.URLNEXT+'/page?id=' + response['id'];
			$("#modalSubmitInfo").modal("toggle");
		}
	}
	function validateAndSaveData() {
		$("#hiddenJobId").attr("action", ACTION.SAVE);
		var response = validateAndSave();
		var isSuccess = (response['status'] == '1000');
		JobLibrary.showJobActionStatus(isSuccess);
		if (isSuccess) {
			 $("#hiddenJobId").val(response['id']);	
			 JobLibrary.setItemStatus(response['item_status'], PAGE.STATUS);
		}
	}	
	function validateAndSave() {
		var hiddenJobId = $("#hiddenJobId").val();
		var textAreaDeliveryComment = $("#textAreaDeliveryComment").val();
		var textDeliveryDate = $("#textDeliveryDate").val();
		
		//TODO- validation check
		var userAction = $("#hiddenJobId").attr("action");
			
		var postData = {
			action: userAction,
			id:hiddenJobId,
			deliveredComment: textAreaDeliveryComment,
			deliveredDate : textDeliveryDate
		};
		var postUrl = "../delivery/saveData";
		var result = postRequest(postUrl, postData);
		return result;
	};
	function validateForm() {
		var buttonAction= $(submitButton).attr("action")
    	var response = validateAndSave();
		var isSuccess = ((typeof response != 'undefined') && (typeof response['status'] != 'undefined') && (response['status'] == '1000'));
		JobLibrary.showJobActionStatus(isSuccess);
		if (isSuccess) {
			if (buttonAction == ACTION.NEXT) {
				$("#hiddenJobId").val(response['id']);	
				window.location.href='../'+PAGE.URLNEXT+'/page?id=' + response['id']
			} else {
				$("#hiddenJobId").val(response['id']);	
	 			JobLibrary.setItemStatus(response['item_status'], PAGE.STATUS);
			}
		}
	}
	</script>
</body>
</html>

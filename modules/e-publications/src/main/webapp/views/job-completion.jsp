<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">

<jsp:include page="layout/header.jsp" />
<body>

	<div id="wrapper">
		<jsp:include page="layout/menu.jsp" />

		<!-- Page Content -->
		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h2 class="page-header">Completion</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<jsp:include page="content/job-completion-body.jsp" />
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->
	<jsp:include page="layout/footer.jsp" />
	<script type="text/javascript">
	$(function() {
		loadDataForId(${id});
		
		$("#btnReset").click(resetData);
		$('#btnComplete').click(confirmStatusOfItem);
	});
	function downloadeResource() {
		var jobId = $("#hiddenJobId").val();	
		window.location.href='../status/export/'+jobId
	}
	
	function loadDataForId(itemId) {
		var usersUrl = "../jobComplete/load/"+itemId;
		var sectionData = getRequest(usersUrl);
		var isCanComplete = (null != sectionData['can_complete']) ? sectionData['can_complete'] : false
		var isComplete = (null != sectionData['complete_done']) ? sectionData['complete_done'] : false
		var itemStatus = null;
		if ((null != sectionData['item_status']) && (sectionData['item_status'] !="")) {
			itemStatus = sectionData['item_status'];
		}
		if (null != sectionData['canEdit']) {
			var isDisable = !sectionData['canEdit'];
			JobLibrary.setDisableAllInput(isDisable);
		}
		JobLibrary.setItemStatus(itemStatus, JobStatus.COMPLETED);
		$('#contentForInvalid').addClass("hidden");
		$('#listUsers').addClass("hidden");
		if(isComplete) {
			$('#contentForInvalid > div > div > div > div > span').html("This Publication Job has been already completed");
			$('#contentForInvalid').removeClass("hidden");
		} else {
			$('#contentForInvalid > div > div > div >  div > span').html("Please fill other details before complete the Publication Job, Thanks");
			if (isCanComplete) {
				$('#contentForInvalid').addClass("hidden");
				$('#listUsers').removeClass("hidden");
				var users = sectionData.users;
				var tableData = prepareUsersData(users);
				renderDataTable(tableData);
			} else {
				$('#listUsers').addClass("hidden");
				$('#contentForInvalid').removeClass("hidden");
			}
		}
	}
	function prepareUsersData(users) {	
		var rows = {};
		var dataofTable = [];
		for (var i = 0; i < users[0].length; i++) {
			var nameValue = (users[0][i]['display']) ? users[0][i]['display'] : "";
			var emailValue = (users[0][i]['email']) ? users[0][i]['email'] : "";
			var responseRow = {
					name : nameValue,	
					email : emailValue,	
					select : '<input type="checkbox" name="checkboxNotifyUser" display="'+nameValue+'" email="'+emailValue+'" />'
			};
			dataofTable.push(responseRow);
		}
		rows["rows"] = dataofTable;
		return rows;
	}
	function renderDataTable(dataSet) {
		$('#notify-users-data-table').DataTable({
			"bAutoWidth" : false,
			"bJQueryUI" : true,
			"bDestroy" : true,
			"searching": true,
			"aaData" : dataSet.rows,
			"aoColumns" : [ {
				"sTitle" : "Name",
				"mData" : "name",
			}, {
				"sTitle" : "Email",
				"mData" : "email"
			}, {
				"sTitle" : "Select",
				"mData" : "select",
			} ]
		});
	}
	function confirmStatusOfItem() {
		JobLibrary.clearJobActionStatus()
		var selectedUsers = $('input[name="checkboxNotifyUser"]:checked');
		var notifyUsers = [];
		if(selectedUsers.length >= 0) {
			bootbox.dialog({
				  message: "Are you sure you want to confirm the completion of this job..?",
				  title: "Confirmation",
				  buttons: {
				    success: {
				      label: "Confirm",
				      className: "btn-success",
				      callback: proceedCompleteJob
				    },
				    danger: {
				      label: "No",
				      className: "btn-danger",
				      callback: function() {
				    	  return;
				      }
				    }
				  }
				});
			
		} else {
			bootbox.dialog({
				  message: "Please select at least one user before complete the job.",
				  title: "Information",
				  buttons: {
				    success: {
				      label: "Ok",
				      className: "btn-success",
				      callback: function() {
				        	return;
				      }
					}
				  }
			});
		}
	}
	function proceedCompleteJob() {
		$('#fileUploadingDiv').removeClass("off").addClass("on");
		var hiddenJobId = $('#hiddenJobId').val();
		var selectedUsers = $('input[name="checkboxNotifyUser"]:checked');
		var notifyUsers = [];
		$( "input[name='checkboxNotifyUser']:checked" ).each(function( index ) {
			var email = $(this).attr("email");
			notifyUsers.push(email)
		});
		if(notifyUsers.length > 0) {
			var formatedPostData = {
					id: hiddenJobId,
					notify_users: notifyUsers
			}
			
			var postUrl = "../jobComplete/confirm";
			var response = postRequest(postUrl, formatedPostData);
			var isSuccess = (response['status'] == '1000');
			if(isSuccess) {
				$('#fileUploadingDiv').removeClass("on").addClass("off");
				resetData()
			} else {
				$('#fileUploadingDiv').removeClass("on").addClass("off");
			}
			var message = (isSuccess) ? "Your job has been confirmed successfully." : "Sorry, unable to confirm job status."
			JobLibrary.showJobActionStatusWithMesage(isSuccess, message);
		} else {
			$('#fileUploadingDiv').removeClass("on").addClass("off");
		}
	}
	function resetData() {
		$( "input[name='checkboxNotifyUser']").attr("CHECKED",false);
	}
	</script>
</body>
</html>
